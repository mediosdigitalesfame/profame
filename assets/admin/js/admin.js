

jQuery(function($) {

	"use strict";


  var loading_html = '<div class="spinner"><div class="rect1" style="margin-right: 1px;"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>';
  var base_url = $('#base_url').val();

  $('[data-toggle="tooltip"]').tooltip(); 
	
  $('.datatable').dataTable();


    $(document).on('change', '.btn-file :file', function() {
      var input = $(this),
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [label]);
    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }


    $("#imgInp").on('change', function() {
        readURL(this);
    });   


    $(".custom-btngp").on('click', function() {
        var priceVal = $(this).find('.switch_price').val();

        if (priceVal == 'monthly') {
            $('.monthly_price').show();
            $('.yearly_price').hide();
            $('.billing_type').val('monthly');
        } else {
            $('.yearly_price').show();
            $('.monthly_price').hide();            
            $('.billing_type').val('yearly');
        }
    });


    $(document).on('click', ".dash_package_btn", function() {
      var billType = $('.billing_type').val();
      var url = $(this).attr('href')+'/'+billType;
      window.location.href=url;
      return false;
    });


    $(".checkcolor").on('click', function() {
      var $box = $(this);
      if ($box.is(":checked")) {
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        $(group).prop("checked", false);
        $box.prop("checked", true);
      } else {
        $box.prop("checked", false);
      }
    });


    $(".check_all").on('click', function() {

        $('input:checkbox').not(this).prop('checked', this.checked);
        
        if ($(".check_all").is(":checked")) {
            $(".multiple_delete_btn").show()
        } else {
            $(".multiple_delete_btn").hide()
        }
    });



    $(".checkItem").on('click', function() {
        if ($(".checkItem").is(":checked")) {
            $(".multiple_delete_btn").show()
        } else {
            $(".multiple_delete_btn").hide()
        }
    });

    $(document).on('click', ".set_layout", function() {
        var designId = $(this).val();
        var url = base_url+'admin/profile/change_layout/'+designId;
        $.post(url, { data: 'value', 'csrf_test_name': csrf_token }, function(json) {
           if(json.st == 1){
                
              swal({
                  title: "Success",
                  text: 'El diseño cambió correctamente',
                  type: "success",
                  showConfirmButton: true
              }, function(){
                  window.location.reload();
              });

            }
        }, 'json' );
        return false;
    });
    

    $(function(){

        $(document).on('submit', "#cahage_pass_form", function() {

            $.post($('#cahage_pass_form').attr('action'), $('#cahage_pass_form').serialize(), function(json){

                if (json.st == 1) {
                    $('#cahage_pass_form')[0].reset();
                    swal({
                          title: "Felicidades!",
                          text: "Tu contraseña ha sido cambiada exitosamente",
                          type: "success",
                          showConfirmButton: true
                    });
                }else if (json.st == 2) {
                    $('#cahage_pass_form')[0].reset();
                    swal({
                      title: "Opps !",
                      text: "Tu contraseña de confirmación no coincide",
                      type: "error",
                      showConfirmButton: true
                    });
                }else {
                    $('#cahage_pass_form')[0].reset();
                    swal({
                      title: "Error!",
                      text: "Tu contraseña anterior no coincide",
                      type: "error",
                      showConfirmButton: true
                    });
                }
            },'json');
            return false;
        });

    });




    $(document).on('click', "#make_embaded", function() {

        var url = $("#video_url").val();
        var post_data = {
            'url': url,
            'csrf_test_name' : csrf_token
        };

        $.ajax({
            type: "POST",
            url: base_url + "admin/video/generate_embed_code",
            data: post_data,
            success: function (response) {
                $("#video_embed_code").html(response);
                if (response != "Invalid Url") {
                    $("#video_preview").attr('src', response);
                    $("#video_play_icon").hide();
                }
            }
        });


        $.ajax({
            type: "POST",
            url: base_url + "admin/video/get_video_thumbnails",
            data: post_data,
            success: function (response) {
                $("#video_thumbnails_url").val(response);
                $("#video_thumbnails_img").attr('src', response);
            }
        });

    });



    $(document).on('click', ".approve_img", function() {
        var imgId = $(this).attr('data-id');

        var url = base_url+'admin/photos/approve_img/0/'+imgId;
        $.post(url, { data: 'value', 'csrf_test_name': csrf_token }, function(json) {
           if(json.st == 1){
                swal({
                  title: "Success",
                  text: "La imagen ha sido aprobada.",
                  type: "success",
                  showCancelButton: false
                }),
                $('#img_'+imgId).slideUp();
            }
        }, 'json' );
        return false;
    });


    $(document).on('click', ".add_featured", function() {
        var imgId = $(this).attr('data-id');

        var url = base_url+'admin/photos/approve_img/1/'+imgId;

        $.post(url, { data: 'value', 'csrf_test_name': csrf_token }, function(json) {
           if(json.st == 1){
                swal({
                  title: "Success",
                  text: "La imagen ha sido aprobada y seleccionada como destacada.",
                  type: "success",
                  showCancelButton: false
                }),
                $('#img_'+imgId).slideUp();
            }
        }, 'json' );
        return false;
    });


    $(document).on('click', ".reject_img", function() {
        var imgId = $(this).attr('data-id');

        var url = base_url+'admin/photos/reject_img/'+imgId;

        $.post(url, { data: 'value', 'csrf_test_name': csrf_token }, function(json) {
           if(json.st == 1){
                swal({
                  title: "Success",
                  text: "La imagen ha sido rechazada.",
                  type: "success",
                  showCancelButton: false
                }),
                $('#img_'+imgId).slideUp();
            }
        }, 'json' );
        return false;
    });


    $(document).on('change', ".sort", function() {
        $('.sort_form').submit();
    });

    
    $(document).on('click', ".add_btn", function() {
        $('.add_area').show();
        $('.list_area').hide();
        return false;
    });

    $(document).on('click', ".cancel_btn", function() {
        $('.add_area').hide();
        $('.list_area').show();
        return false;
    });


    $(document).on('click', ".scheduled_post", function() {
        $('.date_area').slideToggle();
        $('this').checked();
        return false;
    });


    $(document).on('change', "#category", function() {
        var catId = $(this).val();
        if(catId != ''){
            var url = base_url+'admin/post/load_subcategory/'+catId;
                $.post(url,{ data: 'value', 'csrf_test_name': csrf_token },function(data){
                    $('#sub_category').html(data);
                    $('#sub_category').prop('disabled', false);
                }
            );
        }  
    });

  


    $(document).on('click', ".delete_item", function() {

        var del_url = $(this).attr('href');
        var imgId = $(this).attr('data-id');


            swal({
              title: "Estas seguro?",
              text: "No podrás recuperar este archivo",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Si, bórralo!",
              closeOnConfirm: false
            },
            function(){ 

                $.post(del_url, { data: 'value', 'csrf_test_name': csrf_token }, function(json) {
                    if(json.st == 1){     
                        swal({
                          title: "Success",
                          text: "Borrado exitosamente",
                          type: "success",
                          showCancelButton: false
                        }),                
                        $("#row_"+imgId).slideUp();
                    }
                },'json');

            });

        return false;

    });
    
    $(document).on('click', ".change_pass", function() {
        $('.change_password_area').slideDown();
        $('.edit_account_area').hide();
        $("html, body").animate({ scrollTop: 200 }, "slow");
        return false;
    });

    $(document).on('click', ".cancel_pass", function() {
        $('.change_password_area').hide();
        $('.edit_account_area').slideDown();
        return false;
    });

});



(function ($) {

    var createdElements = [];

    var defaults = {
        options: {
            prependExistingHelpBlock: false,
            sniffHtml: true, // sniff for 'required', 'maxlength', etc
            preventSubmit: true, // stop the form submit event from firing if validation fails
            submitError: false, // function called if there is an error when trying to submit
            submitSuccess: false, // function called just before a successful submit event is sent to the server
            semanticallyStrict: false, // set to true to tidy up generated HTML output
            removeSuccess : true,
            bindEvents: [],
            autoAdd: {
                helpBlocks: true
            },
            filter: function () {
                // return $(this).is(":visible"); // only validate elements you can see
                return true; // validate everything
            }
        },
        methods: {
            init: function (options) {

                // Get a clean copy of the defaults for extending
                var settings = $.extend(true, {}, defaults);
                // Set up the options based on the input
                settings.options = $.extend(true, settings.options, options);

                var $siblingElements = this;

                var uniqueForms = $.unique(
                    $siblingElements.map(function () {
                        return $(this).parents("form")[0];
                    }).toArray()
                );

                $(uniqueForms).bind("submit.validationSubmit", function (e) {
                    var $form = $(this);
                    var warningsFound = 0;
                    // Get all inputs
                    var $allInputs = $form.find("input,textarea,select").not("[type=submit],[type=image]").filter(settings.options.filter);
                    var $allControlGroups = $form.find(".control-group");

                    // Only trigger validation on the ones that actually _have_ validation
                    var $inputsWithValidators = $allInputs.filter(function () {
                        return $(this).triggerHandler("getValidatorCount.validation") > 0;
                    });
                    $inputsWithValidators.trigger("submit.validation");

                    // But all of them are out-of-focus now, because we're submitting.
                    $allInputs.trigger("validationLostFocus.validation");

                    // Okay, now check each controlgroup for errors (or warnings)
                    $allControlGroups.each(function (i, el) {
                        var $controlGroup = $(el);
                        if ($controlGroup.hasClass("warning") || $controlGroup.hasClass("error")) {
                            $controlGroup.removeClass("warning").addClass("error");
                            warningsFound++;
                        }
                    });

                    if (warningsFound) {
                        // If we found any warnings, maybe we should prevent the submit
                        // event, and trigger 'submitError' (if they're set up)
                        if (settings.options.preventSubmit) {
                            e.preventDefault();
                            e.stopImmediatePropagation();
                        }
                        $form.addClass("error");
                        if ($.isFunction(settings.options.submitError)) {
                            settings.options.submitError($form, e, $inputsWithValidators.jqBootstrapValidation("collectErrors", true));
                        }
                    } else {
                        // Woo! No errors! We can pass the submit event to submitSuccess
                        // (if it has been set up)
                        $form.removeClass("error");
                        if ($.isFunction(settings.options.submitSuccess)) {
                            settings.options.submitSuccess($form, e);
                        }
                    }
                });

                return this.each(function () {

                    // Get references to everything we're interested in
                    var $this = $(this),
                        $controlGroup = $this.parents(".control-group").first(),
                        $helpBlock = $controlGroup.find(".help-block").first(),
                        $form = $this.parents("form").first(),
                        validatorNames = [];

                    // create message container if not exists
                    if (!$helpBlock.length && settings.options.autoAdd && settings.options.autoAdd.helpBlocks) {
                        $helpBlock = $('<div class="help-block" />');
                        $controlGroup.find('.controls').append($helpBlock);
                        createdElements.push($helpBlock[0]);
                    }

                    // =============================================================
                    //                                     SNIFF HTML FOR VALIDATORS
                    // =============================================================

                    // *snort sniff snuffle*

                    if (settings.options.sniffHtml) {
                        var message;
                        // ---------------------------------------------------------
                        //                                                   PATTERN
                        // ---------------------------------------------------------
                        if ($this.data("validationPatternPattern")) {
                            $this.attr("pattern", $this.data("validationPatternPattern"));
                        }
                        if ($this.attr("pattern") !== undefined) {
                            message = "Not in the expected format<!-- data-validation-pattern-message to override -->";
                            if ($this.data("validationPatternMessage")) {
                                message = $this.data("validationPatternMessage");
                            }
                            $this.data("validationPatternMessage", message);
                            $this.data("validationPatternRegex", $this.attr("pattern"));
                        }
                        // ---------------------------------------------------------
                        //                                                       MAX
                        // ---------------------------------------------------------
                        if ($this.attr("max") !== undefined || $this.attr("aria-valuemax") !== undefined) {
                            var max = ($this.attr("max") !== undefined ? $this.attr("max") : $this.attr("aria-valuemax"));
                            message = "Too high: Maximum of '" + max + "'<!-- data-validation-max-message to override -->";
                            if ($this.data("validationMaxMessage")) {
                                message = $this.data("validationMaxMessage");
                            }
                            $this.data("validationMaxMessage", message);
                            $this.data("validationMaxMax", max);
                        }
                        // ---------------------------------------------------------
                        //                                                       MIN
                        // ---------------------------------------------------------
                        if ($this.attr("min") !== undefined || $this.attr("aria-valuemin") !== undefined) {
                            var min = ($this.attr("min") !== undefined ? $this.attr("min") : $this.attr("aria-valuemin"));
                            message = "Too low: Minimum of '" + min + "'<!-- data-validation-min-message to override -->";
                            if ($this.data("validationMinMessage")) {
                                message = $this.data("validationMinMessage");
                            }
                            $this.data("validationMinMessage", message);
                            $this.data("validationMinMin", min);
                        }
                        // ---------------------------------------------------------
                        //                                                 MAXLENGTH
                        // ---------------------------------------------------------
                        if ($this.attr("maxlength") !== undefined) {
                            message = "Too long: Maximum of '" + $this.attr("maxlength") + "' characters<!-- data-validation-maxlength-message to override -->";
                            if ($this.data("validationMaxlengthMessage")) {
                                message = $this.data("validationMaxlengthMessage");
                            }
                            $this.data("validationMaxlengthMessage", message);
                            $this.data("validationMaxlengthMaxlength", $this.attr("maxlength"));
                        }
                        // ---------------------------------------------------------
                        //                                                 MINLENGTH
                        // ---------------------------------------------------------
                        if ($this.attr("minlength") !== undefined) {
                            message = "Too short: Minimum of '" + $this.attr("minlength") + "' characters<!-- data-validation-minlength-message to override -->";
                            if ($this.data("validationMinlengthMessage")) {
                                message = $this.data("validationMinlengthMessage");
                            }
                            $this.data("validationMinlengthMessage", message);
                            $this.data("validationMinlengthMinlength", $this.attr("minlength"));
                        }
                        // ---------------------------------------------------------
                        //                                                  REQUIRED
                        // ---------------------------------------------------------
                        if ($this.attr("required") !== undefined || $this.attr("aria-required") !== undefined) {
                            message = settings.builtInValidators.required.message;
                            if ($this.data("validationRequiredMessage")) {
                                message = $this.data("validationRequiredMessage");
                            }
                            $this.data("validationRequiredMessage", message);
                        }
                        // ---------------------------------------------------------
                        //                                                    NUMBER
                        // ---------------------------------------------------------
                        if ($this.attr("type") !== undefined && $this.attr("type").toLowerCase() === "number") {
                            message = settings.validatorTypes.number.message; // TODO: fix this
                            if ($this.data("validationNumberMessage")) {
                                message = $this.data("validationNumberMessage");
                            }
                            $this.data("validationNumberMessage", message);

                            var step = settings.validatorTypes.number.step; // TODO: and this
                            if ($this.data("validationNumberStep")) {
                                step = $this.data("validationNumberStep");
                            }
                            $this.data("validationNumberStep", step);

                            var decimal = settings.validatorTypes.number.decimal;
                            if ($this.data("validationNumberDecimal")) {
                                decimal = $this.data("validationNumberDecimal");
                            }
                            $this.data("validationNumberDecimal", decimal);
                        }
                        // ---------------------------------------------------------
                        //                                                     EMAIL
                        // ---------------------------------------------------------
                        if ($this.attr("type") !== undefined && $this.attr("type").toLowerCase() === "email") {
                            message = "Not a valid email address<!-- data-validation-email-message to override -->";
                            if ($this.data("validationEmailMessage")) {
                                message = $this.data("validationEmailMessage");
                            }
                            $this.data("validationEmailMessage", message);
                        }
                        // ---------------------------------------------------------
                        //                                                MINCHECKED
                        // ---------------------------------------------------------
                        if ($this.attr("minchecked") !== undefined) {
                            message = "Not enough options checked; Minimum of '" + $this.attr("minchecked") + "' required<!-- data-validation-minchecked-message to override -->";
                            if ($this.data("validationMincheckedMessage")) {
                                message = $this.data("validationMincheckedMessage");
                            }
                            $this.data("validationMincheckedMessage", message);
                            $this.data("validationMincheckedMinchecked", $this.attr("minchecked"));
                        }
                        // ---------------------------------------------------------
                        //                                                MAXCHECKED
                        // ---------------------------------------------------------
                        if ($this.attr("maxchecked") !== undefined) {
                            message = "Too many options checked; Maximum of '" + $this.attr("maxchecked") + "' required<!-- data-validation-maxchecked-message to override -->";
                            if ($this.data("validationMaxcheckedMessage")) {
                                message = $this.data("validationMaxcheckedMessage");
                            }
                            $this.data("validationMaxcheckedMessage", message);
                            $this.data("validationMaxcheckedMaxchecked", $this.attr("maxchecked"));
                        }
                    }

                    // =============================================================
                    //                                       COLLECT VALIDATOR NAMES
                    // =============================================================

                    // Get named validators
                    if ($this.data("validation") !== undefined) {
                        validatorNames = $this.data("validation").split(",");
                    }

                    // Get extra ones defined on the element's data attributes
                    $.each($this.data(), function (i, el) {
                        var parts = i.replace(/([A-Z])/g, ",$1").split(",");
                        if (parts[0] === "validation" && parts[1]) {
                            validatorNames.push(parts[1]);
                        }
                    });

                    // =============================================================
                    //                                     NORMALISE VALIDATOR NAMES
                    // =============================================================

                    var validatorNamesToInspect = validatorNames;
                    var newValidatorNamesToInspect = [];

                    var uppercaseEachValidatorName = function (i, el) {
                        validatorNames[i] = formatValidatorName(el);
                    };

                    var inspectValidators = function (i, el) {
                        if ($this.data("validation" + el + "Shortcut") !== undefined) {
                            // Are these custom validators?
                            // Pull them out!
                            $.each($this.data("validation" + el + "Shortcut").split(","), function (i2, el2) {
                                newValidatorNamesToInspect.push(el2);
                            });
                        } else if (settings.builtInValidators[el.toLowerCase()]) {
                            // Is this a recognised built-in?
                            // Pull it out!
                            var validator = settings.builtInValidators[el.toLowerCase()];
                            if (validator.type.toLowerCase() === "shortcut") {
                                $.each(validator.shortcut.split(","), function (i, el) {
                                    el = formatValidatorName(el);
                                    newValidatorNamesToInspect.push(el);
                                    validatorNames.push(el);
                                });
                            }
                        }
                    };

                    do // repeatedly expand 'shortcut' validators into their real validators
                    {
                        // Uppercase only the first letter of each name
                        $.each(validatorNames, uppercaseEachValidatorName);

                        // Remove duplicate validator names
                        validatorNames = $.unique(validatorNames);

                        // Pull out the new validator names from each shortcut
                        newValidatorNamesToInspect = [];
                        $.each(validatorNamesToInspect, inspectValidators);

                        validatorNamesToInspect = newValidatorNamesToInspect;

                    } while (validatorNamesToInspect.length > 0);

                    // =============================================================
                    //                                       SET UP VALIDATOR ARRAYS
                    // =============================================================

                    /* We're gonna generate something like
                     *
                     * {
                     *   "regex": [
                     *     { -- a validator object here --},
                     *     { -- a validator object here --}
                     *   ],
                     *   "required": [
                     *     { -- a validator object here --},
                     *     { -- a validator object here --}
                     *   ]
                     * }
                     *
                     * with a few more entries.
                     *
                     * Because we only add a few validators to each field, most of the
                     * keys will be empty arrays with no validator objects in them, and
                     * thats fine.
                     */

                    var validators = {};

                    $.each(validatorNames, function (i, el) {
                        // Set up the 'override' message
                        var message = $this.data("validation" + el + "Message");
                        var hasOverrideMessage = !!message;
                        var foundValidator = false;
                        if (!message) {
                            message = "'" + el + "' validation failed <!-- Add attribute 'data-validation-" + el.toLowerCase() + "-message' to input to change this message -->";
                        }

                        $.each(
                            settings.validatorTypes,
                            function (validatorType, validatorTemplate) {
                                if (validators[validatorType] === undefined) {
                                    validators[validatorType] = [];
                                }
                                if (!foundValidator && $this.data("validation" + el + formatValidatorName(validatorTemplate.name)) !== undefined) {
                                    var initted = validatorTemplate.init($this, el);
                                    if (hasOverrideMessage) {
                                        initted.message = message;
                                    }

                                    validators[validatorType].push(
                                        $.extend(
                                            true,
                                            {
                                                name: formatValidatorName(validatorTemplate.name),
                                                message: message
                                            },
                                            initted
                                        )
                                    );
                                    foundValidator = true;
                                }
                            }
                        );

                        if (!foundValidator && settings.builtInValidators[el.toLowerCase()]) {

                            var validator = $.extend(true, {}, settings.builtInValidators[el.toLowerCase()]);
                            if (hasOverrideMessage) {
                                validator.message = message;
                            }
                            var validatorType = validator.type.toLowerCase();

                            if (validatorType === "shortcut") {
                                foundValidator = true;
                            } else {
                                $.each(
                                    settings.validatorTypes,
                                    function (validatorTemplateType, validatorTemplate) {
                                        if (validators[validatorTemplateType] === undefined) {
                                            validators[validatorTemplateType] = [];
                                        }
                                        if (!foundValidator && validatorType === validatorTemplateType.toLowerCase()) {
                                            $this.data(
                                                "validation" + el + formatValidatorName(validatorTemplate.name),
                                                validator[validatorTemplate.name.toLowerCase()]
                                            );
                                            validators[validatorType].push(
                                                $.extend(
                                                    validator,
                                                    validatorTemplate.init($this, el)
                                                )
                                            );
                                            foundValidator = true;
                                        }
                                    }
                                );
                            }
                        }

                        if (!foundValidator) {
                            $.error("Cannot find validation info for '" + el + "'");
                        }
                    });

                    // =============================================================
                    //                                         STORE FALLBACK VALUES
                    // =============================================================

                    $helpBlock.data(
                        "original-contents",
                        (
                            $helpBlock.data("original-contents") ?
                                $helpBlock.data("original-contents") :
                                $helpBlock.html()
                            )
                    );

                    $helpBlock.data(
                        "original-role",
                        (
                            $helpBlock.data("original-role") ?
                                $helpBlock.data("original-role") :
                                $helpBlock.attr("role")
                            )
                    );

                    $controlGroup.data(
                        "original-classes",
                        (
                            $controlGroup.data("original-clases") ?
                                $controlGroup.data("original-classes") :
                                $controlGroup.attr("class")
                            )
                    );

                    $this.data(
                        "original-aria-invalid",
                        (
                            $this.data("original-aria-invalid") ?
                                $this.data("original-aria-invalid") :
                                $this.attr("aria-invalid")
                            )
                    );

                    // =============================================================
                    //                                                    VALIDATION
                    // =============================================================

                    $this.bind(
                        "validation.validation",
                        function (event, params) {

                            var value = getValue($this);

                            // Get a list of the errors to apply
                            var errorsFound = [];

                            $.each(validators, function (validatorType, validatorTypeArray) {
                                if (
                                    value || // has a truthy value
                                        value.length || // not an empty string
                                        ( // am including empty values
                                            (
                                                params &&
                                                    params.includeEmpty
                                                ) || !!settings.validatorTypes[validatorType].includeEmpty
                                            ) ||
                                        ( // validator is blocking submit
                                            !!settings.validatorTypes[validatorType].blockSubmit &&
                                                params && !!params.submitting
                                            )
                                    ) {
                                    $.each(
                                        validatorTypeArray,
                                        function (i, validator) {
                                            if (settings.validatorTypes[validatorType].validate($this, value, validator)) {
                                                errorsFound.push(validator.message);
                                            }
                                        }
                                    );
                                }
                            });

                            return errorsFound;
                        }
                    );

                    $this.bind(
                        "getValidators.validation",
                        function () {
                            return validators;
                        }
                    );

                    var numValidators = 0;

                    $.each(validators, function (i, el) {
                        numValidators += el.length;
                    });

                    $this.bind("getValidatorCount.validation", function () {
                        return numValidators;
                    });

                    // =============================================================
                    //                                             WATCH FOR CHANGES
                    // =============================================================
                    $this.bind(
                        "submit.validation",
                        function () {
                            return $this.triggerHandler("change.validation", {submitting: true});
                        }
                    );
                    $this.bind(
                        (
                            settings.options.bindEvents.length > 0 ?
                                settings.options.bindEvents :
                                [
                                    "keyup",
                                    "focus",
                                    "blur",
                                    "click",
                                    "keydown",
                                    "keypress",
                                    "change"
                                ]
                            ).concat(["revalidate"]).join(".validation ") + ".validation",
                        function (e, params) {

                            var value = getValue($this);

                            var errorsFound = [];

                            if (params && !!params.submitting) {
                                $controlGroup.data("jqbvIsSubmitting", true);
                            } else if (e.type !== "revalidate") {
                                $controlGroup.data("jqbvIsSubmitting", false);
                            }

                            var formIsSubmitting = !!$controlGroup.data("jqbvIsSubmitting");

                            $controlGroup.find("input,textarea,select").not('[type=submit]').each(function (i, el) {
                                var oldCount = errorsFound.length;
                                $.each($(el).triggerHandler("validation.validation", params) || [], function (j, message) {
                                    errorsFound.push(message);
                                });
                                if (errorsFound.length > oldCount) {
                                    $(el).attr("aria-invalid", "true");
                                } else {
                                    var original = $this.data("original-aria-invalid");
                                    $(el).attr("aria-invalid", (original !== undefined ? original : false));
                                }
                            });

                            $form.find("input,select,textarea").not($this).not("[name=\"" + $this.attr("name") + "\"]").trigger("validationLostFocus.validation");

                            errorsFound = $.unique(errorsFound.sort());

                            // Were there any errors?
                            if (errorsFound.length) {
                                // Better flag it up as a warning.
                                $controlGroup.removeClass("success error warning").addClass(formIsSubmitting ? "error" : "warning");

                                // How many errors did we find?
                                if (settings.options.semanticallyStrict && errorsFound.length === 1) {
                                    // Only one? Being strict? Just output it.
                                    $helpBlock.html(errorsFound[0] +
                                        ( settings.options.prependExistingHelpBlock ? $helpBlock.data("original-contents") : "" ));
                                } else {
                                    // Multiple? Being sloppy? Glue them together into an UL.
                                    $helpBlock.html("<ul role=\"alert\"><li>" + errorsFound.join("</li><li>") + "</li></ul>" +
                                        ( settings.options.prependExistingHelpBlock ? $helpBlock.data("original-contents") : "" ));
                                }
                            } else {
                                $controlGroup.removeClass("warning error success");
                                if (value.length > 0) {
                                    $controlGroup.addClass("success");
                                }
                                $helpBlock.html($helpBlock.data("original-contents"));
                            }

                            if (e.type === "blur") {
                                if( settings.options.removeSuccess ){
                                    $controlGroup.removeClass("success");
                                }
                            }
                        }
                    );
                    $this.bind("validationLostFocus.validation", function () {
                        if( settings.options.removeSuccess ){
                            $controlGroup.removeClass("success");
                        }
                    });
                });
            },
            destroy: function () {

                return this.each(
                    function () {

                        var
                            $this = $(this),
                            $controlGroup = $this.parents(".control-group").first(),
                            $helpBlock = $controlGroup.find(".help-block").first(),
                            $form = $this.parents("form").first();

                        // remove our events
                        $this.unbind('.validation'); // events are namespaced.
                        $form.unbind(".validationSubmit");
                        // reset help text
                        $helpBlock.html($helpBlock.data("original-contents"));
                        // reset classes
                        $controlGroup.attr("class", $controlGroup.data("original-classes"));
                        // reset aria
                        $this.attr("aria-invalid", $this.data("original-aria-invalid"));
                        // reset role
                        $helpBlock.attr("role", $this.data("original-role"));
                        // remove all elements we created
                        if ($.inArray($helpBlock[0], createdElements) > -1) {
                            $helpBlock.remove();
                        }

                    }
                );

            },
            collectErrors: function (includeEmpty) {

                var errorMessages = {};
                this.each(function (i, el) {
                    var $el = $(el);
                    var name = $el.attr("name");
                    var errors = $el.triggerHandler("validation.validation", {includeEmpty: true});
                    errorMessages[name] = $.extend(true, errors, errorMessages[name]);
                });

                $.each(errorMessages, function (i, el) {
                    if (el.length === 0) {
                        delete errorMessages[i];
                    }
                });

                return errorMessages;

            },
            hasErrors: function () {

                var errorMessages = [];

                this.find('input,select,textarea').add(this).each(function (i, el) {
                    errorMessages = errorMessages.concat(
                        $(el).triggerHandler("getValidators.validation") ? $(el).triggerHandler("validation.validation", {submitting: true}) : []
                    );
                });

                return (errorMessages.length > 0);
            },
            override: function (newDefaults) {
                defaults = $.extend(true, defaults, newDefaults);
            }
        },
        validatorTypes: {
            callback: {
                name: "callback",
                init: function ($this, name) {
                    var result = {
                        validatorName: name,
                        callback: $this.data("validation" + name + "Callback"),
                        lastValue: $this.val(),
                        lastValid: true,
                        lastFinished: true
                    };

                    var message = "No valido";
                    if ($this.data("validation" + name + "Message")) {
                        message = $this.data("validation" + name + "Message");
                    }
                    result.message = message;

                    return result;
                },
                validate: function ($this, value, validator) {
                    if (validator.lastValue === value && validator.lastFinished) {
                        return !validator.lastValid;
                    }

                    if (validator.lastFinished === true) {
                        validator.lastValue = value;
                        validator.lastValid = true;
                        validator.lastFinished = false;

                        var rrjqbvValidator = validator;
                        var rrjqbvThis = $this;
                        executeFunctionByName(
                            validator.callback,
                            window,
                            $this,
                            value,
                            function (data) {
                                if (rrjqbvValidator.lastValue === data.value) {
                                    rrjqbvValidator.lastValid = data.valid;
                                    if (data.message) {
                                        rrjqbvValidator.message = data.message;
                                    }
                                    rrjqbvValidator.lastFinished = true;
                                    rrjqbvThis.data(
                                        "validation" + rrjqbvValidator.validatorName + "Message",
                                        rrjqbvValidator.message
                                    );

                                    // Timeout is set to avoid problems with the events being considered 'already fired'
                                    setTimeout(function () {
                                        if (!$this.is(":focus") && $this.parents("form").first().data("jqbvIsSubmitting")) {
                                            rrjqbvThis.trigger("blur.validation");
                                        } else {
                                            rrjqbvThis.trigger("revalidate.validation");
                                        }
                                    }, 1); // doesn't need a long timeout, just long enough for the event bubble to burst
                                }
                            }
                        );
                    }

                    return false;

                }
            },
            ajax: {
                name: "ajax",
                init: function ($this, name) {
                    return {
                        validatorName: name,
                        url: $this.data("validation" + name + "Ajax"),
                        lastValue: $this.val(),
                        lastValid: true,
                        lastFinished: true
                    };
                },
                validate: function ($this, value, validator) {
                    if ("" + validator.lastValue === "" + value && validator.lastFinished === true) {
                        return validator.lastValid === false;
                    }

                    if (validator.lastFinished === true) {
                        validator.lastValue = value;
                        validator.lastValid = true;
                        validator.lastFinished = false;
                        $.ajax({
                            url: validator.url,
                            data: "value=" + encodeURIComponent(value) + "&field=" + $this.attr("name"),
                            dataType: "json",
                            success: function (data) {
                                if ("" + validator.lastValue === "" + data.value) {
                                    validator.lastValid = !!(data.valid);
                                    if (data.message) {
                                        validator.message = data.message;
                                    }
                                    validator.lastFinished = true;
                                    $this.data("validation" + validator.validatorName + "Message", validator.message);
                                    // Timeout is set to avoid problems with the events being considered 'already fired'
                                    setTimeout(function () {
                                        $this.trigger("revalidate.validation");
                                    }, 1); // doesn't need a long timeout, just long enough for the event bubble to burst
                                }
                            },
                            failure: function () {
                                validator.lastValid = true;
                                validator.message = "ajax call failed";
                                validator.lastFinished = true;
                                $this.data("validation" + validator.validatorName + "Message", validator.message);
                                // Timeout is set to avoid problems with the events being considered 'already fired'
                                setTimeout(function () {
                                    $this.trigger("revalidate.validation");
                                }, 1); // doesn't need a long timeout, just long enough for the event bubble to burst
                            }
                        });
                    }

                    return false;

                }
            },
            regex: {
                name: "regex",
                init: function ($this, name) {
                    var result = {};
                    var regexString = $this.data("validation" + name + "Regex");
                    result.regex = regexFromString(regexString);
                    if (regexString === undefined) {
                        $.error("Can't find regex for '" + name + "' validator on '" + $this.attr("name") + "'");
                    }

                    var message = "No en el formato esperado";
                    if ($this.data("validation" + name + "Message")) {
                        message = $this.data("validation" + name + "Message");
                    }

                    result.message = message;

                    result.originalName = name;
                    return result;
                },
                validate: function ($this, value, validator) {
                    return (!validator.regex.test(value) && !validator.negative) ||
                        (validator.regex.test(value) && validator.negative);
                }
            },
            email: {
                name: "email",
                init: function ($this, name) {
                    var result = {};
                    result.regex = regexFromString('[a-zA-Z0-9.!#$%&\u2019*+/=?^_`{|}~-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}');

                    var message = "La dirección de email no es válida";
                    if ($this.data("validation" + name + "Message")) {
                        message = $this.data("validation" + name + "Message");
                    }

                    result.message = message;

                    result.originalName = name;
                    return result;
                },
                validate: function ($this, value, validator) {
                    return (!validator.regex.test(value) && !validator.negative) ||
                        (validator.regex.test(value) && validator.negative);
                }
            },
            required: {
                name: "required",
                init: function ($this, name) {
                    var message = "Esto es requerido";
                    if ($this.data("validation" + name + "Message")) {
                        message = $this.data("validation" + name + "Message");
                    }

                    return {message: message, includeEmpty: true};
                },
                validate: function ($this, value, validator) {
                    return !!(
                        (value.length === 0 && !validator.negative) ||
                            (value.length > 0 && validator.negative)
                        );
                },
                blockSubmit: true
            },
            match: {
                name: "match",
                init: function ($this, name) {
                    var elementName = $this.data("validation" + name + "Match");
                    var $form = $this.parents("form").first();
                    var $element = $form.find("[name=\"" + elementName + "\"]").first();
                    $element.bind("validation.validation", function () {
                        $this.trigger("revalidate.validation", {submitting: true});
                    });
                    var result = {};
                    result.element = $element;

                    if ($element.length === 0) {
                        $.error("Can't find field '" + elementName + "' to match '" + $this.attr("name") + "' against in '" + name + "' validator");
                    }

                    var message = "Debe coincidir con";
                    var $label = null;
                    if (($label = $form.find("label[for=\"" + elementName + "\"]")).length) {
                        message += " '" + $label.text() + "'";
                    } else if (($label = $element.parents(".control-group").first().find("label")).length) {
                        message += " '" + $label.first().text() + "'";
                    }

                    if ($this.data("validation" + name + "Message")) {
                        message = $this.data("validation" + name + "Message");
                    }

                    result.message = message;

                    return result;
                },
                validate: function ($this, value, validator) {
                    return (value !== validator.element.val() && !validator.negative) ||
                        (value === validator.element.val() && validator.negative);
                },
                blockSubmit: true,
                includeEmpty: true
            },
            max: {
                name: "max",
                init: function ($this, name) {
                    var result = {};

                    result.max = $this.data("validation" + name + "Max");

                    result.message = "Too high: Maximum of '" + result.max + "'";
                    if ($this.data("validation" + name + "Message")) {
                        result.message = $this.data("validation" + name + "Message");
                    }

                    return result;
                },
                validate: function ($this, value, validator) {
                    return (parseFloat(value, 10) > parseFloat(validator.max, 10) && !validator.negative) ||
                        (parseFloat(value, 10) <= parseFloat(validator.max, 10) && validator.negative);
                }
            },
            min: {
                name: "min",
                init: function ($this, name) {
                    var result = {};

                    result.min = $this.data("validation" + name + "Min");

                    result.message = "Too low: Minimum of '" + result.min + "'";
                    if ($this.data("validation" + name + "Message")) {
                        result.message = $this.data("validation" + name + "Message");
                    }

                    return result;
                },
                validate: function ($this, value, validator) {
                    return (parseFloat(value) < parseFloat(validator.min) && !validator.negative) ||
                        (parseFloat(value) >= parseFloat(validator.min) && validator.negative);
                }
            },
            maxlength: {
                name: "maxlength",
                init: function ($this, name) {
                    var result = {};

                    result.maxlength = $this.data("validation" + name + "Maxlength");

                    result.message = "Too long: Maximum of '" + result.maxlength + "' characters";
                    if ($this.data("validation" + name + "Message")) {
                        result.message = $this.data("validation" + name + "Message");
                    }

                    return result;
                },
                validate: function ($this, value, validator) {
                    return ((value.length > validator.maxlength) && !validator.negative) ||
                        ((value.length <= validator.maxlength) && validator.negative);
                }
            },
            minlength: {
                name: "minlength",
                init: function ($this, name) {
                    var result = {};

                    result.minlength = $this.data("validation" + name + "Minlength");

                    result.message = "Too short: Minimum of '" + result.minlength + "' characters";
                    if ($this.data("validation" + name + "Message")) {
                        result.message = $this.data("validation" + name + "Message");
                    }

                    return result;
                },
                validate: function ($this, value, validator) {
                    return ((value.length < validator.minlength) && !validator.negative) ||
                        ((value.length >= validator.minlength) && validator.negative);
                }
            },
            maxchecked: {
                name: "maxchecked",
                init: function ($this, name) {
                    var result = {};

                    var elements = $this.parents("form").first().find("[name=\"" + $this.attr("name") + "\"]");
                    elements.bind("change.validation click.validation", function () {
                        $this.trigger("revalidate.validation", {includeEmpty: true});
                    });

                    result.elements = elements;
                    result.maxchecked = $this.data("validation" + name + "Maxchecked");

                    var message = "Demasiados: Maximo '" + result.maxchecked + "' checked";
                    if ($this.data("validation" + name + "Message")) {
                        message = $this.data("validation" + name + "Message");
                    }
                    result.message = message;

                    return result;
                },
                validate: function ($this, value, validator) {
                    return (validator.elements.filter(":checked").length > validator.maxchecked && !validator.negative) ||
                        (validator.elements.filter(":checked").length <= validator.maxchecked && validator.negative);
                },
                blockSubmit: true
            },
            minchecked: {
                name: "minchecked",
                init: function ($this, name) {
                    var result = {};

                    var elements = $this.parents("form").first().find("[name=\"" + $this.attr("name") + "\"]");
                    elements.bind("change.validation click.validation", function () {
                        $this.trigger("revalidate.validation", {includeEmpty: true});
                    });

                    result.elements = elements;
                    result.minchecked = $this.data("validation" + name + "Minchecked");

                    var message = "Muy Pocos: Minimo '" + result.minchecked + "' checked";
                    if ($this.data("validation" + name + "Message")) {
                        message = $this.data("validation" + name + "Message");
                    }
                    result.message = message;

                    return result;
                },
                validate: function ($this, value, validator) {
                    return (validator.elements.filter(":checked").length < validator.minchecked && !validator.negative) ||
                        (validator.elements.filter(":checked").length >= validator.minchecked && validator.negative);
                },
                blockSubmit: true,
                includeEmpty: true
            },
            number: {
                name: "number",
                init: function ($this, name) {
                    var result = {};
                    result.step = 1;
                    if ($this.attr("step")) {
                        result.step = $this.attr("step");
                    }
                    if ($this.data("validation" + name + "Step")) {
                        result.step = $this.data("validation" + name + "Step");
                    }

                    result.decimal = ".";
                    if ($this.data("validation" + name + "Decimal")) {
                        result.decimal = $this.data("validation" + name + "Decimal");
                    }

                    result.thousands = "";
                    if ($this.data("validation" + name + "Thousands")) {
                        result.thousands = $this.data("validation" + name + "Thousands");
                    }

                    result.regex = regexFromString("([+-]?\\d+(\\" + result.decimal + "\\d+)?)?");

                    result.message = "Tiene que ser un número";
                    var dataMessage = $this.data("validation" + name + "Message");
                    if (dataMessage) {
                        result.message = dataMessage;
                    }

                    return result;
                },
                validate: function ($this, value, validator) {
                    var globalValue = value.replace(validator.decimal, ".").replace(validator.thousands, "");
                    var multipliedValue = parseFloat(globalValue);
                    var multipliedStep = parseFloat(validator.step);
                    while (multipliedStep % 1 !== 0) {
                        /* thanks to @jkey #57 */
                        multipliedStep = parseFloat(multipliedStep.toPrecision(12)) * 10;
                        multipliedValue = parseFloat(multipliedValue.toPrecision(12)) * 10;
                    }
                    var regexResult = validator.regex.test(value);
                    var stepResult = parseFloat(multipliedValue) % parseFloat(multipliedStep) === 0;
                    var typeResult = !isNaN(parseFloat(globalValue)) && isFinite(globalValue);
                    var result = !(regexResult && stepResult && typeResult);
                    return result;
                },
                message: "Tiene que ser un número"
            }
        },
        builtInValidators: {
            email: {
                name: "Email",
                type: "email"
            },
            passwordagain: {
                name: "Passwordagain",
                type: "match",
                match: "password",
                message: "Does not match the given password<!-- data-validator-paswordagain-message to override -->"
            },
            positive: {
                name: "Positive",
                type: "shortcut",
                shortcut: "number,positivenumber"
            },
            negative: {
                name: "Negative",
                type: "shortcut",
                shortcut: "number,negativenumber"
            },
            integer: {
                name: "Integer",
                type: "regex",
                regex: "[+-]?\\d+",
                message: "No decimal places allowed<!-- data-validator-integer-message to override -->"
            },
            positivenumber: {
                name: "Positivenumber",
                type: "min",
                min: 0,
                message: "Must be a positive number<!-- data-validator-positivenumber-message to override -->"
            },
            negativenumber: {
                name: "Negativenumber",
                type: "max",
                max: 0,
                message: "Must be a negative number<!-- data-validator-negativenumber-message to override -->"
            },
            required: {
                name: "Required",
                type: "required",
                message: "Esto es requerido<!-- data-validator-required-message to override -->"
            },
            checkone: {
                name: "Checkone",
                type: "minchecked",
                minchecked: 1,
                message: "Check at least one option<!-- data-validation-checkone-message to override -->"
            },
            number: {
                name: "Number",
                type: "number",
                decimal: ".",
                step: "1"
            },
            pattern: {
                name: "Pattern",
                type: "regex",
                message: "Not in expected format"
            }
        }
    };

    var formatValidatorName = function (name) {
        return name
            .toLowerCase()
            .replace(
            /(^|\s)([a-z])/g,
            function (m, p1, p2) {
                return p1 + p2.toUpperCase();
            }
        )
            ;
    };

    var getValue = function ($this) {
        // Extract the value we're talking about
        var value = null;
        var type = $this.attr("type");
        if (type === "checkbox") {
            value = ($this.is(":checked") ? value : "");
            var checkboxParent = $this.parents("form").first() || $this.parents(".control-group").first();
            if (checkboxParent) {
                value = checkboxParent.find("input[name='" + $this.attr("name") + "']:checked").map(function (i, el) {
                    return $(el).val();
                }).toArray().join(",");
            }
        }
        else if (type === "radio") {
            value = ($('input[name="' + $this.attr("name") + '"]:checked').length > 0 ? $this.val() : "");
            var radioParent = $this.parents("form").first() || $this.parents(".control-group").first();
            if (radioParent) {
                value = radioParent.find("input[name='" + $this.attr("name") + "']:checked").map(function (i, el) {
                    return $(el).val();
                }).toArray().join(",");
            }
        } else if (type === "number") {
            if ($this[0].validity.valid) {
                value = $this.val();
            } else {
                if ($this[0].validity.badInput || $this[0].validity.stepMismatch) {
                    value = "NaN";
                } else {
                    value = "";
                }
            }
        } else {
            value = $this.val();
        }
        return value;
    };

    function regexFromString(inputstring) {
        return new RegExp("^" + inputstring + "$");
    }

    /**
     * Thanks to Jason Bunting / Alex Nazarov via StackOverflow.com
     *
     * http://stackoverflow.com/a/4351575
     **/
    function executeFunctionByName(functionName, context /*, args */) {
        var args = Array.prototype.slice.call(arguments, 2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(context, args);
    }

    $.fn.jqBootstrapValidation = function (method) {

        if (defaults.methods[method]) {
            return defaults.methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return defaults.methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.jqBootstrapValidation');
            return null;
        }

    };

    $.jqBootstrapValidation = function (options) {
        $(":input").not("[type=image],[type=submit]").jqBootstrapValidation.apply(this, arguments);
    };

})(jQuery);
