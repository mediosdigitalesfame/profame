<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the "Database Connection"
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the "default" group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class

//Configuracion de la otra BD
$db['another_db'] = array(
    'dsn'       => '',
    'hostname' => 'localhost',
    'username' => 'db_username',
    'password' => 'db_password',
    'database' => 'db_name2',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt'  => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

//Cargamos la otra base de datos
$DB2 = $this->load->database('another_db', TRUE);

//Query a la base de datos por defecto
$this->db->select('first_name, last_name');
$this->db->from('users');
$this->db->where('id', 99);
$query = $this->db->get();

//Query a la otra base de datos
$DB2->select('image');
$DB2->from('cdn_images');
$DB2->where('id', 25);
$query = $DB2->get();

---------------------------------------------------

$db['default']['hostname'] = "192.168.2.104";
$db['default']['username'] = "webuser";
$db['default']['password'] = "----";
$db['default']['database'] = "vad";
$db['default']['dbdriver'] = "mysql";
$db['default']['dbprefix'] = "";
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = "";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";

$db['stats']['hostname'] = "192.168.2.104";
$db['stats']['username'] = "webuser";
$db['stats']['password'] = "---";
$db['stats']['database'] = "vad_stats";
$db['stats']['dbdriver'] = "mysql";
$db['stats']['dbprefix'] = "";
$db['stats']['pconnect'] = TRUE;
$db['stats']['db_debug'] = TRUE;
$db['stats']['cache_on'] = FALSE;
$db['stats']['cachedir'] = "";
$db['stats']['char_set'] = "utf8";
$db['stats']['dbcollat'] = "utf8_general_ci";

$DB2 = $this->load->database('stats', TRUE);

$database1 = $this->load->database('database1', TRUE);
$database2 = $this->load->database('database2', TRUE); 



// The following values will probably need to be changed.
$db['default']['username'] = "profame";
$db['default']['password'] = "12qwaszx_12fame2020";
$db['default']['database'] = "profame";

*/

// The following values will probably need to be changed.
$db['default']['username'] = "root";
$db['default']['password'] = "";
$db['default']['database'] = "profame";

// The following values can probably stay the same.
$db['default']['hostname'] = "localhost";
$db['default']['dbdriver'] = "mysqli"; //Updated to latest driver.
$db['default']['dbprefix'] = "";
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = "application/cache/db";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";

$active_group = "default";
$active_record = TRUE;

/* End of file database.php */
/* Location: ./application/config/database.php */