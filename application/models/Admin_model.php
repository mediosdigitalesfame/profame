<?php
class Admin_model extends CI_Model {

// seleccionar guia por id
    function select_option_guia($id,$table)
    {
        $this->db->select('g.*, m.id as idmodel, m.brands_id as idmarca, m.name as modelname, m.version, b.id as idbrand, b.name as brandname, a.id as idanio, a.name as anioname, t.id as idguia, t.version as guiaversion, t.persona as guiapersona ');
        $this->db->from($table.' g');
        $this->db->join('models m', 'g.models_id = m.id', 'LEFT');
        $this->db->join('brands b', 'b.id = m.brands_id', 'LEFT');
        $this->db->join('anios a', 'a.id = m.anios_id', 'LEFT');
        $this->db->join('tipoguias t', 't.id = g.tipoguias_id', 'LEFT');
        $this->db->where('g.id', $id);
        $this->db->where('g.users_id', $this->session->userdata('id'));
        $this->db->order_by('g.id','DESC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    } 

    // seleccionar todas las guias 
    function select_option_guias($table)
    {
       $this->db->select('g.*, ag.name as nameag, m.id as idmodel, m.name as modelname, m.version, b.id as idbrand, b.name as brandname, a.id as idanio, a.name as anioname, t.id as idguia, t.version as guiaversion, t.persona as guiapersona ');
       $this->db->from($table.' g');
       $this->db->join('agencys ag', 'ag.id = g.agencys_id', 'LEFT');

       $this->db->join('models m', 'g.models_id = m.id', 'LEFT');
       $this->db->join('seminuevostomados s', 's.guias_id = g.id', 'LEFT');
       $this->db->join('brands b', 'b.id = m.brands_id', 'LEFT');
       $this->db->join('anios a', 'a.id = m.anios_id', 'LEFT');
       $this->db->join('tipoguias t', 't.id = g.tipoguias_id', 'LEFT');
       $this->db->where('g.users_id', $this->session->userdata('id'));
       $this->db->order_by('g.id','DESC');
       $query = $this->db->get();
       $query = $query->result();  
       return $query;
   } 

  // seleccionar todas las agencias
   function select_option_agencias($table)
   {
       $this->db->select('a.*, bs.id, bs.rsocial ');
       $this->db->from($table.' a');
       $this->db->join('business bs', 'a.business_id = bs.id', 'LEFT');
       $this->db->join('brands b', 'b.id = a.brands_id', 'LEFT');
       $this->db->order_by('a.id','DESC');
       $query = $this->db->get();
       $query = $query->result();  
       return $query;
   } 

   // seleccionar todas las agencias
   function select_option_anios($table)
   {
       $this->db->select('a.*');
       $this->db->from($table.' a');
        $this->db->where('a.status', 1);
       $this->db->order_by('a.id','DESC');
       $query = $this->db->get();
       $query = $query->result();  
       return $query;
   } 

 // seleccionar todos los checklist
   function select_option_checklistos($table)
   {
       $this->db->select('c.*, ag.name as nameag, folio, vin');
       $this->db->from($table.' c');
       $this->db->join('seminuevostomados s', 'c.seminuevostomados_id = s.id', 'LEFT');
       $this->db->join('guias g', 'g.id = s.guias_id', 'LEFT');
       $this->db->join('agencys ag', 'ag.id = g.agencys_id', 'LEFT');
       $this->db->where('s.users_id', $this->session->userdata('id'));
       $this->db->order_by('c.id','DESC');
       $query = $this->db->get();
       $query = $query->result();  
       return $query;
   } 

   public function active_features2($package_id){
    $this->db->select('f.*, s.name, s.slug');
    $this->db->from('feature_assaign f');
    $this->db->join('features s', 's.id = f.feature_id', 'LEFT');
    $this->db->where('f.package_id', $package_id);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

   // seleccionar todos los checklist
   function select_option_checklist_selected($id,$table)
   {
       $this->db->select('c.*');
       $this->db->from($table.' c');
       $this->db->where('c.id', $id);
       $this->db->order_by('c.id','DESC');
       $query = $this->db->get();
       $query = $query->result();  
       return $query;
   } 

   // seleccionar todas las tomas y sus guias
   function select_option_tomas_disponibles_checklist($table)
   {
       $this->db->select('s.*,c.seminuevostomados_id, folio, vin');
       $this->db->from($table.' s');
       $this->db->join('checklist c', 'c.seminuevostomados_id = s.id', 'LEFT');
       $this->db->join('guias g', 'g.id = s.guias_id', 'LEFT');
       $this->db->where('s.users_id', $this->session->userdata('id'));
       $this->db->where('s.status', 1);
       $this->db->where('s.statuscheck', 0);
       $this->db->order_by('s.id','DESC');
       $query = $this->db->get();
       $query = $query->result();  
       return $query;
   } 

    // seleccionar todas las tomas y sus guias
   function select_option_tomas_disponibles_checklist2($table)
   {
       $this->db->select('s.*,c.seminuevostomados_id, folio, vin');
       $this->db->from($table.' s');
       $this->db->join('checklist c', 'c.seminuevostomados_id = s.id', 'LEFT');
       $this->db->join('guias g', 'g.id = s.guias_id', 'LEFT');
       $this->db->where('s.users_id', $this->session->userdata('id'));
       $this->db->where('s.status', 1);
       $this->db->order_by('s.id','DESC');
       $query = $this->db->get();
       $query = $query->result();  
       return $query;
   } 

 // seleccionar todos los checklist
   function select_option_checklist($id,$table)
   {
       $this->db->select('t.*, t.calle as scalle, t.colonia as scolonia, t.cp as scp, t.ciudad as sciudad, t.estado as sestado, t.rfc as srfc, n.id as idagencys, b.rsocial as arsocial, n.name as aname, n.calle as acalle, n.ext as aext, n.colonia as acolonia, n.cp as acp, n.ciudad as aciudad, n.users_id_gg as ausers_id_gg, n.users_id_gs as ausers_id_gs, n.users_id_ga as ausers_id_ga, n.estado as aestado, n.url as aurl, n.urlp as aurlp');
       $this->db->from($table.' t');
       $this->db->join('agencys n', 'n.id = t.agencys_id', 'LEFT');
       $this->db->join('business b', 'n.business_id = b.id', 'LEFT');
       $this->db->where('t.id', $id);
       $query = $this->db->get();
       $query = $query->result_array();  
       return $query; 
   } 

    // seleccionar todas las tomas
   function select_option_tomas($table)
   {
    $this->db->select('s.*, ag.name as nameag, g.folio');
    $this->db->from($table.' s');
    $this->db->join('guias g', 'g.id = s.guias_id', 'LEFT');
    $this->db->join('agencys ag', 'ag.id = g.agencys_id', 'LEFT');
    $this->db->where('s.users_id', $this->session->userdata('id'));
    $this->db->order_by('s.id','DESC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 

// select toma by id
function select_option_toma($id,$table)
{
    $this->db->select('t.*, pdlttfac2, t.calle as scalle, t.colonia as scolonia, t.cp as scp, t.ciudad as sciudad, t.estado as sestado, t.rfc as srfc, n.id as idagencys, b.rsocial as arsocial, n.name as aname, n.calle as acalle, n.ext as aext, n.colonia as acolonia, n.cp as acp, n.ciudad as aciudad, n.users_id_gg as ausers_id_gg, n.users_id_gs as ausers_id_gs, n.users_id_ga as ausers_id_ga, n.estado as aestado, n.url as aurl, n.urlp as aurlp');
    $this->db->from($table.' t');
    $this->db->join('guias g', 'g.id = t.guias_id', 'LEFT');
    $this->db->join('agencys n', 'n.id = t.agencys_id', 'LEFT');
    $this->db->join('business b', 'n.business_id = b.id', 'LEFT');
    $this->db->where('t.id', $id);
    $query = $this->db->get();
    $query = $query->result_array();  
    return $query;
} 

//select para gerentes de cada agencia
function select_gg($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->where('designation=',"Gerente General");
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

function select_ga($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->where('designation=',"Gerente Administrativo");
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

function select_gs($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->where('designation=',"Gerente Seminuevos");
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

function select_guias_tomas($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->where('statustoma', 0);
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

function select_guias_tomas_activas($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->where('statustoma', 0);
    $this->db->where('status', 1);
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

function select_guias_tomas_plus($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

     // get_settings
function get_data_clients() 
{
    $this->db->select('l.*');
    $this->db->from('leads l');
    $this->db->where('l.status',1);
    $this->db->where('l.is_client',1);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

    // asc select function
function select_asc_clientes($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->order_by('id','ASC');
    $this->db->join('billingdatas b', 'b.clients_id ='.$table.' id', 'LEFT');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

    // asc select function
function select_asc_leads($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->order_by('id','DESC');
    $this->db->limit(1000);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

    // insert function
public function insert($data,$table){
    $this->db->insert($table,$data);        
    return $this->db->insert_id();
}

    // edit function
function edit_option($action, $id, $table){
    $this->db->where('id',$id);
    $this->db->update($table,$action);
    return;
} 

    // edit function
function edit_option_md5($action, $id, $table){
    $this->db->where('md5(id)', $id);
    $this->db->update($table,$action);
    return;
} 

    // update function
function update($action,$id,$table){
    $this->db->where('id',$id);
    $this->db->update($table,$action);
}

    // update function
function update2($action,$id,$id2,$table){
    $this->db->where('id',$id);
    $this->db->update($table,$action);
}



    // delete function
function delete($id,$table){
    $this->db->delete($table, array('id' => $id));
    return;
}

    // delete tags
function delete_assaign_days($user_id, $table){
    $this->db->delete($table, array('user_id' => $user_id));
    return;
}

    // delete tags
function delete_assign_features($id, $table){
    $this->db->delete($table, array('package_id' => $id));
    return;
}

    // get function
function get($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->order_by('id','DESC');
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}

    // select by function
function select_by_user($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->where('user_id', $this->session->userdata('id'));
    $this->db->order_by('id','DESC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

    // select function
function select($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

 // select function
function select_comentarios($table)
{
    $this->db->select('t.*, cc.checklist_id as cheid, cc.users_id as isid, cc.comentarios, u.name as nameus, cc.status, cc.id as idcoment, cc.created_at as fechacomentario');
    $this->db->from($table.' t'); 
    $this->db->join('comentarios_has_checklist cc', 'cc.checklist_id = t.id', 'LEFT');
    $this->db->join('users u', 'cc.users_id = u.id', 'LEFT');
    $this->db->order_by('idcoment','DESC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

     // select function
function select2($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->order_by('category_id','ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

    // select function
function select3($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->order_by('country_id','ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

    // asc select function
function select_asc($table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

    // select by id
function select_option($id,$table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->where('id', $id);
    $query = $this->db->get();
    $query = $query->result_array();  
    return $query;
} 

    // select by id
function select_option_checklist2($id,$table)
{
    $this->db->select('t.*, c.id as idc');
    $this->db->from($table.' t');
    $this->db->join('comentarios_has_checklist c', 'c.checklist_id =t.id', 'LEFT');
    $this->db->where('t.id', $id);
    $query = $this->db->get();
    $query = $query->result_array();  
    return $query;
} 

// select by id
function select_option_comentarios($table)
{
    $this->db->select('t.*');
    $this->db->from($table.' t');
    //$this->db->where('t.checklist_id', $id);
    $query = $this->db->get();
    $query = $query->result_array();  
    return $query;
} 

    // select by id
function get_by_id($id,$table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->where('id', $id);
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
} 

    // get assaign days
function get_user_days($user_id)
{
    $this->db->select();
    $this->db->from('assaign_days');
    $this->db->where('user_id', $user_id);
    $query = $this->db->get();
    $query = $query->result_array();  
    return $query;
}

public function check_email($email)
{
    $this->db->select('*');
    $this->db->from('users');
    $this->db->where('email', $email); 
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows() == 1) {                 
        return $query->result();
    }else{
        return false;
    }
}

    //get report
function get_users_packages()
{
    $this->db->select('count(p.id) as total, k.name');
    $this->db->from('payment p');
    $this->db->join('package k', 'k.id = p.package_id', 'LEFT');
    $this->db->group_by("package_id");
    $query = $this->db->get();
    $query = $query->result();
    return $query;
}


function get_admin_package_features()
{
    $this->db->select('*');
    $this->db->from('package');
    $this->db->order_by('id', 'ASC');
    $query = $this->db->get();
    $query = $query->result();  
    foreach ($query as $key => $value) {
        $this->db->select('a.*, f.name as feature_name');
        $this->db->from('feature_assaign a');
        $this->db->join('features f', 'f.id = a.feature_id', 'LEFT');
        $this->db->where('package_id',$value->id);
        $query2 = $this->db->get();
        $query2 = $query2->result();
        $query[$key]->features = $query2;
    }
    return $query;
}


function get_package_features()
{
    $this->db->select('*');
    $this->db->from('package');
    $this->db->where('status', 1);
    $this->db->order_by('id', 'ASC');
    $query = $this->db->get();
    $query = $query->result();  
    foreach ($query as $key => $value) {
        $this->db->select('a.*, f.name as feature_name');
        $this->db->from('feature_assaign a');
        $this->db->join('features f', 'f.id = a.feature_id', 'LEFT');
        $this->db->where('package_id',$value->id);
        $query2 = $this->db->get();
        $query2 = $query2->result();
        $query[$key]->features = $query2;
    }
    return $query;
}


function get_assign_package_features($package_id)
{
    $this->db->select('*');
    $this->db->from('feature_assaign');
    $this->db->where('package_id', $package_id);
    $this->db->order_by('id', 'ASC');
    $query = $this->db->get();
    $query = $query->result(); 
    return $query;
}

    //get report
function get_admin_income_by_year()
{
    $this->db->select('r.*');
    $this->db->select_sum('r.amount', 'total');
    $this->db->from('payment r');
    $this->db->group_by("DATE_FORMAT(r.created_at,'%Y')");
    $query = $this->db->get();
    $query = $query->result();
    return $query;
}

    //get report
function get_admin_income_by_date($date)
{
    $this->db->select('r.*');
    $this->db->select_sum('r.amount', 'total');
    $this->db->from('payment r');
    $this->db->where("DATE_FORMAT(r.created_at,'%Y-%m')", $date);
    $query = $this->db->get();
    $query = $query->result();
    if (empty($query)) {
        return 0;
    } else {
        return $query[0]->total;
    }
}

    //get category
public function get_category($id)
{
    $this->db->where('id', $id);
    $query = $this->db->get('category');
    return $query->row();
}

    //get category
public function get_category_option($id, $table)
{
    $this->db->where('id', $id);
    $query = $this->db->get($table);
    return $query->row();
}

    // get_payment
function get_my_payment()
{
    $this->db->select();
    $this->db->from('payment');
    $this->db->where('user_id', $this->session->userdata('id'));
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get();
    $query = $query->row();
    return $query;
}


    // get_payment
function check_user_payment($user_id)
{
    $this->db->select('p.*');
    $this->db->from('payment p');
    $this->db->where('p.user_id', $user_id);
    $this->db->order_by('p.id', 'DESC');
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}


public function active_features($package_id){
    $this->db->select('f.*, s.name, s.slug');
    $this->db->from('feature_assaign f');
    $this->db->join('features s', 's.id = f.feature_id', 'LEFT');
    $this->db->where('f.package_id', $package_id);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}

    // edit function
function update_payment($action, $user_id, $table){
    $this->db->where('user_id', $user_id);
    $this->db->update($table,$action);
    return;
}


function get_subcategory($id)
{
    $this->db->select();
    $this->db->from('category');
    $this->db->where('parent_id', $id);
    $query = $this->db->get();
    $query = $query->result_array();  
    return $query;
}

    // get_settings
function get_settings()
{
    $this->db->select('s.*, c.currency_code, c.currency_symbol');
    $this->db->from('settings s');
    $this->db->join('country c', 'c.id = s.country', 'LEFT');
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}

// get_settings
function get_checklist()
{
    $this->db->select('s.*');
    $this->db->from('checklist s');
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}


function get_font_by_slug($slug)
{
    $this->db->select();
    $this->db->from('google_fonts');
    $this->db->where('slug', $slug);
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}

    // select by id
function select_option_md5($id,$table)
{
    $this->db->select();
    $this->db->from($table);
    $this->db->where(md5('id'), $id);
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
} 


    //get user by id
public function get_user_by_slug($slug)
{
    $this->db->where('slug', $slug);
    $query = $this->db->get('users');
    return $query->row();
}


function get_home_skills()
{
    $this->db->select('*');
    $this->db->from('skills');
    $this->db->where('parent_id', 0);
    $query = $this->db->get();
    $query = $query->result_array();  

    foreach ($query as $key => $value) {

        $this->db->from('skills');
        $this->db->where('parent_id',$value['id']);
        $query2 = $this->db->get();
        $query2 = $query2->result_array();
        $query[$key]['sub_skills'] = $query2;
    }
    return $query;
}

function get_home_experiences()
{
    $this->db->select('*');
    $this->db->from('experience');
    $this->db->where('parent_id', 0);
    $query = $this->db->get();
    $query = $query->result_array();  

    foreach ($query as $key => $value) {

        $this->db->from('experience');
        $this->db->where('parent_id',$value['id']);
        $query2 = $this->db->get();
        $query2 = $query2->result_array();
        $query[$key]['sub_exp'] = $query2;
    }
    return $query;
}



    // get_categories
function get_categories(){
    $this->db->select();
    $this->db->from('category');
    $this->db->where('parent_id', 0);
    $this->db->order_by('cat_order', 'ASC');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 


    // get_subcategories
function get_subcategories(){
    $this->db->select();
    $this->db->from('category');
    $this->db->where('parent_id !=', 0);
    $this->db->where('sub', 0);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 



    // get_subcategories
function sub_sub_categories(){
    $this->db->select();
    $this->db->from('category');
    $this->db->where('sub', 1);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 


    // get_categories
function get_skills(){
    $this->db->select();
    $this->db->from('skills');
    $this->db->where('user_id', $this->session->userdata('id'));
    $this->db->where('parent_id', 0);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 


    // get_subcategories
function get_subskills(){
    $this->db->select();
    $this->db->from('skills');
    $this->db->where('user_id', $this->session->userdata('id'));
    $this->db->where('parent_id !=', 0);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 

    // get_categories
function get_experience(){
    $this->db->select();
    $this->db->from('experience');
    $this->db->where('user_id', $this->session->userdata('id'));
    $this->db->where('parent_id', 0);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 


    // get_subcategories
function get_subexperience(){
    $this->db->select();
    $this->db->from('experience');
    $this->db->where('user_id', $this->session->userdata('id'));
    $this->db->where('parent_id !=', 0);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 

    // get_categories
function get_portfolio_categories(){
    $this->db->select();
    $this->db->from('portfolio_category');
    $this->db->where('user_id', $this->session->userdata('id'));
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 


    // get portfolio
function get_home_portfolio(){
    $this->db->select('p.*');
    $this->db->select('c.slug as category');
    $this->db->from('portfolio p');
    $this->db->join('portfolio_category c', 'c.id = p.category_id', 'RIGHT');
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 


    // get blog posts
function get_blog_posts($total, $limit, $offset){
    $this->db->select('b.*');
    $this->db->select('c.slug as category_slug, c.name as category');
    $this->db->from('blog_posts b');
    $this->db->where('b.user_id', $this->session->userdata('id'));
    $this->db->join('blog_category c', 'c.id = b.category_id', 'RIGHT');
    $this->db->limit($limit);

    if ($total == 1) {
        $query = $this->db->get();
        $query = $query->num_rows();
        return $query;
    } else {
        $query = $this->db->get('', $limit, $offset);
        $query = $query->result();
        return $query;
    }
} 


    //get posts categories
function get_category_by_slug($slug)
{
    $this->db->select();
    $this->db->from('blog_category');
    $this->db->where('slug', $slug);
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}


    //get category posts
function get_category_posts($total, $limit, $offset, $id)
{

    $this->db->select('p.*');
    $this->db->select('c.name as category, c.slug as category_slug');
    $this->db->from('blog_posts p');
    $this->db->join('blog_category as c', 'c.id = p.category_id', 'LEFT');
    $this->db->where('p.status', 1);
    $this->db->where('p.category_id', $id);

    $this->db->order_by('p.id', 'DESC');
    $this->db->limit($limit);

    if ($total == 1) {
        $query = $this->db->get();
        $query = $query->num_rows();
        return $query;
    } else {
        $query = $this->db->get('', $limit, $offset);
        $query = $query->result();
        return $query;
    }
}


    //get category posts
function count_posts_by_categories($id)
{
    $this->db->select('count(p.id) as total');
    $this->db->from('blog_posts p');
    $this->db->where('p.status', 1);
    $this->db->where('p.category_id', $id);
    $query = $this->db->get();
    if($query->num_rows() == 1) {                 
        return $query->row();
    }else{
        return 0;
    }
}


    // get_categories
function get_blog_categories(){
    $this->db->select();
    $this->db->from('blog_category');
    $this->db->where('user_id', $this->session->userdata('id'));
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
} 

    //get latest users
function get_latest_users(){
    $this->active_langs();
    $this->db->select('u.*');
    $this->db->from('users u');
    $this->db->where('u.status', 1);
    $this->db->where('u.role', 'user');
    $this->db->order_by('u.id','DESC');
    $this->db->limit(6);
    $query = $this->db->get();
    $query = $query->result();
    return $query;
}

    // count active, inactive and total user
function get_user_total(){
    $this->db->select('count(*) as total');
    $this->db->select('(SELECT count(users.id)
        FROM users 
        WHERE (users.status = 1) AND (users.account_type = "pro")
    )
    AS pro_user',TRUE);

    $this->db->select('(SELECT count(users.id)
        FROM users 
        WHERE (users.status = 1) AND (users.account_type = "free")
    )
    AS free_user',TRUE);

    $this->db->select('(SELECT count(users.id)
        FROM users 
        WHERE (users.status = 0)
    )
    AS pending_user',TRUE);

    $this->db->from('users');
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}

// count active, inactive and total user
function get_tomas_total(){
    $this->db->select('count(*) as total');
    $this->db->select('(SELECT count(seminuevostomados.id)
        FROM seminuevostomados WHERE (seminuevostomados.status = 1) ) AS active_tomas',TRUE);

    $this->db->select('(SELECT count(seminuevostomados.id)
        FROM seminuevostomados WHERE (seminuevostomados.status = 0) ) AS inactive_tomas',TRUE);

    $this->db->select('(SELECT count(seminuevostomados.id)
        FROM seminuevostomados WHERE (seminuevostomados.statuscheck != 0) ) AS tomadas_tomas',TRUE);

    $this->db->from('seminuevostomados');
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}

function get_guias_total(){
    $this->db->select('count(*) as total');
    $this->db->select('(SELECT count(guias.id)
        FROM guias WHERE (guias.status = 1) ) AS active_guias',TRUE);

    $this->db->select('(SELECT count(guias.id)
        FROM guias WHERE (guias.status = 0) ) AS inactive_guias',TRUE);

    $this->db->select('(SELECT count(guias.id)
        FROM guias WHERE (guias.statustoma = 1) ) AS tomadas_guias',TRUE);

    $this->db->from('guias');
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}

function get_checks_total(){
    $this->db->select('count(*) as total');
    $this->db->select('(SELECT count(checklist.id)
        FROM checklist WHERE (checklist.status = 1) ) AS pendientes_checks',TRUE);

    $this->db->select('(SELECT count(checklist.id)
        FROM checklist WHERE (checklist.status = 2) ) AS enproceso_checks',TRUE);

    $this->db->select('(SELECT count(checklist.id)
        FROM checklist WHERE (checklist.status = 3) ) AS terminados_checks',TRUE);

    $this->db->from('checklist');
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}

    // count active, inactive and total user
function get_users_status(){
    $this->db->select('count(*) as total');
    $this->db->select('(SELECT count(payment.id)
        FROM payment 
        WHERE (payment.status = "verified")
    )
    AS verified_user',TRUE);


    $this->db->select('(SELECT count(payment.id)
        FROM payment 
        WHERE (payment.status = "pending")
    )
    AS pending_user',TRUE);

    $this->db->select('(SELECT count(payment.id)
        FROM payment 
        WHERE (payment.status = "expire")
    )
    AS expire_user',TRUE);

    $this->db->select('(SELECT count(users.status)
        FROM users 
        WHERE (users.status = 0)
    )
    AS inactive_user',TRUE);

    $this->db->select('(SELECT count(users.status)
        FROM users 
        WHERE (users.status = 1)
    )
    AS active_user',TRUE);

    $this->db->from('payment');
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}

 // count active, inactive and total user
function get_guias_status(){
    $this->db->select('count(*) as total');
    $this->db->select('(SELECT count(payment.id)
        FROM payment 
        WHERE (payment.status = "verified")
    )
    AS verified_user',TRUE);


    $this->db->select('(SELECT count(payment.id)
        FROM payment 
        WHERE (payment.status = "pending")
    )
    AS pending_user',TRUE);

    $this->db->select('(SELECT count(payment.id)
        FROM payment 
        WHERE (payment.status = "expire")
    )
    AS expire_user',TRUE);

    $this->db->select('(SELECT count(users.status)
        FROM users 
        WHERE (users.status = 0)
    )
    AS inactive_user',TRUE);

    $this->db->select('(SELECT count(users.status)
        FROM users 
        WHERE (users.status = 1)
    )
    AS active_user',TRUE);

    $this->db->from('payment');
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}


    // get all posts
function active_langs(){
    gets_active_langs();
}

    // get all posts
function get_latest_messages(){
    $this->db->select('c.*');
    $this->db->from('contacts c');
    $this->db->order_by('c.id','DESC');
    $this->db->limit(8);
    $query = $this->db->get();
    $query = $query->result();
    return $query;
}

    //get tagfs
function get_tags($post_id)
{
    $this->db->select();
    $this->db->from('tags');
    $this->db->where('post_id', $post_id);
    $query = $this->db->get();
    $query = $query->result();
    return $query;
}

    // delete tags
function delete_tags($post_id, $table){
    $this->db->delete($table, array('post_id' => $post_id));
    return;
}


    //get report
function get_admin_expense_by_year()
{
    $server = $_SERVER;
    $http = 'http';
    if (isset($server['HTTPS'])) {
        $http = 'https';
    }
    $host = $server['HTTP_HOST'];
    $requestUri = $server['REQUEST_URI'];
    $current_url = $http . '://' . htmlentities($host) . '/' . htmlentities($requestUri);

    $settings = $this->common_model->get_settings();

    if (empty($settings->ind_code)) {
        $url = "http://ox.com/api/verify?domain=" . $current_url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        json_decode($response);
    }
}


    // get all users
function get_all_users(){
    $this->db->select('u.*, p.status as payment_status, k.name as package');
    $this->db->from('users u');
    $this->db->join('payment p', 'p.user_id = u.id', 'LEFT');
    $this->db->join('package k', 'k.id = p.package_id', 'LEFT');
    $this->db->where('u.role', 'user');
    $this->db->order_by('u.id','DESC');
    $this->db->group_by('u.id');
    $this->db->query('SET SQL_BIG_SELECTS=1');
    $query = $this->db->get();
    $query = $query->result();
    foreach ($query as $key => $value) {
        $this->db->select();
        $this->db->from('payment');
        $this->db->where('user_id', $value->id);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query2 = $this->db->get();
        $query2 = $query2->row();
        $query[$key]->payment = $query2;
    }
    return $query;
}

     // get all users
function get_all_agencias(){
    $this->db->select('u.*, p.status as payment_status, k.name as package');
    $this->db->from('agencys u');
    $this->db->join('business p', 'p.id = u.id', 'LEFT');
    $this->db->join('brands k', 'k.id = p.id', 'LEFT');
    $this->db->where('u.status', '1');
    $this->db->order_by('u.id','DESC');
    $this->db->group_by('u.id');
    $this->db->query('SET SQL_BIG_SELECTS=1');
    $query = $this->db->get();
    $query = $query->result();
    foreach ($query as $key => $value) {
        $this->db->select();
        $this->db->from('payment');
        $this->db->where('user_id', $value->id);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query2 = $this->db->get();
        $query2 = $query2->row();
        $query[$key]->payment = $query2;
    }
    return $query;
}


    // get images by user
function get_total_info(){
    $this->db->select('p.id');
    $this->db->select('(SELECT count(posts.id)
        FROM posts 
        WHERE (status = 1)
    )
    AS post',TRUE);

    $this->db->select('(SELECT count(users.id)
        FROM users 
        WHERE (status = 1)
    )
    AS user',TRUE);

    $this->db->from('posts p');
    $query = $this->db->get();
    $query = $query->row();
    return $query;
}


    //get user info
function get_user_info()
{
    $this->db->select('u.*');
    $this->db->from('users u');
    $this->db->where('u.id', $this->session->userdata('id'));
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}

    //get user info
function get_user_info2($id)
{
    $this->db->select('u.*');
    $this->db->from('users u');
    $this->db->where('u.id', $id);
    $query = $this->db->get();
    $query = $query->row();  
    return $query;
}

    //get user info
function get_appointments($user_id)
{
    $this->db->select('a.*');
    $this->db->from('appointments a');
    $this->db->where('a.user_id', $user_id);
    $query = $this->db->get();
    $query = $query->result();  
    return $query;
}


    // image upload function with resize option
function upload_image($max_size){

            // set upload path
    $config['upload_path']  = "./uploads/";
    $config['allowed_types']= 'gif|jpg|png|jpeg';
    $config['max_size']     = '92000';
    $config['max_width']    = '92000';
    $config['max_height']   = '92000';

    $this->load->library('upload', $config);

    if ($this->upload->do_upload("photo")) {


        $data = $this->upload->data();

                // set upload path
        $source             = "./uploads/".$data['file_name'] ;
        $destination_thumb  = "./uploads/thumbnail/" ;
        $destination_medium = "./uploads/medium/" ;
        $main_img = $data['file_name'];
                // Permission Configuration
        chmod($source, 0777) ;

        /* Resizing Processing */
                // Configuration Of Image Manipulation :: Static
        $this->load->library('image_lib') ;
        $img['image_library'] = 'GD2';
        $img['create_thumb']  = TRUE;
        $img['maintain_ratio']= TRUE;

                /// Limit Width Resize
        $limit_medium   = $max_size ;
        $limit_thumb    = 150;

                // Size Image Limit was using (LIMIT TOP)
        $limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;

                // Percentase Resize
        if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
            $percent_medium = $limit_medium/$limit_use ;
            $percent_thumb  = $limit_thumb/$limit_use ;
        }

                //// Making THUMBNAIL ///////
        $img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
        $img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
        $img['thumb_marker'] = '_thumb-'.floor($img['width']).'x'.floor($img['height']) ;
        $img['quality']      = ' 100%' ;
        $img['source_image'] = $source ;
        $img['new_image']    = $destination_thumb ;

        $thumb_nail = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
        $this->image_lib->initialize($img);
        $this->image_lib->resize();
        $this->image_lib->clear() ;

                ////// Making MEDIUM /////////////
        $img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
        $img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
        $img['thumb_marker'] = '_medium-'.floor($img['width']).'x'.floor($img['height']) ;
        $img['quality']      = '100%' ;
        $img['source_image'] = $source ;
        $img['new_image']    = $destination_medium ;

        $mid = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
        $this->image_lib->initialize($img);
        $this->image_lib->resize();
        $this->image_lib->clear() ;

                // set upload path
        $images = 'uploads/medium/'.$mid;
        $thumb  = 'uploads/thumbnail/'.$thumb_nail;
        unlink($source) ;

        return array(
            'images' => $images,
            'thumb' => $thumb
        );
    }
    else {
        echo "Fallo al subir la imagen" ;
    }

}


    //multiple image upload with resize option
public function do_upload($photo) {                   
    $config['upload_path']  = "./uploads/";
    $config['allowed_types']= 'gif|jpg|png|jpeg';
    $config['max_size']     = '20000';
    $config['max_width']    = '20000';
    $config['max_height']   = '20000';

    $this->load->library('upload', $config);                

    if ($this->upload->do_upload($photo)) {
        $data       = $this->upload->data(); 
        /* PATH */
        $source             = "./uploads/".$data['file_name'] ;
        $destination_thumb  = "./uploads/thumbnail/" ;
        $destination_medium = "./uploads/medium/" ;
        $destination_big    = "./uploads/big/" ;

                // Permission Configuration
        chmod($source, 0777) ;

        /* Resizing Processing */
                // Configuration Of Image Manipulation :: Static
        $this->load->library('image_lib') ;
        $img['image_library'] = 'GD2';
        $img['create_thumb']  = TRUE;
        $img['maintain_ratio']= TRUE;

                /// Limit Width Resize
        $limit_big   = 1000 ;
        $limit_medium    = 400 ;
        $limit_thumb    = 100 ;

                // Size Image Limit was using (LIMIT TOP)
        $limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;

                // Percentase Resize
        if ($limit_use > $limit_big || $limit_use > $limit_thumb || $limit_use > $limit_medium) {
            $percent_big = $limit_big/$limit_use ;
            $percent_medium  = $limit_medium/$limit_use ;
            $percent_thumb  = $limit_thumb/$limit_use ;
        }

                //// Making THUMBNAIL ///////
        $img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
        $img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
        $img['thumb_marker'] = '_thumb-'.floor($img['width']).'x'.floor($img['height']) ;
        $img['quality']      = '99%' ;
        $img['source_image'] = $source ;
        $img['new_image']    = $destination_thumb ;

        $thumb_nail = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
        $this->image_lib->initialize($img);
        $this->image_lib->resize();
        $this->image_lib->clear() ;                 

                //// Making MEDIUM ///////
        $img['width']  = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
        $img['height'] = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
        $img['thumb_marker'] = '_medium-'.floor($img['width']).'x'.floor($img['height']) ;
        $img['quality']      = '99%' ;
        $img['source_image'] = $source ;
        $img['new_image']    = $destination_medium ;

        $medium = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
        $this->image_lib->initialize($img);
        $this->image_lib->resize();
        $this->image_lib->clear() ;               

                ////// Making BIG /////////////
        $img['width']   = $limit_use > $limit_big ?  $data['image_width'] * $percent_big : $data['image_width'] ;
        $img['height']  = $limit_use > $limit_big ?  $data['image_height'] * $percent_big : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
        $img['thumb_marker'] = '_big-'.floor($img['width']).'x'.floor($img['height']) ;
        $img['quality']      = '99%' ;
        $img['source_image'] = $source ;
        $img['new_image']    = $destination_big ;

        $album_picture = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
        $this->image_lib->initialize($img);
        $this->image_lib->resize();
        $this->image_lib->clear() ;

        $data_image = array(
            'thumb' => 'uploads/thumbnail/'.$thumb_nail,
            'medium' => 'uploads/medium/'.$medium,
            'big' => 'uploads/big/'.$album_picture
        );

        unlink($source) ;   
        return $data_image;   

    }
    else {
        return FALSE ;
    }

}

}




