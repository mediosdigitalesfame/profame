<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model{
	
	function get_category(){
		$query = $this->db->get('category');
		return $query;	
	}

	function get_sub_category($category_id){
		$query = $this->db->get_where('sub_category', array('subcategory_category_id' => $category_id));
		return $query;
	}

	function get_modelos($marca_id){
		$this->db->select();
                $this->db->from('models');
                $this->db->order_by('id','ASC');
                $this->db->where('brands_id',$marca_id);
                $this->db->where('status',1);
                $query = $this->db->get();
                return $query;
        }

        function get_modelos2($marca_id,$anio_id){
              $this->db->select();
              $this->db->from('models');
              $this->db->order_by('id','ASC');
              $this->db->where('brands_id',$marca_id);
              $this->db->where('anios_id',$anio_id);
              $this->db->where('status',1);
              $query = $this->db->get();
              return $query;
      }

      function get_anios($marca_id){
              $this->db->distinct()->select('a.id, a.status as statusanios, a.name, m.brands_id, m.status');
              $this->db->from('models m');
              $this->db->join('anios a', 'm.anios_id = a.id', 'LEFT');
              $this->db->join('brands b', 'm.brands_id = b.id', 'LEFT');
              $this->db->order_by('a.id','ASC');
              $this->db->where('m.brands_id',$marca_id);
              $this->db->where('m.status',1);
              $this->db->where('a.status',1);
              $query = $this->db->get();
              return $query;
      }

      function get_anios2($marca_id){
              $this->db->distinct()->select('a.*, m.brands_id, m.status');
              $this->db->from('anios a');
              $this->db->join('models m', 'm.brands_id ='.$marca_id, 'LEFT');
              $this->db->where('m.brands_id',$marca_id);
              $this->db->where('m.status',1);
              $this->db->where('a.status',1);
              $query = $this->db->get();
              return $query;
      }

      function get_edit(){
        $product_id = $this->uri->segment(3);
        $data['product_id'] = $product_id;
        $data['category'] = $this->product_model->get_category()->result();
        $get_data = $this->product_model->get_product_by_id($product_id);
        if($get_data->num_rows() > 0){
                $row = $get_data->row_array();
                $data['sub_category_id'] = $row['product_subcategory_id'];
        }
        $this->load->view('edit_product_view',$data);
}



function get_guia_by_id($guia_id){
               $this->db->select('g.*, m.id as idmodel, m.brands_id as idmarca, m.name as modelname, m.version, b.id as idbrand, b.name as brandname, a.id as idanio, a.name as anioname, t.id as idguia, t.version as guiaversion, t.persona as guiapersona ');
        $this->db->from('guias g');
        $this->db->join('models m', 'g.models_id = m.id', 'LEFT');
        $this->db->join('brands b', 'b.id = m.brands_id', 'LEFT');
        $this->db->join('anios a', 'a.id = m.anios_id', 'LEFT');
        $this->db->join('tipoguias t', 't.id = g.tipoguias_id', 'LEFT');
        $this->db->where('g.id', $guia_id);
        
        $query = $this->db->get();
       
        return $query;
        }




}