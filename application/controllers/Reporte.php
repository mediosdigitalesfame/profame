<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// usamos los espacios de nombres para el paquete de PhpSpreadsheet  
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  
class Reporte extends CI_Controller {
// Creamos nuestro constructor en este caso para cargar algún modelo
public function __construct() {
parent::__construct();
// $this->load->model('alumno_model');
}
  
public function index(){
$this->load->view('welcome_message');
}





  
// NUESTRO ARCHIVO EN PDF
public function pdf(){

$pdf = new FPDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 16);
//inserto la cabecera poniendo una imagen dentro de una celda
$pdf->Cell(700,85,$pdf->Image(base_url().'assets/porto/img/logo.png',30,12,160),0,0,'C');


$pdf->Cell(100,12,"Presupuesto: ".  "x");
 
$pdf->Cell(100,12,"Fecha: ". date('d/m/Y'));
$pdf->Line(35,40,190,40);
$pdf->Ln(7);
$pdf->Cell(100,12,"Nombre : "."x");
$pdf->Cell(90,12,"Nif: "."x");
$pdf->Line(35,48,190,48);
$pdf->Ln(7);
$pdf->Cell(100,12,"Domicilio: ". "x");
$pdf->Line(35,56,190,56);
$pdf->Ln(7);
$pdf->Cell(90,12 );
$pdf->Line(35,62,190,62);
$pdf->Ln(7);
$pdf->Cell(100,12,"Equipo: "."x");
$pdf->Line(35,68,190,68);
$pdf->Ln(9);
$pdf->SetFont('Arial','B',10);

$pdf->Cell(60,12,'PRESUPUESTO');

$pdf->Ln(2);





$pdf->Output('hola_mudo.pdf', 'I');
}



  
// NUESTRO ARCHIVO EN EXCEL
public function excel(){
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'Hello World !');
$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="hello world.xlsx"');
header('Cache-Control: max-age=0');
$writer->save('php://output');
}
  
// NUESTRO ARCHIVO EN WORD
public function word(){
$phpWord = new \PhpOffice\PhpWord\PhpWord();
  
$section = $phpWord->addSection();
  
$section->addText('"Learn from yesterday, live for today, hope for tomorrow. '
. 'The important thing is not to stop questioning." '
. '(Albert Einstein)');
  
$section->addText('Great achievement is usually born of great sacrifice, '
. 'and is never the result of selfishness. (Napoleon Hill)',
array('name' => 'Tahoma', 'size' => 10));
  
$fontStyleName = 'oneUserDefinedStyle';
$phpWord->addFontStyle($fontStyleName,
array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true));
          
$section->addText('"The greatest accomplishment is not in never falling, '
. 'but in rising again after you fall." '
. '(Vince Lombardi)',
$fontStyleName);
  
$fontStyle = new \PhpOffice\PhpWord\Style\Font();
$fontStyle->setBold(true);
$fontStyle->setName('Tahoma');
$fontStyle->setSize(13);
$myTextElement = $section->addText('"Believe you can and you\'re halfway there." (Theodor Roosevelt)');
$myTextElement->setFontStyle($fontStyle);
  
$file = 'HelloWorld.docx';
header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="' . $file . '"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');
$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$xmlWriter->save("php://output");
}
  
}





