<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Home_Controller {

    public function __construct()
    {
        parent::__construct();
        load_settings_data();
        get_header_info();expire_payments();
    }


    public function mask()
    {   
        get_active_pages();
        $data = array();
        $data['page_title'] = 'Inicio';
        $data['page'] = 'Inicio';
        $data['services'] = $this->common_model->select('product_services');
        $data['posts'] = $this->common_model->get_home_blog_posts();
        $data['features'] = $this->common_model->gets_home_features();
        $data['main_content'] = $this->load->view('mask', $data, TRUE);
        $this->load->view('index', $data);
    }

     public function index()
    {   
        get_active_pages();
        $data = array();
        $data['page_title'] = 'Inicio';
        $data['page'] = 'Inicio';
        $data['services'] = $this->common_model->select('product_services');
        $data['posts'] = $this->common_model->get_home_blog_posts();
        $data['features'] = $this->common_model->gets_home_features();
        $data['main_content'] = $this->load->view('home', $data, TRUE);
        $this->load->view('index', $data);
    }

     public function perfiles()
    {   
        get_active_pages();
        $data = array();
        $data['page_title'] = 'Inicio';
        $data['page'] = 'Inicio';

        $data['services'] = $this->common_model->select('product_services');
        $data['posts'] = $this->common_model->get_home_blog_posts();
        $data['features'] = $this->common_model->gets_home_features();
        $data['main_content'] = $this->load->view('homeperfil', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

    //features
    public function features()
    {   
        $data = array();
        $data['page_title'] = 'Features';
        $data['features'] = $this->common_model->select('features');
        $data['main_content'] = $this->load->view('features', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

    //faq
    public function faq()
    {   
        $data = array();
        $data['page_title'] = 'Faq';
        $data['faqs'] = $this->common_model->select('faqs');
        $data['main_content'] = $this->load->view('faqs', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

    //faq
    public function status()
    {   
        $data = array();
        $data['page_title'] = 'Profile Status';
        $data['main_content'] = $this->load->view('status', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

    //pricing
    public function pricing()
    {   
        $data = array();
        $data['page_title'] = 'Pricing';
        $data['packages'] = $this->admin_model->get_package_features();
        $data['features'] = $this->admin_model->select('features');
        $data['main_content'] = $this->load->view('price', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

     
    //features
    public function users()
    {   
        $data = array();
        //initialize pagination
        $this->load->library('pagination');
        $config['base_url'] = base_url('home/users');
        $total_row = $this->common_model->get_home_users(1 , 0, 0);
        $config['total_rows'] = $total_row;
        $config['per_page'] = 9;
        $this->pagination->initialize($config);
        $page = $this->security->xss_clean($this->input->get('page'));
        if (empty($page)) {
            $page = 0;
        }
        if ($page != 0) {
            $page = $page - 1;
        }

        $data['page_title'] = 'Users';
        $data['users'] = $this->common_model->get_home_users(0 , $config['per_page'], $page * $config['per_page']);
        $data['pro'] = $this->common_model->get_total_user_by_type('pro');
        $data['free'] = $this->common_model->get_total_user_by_type('free');
        $data['skills'] = $this->common_model->get_common_skills();
        $data['main_content'] = $this->load->view('users', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }


    //follow user
    public function follow($id)
    {   
        $data = array(
            'follower_id' => $id,
            'action_id' => $this->session->userdata('id'),
            'status' => 1,
            'created_at' => my_date_now()
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->insert($data, 'follower');
        echo json_encode(array('st' => 1));
    }


    //unfollow user
    public function unfollow($id){
        $this->common_model->remove_follower($id, 'follower');
        echo json_encode(array('st' => 1));
    }

    


    //payment success
    public function payment_success($payment_id)
    {   
        $payment = $this->common_model->get_payment($payment_id);
        $data = array(
            'status' => 'verified',
            'expire_on' => date('Y-m-d', strtotime('+12 month')),
            'created_at' => my_date_now()
        );
        $data = $this->security->xss_clean($data);

        $user_data = array(
            'status' => 1
        );
        $user_data = $this->security->xss_clean($user_data);

        if (!empty($payment)) {
            $this->common_model->edit_option($user_data, $payment->user_id,'users');
            $this->common_model->edit_option($data, $payment->id,'payment');
        }
        $data['success_msg'] = 'Success';
        $this->load->view('purchase', $data);

    }

    //payment cancel
    public function payment_cancel($payment_id)
    {   
        $payment = $this->common_model->get_payment($payment_id);
        $data = array(
            'status' => 'pending'
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->edit_option($data, $payment->id,'payment');
        $data['error_msg'] = 'Error';
        $this->load->view('purchase', $data);
    }


    //create profile
    public function create_profile($package = '')
    {   
        $data = array();
        $data['page_title'] = 'Create Profile';
        $data['agencias'] = $this->admin_model->select('agencys');
        $data['departamentos'] = $this->admin_model->select('departaments');
        $data['main_content'] = $this->load->view('create_profile', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

     //create profile
    public function crear_perfil($package = '')
    {   
        $data = array();
        $data['page_title'] = 'Crear Perfil';
        $data['main_content'] = $this->load->view('grupo/crear_perfil', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }


    //check username using ajax
    public function check_username($value)
    {   
        $result = $this->common_model->check_username($value);
        if (!empty($result)) {
            echo json_encode(array('st' => 2));
        } else {
            echo json_encode(array('st' => 1));
        }
    }

    //send contact message
    public function send_message()
    {     
        if ($_POST) {
            $data = array(
                'name' => $this->input->post('name', true),
                'email' => $this->input->post('email', true),
                'message' => $this->input->post('message', true),
                'created_at' => my_date_now()
            );
            $data = $this->security->xss_clean($data);
            
            //check reCAPTCHA status
            if (!$this->recaptcha_verify_request()) {
                $this->session->set_flashdata('error', 'Recaptcha is required'); 
            } else {
                $this->common_model->insert($data, 'site_contacts');
                $this->session->set_flashdata('msg', 'Message send Successfully');
            }
            redirect($_SERVER['HTTP_REFERER']);
        }
    }


    //blogs
    public function blogs()
    {   
        $data = array();
        //initialize pagination
        $this->load->library('pagination');
        $config['base_url'] = base_url('blog');
        $total_row = $this->common_model->get_site_blog_posts(1 , 0, 0);
        $config['total_rows'] = $total_row;
        $config['per_page'] = 9;
        $this->pagination->initialize($config);
        $page = $this->security->xss_clean($this->input->get('page'));
        if (empty($page)) {
            $page = 0;
        }
        if ($page != 0) {
            $page = $page - 1;
        }
        
        $data['page_title'] = 'Blog Posts';
        $data['posts'] = $this->common_model->get_site_blog_posts(0 , $config['per_page'], $page * $config['per_page']);
        $data['categories'] = $this->common_model->get_blog_categories();
        $data['main_content'] = $this->load->view('blog_posts', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

    //category
    public function category($slug)
    {   
        $data = array();
        $slug = $this->security->xss_clean($slug);
        $category = $this->common_model->get_category_by_slug($slug);
        
        if (empty($category)) {
            redirect(base_url('blog'));
        }

        //initialize pagination
        $this->load->library('pagination');
        $config['base_url'] = base_url('category/'.$slug);
        $total_row = $this->common_model->get_site_category_posts(1 , 0, 0, $category->id);
        $config['total_rows'] = $total_row;
        $config['per_page'] = 9;
        $this->pagination->initialize($config);
        $page = $this->security->xss_clean($this->input->get('page'));
        if (empty($page)) {
            $page = 0;
        }
        if ($page != 0) {
            $page = $page - 1;
        }
        
        $data['page_title'] = 'Category Posts';
        $data['title'] = $category->name;
        $data['posts'] = $this->common_model->get_site_category_posts(0, $config['per_page'], $page * $config['per_page'], $category->id);
        $data['categories'] = $this->common_model->get_blog_categories();
        $data['main_content'] = $this->load->view('blog_posts', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

    //post details
    public function post_details($slug)
    {   
        $data = array();
        $slug = $this->security->xss_clean($slug);
        $data['page_title'] = 'Post details';
        $data['page'] = 'Post';
        $data['post'] = $this->common_model->get_post_details($slug);

        if (empty($data['post'])) {
            redirect(base_url());
        }
        $category_id = $data['post']->category_id;
        $post_id = $data['post']->id;
        $data['post_id'] = $post_id;

        $data['related_posts'] = $this->common_model->get_site_related_post($category_id, $post_id);
        $data['categories'] = $this->common_model->get_blog_category();
        $data['tags'] = $this->common_model->get_post_tags($post_id);
        $data['main_content'] = $this->load->view('single_post', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

  
    public function contact()
    {   
        $data = array();
        $data['page_title'] = 'Contact';
        $data['settings'] = $this->common_model->get('settings');
        $data['main_content'] = $this->load->view('contact', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

    //show pages
    public function page($slug)
    {   
        $data = array();
        $data['page_title'] = 'Page';
        $data['page'] = $this->common_model->get_single_page($slug);
        $data['page_name'] = $data['page']->title;
        $data['main_content'] = $this->load->view('page', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }

    //show pages
    public function terms()
    {   
        $data = array();
        $data['page_title'] = 'Terminos y Condiciones';
        $data['main_content'] = $this->load->view('page', $data, TRUE);
        $this->load->view('indexperfil', $data);
    }


    // not found page
    public function error_404()
    {
        $data['page_title'] = "Error 404";
        $data['description'] = "Error 404";
        $data['keywords'] = "error,404";
        $this->load->view('error_404');
    }
 

   /*
    |--------------------------------------------------------------------------
    | PARTE DEL FRON PAGINA DE FAME CON PORTO
    |--------------------------------------------------------------------------
    */

    
     /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS AUDI
    |--------------------------------------------------------------------------
    */

    private $paginaaudijuriquilla = 'http://www.audicenterjuriquilla.com.mx/';
    private $ubicacionaudijuriquilla = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3733.0757002247115!2d-100.39957568559856!3d20.666499005423386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d35a3374ec44f9%3A0x92e7646c22fa58ad!2sAudi%20Center%20Juriquilla%20FAME!5e0!3m2!1ses-419!2smx!4v1597771976712!5m2!1ses-419!2smx';


     /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS MINI
    |--------------------------------------------------------------------------
    */

    private $paginaminitalisman = 'http://www.morelia.mini.com.mx/es_MX/home.html';
    private $ubicacionminitalisman = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1878.1988811479762!2d-101.15886414207462!3d19.695682621974573!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d11ff6199b761%3A0xc4080e711ec2f675!2sMINI%20Talism%C3%A1n%20-%20Morelia!5e0!3m2!1ses-419!2smx!4v1597771790586!5m2!1ses-419!2smx';

     /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS BMW
    |--------------------------------------------------------------------------
    */

    private $paginabmwtalisman = 'https://www.bmw.com.mx/talisman-automotriz';
    private $ubicacionbmwtalisman = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1878.1998307143579!2d-101.15826319950646!3d19.695601700374773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d11ff615feee3%3A0xa3b0dce74cd951d6!2sBMW%20Talism%C3%A1n%20Automotriz!5e0!3m2!1ses-419!2smx!4v1597771710409!5m2!1ses-419!2smx';


    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS GMC
    |--------------------------------------------------------------------------
    */

    private $paginagmcmorelia = 'https://www.famemanantiales.com/';
    private $ubicaciongmcmorelia = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.670491169684!2d-101.16896718561405!3d19.684058437693494!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d0c1d7c2fe3ef%3A0xd6028454e5bb7cc1!2sGMC%2C%20Buick%20y%20Cadillac%20Morelia%20FAME!5e0!3m2!1ses-419!2smx!4v1597771413071!5m2!1ses-419!2smx';

    private $paginagmcuruapan = 'https://www.famemanantiales.com/';
    private $ubicaciongmcuruapan = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.7170792513098!2d-102.06145318561867!3d19.38139934733932!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de297c0267407%3A0xb0c587a20f09ac17!2sGMC%2C%20Buick%20y%20Cadillac%20Uruapan%20FAME!5e0!3m2!1ses-419!2smx!4v1597771477778!5m2!1ses-419!2smx';

     /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS KIA
    |--------------------------------------------------------------------------
    */

    private $paginakiapedregal = 'https://dealers.kia.com/mx/pedregal/';
    private $ubicacionkiapedregal = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3765.4105015050027!2d-99.21756638561975!3d19.30798544965799!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cdffbef0c6109f%3A0xe1b9f36f0f29956f!2sKIA%20Pedregal%20FAME!5e0!3m2!1ses-419!2smx!4v1597771213299!5m2!1ses-419!2smx';

    private $paginakiaparicutin = 'https://dealers.kia.com/mx/paricutin/';
    private $ubicacionkiaparicutin = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.5742468514136!2d-102.04337148561861!3d19.3875792471438!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de2eec32395f1%3A0xaada31ab134c9d03!2sKIA%20Paricut%C3%ADn%20Uruapan!5e0!3m2!1ses-419!2smx!4v1597771167811!5m2!1ses-419!2smx';

    private $paginakiadelduero = 'https://dealers.kia.com/mx/duero/';
    private $ubicacionkiadelduero = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3749.6806002630237!2d-102.2917907856095!3d19.9799302281292!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842e88c5380a4a21%3A0xd00f3caf51095c24!2sKIA%20Del%20Duero%20Zamora%20FAME!5e0!3m2!1ses-419!2smx!4v1597771127109!5m2!1ses-419!2smx';

    private $paginakiamilcumbres = 'https://dealers.kia.com/mx/milcumbres/';
    private $ubicacionkiamilcumbres = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.422244011858!2d-101.14945928561389!3d19.69463943735382!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d121da23c5319%3A0x384d46049af621e9!2sKIA%20Mil%20Cumbres%20FAME!5e0!3m2!1ses-419!2smx!4v1597771088597!5m2!1ses-419!2smx';

    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS NISSAN
    |--------------------------------------------------------------------------
    */

    private $paginanissanaltozano = 'https://www.nissanaltozano.com.mx/';
    private $ubicacionnissanaltozano = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.8845105235605!2d-101.16262708561422!3d19.674931937986464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d0d3f09a56753%3A0xc2a35ed107da2c54!2sNissan%20Altozano%20FAME!5e0!3m2!1ses-419!2smx!4v1597770519078!5m2!1ses-419!2smx';

    private $paginanissanacueducto = 'https://www.nissanacueducto.com.mx/';
    private $ubicacionnissanacueducto = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.415642516389!2d-101.1502980856139!3d19.6949207373447!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d121d72f3a7c1%3A0x303327e7ad3e0c77!2sNissan%20Acueducto%20FAME!5e0!3m2!1ses-419!2smx!4v1597770718422!5m2!1ses-419!2smx';

    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS MITSUBISHI
    |--------------------------------------------------------------------------
    */

    private $paginamitsubishiqueretaro = 'https://mitsubishi-corregidora.mx/';
    private $ubicacionmitsubishiqueretaro = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12563.934871527752!2d-100.38794730551923!3d20.57661238479253!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d344d9e5a3cb6d%3A0x8b6f929eb7c9cb51!2sMitsubishi%20Quer%C3%A9taro%20FAME!5e0!3m2!1ses-419!2smx!4v1597770076016!5m2!1ses-419!2smx';

    private $paginamitsubishiuruapan = 'https://mitsubishi-uruapan.mx/';
    private $ubicacionmitsubishiuruapan = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.724626547184!2d-102.06129748561867!3d19.38107274734967!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de29796a3609f%3A0xcd856d79bad2017d!2sMitsubishi%20Uruapan!5e0!3m2!1ses-419!2smx!4v1597769963959!5m2!1ses-419!2smx';

    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS FIAT
    |--------------------------------------------------------------------------
    */

    private $paginafiatqueretaro = 'https://famequeretaro.com.mx/';
    private $ubicacionfiatqueretaro = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1176.538398355122!2d-100.38956035404874!3d20.5767574977047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf9afdcf491a80d06!2sFIAT%20Quer%C3%A9taro%20FAME!5e0!3m2!1ses-419!2smx!4v1597769656525!5m2!1ses-419!2smx';

    private $paginafiaturuapan = 'https://medina-automotriz.com.mx/';
    private $ubicacionfiaturuapan = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.724388529615!2d-102.06140868561864!3d19.38108304734938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de29794240af3%3A0x31672987e489c827!2sFIAT%20CHRYSLER%20-Fame%20Automotriz%20-%20Medina!5e0!3m2!1ses-419!2smx!4v1597769759086!5m2!1ses-419!2smx';

    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS CHRYSLER
    |--------------------------------------------------------------------------
    */

    private $paginachryslerqueretaro = 'https://famequeretaro.com.mx/';
    private $ubicacionchryslerqueretaro = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29878.722308875575!2d-100.42196135539221!3d20.594578892227425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf6ce86ace286a9ec!2sCHRYSLER%2C%20DODGE%2C%20JEEP%20Y%20RAM%20Quer%C3%A9taro%20FAME!5e0!3m2!1ses-419!2smx!4v1597769493122!5m2!1ses-419!2smx';

    private $paginachrysleruruapan = 'https://medina-automotriz.com.mx/';
    private $ubicacionchrysleruruapan = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d940.9313345462632!2d-102.05948941693165!3d19.381041951819025!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de2979740f9b3%3A0xcdb3e002a6077b3f!2sFAME%20Medina%20Uruapan%20(Chrysler%2C%20Dodge%2C%20RAM%2C%20Jeep)!5e0!3m2!1ses-419!2smx!4v1597769307481!5m2!1ses-419!2smx';

    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS VOLKSWAGEN
    |--------------------------------------------------------------------------
    */

    private $paginavolkswagen = 'https://vw-fame.com.mx/';
    private $ubicacionvolkswagen = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d60102.547676534225!2d-101.19234540517701!3d19.695193990178854!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe285e9731525fc10!2sVolkswagen%20Autos%20Fame!5e0!3m2!1ses-419!2smx!4v1597768575679!5m2!1ses-419!2smx';

    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS MOTORRAD
    |--------------------------------------------------------------------------
    */

    private $paginamotorradmorelia = 'https://www.bmw-motorrad.com.mx/talisman-morelia/es/home.html';
    private $ubicacionmotorradmorelia = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.4111648063526!2d-101.15951438561389!3d19.69511153733862!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d1201e0db89fd%3A0xc71975a8ace1524a!2sBMW%20Motorrad%20Morelia%20Talism%C3%A1n!5e0!3m2!1ses-419!2smx!4v1597768425527!5m2!1ses-419!2smx';

    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS ISUZU
    |--------------------------------------------------------------------------
    */

    private $paginaisuzu = 'https://www.famecamiones.com/';
    private $ubicacionisuzu = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.5132828451538!2d-101.24109978561394!3d19.69075973747834!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d0ee8cef7d195%3A0x74d1ea8a2c8104d0!2sFame%20Camiones!5e0!3m2!1ses-419!2smx!4v1597768027648!5m2!1ses-419!2smx';


    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS TOYOTA
    |--------------------------------------------------------------------------
    */

    private $paginatoyotauruapan = 'https://www.toyotauruapan.com/';
    private $ubicaciontoyotauruapan = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d940.8910532372248!2d-102.0411506335782!3d19.38801331071826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa210a2f9a7f3bf16!2sToyota%20Uruapan%20FAME!5e0!3m2!1ses-419!2smx!4v1597766644152!5m2!1ses-419!2smx';

    private $paginatoyotavalladolid = 'https://www.famevalladolid.com/';
    private $ubicaciontoyotavalladolid = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1878.2051454878904!2d-101.15345784204246!3d19.69514877198303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf2288c8d94cfa1f8!2sToyota%20Valladolid%20FAME!5e0!3m2!1ses-419!2smx!4v1597766803498!5m2!1ses-419!2smx';

    private $paginatoyotaperisur = 'https://www.fameperisur.com/';
    private $ubicaciontoyotaperisur = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3765.4175320307468!2d-99.2159959593266!3d19.307680100000006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cdffbf04b1453f%3A0x4be68c599a185ef7!2sToyota%20FAME%20Perisur!5e0!3m2!1ses-419!2smx!4v1597766888248!5m2!1ses-419!2smx';

    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS CHEVROLET
    |--------------------------------------------------------------------------
    */
    private $paginachevroletmorelia = 'https://www.famechevrolet.com.mx/';
    private $ubicacionchevroletmorelia = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d60107.167336352686!2d-101.1993375285939!3d19.68288736541394!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd141da53c58a6a63!2sFAME%20MORELIA%20Chevrolet!5e0!3m2!1ses-419!2smx!4v1597418186511!5m2!1ses-419!2smx';

    private $paginachevroletapatzingan = 'https://www.chevroletapatzingan.com/';
    private $ubicacionchevroletapatzingan = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.805604632871!2d-102.34682728562328!3d19.072282757046725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8431e3e23e5e47cd%3A0x1b669ca26a77ff89!2sChevrolet%20Apatzing%C3%A1n%20FAME!5e0!3m2!1ses-419!2smx!4v1597765857747!5m2!1ses-419!2smx';

    private $paginachevroleturuapan = 'https://www.famecupatitzio.com/';
    private $ubicacionchevroleturuapan = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.711729517128!2d-102.06124418561865!3d19.381630847331987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de297999465d3%3A0x4c329d7b35d04a5b!2sChevrolet%20Uruapan%20FAME!5e0!3m2!1ses-419!2smx!4v1597766167711!5m2!1ses-419!2smx';

    private $paginachevroletzamora = 'https://www.famezamora.com/';
    private $ubicacionchevroletzamora = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3749.6860490152826!2d-102.2910985856095!3d19.97970122813671!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842e88db366d8b4b%3A0xa06a188e013b9d46!2sChevrolet%20Zamora%20FAME!5e0!3m2!1ses-419!2smx!4v1597766216302!5m2!1ses-419!2smx';

    private $paginachevroletlazaro = 'https://www.famelazarocardenas.com/';
    private $ubicacionchevroletlazaro = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1341.655589342754!2d-102.21711811970128!3d17.986443402587977!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xba32aa50c7b731ff!2sChevrolet%20L%C3%A1zaro%20C%C3%A1rdenas%20FAME!5e0!3m2!1ses-419!2smx!4v1597766280640!5m2!1ses-419!2smx';

    /*
    |--------------------------------------------------------------------------
    | DATOS DE AGENCIAS HONDA
    |--------------------------------------------------------------------------
    */

    private $paginahondaaltozano = 'https://www.hondaaltozano.com/';
    private $ubicacionhondaaltozano = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.8933316903067!2d-101.16262358465201!3d19.674555686747322!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d120a88d7dc89%3A0x945ba1aec77d55cf!2sHonda%20Altozano%20FAME!5e0!3m2!1ses-419!2smx!4v1597333938618!5m2!1ses-419!2smx';

    private $paginahondamonarcamorelia = 'https://www.famemonarca.com/';
    private $ubicacionhondamonarcamorelia = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.3840078362205!2d-101.16037428465175!3d19.696268686735085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d0e008a2f2e93%3A0xf1532b368d2b0a3a!2sHonda%20Monarca%20FAME!5e0!3m2!1ses-419!2smx!4v1597334058519!5m2!1ses-419!2smx';

    private $paginahondamonarcadf = 'https://www.famemonarcadf.com/';
    private $ubicacionhondamonarcadf = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470.337719376676!2d-99.14427422509814!3d19.42528812649527!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fed63eaf8461%3A0xf27530b408735dda!2sHonda%20Monarca%20D.F.%20FAME!5e0!3m2!1ses-419!2smx!4v1597334163879!5m2!1ses-419!2smx';

    private $paginahondamanantiales = 'https://www.hondamanantiales.com/';
    private $ubicacionhondamanantiales = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.5145615885276!2d-101.24106348461031!3d19.690705237477903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d0df9f07ad74d%3A0x1899fe2ab368acb4!2sHonda%20Manantiales%20FAME!5e0!3m2!1ses-419!2smx!4v1597334420678!5m2!1ses-419!2smx';

    private $paginahondaatizapan = 'https://www.hondaatizapan.com/';
    private $ubicacionhondaatizapan = 'https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d7519.43631574675!2d-99.26755967333531!3d19.553710107365728!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1shonda%20atizapan!5e0!3m2!1ses-419!2smx!4v1597334564291!5m2!1ses-419!2smx';

    private $paginahondauruapan = 'https://www.hdu.com.mx/';
    private $ubicacionhondauruapan = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3762.982179125575!2d-102.05398268461285!3d19.41317594633083!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de2664c9099c5%3A0xa3a0831f37e2d642!2sHonda%20Uruapan%20FAME!5e0!3m2!1ses-419!2smx!4v1597334928071!5m2!1ses-419!2smx';

    private $paginahondacorregidora = 'https://www.famecorregidora.com/';
    private $ubicacionhondacorregidora = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d479690.0074641297!2d-100.07316284051242!3d20.068103484861762!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x86a2827b4a6e9d46!2sHonda%20Corregidora%20FAME!5e0!3m2!1ses-419!2smx!4v1597335080907!5m2!1ses-419!2smx';

    private $paginahondamarquesa = 'https://www.famemarquesa.com/';
    private $ubicacionhondamarquesa = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3734.2382108840748!2d-100.4098511846017!3d20.619145007010026!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d35aef44ae8a19%3A0x46582a9d1bd0a325!2sHonda%20Marquesa%20FAME!5e0!3m2!1ses-419!2smx!4v1597335146760!5m2!1ses-419!2smx';

    private $paginahondasanjuan = 'https://www.hondasanjuandelrio.com/';
    private $ubicacionhondasanjuan = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3739.913661459732!2d-99.98724378460383!3d20.386449414769086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d30c86988ec97d%3A0x441f8749da965c58!2sHonda%20San%20Juan%20del%20Rio!5e0!3m2!1ses-419!2smx!4v1597335334798!5m2!1ses-419!2smx';

 
    public function contacto()
    {
        $data = array();
        $data['page_title'] = 'Contacto - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/contacto/inicio', $data, TRUE);
        $this->load->view('index', $data);
  
    }

   public function guias()
    {

        $data = array();
        $data['page_title'] = 'Guias';
        $data['main_content'] = $this->load->view('grupo/guias/inicio', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function guiafacturacompra()
    {
        $data = array();
        $data['page_title'] = 'Guias';
        $data['main_content'] = $this->load->view('grupo/guias/facturacompra', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function cotizador()
    {

        $data = array();
        $data['page_title'] = 'Cotizador';
        $data['main_content'] = $this->load->view('grupo/cotizador/inicio', $data, TRUE);
        $this->load->view('index', $data);
    }


    public function cotizarnuevos()
    {
        $data = array();
        $data['page_title'] = 'Cotizador - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/cotizador/nuevos', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function cotizarseminuevos()
    {
        $data = array();
        $data['page_title'] = 'Cotizador - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/cotizador/seminuevos', $data, TRUE);
        $this->load->view('index', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | AGENCIAS POR ESTADO
    |--------------------------------------------------------------------------
    */

    public function agencias()
    {
        $data = array();
        $data['page_title'] = 'Agencias';
        $data['main_content'] = $this->load->view('grupo/agencias/inicio', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function michoacan()
    {
        $data = array();
        $data['page_title'] = 'Agencias Michoacán- Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/michoacan', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function queretaro()
    {
        $data = array();
        $data['page_title'] = 'Agencias Querétaro- Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/queretaro', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function cdmx()
    {
        $data = array();
        $data['page_title'] = 'Agencias CDMX- Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/cdmx', $data, TRUE);
        $this->load->view('index', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | AGENCIAS POR MARCA
    |--------------------------------------------------------------------------
    */

    public function agenciashonda()
    {
        $data = array();
        $data['page_title'] = 'Agencias Honda - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/honda', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciaschevrolet()
    {
        $data = array();
        $data['page_title'] = 'Agencias Chevrolet - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/chevrolet', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciastoyota()
    {
        $data = array();
        $data['page_title'] = 'Agencias Toyota - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/toyota', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciasisuzu()
    {
        $data = array();
        $data['page_title'] = 'Agencias Isuzu - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/isuzu', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciasmitsubishi()
    {
        $data = array();
        $data['page_title'] = 'Agencias Mitsubishi - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/mitsubishi', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciaskia()
    {
        $data = array();
        $data['page_title'] = 'Agencias KIA - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/kia', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciasnissan()
    {
        $data = array();
        $data['page_title'] = 'Agencias Nissan - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/nissan', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciasmotorrad()
    {
        $data = array();
        $data['page_title'] = 'Agencias Motorrad - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/motorrad', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciasvolkswagen()
    {
        $data = array();
        $data['page_title'] = 'Agencias Volkswagen - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/volkswagen', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciaschrysler()
    {
        $data = array();
        $data['page_title'] = 'Agencias Chrysler - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/chrysler', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciasfiat()
    {
        $data = array();
        $data['page_title'] = 'Agencias FIAT - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/fiat', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciasgmc()
    {
        $data = array();
        $data['page_title'] = 'Agencias GMC - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/gmc', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciasbmw()
    {
        $data = array();
        $data['page_title'] = 'Agencias BMW - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/bmw', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciasmini()
    {
        $data = array();
        $data['page_title'] = 'Agencias MINI - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/mini', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function agenciasaudi()
    {
        $data = array();
        $data['page_title'] = 'Agencias AUDI - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/xmarca/audi', $data, TRUE);
        $this->load->view('index', $data);
    }



    /*
    |--------------------------------------------------------------------------
    | AGENCIAS CHEVROLET
    |--------------------------------------------------------------------------
    */

    public function chevroletmorelia()
    {
        $data = array();
        $data['nombre_agencia'] = 'Morelia';
        $data['nombre_marca'] = 'Chevrolet';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciaschevrolet';
        $data['nombre_pagina'] = $this->paginachevroletmorelia;
        $data['ubicacion_agencia'] = $this->ubicacionchevroletmorelia;
        $data['vista_logo'] = 'chevrolet'; //Nombre de la imagen para logo

        $data['page_title'] = 'Chevrolet Morelia - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function chevroletapatzingan()
    {
        $data = array();
        $data['nombre_agencia'] = 'Apatzingán';
        $data['nombre_marca'] = 'Chevrolet';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciaschevrolet';
        $data['nombre_pagina'] = $this->paginachevroletapatzingan;
        $data['ubicacion_agencia'] = $this->ubicacionchevroletapatzingan;
        $data['vista_logo'] = 'chevrolet'; //Nombre de la imagen para logo

        $data['page_title'] = 'Chevrolet Apatzingán - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function chevroleturuapan()
    {
        $data = array();
        $data['nombre_agencia'] = 'Uruapan';
        $data['nombre_marca'] = 'Chevrolet';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciaschevrolet';
        $data['nombre_pagina'] = $this->paginachevroleturuapan;
        $data['ubicacion_agencia'] = $this->ubicacionchevroleturuapan;
        $data['vista_logo'] = 'chevrolet'; //Nombre de la imagen para logo

        $data['page_title'] = 'Chevrolet Uruapan - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function chevroletzamora()
    {
        $data = array();
        $data['nombre_agencia'] = 'Zamora';
        $data['nombre_marca'] = 'Chevrolet';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciaschevrolet';
        $data['nombre_pagina'] = $this->paginachevroletzamora;
        $data['ubicacion_agencia'] = $this->ubicacionchevroletzamora;
        $data['vista_logo'] = 'chevrolet'; //Nombre de la imagen para logo

        $data['page_title'] = 'Chevrolet Zamora - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function chevroletlazaro()
    {
        $data = array();
        $data['nombre_agencia'] = 'Lázaro Cárdenas';
        $data['nombre_marca'] = 'Chevrolet';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciaschevrolet';
        $data['nombre_pagina'] = $this->paginachevroletlazaro;
        $data['ubicacion_agencia'] = $this->ubicacionchevroletlazaro;
        $data['vista_logo'] = 'chevrolet'; //Nombre de la imagen para logo

        $data['page_title'] = 'Chevrolet Lázaro Cárdenas - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | AGENCIAS HONDA
    |--------------------------------------------------------------------------
    */

    public function hondamonarcamorelia()
    {
        $data = array();
        $data['nombre_agencia'] = 'Monarca Morelia';
        $data['nombre_marca'] = 'Honda';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciashonda';
        $data['nombre_pagina'] = $this->paginahondamonarcamorelia;
        $data['ubicacion_agencia'] = $this->ubicacionhondamonarcamorelia;
        $data['vista_logo'] = 'honda'; //Nombre de la imagen para logo

        $data['page_title'] = 'Honda Monarca Morelia - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function hondaaltozano()
    {
        $data = array();
        $data['nombre_agencia'] = 'Altozano Morelia';
        $data['nombre_marca'] = 'Honda';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciashonda';
        $data['nombre_pagina'] = $this->paginahondaaltozano;
        $data['ubicacion_agencia'] = $this->ubicacionhondaaltozano;
        $data['vista_logo'] = 'honda'; //Nombre de la imagen para logo

        $data['page_title'] = 'Honda Altozano - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function hondamanantiales()
    {
        $data = array();
        $data['nombre_agencia'] = 'Manantiales Morelia';
        $data['nombre_marca'] = 'Honda';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciashonda';
        $data['nombre_pagina'] = $this->paginahondamanantiales;
        $data['ubicacion_agencia'] = $this->ubicacionhondamanantiales;
        $data['vista_logo'] = 'honda'; //Nombre de la imagen para logo

        $data['page_title'] = 'Manantiales Morelia - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function hondaatizapan()
    {
        $data = array();
        $data['nombre_agencia'] = 'Altozano Atizapan';
        $data['nombre_marca'] = 'Honda';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciashonda';
        $data['nombre_pagina'] = $this->paginahondaatizapan;
        $data['ubicacion_agencia'] = $this->ubicacionhondaatizapan;
        $data['vista_logo'] = 'honda'; //Nombre de la imagen para logo

        $data['page_title'] = 'Honda Atizapan - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function hondamonarcadf()
    {
        $data = array();
        $data['nombre_agencia'] = 'Altozano Monarca DF';
        $data['nombre_marca'] = 'Honda';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciashonda';
        $data['nombre_pagina'] = $this->paginahondamonarcadf;
        $data['ubicacion_agencia'] = $this->ubicacionhondamonarcadf;
        $data['vista_logo'] = 'honda'; //Nombre de la imagen para logo
    }

    public function hondauruapan()
    {
        $$data = array();
        $data['nombre_agencia'] = 'Uruapan';
        $data['nombre_marca'] = 'Honda';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciashonda';
        $data['nombre_pagina'] = $this->paginahondauruapan;
        $data['ubicacion_agencia'] = $this->ubicacionhondauruapan;
        $data['vista_logo'] = 'honda'; //Nombre de la imagen para logo
    }

    public function hondacorregidora()
    {
        $data = array();
        $data['nombre_agencia'] = 'Corregidora';
        $data['nombre_marca'] = 'Honda';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciashonda';
        $data['nombre_pagina'] = $this->paginahondacorregidora;
        $data['ubicacion_agencia'] = $this->ubicacionhondacorregidora;
        $data['vista_logo'] = 'honda'; //Nombre de la imagen para logo
    }

    public function hondamarquesa()
    {
        $data = array();
        $data['nombre_agencia'] = 'Altozano Marquesa';
        $data['nombre_marca'] = 'Honda';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciashonda';
        $data['nombre_pagina'] = $this->paginahondamarquesa;
        $data['ubicacion_agencia'] = $this->ubicacionhondamarquesa;
        $data['vista_logo'] = 'honda'; //Nombre de la imagen para logo
    }

    public function hondasanjuandelrio()
    {
        $data = array();
        $data['nombre_agencia'] = 'San Juan del Río';
        $data['nombre_marca'] = 'Honda';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciashonda';
        $data['nombre_pagina'] = $this->paginahondasanjuan;
        $data['ubicacion_agencia'] = $this->ubicacionhondasanjuan;
        $data['vista_logo'] = 'honda'; //Nombre de la imagen para logo
    }

    /*
    |--------------------------------------------------------------------------
    | AGENCIAS VOLKSWAGEN
    |--------------------------------------------------------------------------
    */

    public function volkswagen()
    {
        $data = array();
        $data['nombre_agencia'] = 'Morelia';
        $data['nombre_marca'] = 'Volkswagen';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciasvolkswagen';
        $data['nombre_pagina'] = $this->paginavolkswagen;
        $data['ubicacion_agencia'] = $this->ubicacionvolkswagen;
        $data['vista_logo'] = 'volkswagen'; //Nombre de la imagen para logo

        $data['page_title'] = 'Volkswagen Morelia - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | AGENCIAS BMW
    |--------------------------------------------------------------------------
    */

    public function bmwtalisman()
    {
        $data = array();
        $data['nombre_agencia'] = 'Talismán';
        $data['nombre_marca'] = 'BMW';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_agencias'] = 'agenciasbmw';
        $data['nombre_pagina'] = $this->paginabmwtalisman;
        $data['ubicacion_agencia'] = $this->ubicacionbmwtalisman;
        $data['vista_logo'] = 'bmw'; //Nombre de la imagen para logo

        $data['page_title'] = 'BMW Talismán - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | AGENCIAS MINI
    |--------------------------------------------------------------------------
    */

    public function minitalisman()
    {
        $data = array();
        $data['nombre_agencia'] = 'Talismán';
        $data['nombre_marca'] = 'MINI';
        $data['nombre_agencias'] = 'agenciasmini';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_pagina'] = $this->paginaminitalisman;
        $data['ubicacion_agencia'] = $this->ubicacionminitalisman;
        $data['vista_logo'] = 'mini'; //Nombre de la imagen para logo

        $data['page_title'] = 'MINI Talismán - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | AGENCIAS MOTORRAD
    |--------------------------------------------------------------------------
    */

    public function motorradmorelia()
    {
        $data = array();
        $data['nombre_agencia'] = 'Morelia';
        $data['nombre_marca'] = 'Motorrad';
        $data['nombre_agencias'] = 'agenciasmotorrad';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_pagina'] = $this->paginamotorradmorelia;
        $data['ubicacion_agencia'] = $this->ubicacionmotorradmorelia;
        $data['vista_logo'] = 'motorrad'; //Nombre de la imagen para logo

        $data['page_title'] = 'Motorrad Morelia - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | AGENCIAS AUDI
    |--------------------------------------------------------------------------
    */

    public function audijuriquilla()
    {
        $data = array();
        $data['nombre_agencia'] = 'Juriquilla';
        $data['nombre_marca'] = 'AUDI';
        $data['nombre_agencias'] = 'agenciasaudi';
        $data['nombre_grupo'] = 'agencias';
        $data['nombre_pagina'] = $this->paginaaudijuriquilla;
        $data['ubicacion_agencia'] = $this->ubicacionaudijuriquilla;
        $data['vista_logo'] = 'audi'; //Nombre de la imagen para logo

        $data['page_title'] = 'AUDI Juriquilla - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/agencias/gxagencia', $data, TRUE);
        $this->load->view('index', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | MODELOS
    |--------------------------------------------------------------------------
    */

    public function modelos()
    {
        $data = array();
        $data['page_title'] = 'Grupo FAME - Modelos';
        $data['main_content'] = $this->load->view('grupo/modelos/inicio', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function modeloshonda()
    {
        $data = array();
        $data['nombre_marca'] = 'Honda';
        $data['nombre_modelos'] = 'honda';

        $data['page_title'] = 'Modelos Honda - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/modelos/globalmodelos', $data, TRUE);
        $this->load->view('index', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | MODELOS HONDA
    |--------------------------------------------------------------------------
    */

    public function insight()
    {
        $data = array();
        $data['nombre_marca'] = 'Honda';
        $data['nombre_modelos'] = 'honda';
        $data['nombre_modelo'] = 'Insight';
        $data['nombre_imagen_modelo'] = 'insight.png';
        $data['nombre_pdf_modelo'] = 'insight.pdf';
        $data['ano_modelo'] = '2021';
        $data['descripcion_modelo'] = 'Creemos que un auto híbrido no tiene por qué sacrificar el aspecto, al contrario, tiene que lucir elegante y atractivo. Honda Insight es diferente a otros híbridos pues no sólo representa un gran avance para la preservación del ambiente, es también la materialización de un concepto que conjuga buen gusto y funcionalidad.';

        $data['page_title'] = 'Insight - Honda';
        $data['main_content'] = $this->load->view('grupo/modelos/globalxmodelo', $data, TRUE);
        $this->load->view('index', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | MARCAS
    |--------------------------------------------------------------------------
    */

    public function marcas()
    {
        $data = array();
        $data['page_title'] = 'Marcas - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/marcas/inicio', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function honda()
    {
        $data = array();
        $data['page_title'] = 'Honda - Grupo FAME';
        $data['main_content'] = $this->load->view('grupo/marcas/honda/inicio', $data, TRUE);
        $this->load->view('index', $data);
    }


}