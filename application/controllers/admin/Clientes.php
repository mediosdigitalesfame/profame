<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //comprobar la autenticación
        if (!is_admin()) {
            redirect(base_url());
        }
    }

 public function export_pdf2($id) {
        //$this->load->library('f_pdf');
        //$data['sSQL'] = $this->pegawai->export_pdfmodel();
         $data['agencia'] = $this->admin_model->select_option($id, 'agencys');
        $this->load->view('pdf2',$data); //memanggil view 
    }

    public function export_pdf() {
        //$this->load->library('f_pdf');
        //$data['sSQL'] = $this->pegawai->export_pdfmodel();
        $data['sSQL'] = $this->admin_model->select('agencys');
        $this->load->view('pdf1',$data); //memanggil view 
    }

    public function index()
    {
        $data = array();
        $data['page_title'] = 'Clientes';  
        $data['page'] = 'Clientes';
        $data['cliente'] = FALSE;
        $data['clientes'] = $this->admin_model->select('clients');
        $data['empresas'] = $this->admin_model->select_asc('business');
        $data['marcas'] = $this->admin_model->select_asc('brands');
        
        $data['main_content'] = $this->load->view('admin/clientes',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    public function add()
    {	
        if($_POST)
        {   
            $id = $this->input->post('id', true);

            $data=array(
                'name' => $this->input->post('nombre', true),
                'url' => $this->input->post('url', true), 
                'calle' => $this->input->post('calle', true),
                'int' => $this->input->post('int', true),
                'ext' => $this->input->post('ext', true), 
                'colonia' => $this->input->post('colonia', true), 
                'cp' => $this->input->post('cp', true), 
                'ciudad' => $this->input->post('ciudad', true), 
                'estado' => $this->input->post('estado', true), 
                'tel' => $this->input->post('tel', true),
                'business_id' => $this->input->post('empresa', true), 
                'brands_id' => $this->input->post('marca', true),
                'created_at' => my_date_now()
            );
            $data = $this->security->xss_clean($data);

            //si se editará la información de identificación disponible
            if ($id != '') {
                $this->admin_model->edit_option($data, $id, 'agencys');
                $this->session->set_flashdata('msg', 'Agencia editada correctamente'); 
            } else {
                $id = $this->admin_model->insert($data, 'agencys');
                $this->session->set_flashdata('msg', 'Agencia agregada correctamente'); 
            }
            redirect(base_url('admin/agencias'));
        }      
    }

    public function edit($id)
    {  
        $data = array();
        $data['page_title'] = 'Editar';   
        $data['agencia'] = $this->admin_model->select_option($id, 'agencys');
        $data['empresas'] = $this->admin_model->select_asc('business');
        $data['marcas'] = $this->admin_model->select_asc('brands');
        $data['agencias'] = $this->admin_model->select('agencys');
        $data['main_content'] = $this->load->view('admin/agencias',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    
    public function active($id) 
    {
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'agencys');
        $this->session->set_flashdata('msg', 'Agencia activada con éxito'); 
        redirect(base_url('admin/agencias'));
    }

    public function deactive($id) 
    {
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'agencys');
        $this->session->set_flashdata('msg', 'Agencia desactivada con éxito'); 
        redirect(base_url('admin/agencias'));
    }

    public function delete($id)
    {
        $this->admin_model->delete($id,'agencys'); 
        echo json_encode(array('st' => 1));
    }

}
	

