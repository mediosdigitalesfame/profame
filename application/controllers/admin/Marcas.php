<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marcas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //comprobar la autenticación
        if (!is_admin()) {
            redirect(base_url());
        }
    }

 public function export_pdf2($id) {
        //$this->load->library('f_pdf');
        //$data['sSQL'] = $this->pegawai->export_pdfmodel();
         $data['marcas'] = $this->admin_model->select_option($id, 'brands');
        $this->load->view('pdf2',$data); //memanggil view 
    }

    public function export_pdf() {
        //$this->load->library('f_pdf');
        //$data['sSQL'] = $this->pegawai->export_pdfmodel();
        $data['sSQL'] = $this->admin_model->select('brands');
        $this->load->view('pdf1',$data); //memanggil view 
    }

    public function index()
    {
        $data = array();
        $data['page_title'] = 'Grupo Marcas';  
        $data['page'] = 'Grupo';
        $data['marca'] = FALSE;
        $data['marcas'] = $this->admin_model->select('brands');
        $data['main_content'] = $this->load->view('admin/marcas',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    public function add()
    {	
        if($_POST)
        {   
            $id = $this->input->post('id', true);

            $data=array(
                'name' => $this->input->post('name', true)                
            );
            $data = $this->security->xss_clean($data);

            //si se editará la información de identificación disponible
            if ($id != '') {
                $this->admin_model->edit_option($data, $id, 'brands');
                $this->session->set_flashdata('msg', 'Marca editada correctamente'); 
            } else {
                $id = $this->admin_model->insert($data, 'brands');
                $this->session->set_flashdata('msg', 'Marca agregada correctamente'); 
            }
            redirect(base_url('admin/marcas'));
        }      
    }

    public function edit($id)
    {  
        $data = array();
        $data['page_title'] = 'Editar';   
        $data['marca'] = $this->admin_model->select_option($id, 'brands');
        $data['main_content'] = $this->load->view('admin/marcas',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    
    public function active($id) 
    {
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'brands');
        $this->session->set_flashdata('msg', 'Marca activada con éxito'); 
        redirect(base_url('admin/marcas'));
    }

    public function deactive($id) 
    {
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'brands');
        $this->session->set_flashdata('msg', 'Marca desactivada con éxito'); 
        redirect(base_url('admin/marcas'));
    }

    public function delete($id)
    {
        $this->admin_model->delete($id,'brands'); 
        echo json_encode(array('st' => 1));
    }

}
	

