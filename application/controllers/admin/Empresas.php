<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //comprobar la autenticación
        if (!is_admin()) {
            redirect(base_url());
        }
    }

   

    public function pdf(){

$pdf = new FPDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 16);
//inserto la cabecera poniendo una imagen dentro de una celda
$pdf->Cell(700,85,$pdf->Image(base_url().'assets/porto/img/logo.png',30,12,160),0,0,'C');
$pdf->Cell(100,12,"Presupuesto de Empresas: ".  "x");
 
$pdf->Cell(100,12,"Fecha: ". date('d/m/Y'));
$pdf->Line(35,40,190,40);
$pdf->Ln(7);
$pdf->Cell(100,12,"Nombre : ".$empresas->nombre);
$pdf->Cell(90,12,"Nif: "."x");
$pdf->Line(35,48,190,48);
$pdf->Ln(7);
$pdf->Cell(100,12,"Domicilio: ". "x");
$pdf->Line(35,56,190,56);
$pdf->Ln(7);
$pdf->Cell(90,12 );
$pdf->Line(35,62,190,62);
$pdf->Ln(7);
$pdf->Cell(100,12,"Equipo: "."x");
$pdf->Line(35,68,190,68);
$pdf->Ln(9);
$pdf->SetFont('Arial','B',10);

$pdf->Cell(60,12,'PRESUPUESTO');
$pdf->Ln(2);
$pdf->Output('hola_mudo.pdf', 'I');
}



 public function export_pdf2($id) {
        //$this->load->library('f_pdf');
        //$data['sSQL'] = $this->pegawai->export_pdfmodel();
         $data['empresa'] = $this->admin_model->select_option($id, 'business');
        $this->load->view('pdf2',$data); //memanggil view 
    }

    public function export_pdf() {
        //$this->load->library('f_pdf');
        //$data['sSQL'] = $this->pegawai->export_pdfmodel();
        $data['sSQL'] = $this->admin_model->select('business');
        $this->load->view('pdf1',$data); //memanggil view 
    }

    public function index()
    {
        $data = array();
        $data['page_title'] = 'Grupo Empresas';  
        $data['page'] = 'Grupo';
        $data['empresa'] = FALSE;
        $data['empresas'] = $this->admin_model->select('business');
        $data['ciudades'] = $this->admin_model->select('ciudades');
        $data['main_content'] = $this->load->view('admin/empresas',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

     public function depende()
    {
        $data = array();
        $data['page_title'] = 'Grupo Empresas';  
        $data['page'] = 'Grupo';
        $data['empresa'] = FALSE;
        $data['empresas'] = $this->admin_model->select('business');
        $data['ciudades'] = $this->admin_model->select('ciudades');
        $data['main_content'] = $this->load->view('dynamic_dependent',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    public function add()
    {	
        if($_POST)
        {   
            $id = $this->input->post('id', true);

            $data=array(
                'nombre' => $this->input->post('nombre', true),
                'rfc' => $this->input->post('rfc', true), 
                'rsocial' => $this->input->post('rsocial', true),
                'tel' => $this->input->post('telefono', true),
                'calle' => $this->input->post('calle', true), 
                'int' => $this->input->post('int', true), 
                'ext' => $this->input->post('ext', true), 
                'colonia' => $this->input->post('colonia', true), 
                'cp' => $this->input->post('cp', true), 
                'ciudad' => $this->input->post('ciudad', true), 
                'estado' => $this->input->post('estado', true), 
                'bank' => $this->input->post('banco', true),
                'account' => $this->input->post('cuenta', true),
                'pay' => $this->input->post('pago', true), 
                'created_at' => my_date_now()
            );
            $data = $this->security->xss_clean($data);

            //si se editará la información de identificación disponible
            if ($id != '') {
                $this->admin_model->edit_option($data, $id, 'business');
                $this->session->set_flashdata('msg', 'Empresa editada correctamente'); 
            } else {
                $id = $this->admin_model->insert($data, 'business');
                $this->session->set_flashdata('msg', 'Empresa agregada correctamente'); 
            }
            redirect(base_url('admin/empresas'));
        }      
    }

    public function edit($id)
    {  
        $data = array();
        $data['page_title'] = 'Editar';   
        $data['empresa'] = $this->admin_model->select_option($id, 'business');
        $data['main_content'] = $this->load->view('admin/empresas',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    
    public function active($id) 
    {
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'business');
        $this->session->set_flashdata('msg', 'Empresa activada con éxito'); 
        redirect(base_url('admin/empresas'));
    }

    public function deactive($id) 
    {
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'business');
        $this->session->set_flashdata('msg', 'Empresa desactivada con éxito'); 
        redirect(base_url('admin/empresas'));
    }

    public function delete($id)
    {
        $this->admin_model->delete($id,'business'); 
        echo json_encode(array('st' => 1));
    }

}
	

