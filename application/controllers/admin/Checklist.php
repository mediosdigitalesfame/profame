<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checklist extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //comprobar la autenticación
        if (!is_admin()) {
            redirect(base_url());
        }
    }

    public function export_pdf_checklist($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/tomaseminuevo',$data); //memanggil view 
    }

    public function index()
    {
        $data = array();
        $data['page_title'] = 'Checklist Tomar Seminuevos';  
        $data['page'] = 'Checklist Seminuevos';
        $data['toma'] = FALSE;
        $data['tomas'] = $this->admin_model->select_option_checklistos('checklist');
        $data['tomag'] = $this->admin_model->select_option_tomas_disponibles_checklist('seminuevostomados');
        //data['checklist'] = $this->admin_model->get_checklist('checklist');
        $data['main_content'] = $this->load->view('admin/checklist',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    // get sub category by category_id
    function get_datoscliente(){
        $cliente_id = $this->input->post('id',TRUE);
        $data = $this->admin_model->get_datacli($cliente_id)->result();
        echo json_encode($data);
    }

    public function add()
    {	
        if($_POST)
        {   
            if(!empty($this->input->post('solicitudcfdi'))){$solicitudcfdi = $this->input->post('solicitudcfdi', true);}
            else{$solicitudcfdi = 0;}

            if(!empty($this->input->post('cambioderol'))){$cambioderol = $this->input->post('cambioderol', true);}
            else{$cambioderol = 0;}

            if(!empty($this->input->post('cedulafiscal'))){$cedulafiscal = $this->input->post('cedulafiscal', true);}
            else{$cedulafiscal = 0;}

            if(!empty($this->input->post('cfdisinactividad'))){$cfdisinactividad = $this->input->post('cfdisinactividad', true);}
            else{$cfdisinactividad = 0;}

            if(!empty($this->input->post('xml'))){$xml = $this->input->post('xml', true);}
            else{$xml = 0;}

            if(!empty($this->input->post('rfc'))){$rfc = $this->input->post('rfc', true);}
            else{$rfc = 0;}

            if(!empty($this->input->post('validacioncfdi'))){$validacioncfdi = $this->input->post('validacioncfdi', true);}
            else{$validacioncfdi = 0;}

            if(!empty($this->input->post('verificacioncfdi'))){$verificacioncfdi = $this->input->post('verificacioncfdi', true);}
            else{$verificacioncfdi = 0;}

            if(!empty($this->input->post('autenticaciondoctos'))){$autenticaciondoctos = $this->input->post('autenticaciondoctos', true);}
            else{$autenticaciondoctos = 0;}

            if(!empty($this->input->post('reportetransunion'))){$reportetransunion = $this->input->post('reportetransunion', true);}
            else{$reportetransunion = 0;}

            if(!empty($this->input->post('noretencion'))){$noretencion = $this->input->post('noretencion', true);}
            else{$noretencion = 0;}

            if(!empty($this->input->post('secuenciafacturas'))){$secuenciafacturas = $this->input->post('secuenciafacturas', true);}
            else{$secuenciafacturas = 0;}

            if(!empty($this->input->post('identificacionoficial'))){$identificacionoficial = $this->input->post('identificacionoficial', true);}
            else{$identificacionoficial = 0;}

            if(!empty($this->input->post('curp'))){$curp = $this->input->post('curp', true);}
            else{$curp = 0;}

            if(!empty($this->input->post('comprobante'))){$comprobante = $this->input->post('comprobante', true);}
            else{$comprobante = 0;}

            if(!empty($this->input->post('bajaplacas'))){$bajaplacas = $this->input->post('bajaplacas', true);}
            else{$bajaplacas = 0;}

            if(!empty($this->input->post('tenencias'))){$tenencias = $this->input->post('tenencias', true);}
            else{$tenencias = 0;}

            if(!empty($this->input->post('tarjetacirculacion'))){$tarjetacirculacion = $this->input->post('tarjetacirculacion', true);}
            else{$tarjetacirculacion = 0;}

            if(!empty($this->input->post('responsivacompraventa'))){$responsivacompraventa = $this->input->post('responsivacompraventa', true);}
            else{$responsivacompraventa = 0;}

            if(!empty($this->input->post('contratocompraventa'))){$contratocompraventa = $this->input->post('contratocompraventa', true);}
            else{$contratocompraventa = 0;}

            if(!empty($this->input->post('avisoprivacidad'))){$avisoprivacidad = $this->input->post('avisoprivacidad', true);}
            else{$avisoprivacidad = 0;}

            if(!empty($this->input->post('checklistrevision'))){$checklistrevision = $this->input->post('checklistrevision', true);}
            else{$checklistrevision = 0;}

            if(!empty($this->input->post('evaluacionmecanica'))){$evaluacionmecanica = $this->input->post('evaluacionmecanica', true);}
            else{$evaluacionmecanica = 0;}

            if(!empty($this->input->post('guiaautometrica'))){$guiaautometrica = $this->input->post('guiaautometrica', true);}
            else{$guiaautometrica = 0;}

            if(!empty($this->input->post('polizaderecepcion'))){$polizaderecepcion = $this->input->post('polizaderecepcion', true);}
            else{$polizaderecepcion = 0;}

            if(!empty($this->input->post('cuentasporpagar'))){$cuentasporpagar = $this->input->post('cuentasporpagar', true);}
            else{$cuentasporpagar = 0;}

            if(!empty($this->input->post('formatodedacion'))){$formatodedacion = $this->input->post('formatodedacion', true);}
            else{$formatodedacion = 0;}

            if(!empty($this->input->post('reportederobo'))){$reportederobo = $this->input->post('reportederobo', true);}
            else{$reportederobo = 0;}

            if(!empty($this->input->post('chequedeproveedor'))){$chequedeproveedor = $this->input->post('chequedeproveedor', true);}
            else{$chequedeproveedor = 0;}

            if(!empty($this->input->post('solicitudcheque'))){$solicitudcheque = $this->input->post('solicitudcheque', true);}
            else{$solicitudcheque = 0;}

            if(!empty($this->input->post('edocuenta'))){$edocuenta = $this->input->post('edocuenta', true);}
            else{$edocuenta = 0;}

            if(!empty($this->input->post('calculoretencion'))){$calculoretencion = $this->input->post('calculoretencion', true);}
            else{$calculoretencion = 0;}

            if(!empty($this->input->post('autorizaciontoma'))){$autorizaciontoma = $this->input->post('autorizaciontoma', true);}
            else{$autorizaciontoma = 0;}

            $id = $this->input->post('id', true);
            $id2 = $this->input->post('id', true);
            $idg = $this->input->post('tomasel', true);

            $data=array(
                'solicitudcfdi' => $solicitudcfdi,
                'cambioderol' => $cambioderol,
                'cedulafiscal' => $cedulafiscal,
                'cfdisinactividad' => $cfdisinactividad,
                'xml' => $xml,
                'rfc' => $rfc,
                'validacioncfdi' => $validacioncfdi,
                'verificacioncfdi' => $verificacioncfdi,
                'autenticaciondoctos' => $autenticaciondoctos,
                'reportetransunion' => $reportetransunion,
                'noretencion' => $noretencion,
                'secuenciafacturas' => $secuenciafacturas,
                'identificacionoficial' => $identificacionoficial,
                'curp' => $curp,
                'comprobante' => $comprobante,
                'bajaplacas' => $bajaplacas,
                'tenencias' => $tenencias,
                'tarjetacirculacion' => $tarjetacirculacion,
                'responsivacompraventa' => $responsivacompraventa,
                'contratocompraventa' => $contratocompraventa,
                'avisoprivacidad' => $avisoprivacidad,
                'checklistrevision' => $checklistrevision,
                'evaluacionmecanica' => $evaluacionmecanica,
                'guiaautometrica' => $guiaautometrica,
                'polizaderecepcion' => $polizaderecepcion,
                'cuentasporpagar' => $cuentasporpagar,
                'formatodedacion' => $formatodedacion,
                'reportederobo' => $reportederobo,
                'chequedeproveedor' => $chequedeproveedor,
                'solicitudcheque' => $solicitudcheque,
                'edocuenta' => $edocuenta,
                'calculoretencion' => $calculoretencion,
                'autorizaciontoma' => $autorizaciontoma,
                'status' => $this->input->post('status', true),
                //'statuscheck' => 0,
                'seminuevostomados_id' => $this->input->post('tomasel', true),
                'created_at' => my_date_now()
            );

            $datag=array(                
                'statuscheck' => $this->input->post('status', true),
            );

            $datac=array(                
                'comentarios' => $this->input->post('comentarios', true),
                'checklist_id' => $this->input->post('id', true),
                'users_id' => $this->session->userdata('id'),
                'created_at' => my_date_now()
            );
            
            $data = $this->security->xss_clean($data); 
            $datac = $this->security->xss_clean($datac); 

            //si se editará la información de identificación disponible
            if ($id != '') {
                $this->admin_model->edit_option($data, $id, 'checklist');
                $this->admin_model->update($datag, $idg, 'seminuevostomados');
                $id2=$this->admin_model->insert($datac, 'comentarios_has_checklist');
                $this->session->set_flashdata('msg', 'Nueva Toma de Seminuevos editada correctamente'); 
            } else {
                $id = $this->admin_model->insert($data, 'checklist');
                $this->admin_model->update($datag, $idg, 'seminuevostomados');
                $this->session->set_flashdata('msg', 'Nueva Toma de Seminuevos agregada correctamente'); 
            }
            redirect(base_url('admin/checklist'));
        }      
    }

    public function edit($id)
    {  

        $data = array();
        $data['page_title'] = 'Editar'; 
        $data['page'] = 'Editar';  
        $data['toma'] = $this->admin_model->select_option_checklist2($id,'checklist');
        $data['comentarios'] = $this->admin_model->select_comentarios('checklist');
        //$data['tomas'] = $this->admin_model->select_option_checklistos('checklist');
        
        $data['tomag'] = $this->admin_model->select_option_tomas_disponibles_checklist('seminuevostomados');
        $data['tomag2'] = $this->admin_model->select_option_tomas_disponibles_checklist2('seminuevostomados');
        $data['main_content'] = $this->load->view('admin/checklist',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    public function active($id) 
    {
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'agencys');
        $this->session->set_flashdata('msg', 'Agencia activada con éxito'); 
        redirect(base_url('admin/agencias'));
    }

    public function deactive($id) 
    {
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'agencys');
        $this->session->set_flashdata('msg', 'Agencia desactivada con éxito'); 
        redirect(base_url('admin/agencias'));
    }

     public function activemess($id) 
    {
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'comentarios_has_checklist');
        $this->session->set_flashdata('msg', 'Comentario Marcado como ledido con éxito'); 
        redirect(base_url('admin/checklist/'));
    }

    public function deactivemess($id) 
    {
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'comentarios_has_checklist');
        $this->session->set_flashdata('msg', 'Comentario Marcado como NO ledido con éxito'); 
        redirect(base_url('admin/checklist/'));
    }

    public function delete($id)
    {
        $this->admin_model->delete($id,'agencys'); 
        echo json_encode(array('st' => 1));
    }

}


