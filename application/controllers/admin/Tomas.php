<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tomas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Product_model','product_model');
        //comprobar la autenticación
        if (!is_admin()) {
            redirect(base_url());
        }
    }

    public function export_pdf_toma($id) {
        //$this->load->library('f_pdf');
        //$data['sSQL'] = $this->pegawai->export_pdfmodel();

       $data['toma'] = $this->admin_model->select_option_toma($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/tomaseminuevo',$data); //memanggil view 
    }

    public function export_pdf_solicitudcfdi($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/solicitudcfdi',$data); //memanggil view 
    }

    public function export_pdf_autentificaciondoctos($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/autentificaciondoctos',$data); //memanggil view 
    }
    
    public function export_pdf_cartanoretencion($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/cartanoretencion',$data); //memanggil view 
    }

    public function export_pdf_cartaresponsiva($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/cartaresponsiva',$data); //memanggil view 
    }

    public function export_pdf_contratocompraventa($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/contratocompraventa',$data); //memanggil view 
    }

    public function export_pdf_avisodeprivacidad($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/avisodeprivacidad',$data); //memanggil view 
    }

    public function export_pdf_dacionenpago($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/dacionenpago',$data); //memanggil view 
    }

    public function export_pdf_solicituddecheque($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/solicituddecheque',$data); //memanggil view 
    }

    public function export_pdf_calculoderetencion($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/calculoderetencion',$data); //memanggil view 
    }

    public function export_pdf_reglamento($id) {
       $data['toma'] = $this->admin_model->select_option_checklist($id, 'seminuevostomados');
        $this->load->view('admin/pdfs/reglamento',$data); //memanggil view 
    }

    public function index()
    {
        $data = array();
        $data['page_title'] = 'Tomar Seminuevos';  
        $data['page'] = 'Seminuevos';
        $data['toma'] = FALSE;
        $data['tomas'] = $this->admin_model->select_option_tomas('seminuevostomados');
        $data['agencias'] = $this->admin_model->select_asc('agencys');
        $data['guias'] = $this->admin_model->select_guias_tomas_activas('guias');
        $data['main_content'] = $this->load->view('admin/tomas',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    // get sub category by category_id
    function get_datoscliente(){
        $cliente_id = $this->input->post('id',TRUE);
        $data = $this->admin_model->get_datacli($cliente_id)->result();
        echo json_encode($data);
    }

    public function add()
    {	
        if($_POST)
        {   
            $id = $this->input->post('id', true);
            $idg = $this->input->post('guia', true);
            $guiaactual = $this->input->post('guiaactual', true);

            $data=array(

                'nombre' => $this->input->post('nombre', true),
                'fechacontrato' => $this->input->post('fechacontrato', true),
                'calle' => $this->input->post('calle', true),
                'numext' => $this->input->post('numext', true),
                'numint' => $this->input->post('numint', true),
                'colonia' => $this->input->post('colonia', true),
                'ciudad' => $this->input->post('ciudad', true),
                'municipio' => $this->input->post('municipio', true),
                'estado' => $this->input->post('estado', true),
                'cp' => $this->input->post('cp', true),
                'rfc' => $this->input->post('rfc', true),
                'nidenti' => $this->input->post('nidenti', true),
                'tipoidenti' => $this->input->post('tipoidenti', true),
                'mesrecibo' => $this->input->post('mesrecibo', true),
                'fechaidenti' => $this->input->post('fechaidenti', true),
                'facturavehi' => $this->input->post('facturavehi', true),
                'expedida' => $this->input->post('expedida', true),
                'fechafac' => $this->input->post('fechafac', true),
                'marcavehi' => $this->input->post('marcavehi', true),
                'modelovehi' => $this->input->post('modelovehi', true),
                'aniomodelo' => $this->input->post('aniomodelo', true),
                'versionvehi' => $this->input->post('versionvehi', true),
                'colorext' => $this->input->post('colorext', true),
                'colorint' => $this->input->post('colorint', true),
                'clavevehi' => $this->input->post('clavevehi', true),
                'nserie' => $this->input->post('nserie', true),
                'nmotor' => $this->input->post('nmotor', true),
                'nbaja' => $this->input->post('nbaja', true),
                'edoemisor' => $this->input->post('edoemisor', true),
                'fechabaja' => $this->input->post('fechabaja', true),
                'tenencias' => $this->input->post('tenencias', true),
                'placasbaja' => $this->input->post('placasbaja', true),
                'nverificacion' => $this->input->post('nverificacion', true),
                //'valorcompra' => $this->input->post('valorcompra', true),
                //'valorletra' => $this->input->post('valorletra', true),
                //'valorautometrica' => $this->input->post('valorautometrica', true),
                'aquiensevende' => $this->input->post('aquiensevende', true),
                'fechafacfinal' => $this->input->post('fechafacfinal', true),
                'fechatoma' => $this->input->post('fechatoma', true),
                'costoadquisicion' => $this->input->post('costoadquisicion', true),
                'nombrescfdi' => $this->input->post('nombrescfdi', true),
                'apellido1' => $this->input->post('apellido1', true),
                'apellido2' => $this->input->post('apellido2', true),
                'email' => $this->input->post('email', true),
                'tel' => $this->input->post('tel', true),
                'cel' => $this->input->post('cel', true),
                'fechanaci' => $this->input->post('fechanaci', true),
                'entidadnaci' => $this->input->post('entidadnaci', true),
                'actividad' => $this->input->post('actividad', true),
                'curp' => $this->input->post('curp', true),
                'pais' => $this->input->post('pais', true),
                'banco' => $this->input->post('banco', true),
                'cuenta' => $this->input->post('cuenta', true),
                'clabe' => $this->input->post('clabe', true),
                'sucursal' => $this->input->post('sucursal', true),
                'convenio' => $this->input->post('convenio', true),
                'referencia' => $this->input->post('referencia', true),
                'foliocfdi' => $this->input->post('foliocfdi', true),
                'created_at' => my_date_now(),
                 
                'guias_id' => $this->input->post('guia', true),
                'agencys_id' => $this->input->post('guia', true),
                'users_id' => $this->session->userdata('id')
            );

$datag=array(
    'statustoma' => 1
);

$datag2=array(
    'statustoma' => 0
);

$data = $this->security->xss_clean($data);

            //si se editará la información de identificación disponible
if ($id != '') {
    $this->admin_model->edit_option($data, $id, 'seminuevostomados');
    //if ($guiaactual != $idg) {
        //$this->admin_model->update($datag2, $guiaactual, 'guias');
        //$this->admin_model->update($datag, $idg, 'guias'); }
    $this->session->set_flashdata('msg', 'Nueva Toma de Seminuevos editada correctamente'); 
} else {
    $id = $this->admin_model->insert($data, 'seminuevostomados');
    $this->admin_model->update($datag, $idg, 'guias');
    $this->session->set_flashdata('msg', 'Nueva Toma de Seminuevos agregada correctamente'); 
}
redirect(base_url('admin/tomas'));
}      
}

public function edit($id)
{  
    $data = array();
    $data['page_title'] = 'Editar';  
    $data['page'] = 'Editar';   
    $data['toma'] = $this->admin_model->select_option_toma($id, 'seminuevostomados');
    $data['clientes'] = $this->admin_model->get_data_clients('leads');
    $data['guias'] = $this->admin_model->select_guias_tomas_plus('guias');
    $data['main_content'] = $this->load->view('admin/tomas',$data,TRUE);
    $this->load->view('admin/index',$data);
}

public function active($id) 
{
    $data = array(
        'status' => 1
    );
    $data = $this->security->xss_clean($data);
    $this->admin_model->update($data, $id,'seminuevostomados');
    $this->session->set_flashdata('msg', 'Agencia activada con éxito'); 
    redirect(base_url('admin/tomas'));
}

public function deactive($id) 
{
    $data = array(
        'status' => 0
    );
    $data = $this->security->xss_clean($data);
    $this->admin_model->update($data, $id,'seminuevostomados');
    $this->session->set_flashdata('msg', 'Agencia desactivada con éxito'); 
    redirect(base_url('admin/tomas'));
}

public function delete($id)
{
    $this->admin_model->delete($id,'seminuevostomados'); 
    echo json_encode(array('st' => 1));
}

}


