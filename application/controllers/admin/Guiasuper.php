<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Guias extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Product_model','product_model');
        
        //comprobar la autenticación
        if (!is_admin() && !is_superadmin()) {    
            redirect(base_url());
        }
    }

    public function export_pdf_guia($id) {
        $data['toma'] = $this->admin_model->select_option_guia($id, 'guias');
        $this->load->view('admin/pdfs/guiarapida',$data); 
    }

    public function index()
    {
        $data = array();
        $data['user'] = $this->admin_model->get_user_info();
        $data['page_title'] = 'Guias Rapidas';  
        $data['page'] = 'Agregar Guia';
        $data['guia'] = FALSE;
        $data['guias'] = $this->admin_model->select_option_guias('guias');
        $data['marcas'] = $this->admin_model->select('brands');
        $data['anios'] = $this->admin_model->select('anios');
        $data['modelos'] = $this->admin_model->select('models');
        $data['utilidades'] = $this->admin_model->select_asc('utilidades');
        $data['tipoguias'] = $this->admin_model->select_asc('tipoguias');
        $data['agencias'] = $this->admin_model->select_asc('agencys');
        $data['main_content'] = $this->load->view('admin/guias',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    public function add()
    {	
        if($_POST)
        {   
            $id = $this->input->post('id', true);

            $data=array(

                'valor' => $this->input->post('valor', true),
                'reacondicionamiento' => $this->input->post('reacondicionamiento', true),
                'premio' => $this->input->post('premio', true),
                'equipamiento' => $this->input->post('equipamiento', true),
                'compraautometrica' => $this->input->post('compraautometrica', true),
                'ventaautometrica' => $this->input->post('ventaautometrica', true),
                'km' => $this->input->post('km', true),
                'folio' => $this->input->post('folio', true),

                'pdvapsr1' => $this->input->post('pdvapsr1a', true),
                'pdlttfac2' => $this->input->post('pdlttfac2a', true),
                'crpdltsi3' => $this->input->post('crpdltsi3a', true),
                'pdvapsi4' => $this->input->post('pdvapsi4a', true),
                'pdvapciyr5' => $this->input->post('pdvapciyr5a', true),
                'udlv6' => $this->input->post('udlv6a', true),
                'pdudlv7' => $this->input->post('pdudlv7', true),
                'pmdvap8' => $this->input->post('pmdvap8a', true),
                'cccr9' => $this->input->post('cccr9a', true),
                'pdvap10' => $this->input->post('pdvap10a', true),
                'ucidlu11' => $this->input->post('ucidlu11a', true),
                'idlu12' => $this->input->post('idlu12a', true),
                'fdume13' => $this->input->post('fdume13a', true),

                'models_id' => $this->input->post('modelo', true),
                'users_id' => $this->session->userdata('id'),
                'tipoguias_id' => $this->input->post('tipoguia', true),
                'created_at' => my_date_now()
            );
            $data = $this->security->xss_clean($data);

            //si se editará la información de identificación disponible
            if ($id != '') {
                $this->admin_model->edit_option($data, $id, 'guias');
                $this->session->set_flashdata('msg', 'Nueva Guia para Toma de Seminuevos editada correctamente'); 
            } else {
                $id = $this->admin_model->insert($data, 'guias');
                $this->session->set_flashdata('msg', 'Nueva Guia para Toma de Seminuevos agregada correctamente'); 
            }

            // insert photos
            if($_FILES['photo']['name'] != ''){
                $up_load = $this->admin_model->upload_image('600');
                $data_img = array(
                    'image' => $up_load['images'],
                    'thumb' => $up_load['thumb']
                );
                $this->admin_model->edit_option($data_img, $id, 'guias');   
            }

            redirect(base_url('admin/guias'));
        }      
    }

    public function edit($id)
    {  
        $data = array();
        $data['page_title'] = 'Editar';  
        $data['page'] = 'Editar Guia';

        $data['user'] = $this->admin_model->get_user_info();
        $data['guia'] = $this->admin_model->select_option_guia($id, 'guias');
        $data['marcas'] = $this->admin_model->select('brands');
        $data['anios'] = $this->admin_model->select('anios');
        $data['modelos'] = $this->admin_model->select('models');
        $data['utilidades'] = $this->admin_model->select_asc('utilidades');
        $data['tipoguias'] = $this->admin_model->select_asc('tipoguias');
        $data['agencias'] = $this->admin_model->select_asc('agencys');
        $data['main_content'] = $this->load->view('admin/guias',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    public function ver($id)
    {  
        $data = array();
        $data['page_title'] = 'Ver';  
        $data['page'] = 'Ver Guia';

        $data['user'] = $this->admin_model->get_user_info();
        $data['guia'] = $this->admin_model->select_option_guia($id, 'guias');
        $data['marcas'] = $this->admin_model->select('brands');
        $data['anios'] = $this->admin_model->select('anios');
        $data['modelos'] = $this->admin_model->select('models');
        $data['utilidades'] = $this->admin_model->select_asc('utilidades');
        $data['tipoguias'] = $this->admin_model->select_asc('tipoguias');
        $data['agencias'] = $this->admin_model->select_asc('agencys');
        $data['main_content'] = $this->load->view('admin/guias',$data,TRUE);
        $this->load->view('admin/index',$data);
    }

    public function active($id) 
    {
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'guias');
        $this->session->set_flashdata('msg', 'Guia activada con éxito'); 
        redirect(base_url('admin/guias'));
    }

    public function deactive($id) 
    {
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->admin_model->update($data, $id,'guias');
        $this->session->set_flashdata('msg', 'Guia desactivada con éxito'); 
        redirect(base_url('admin/guias'));
    }

    public function delete($id)
    {
        $this->admin_model->delete($id,'guias'); 
        echo json_encode(array('st' => 1));
    }

}


