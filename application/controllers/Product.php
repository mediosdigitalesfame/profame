<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Product_model','product_model');
		 
	}

	//Obtener las categorias
	function get_sub_category(){
		$category_id = $this->input->post('id',TRUE);
		$data = $this->product_model->get_sub_category($category_id)->result();
		echo json_encode($data);
	}

	// obtener los modelos
	function get_modelos(){
		$marca_id = $this->input->post('id',TRUE);
		$data = $this->product_model->get_modelos($marca_id)->result();
		echo json_encode($data);
	}

	// obtener los modelos
	function get_modelos2(){
		$marca_id = $this->input->post('id',TRUE);
		$anio_id = $this->input->post('id2',TRUE);
		$data = $this->product_model->get_modelos2($marca_id,$anio_id)->result();
		echo json_encode($data);
	}

	// obtener los modelos
	function get_modelos3(){
		$marca_id = $this->input->post('idmarca',TRUE);
		$anio_id = $this->input->post('idanio',TRUE);
		$data = $this->product_model->get_modelos2($marca_id,$anio_id)->result();
		echo json_encode($data);
	}

	// obtener años de los modelos
	function get_anios(){
		$marca_id = $this->input->post('id',TRUE);
		$data = $this->product_model->get_anios($marca_id)->result();
		echo json_encode($data);
	}

	 
function get_guia_edit(){
        $guia_id = $this->input->post('id',TRUE);
        $data = $this->product_model->get_guia_by_id($guia_id)->result();
        echo json_encode($data);
}
	 
 

	 
}