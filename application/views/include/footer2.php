<!-- Footer-Area -->
<footer id="footer">
            <div class="container">

                <div class="row py-5 my-4">
                    <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                        <h5 class="text-3 mb-3">NOSOTROS</h5>
                        <p class="pr-1"><?php echo html_escape($settings->footer_about) ?></p>
                    </div>
                    
                    <div class="col-md-6 col-lg-3 mb-4 mb-md-0">
                        <div class="contact-details">
                            <h5 class="text-3 mb-3">CONTACTANOS</h5>
                            <ul class="list list-icons list-icons-lg">
                                <li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i><p class="m-0">234 Street Name, City Name</p></li>
                                <li class="mb-1"><i class="fab fa-whatsapp text-color-primary"></i><p class="m-0"><a href="tel:8001234567">(800) 123-4567</a></p></li>
                                <li class="mb-1"><i class="far fa-envelope text-color-primary"></i><p class="m-0"><a href="mailto:mail@example.com">mail@example.com</a></p></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <h5 class="text-3 mb-3">SIGUENOS EN</h5>
                        <ul class="social-icons">
                            <?php if (!empty($settings->facebook)) : ?>
                            <li class="social-icons-facebook"><a href="<?php echo html_escape($settings->facebook) ?>" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                             <?php endif; ?>
                             <!--if twitter url exists-->
                            <?php if (!empty($settings->twitter)) : ?>
                            <li class="social-icons-twitter"><a href="<?php echo html_escape($settings->twitter) ?>" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                            <?php endif; ?>
                             <!--if linkedin url exists-->
                            <?php if (!empty($settings->linkedin)) : ?>
                            <li class="social-icons-linkedin"><a href="<?php echo html_escape($settings->linkedin) ?>" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            <?php endif; ?>
                             <!--if instagram url exists-->
                            <?php if (!empty($settings->instagram)) : ?>
                            <li class="social-icons-instagram"><a href="<?php echo html_escape($settings->instagram) ?>" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
                            <?php endif; ?>
                             
                        </ul>
                        
                    </div>

                    <div class="col-md-6 col-lg-3 mb-4 mb-md-0">
                        <div class="contact-details">
                            <h5 class="text-3 mb-3">Paginas</h5>
                            <ul class="list list-icons list-icons-lg">
                                 <?php $pages = get_pages(); ?>
                            <?php foreach ($pages as $page): ?>
                                <li class="mb-1"><a href="<?php echo base_url('page/'.html_escape($page->slug)) ?>" title=""><i class="far fa-dot-circle text-color-primary"></i><p class="m-0"><?php echo html_escape($page->title); ?></a></p></a></li>
                                 <?php endforeach ?>
                            </ul>
                        </div>
                    </div>


                </div>
            </div>
            <div class="footer-copyright">
                <div class="container py-2">
                    <div class="row py-4">
                        <div class="col-lg-6 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0">
                            <a href="<?php echo base_url(); ?>auth" class="logo pr-0 pr-lg-3">
                                <img src="<?php echo base_url($settings->logo) ?>" alt="Footer Logo" class="opacity-5" height="33">
                            </a><?php echo html_escape($settings->copyright) ?>
                              
                        </div>
                         
                        <div class="col-lg-6 d-flex align-items-center justify-content-center justify-content-lg-end">
                            <nav id="sub-menu">
                                <ul>
                                    <li><i class="fas fa-angle-right"></i><a href="page-faq.html" class="ml-1 text-decoration-none"> Aviso de Privasidad</a></li>
                                    <li><i class="fas fa-angle-right"></i><a href="page-faq.html" class="ml-1 text-decoration-none"> Terminos y Condiciones</a></li>
                                    <li><i class="fas fa-angle-right"></i><a href="page-faq.html" class="ml-1 text-decoration-none"> Formato ARCO</a></li>
                                     <li><a href="<?php echo base_url('pricing') ?>">Pricing</a></li>
                                    <!-- 
                                    <li><i class="fas fa-angle-right"></i><a href="page-faq.html" class="ml-1 text-decoration-none"> FAQ's</a></li>
                                    <li><i class="fas fa-angle-right"></i><a href="sitemap.html" class="ml-1 text-decoration-none"> Sitemap</a></li> 
                                    <li><i class="fas fa-angle-right"></i><a href="contact-us.html" class="ml-1 text-decoration-none"> Contactanos</a></li> 
                                -->
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
 
    <!-- /Footer-Area -->



<!-- Vendor -->
<script src="<?php echo base_url(); ?>assets/porto/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/jquery.cookie/jquery.cookie.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/popper/umd/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/common/common.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/jquery.validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/isotope/jquery.isotope.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/vide/jquery.vide.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/vivus/vivus.min.js"></script>
<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url(); ?>assets/porto/js/theme.js"></script>
<!-- Current Page Vendor and Views -->
<!-- <script src="<?php echo base_url(); ?>assets/js/examples/examples.forms.js"></script> -->
<script src="<?php echo base_url(); ?>assets/porto/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url(); ?>assets/porto/js/views/view.contact.js"></script> <!-- JS de contactos-->

<!-- Theme Custom -->
<script src="<?php echo base_url(); ?>assets/porto/js/custom.js"></script>
<!-- Theme Initialization Files -->
<script src="<?php echo base_url(); ?>assets/porto/js/theme.init.js"></script>
<!-- Examples -->
<script src="<?php echo base_url(); ?>assets/porto/js/examples/examples.gallery.js"></script> 


 <script type="text/javascript">
    $( document ).ready(function(){
        $('.alert-default').delay(4000).slideUp();
    });
</script>



    
    <?php $success = $this->session->flashdata('msg'); ?>
    <?php $error = $this->session->flashdata('error'); ?>
    <input type="hidden" id="success" value="<?php echo html_escape($success); ?>">
    <input type="hidden" id="error" value="<?php echo html_escape($error);?>">
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
     

    <?php $this->load->view('include/stripe-js'); ?>
    
    <script type="text/javascript">
        $(document).ready(function() {

            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });
    </script>


    <script src="<?php echo base_url() ?>assets/admin/js/toast.js"></script>
    <script type="text/javascript">
      <?php if (isset($success)): ?>
      $(document).ready(function() {
        var msg = $('#success').val();
        $.toast({
          heading: 'Correcto',
          text: msg,
          position: 'top-right',
          loaderBg:'#fff',
          icon: 'success',
          hideAfter: 33500
        });

      });
      <?php endif; ?>


      <?php if (isset($error)): ?>
      $(document).ready(function() {
        var msg = $('#error').val();
        $.toast({
          heading: 'Error',
          text: msg,
          position: 'top-right',
          loaderBg:'#fff',
          icon: 'error',
          hideAfter: 33500
        });

      });
      <?php endif; ?>
    </script>
    
</body>
</html>