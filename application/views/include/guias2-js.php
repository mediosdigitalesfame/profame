<script language="javascript">
  $(document).ready(function(){
    $('#marca').change(function(){
      var id=$(this).val();
      var id2=2;
      
      $.ajax({
        url : "<?php echo site_url('product/get_anios');?>",
        method : "POST",
        data : {id: id,'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'},
        async : true,
        dataType : 'json',
        success: function(data){
          var html = '';
          var i;
          for(i=0; i<data.length; i++){
            html += '<option value='+data[i].id+'>'+data[i].name+'</option>';
          }
          $('#anio').html(html); 
        }
      });

      $.ajax({
        url : "<?php echo site_url('product/get_modelos2');?>",
        method : "POST",
        data : {id: id,id2: id2,'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'},
        async : true,
        dataType : 'json',
        success: function(data){
          var html = '';
          var i;
          for(i=0; i<data.length; i++){
            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].version+'</option>';
          }
          $('#modelo').html(html);
        }
      });
      return false;
    });

    $('#anio').change(function(){
      var idanio=$(this).val();
      var idmarca = $('#marca').val();

      $.ajax({
        url : "<?php echo site_url('product/get_modelos3');?>",
        method : "POST",
        data : {idanio: idanio,idmarca: idmarca,'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'},
        async : true,
        dataType : 'json',
        success: function(data){
          var html = '';
          var i;
          for(i=0; i<data.length; i++){
            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].version+'</option>';
          }
          $('#modelo').html(html);
        }
      });
    });
  });
</script>