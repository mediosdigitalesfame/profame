<!DOCTYPE html>
<html class="no-js" lang="en">
<?php $settings = get_settings(); ?>
<head>
    <meta charset="utf-8">
    <meta name="author" content="Rafael  - gonguti.com">
    <meta name="description" content="<?php echo html_escape($settings->description) ?>">
    <meta name="keywords" content="<?php echo html_escape($settings->keywords) ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title><?php echo html_escape($settings->site_name) ?> - <?php echo html_escape($settings->site_title) ?></title>
    <!-- Favicon icon -->  
    <link rel="shortcut icon" href="<?php echo base_url($settings->favicon) ?>" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php echo base_url($settings->favicon) ?>">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/animate/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/css/theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/css/theme-elements.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/css/theme-blog.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/rs-plugin/css/navigation.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/vendor/circle-flip-slideshow/css/component.css">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/css/skins/default.css"> 

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/porto/css/custom.css">

    <!-- Head Libs -->
    <script src="<?php echo base_url(); ?>assets/porto/vendor/modernizr/modernizr.min.js"></script>

    <!-- csrf token -->
    <script type="text/javascript">
     var csrf_token = '<?php echo $this->security->get_csrf_hash(); ?>';
     var token_name = '<?php echo $this->security->get_csrf_token_name();?>'
 </script>

 <!-- google analytics -->
 <?php if (!empty($settings->google_analytics)): ?>
    <?php echo base64_decode($settings->google_analytics) ?>
<?php endif ?>

<!-- recaptcha js -->
<script src='https://www.google.com/recaptcha/api.js'></script>

</head>

<!--<body data-spy="scroll" data-target=".primary-menu">-->
    <body>
        <div class="body">

            <!-- Header Area -->
            <header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 45, 'stickySetTop': '-45px', 'stickyChangeLogo': true}">
                <div class="header-body">
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-logo">
                                        <a href="<?php echo base_url() ?>">
                                            <img alt="Grupo FAME" src="<?php echo base_url($settings->logo) ?>">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="header-column justify-content-end">
                                <div class="header-row pt-3">
                                    <nav class="header-nav-top">
                                        <ul class="nav nav-pills">
                                            <li class="nav-item nav-item-anim-icon d-none d-md-block">
                                                <a class="nav-link pl-0" href="about-us.html"><i class="fas fa-angle-right"></i>Nosotros</a>
                                            </li>
                                            <li class="nav-item nav-item-anim-icon d-none d-md-block">
                                                <a class="nav-link" href="<?php echo base_url(); ?>home/contacto"><i class="fas fa-angle-right"></i> Contactanos</a>
                                            </li>
                                            <li class="nav-item nav-item-left-border nav-item-left-border-remove nav-item-left-border-md-show">
                                                <span class="ws-nowrap"><i class="fas fa-phone"></i> 800 670 83 86</span>
                                            </li>
                                        </ul>
                                    </nav>
                                    <div class="header-nav-features">
                                    </div>
                                </div>
                                <div class="header-row">
                                    <div class="header-nav pt-1">
                                        <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                                            <nav class="<?php if(isset($page_title) && $page_title == 'Inicio'){echo"collapse";}else{echo"static";} ?> ">
                                                <ul class="nav nav-pills" id="mainNav">
                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle <?php if(isset($page_title) && $page_title == 'Inicio'){echo'active';} ?>" href="<?php echo base_url() ?>">
                                                            Inicio
                                                        </a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle <?php if(isset($page_title) && $page_title == 'Agencias'){echo'active';} ?>" href="<?php echo base_url(); ?>home/agencias">
                                                            Agencias
                                                        </a>
                                                        <ul class="dropdown-menu">

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/michoacan">Michoacan</a>
                                                                <ul class="dropdown-menu">
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciashonda">Honda</a>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondamanantiales">Manantiales</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondaaltozano">Altozano</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondamonarcamorelia">Monarca</a></li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciaschevrolet">Chevrolet</a>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chevroletmorelia">Morelia</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chevroleturuapan">Uruapan</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chevroletapatzingan">Apatzingan</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chevroletlazaro">Lázaro Cárdenas</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chevroletzamora">Zamora</a></li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciastoyota">Toyota</a>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/toyotavalladolid">Valladolid</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/toyotaaltozano">Altozano</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/toyotauruapan">Uruapan</a></li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciaskia">KIA</a>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/kiamilcumbres">Mil Cumbres</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/kiaparicutin">Paricutin</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/kiadelduero">Del Duero</a></li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/isuzu">Isuzu</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/volkswagen">Volkswagen</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/motorradmorelia">Motorrad</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/bmwtalisman">BMW</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/minitalisman">MINI</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/mitsubishiuruapan">Mitsubishi</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/fiaturuapan">FIAT</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/chrysleruruapan">Chrysler-Dodge-Jeep-RAM</a>
                                                                    </li>                                                                
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/queretaro">Querétaro</a>
                                                                <ul class="dropdown-menu">
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciashonda">Honda</a>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondacorregidora">Corregidora</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondamarquesa">Marquesa</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondasanjuandelrio">San Juan del Río</a></li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/audijuriquilla">AUDI</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/mitsubishiqueretaro">Mitsubishi</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/fiatqueretaro">FIAT</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/chryslerqueretaro">Chrysler-Dodge-Jeep-RAM</a>
                                                                    </li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/cdmx">CDMX</a>
                                                                <ul class="dropdown-menu">
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciashonda">Honda</a>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondamonarcadf">Monarca DF</a></li>
                                                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondaatizapan">Atizapan</a></li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/kiapedregal">KIA</a>
                                                                    </li>
                                                                    <li class="dropdown">
                                                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/toyotaperisur">Toyota</a>
                                                                    </li>
                                                                </ul>
                                                            </li>

                                                        </ul>
                                                    </li>

                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle <?php if(isset($page_title) && $page_title == 'Marcas'){echo'active';} ?>" href="<?php echo base_url(); ?>home/marcas">
                                                            Marcas
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciashonda">Honda</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondamonarcamorelia"> Monarca Morelia </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondaaltozano"> Altozano Morelia</a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondamanantiales"> Manantiales Morelia</a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondaatizapan"> Atizapan </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondamonarcadf"> Monarca DF </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondauruapan"> Uruapan </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondacorregidora"> Corregidora </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondamarquesa"> Marquesa </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/hondasanjuan"> San Juan del Río </a></li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciaschevrolet">Chevrolet</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chevroletmorelia"> Morelia </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chevroleturuapan"> Uruapan </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chevroletapatzingan"> Apatzingán </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chevroletsahuayo"> Sahuayo </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chevroletlazaro"> Lázaro Cárdenas </a></li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciaskia">KIA</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/kiamilcumbres"> Mil Cumbres - Morelia  </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/kiaparicutin"> Paricutin - Uruapan </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/kiadelduero"> Del Duero - Zamora </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/kiapedregal"> Pedregal - CDMX </a></li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciastoyota">Toyota</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/toyotavalladolid"> Valladolid</a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/toyotaaltozano"> Altozano </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/toyotauruapan"> Uruapan </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/toyotaperisur"> Perisur </a></li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciasnissan">Nissan</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/nissanacueducto"> Acueducto - Morelia </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/nissanaltozano"> Altozano - Morelia </a></li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciasmitsubishi">Mitsubishi</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/mitsubishiuruapan"> Uruapan </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/mitsubishiqueretaro"> Querétaro </a></li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciasfiat">FIAT</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/fiaturupan"> Uruapan </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/fiatqueretaro"> Querétaro </a></li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciaschrysler">Chrysler-Dodge-Jeep-RAM</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chrysleruruapan"> Uruapan </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/chryslerqueretaro"> Querétaro </a></li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciasgmc">GMC-Buick-Cadillac</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/gmcmorelia"> Morelia </a></li>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/gmcuruapan"> Uruapan </a></li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/volkswagen">Volkswagen</a>
                                                            </li>
                                                            <li class="dropdown">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/bmwtalisman">BMW</a>
                                                            </li>
                                                            <li class="dropdown">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/minitalisman">MINI</a>
                                                            </li>
                                                            <li class="dropdown">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/motorradmorelia">Motorrad</a>
                                                            </li>
                                                            <li class="dropdown">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/audijuriquilla">Audi</a>
                                                            </li>
                                                        </ul>
                                                    </li>

                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle <?php if(isset($page_title) && $page_title == 'Modelos'){echo'active';} ?>" href="<?php echo base_url(); ?>home/modelos">
                                                            Modelos
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/modeloshonda">Honda</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>modelos"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciaschevrolet">Chevrolet</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciasnissan">Nissan</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="<?php //echo base_url(); ?>modelos/march"> March  </a></li>
                                                                    <li><a class="dropdown-item" href="#"> Versa </a></li>
                                                                    <li><a class="dropdown-item" href="#"> Sentra </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/audijuriquilla">AUDI</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/bmw">BMW</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/mini">MINI</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/motorrad">Motorrad</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciasgmc">GMC</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/isuzu">Isuzu</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciaskia">KIA</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciasmitsubishi">Mitsubishi</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciastoyota">Toyota</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/volkswagen">Volkswagen</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciasfiat">Fiat</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="<?php echo base_url(); ?>home/agenciaschrysler">Chrysler-Dodge-Jeep-RAM</a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="dropdown-item" href="#"> 1 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 2 </a></li>
                                                                    <li><a class="dropdown-item" href="#"> 3 </a></li>
                                                                </ul>
                                                            </li>

                                                        </ul>
                                                    </li>

                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle" href="#">
                                                            Seminuevos
                                                        </a>
                                                    </li>

                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle <?php if(isset($page_title) && $page_title == 'Cotizador'){echo'active';} ?>" href="<?php echo base_url(); ?>home/cotizador">
                                                            Cotizador
                                                        </a>
                                                    </li>

                                                     <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle <?php if(isset($page_title) && $page_title == 'Guias'){echo'active';} ?>" href="<?php echo base_url(); ?>home/guias">
                                                            Guias
                                                        </a>
                                                    </li>

                                                <!--<li class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle" href="#">
                                                        Tienda
                                                    </a>
                                                </li>

                                                <?php if ($settings->enable_blog == 1): ?>
                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle" href="#">
                                                            Blog
                                                        </a>
                                                    </li>
                                                <?php endif ?>

                                                <?php if ($settings->enable_faq == 1): ?>
                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle" href="#">
                                                            Faqs
                                                        </a>
                                                    </li>
                                                <?php endif ?>-->



                                            </ul>
                                        </nav>
                                    </div>
                                    <ul class="header-social-icons social-icons d-none d-sm-block">
                                        <li class="social-icons-facebook">
                                            <a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                                        </li>

                                        <li class="social-icons-twitter">
                                            <a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a>
                                        </li>

                                        <li class="social-icons-youtube">
                                            <a href="https://www.youtube.com.com/" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a>
                                        </li>
                                        <li class="social-icons-instagram">
                                            <a href="https://www.instagram.com/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a>
                                        </li>

                                        <li class="social-icons-linkedin">
                                            <a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a>
                                        </li>

                                        <li class="social-icons-whatsapp">
                                            <a href="https://www.whatsapp.com/" target="_blank" title="WhatsApp"><i class="fab fa-whatsapp"></i></a>
                                        </li>

                                    </ul>
                                    <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>









        <!-- /Header Area -->  