 <div class="content-wrapper">
  <!-- Main content -->
  <section class="content">

    <div class="box add_area" style="display: <?php if($page_title == "Editar"){echo "block";}else{echo "none";} ?>">

      <div class="box-header with-border">
        <?php if (isset($page_title) && $page_title == "Editar"): ?>
          <h3 class="box-title">Editar guia</h3>
        <?php else: ?>
          <h3 class="box-title">Add Nueva guia </h3>
        <?php endif; ?>

        <div class="box-tools pull-right">
          <?php if (isset($page_title) && $page_title == "Editar"): ?>
            <?php $required = ''; ?>
            <?php $disabled = ''; ?>

            <a href="<?php echo base_url('admin/guias') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i>Regresar</a>
          <?php else: ?>
            <?php $required = 'required'; ?>
            <?php $disabled = 'disabled'; ?>
            
            <a href="#" class="text-right btn btn-primary btn-sm cancel_btn"><i class="fa fa-list"></i> Todas las guias</a>
          <?php endif; ?>
        </div>
      </div>

      <div class="box-body">

        <form id="cat-form" method="post" enctype="multipart/form-data" class="validate-form" action="<?php echo base_url('admin/guias/add')?>" role="form" novalidate>

          <?php 
          $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
          $iden = '-'.date('dmY').'-'.substr(str_shuffle($permitted_chars), 0, 8); 
          ?>

          <div class="row m-t-30">

            <div class="col-sm-3">
              <div class="form-group">
                <label>Agencia</label>
                <select class="form-control single_select" id="agencia" name="agencia" onChange="cambiafolio();" style="width: 100%" required >
                  <option value="">Seleccionar</option>
                  <?php foreach ($agencias as $agencia): ?>
                    <?php if (!empty($agencia->name)): ?>
                      <option value="<?php echo html_escape($agencia->id); ?>" 
                        <?php echo ($guia[0]['agencys_id'] == $agencia->id) ? 'selected' : ''; ?>>

                        <?php echo html_escape($agencia->name) ?>                        
                      </option> 
                    <?php endif ?>

                  <?php endforeach ?>
                </select><p class="help-block text-danger"></p>
                <p class="help-block text-danger"></p>
              </div> 
            </div>

            <input type="hidden" id="identi" name="identi" value="<?php echo html_escape($iden); ?>">

           

            <div class="col-sm-2">
              <div class="form-group">
                <label>% Utilidad</label>
                <select class="form-control single_select" id="pdudlv7" name="pdudlv7" style="width: 100%" required>
                  <?php foreach ($utilidades as $utilidad): ?>
                    <?php if (!empty($utilidad->valor)): ?>
                      <option value="<?php echo html_escape($utilidad->valor); ?>" 
                        <?php echo ($guia[0]['pdudlv7'] == $utilidad->valor) ? 'selected' : ''; ?>>
                        <?php echo html_escape($utilidad->valor); ?>
                      </option>
                    <?php endif ?>
                  <?php endforeach ?>
                </select>
                <p class="help-block text-danger"></p>
              </div> 
            </div>

             <div class="col-sm-3">
              <div class="form-group">
                <label>Marca</label>
                <select class="form-control single_select" id="marca" name="marca" style="width: 100%" required>
                  <option value="">Selecciona Marca</option>
                  <?php foreach ($marcas as $marca): ?>
                    <?php if (!empty($marca->name)): ?>                  
                      <option value="<?php echo html_escape($marca->id); ?>" 
                        <?php echo ($guia[0]['idmarca'] == $marca->id) ? 'selected' : ''; ?>>
                        <?php echo html_escape($marca->id).'.-'. html_escape($marca->name); ?>
                      </option>
                    <?php endif ?>
                  <?php endforeach ?>
                </select>
                <p class="help-block text-danger"></p>
              </div>
            </div>

             <div class="col-sm-4">
              <div class="form-group">
                <label>Folio</label>
                <input type="text" class="form-control" id="folio" name="folio" value="<?php echo html_escape($guia[0]['folio']); ?>" readonly required>
                <p class="help-block text-danger"></p>
              </div>
            </div> 

          </div> 

          <div class="row m-t-30">

            <div class="col-sm-2">
              <div class="form-group">
                <label>Año</label>
                <select class="form-control single_select" id="anio" name="anio" style="width: 100%"  required>
                  <option value=" "></option>
                </select>
                <p class="help-block text-danger"></p>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label>Modelo</label>
                <select class="form-control single_select" id="modelo" name="modelo" style="width: 100%"  required>
                  <option value=""></option>
                </select>
                <p class="help-block text-danger"></p>
              </div>
            </div>

            <div class="col-sm-3">
              <div class="form-group">
                <label>Version Guía</label>
                <select class="form-control single_select" id="tipoguia" name="tipoguia" onChange="mostrar(this.value),cambiaValores();"  style="width: 100%" required>
                  <?php foreach ($tipoguias as $tipoguia): ?>
                    <?php if (!empty($tipoguia->version)): ?>
                      <option value="<?php echo html_escape($tipoguia->id); ?>" 
                        <?php echo ($guia[0]['tipoguias_id'] == $tipoguia->id) ? 'selected' : ''; ?>>
                        <?php echo html_escape($tipoguia->id).'.-'. html_escape($tipoguia->version).'-'.html_escape($tipoguia->persona); ?>
                      </option>
                    <?php endif ?>
                  <?php endforeach ?>
                </select>
                <p class="help-block text-danger"></p>
              </div>
            </div>

            <div class="col-sm-3">
              <div class="form-group">
                <label>VIN</label>
                <input class="form-control" name="vin" id="vin" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();" value="<?php echo html_escape($guia[0]['vin']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
              </div>
            </div>



          </div>

          <div class="row m-t-30">
            <div class="col-sm-3">
              <div class="form-group">
                <label>Kilometraje</label>
                <input type="number" minlength="1" maxlength="5" class="form-control" id="km" name="km" value="<?php echo html_escape($guia[0]['km']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>  
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Compra Guía Autometrica </label>
                <input <?php if($page_title == "Editar"){echo "none";}else{echo "disbled";} ?> class="form-control" name="compraautometrica" id="compraautometrica" value="<?php echo html_escape($guia[0]['compraautometrica']); ?>" <?php echo html_escape($disabled); ?> <?php echo html_escape($required); ?>>
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Venta Guía Autometrica</label>
                <input <?php echo html_escape($disabled); ?> class="form-control" id="ventaautometrica" name="ventaautometrica" value="<?php echo html_escape($guia[0]['ventaautometrica']); ?>"   style="text-align: right;" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Equipamiento</label>
                <input class="form-control" id="equipamiento" name="equipamiento" value="<?php echo html_escape($guia[0]['equipamiento']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Premio/Castigo Km</label>
                <input <?php echo html_escape($disabled); ?> class="form-control" id="premio" name="premio" value="<?php echo html_escape($guia[0]['premio']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
              </div>
            </div>
            <!--<div class="col-sm-3">
              <div class="form-group">
                <label>Castigo Km</label>
                <input <?php echo html_escape($disabled); ?> pattern="-.*" data-validation-pattern-message="Debe ser negativo" class="form-control" id="castigo" name="castigo" value="<?php echo html_escape($guia[0]['premio']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
              </div>
            </div>-->

            <div class="col-sm-3">
              <div class="form-group">
                <label>Reacondicionamiento.</label>
                <input <?php echo html_escape($disabled); ?> pattern="-.*" data-validation-pattern-message="Debe ser negativo" class="form-control" max="9" name="reacondicionamiento" id="reacondicionamiento" value="<?php echo html_escape($guia[0]['reacondicionamiento']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Valor del Coche</label>
                <input <?php echo html_escape($disabled); ?> data-toggle="tooltip" data-placement="top" title="Valor estimado que se le da al vehiculo" class="form-control" name="valor" id="valor" value="<?php echo html_escape($guia[0]['valor']); ?>" onblur="cambiaValores()" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
              </div>
            </div>

             <div class="col-sm-3">
            <div class="form-group">
             <?php if (isset($page_title) && $page_title == "Editar"): ?>
              <img src="<?php echo base_url($guia[0]['thumb']) ?>"> <br><br>
            <?php endif ?>
            <div class="input-group">
              <span class="input-group-btn">
                <span class="btn btn-info btn-file">
                  <i class="fa fa-upload"></i>  Imagen Autometrica <input type="file" id="imgInp" name="photo" <?php echo html_escape($required); ?>>
                </span>
              </span>
            </div><br>
            <img width="200px" id='img-upload'/>
            <p class="help-block text-danger"></p>
          </div> 
        </div>

          </div>

          <br>

          <div class="row m-t-30">

          

        <div class="col-sm-6">
          <div class="form-group" style="border-color: red #080;">

           <div class="form-group"><!-- Form Grup de X1 -->

            <div class="row m-t-30" id="xa1" style="display: flex;">
              <div class="col-sm-12" >  
                <div class="form-group mb-40 mr-30">
                  <label class="mt2-5">Precio de venta sin reacondicionamiento:</label>
                  <span class="pull-right"><h5 id="predeventa" style="color: black;">$<?php echo number_format((int)$guia[0]['pdvapsr1'],2,'.',','); ?></h5></span>
                  <hr>
                </div>
              </div>
            </div>

            <div class="row m-t-30" id="xc6" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
              <div class="col-sm-12" >  
                <div class="form-group mb-40 mr-30">
                  <label class="mt2-5">Factor de utilidad minima esperada:</label>
                  <span class="pull-right"><h5 id="facdeu" style="color: black;"><?php echo $guia[0]['fdume13']; ?></h5></span>
                  <hr>
                </div>
              </div>
            </div>

          <!--<div class="row m-t-30" id="xc1" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Precio minimo de venta al publico reacondicionado:</label>
                <span class="pull-right"><h4 id="premindeven" style="color: green;">$<?php echo number_format((int)$guia[0]['pmdvap8'],2,'.',','); ?></h4></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xa2" style="display: flex;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <h4 id="msjr" style="color: green;"></h4>
                <label class="mt2-5">Precio de la toma tope al cliente con iva:</label>
                <span class="pull-right"><h4 id="predetoma" style="color: green;">$<?php echo number_format((int)$guia[0]['pdlttfac2'],2,'.',','); ?></h4></span>
                <hr>
              </div>
            </div>
          </div>-->

          <div class="row m-t-30" id="xb3" style="display: <?php if($guia[0]['tipoguias_id'] >= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Costo real:</label>
                <span class="pull-right"><h5 id="ctoreal" style="color: black;">$<?php echo number_format((int)$guia[0]['crpdltsi3'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xb1" style="display: <?php if($guia[0]['tipoguias_id'] >= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Precio de venta al publico sin iva:</label>
                <span class="pull-right"><h5 id="predevensin" style="color: black;">$<?php echo number_format((int)$guia[0]['pdvapsi4'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xc2" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Costo compra con reacondicionamiento:</label>
                <span class="pull-right"><h5 id="coconrea" style="color: black;">$<?php echo number_format((int)$guia[0]['cccr9'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xc3" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Precio de venta al publico:</label>
                <span class="pull-right"><h5 id="predevenpu" style="color: black;">$<?php echo number_format((int)$guia[0]['pdvap10'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xc4" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Utilidad con IVA de la utilidad:</label>
                <span class="pull-right"><h5 id="uconi" style="color: black;">$<?php echo number_format((int)$guia[0]['ucidlu11'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xc5" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">IVA de la utilidad:</label>
                <span class="pull-right"><h5 id="idelau" style="color: black;">$<?php echo number_format((int)$guia[0]['idlu12'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <!--<div class="row m-t-30" id="xb2" style="display: <?php if($guia[0]['tipoguias_id'] >= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Precio de venta al publico con iva y reacondicionamiento:</label>
                <span class="pull-right"><h5 id="predevencon" style="color: black;">$<?php echo number_format((int)$guia[0]['pdvapciyr5'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>-->

          <div class="row m-t-30" id="xa3" style="display: flex;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Utilidad de la venta:</label>
                <span class="pull-right"><h5 id="udelaven" style="color: black;">$<?php echo number_format((int)$guia[0]['udlv6'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

        </div><!-- Form Grup de X1 -->

      </div> 
    </div>


    <div class="col-sm-6">

      <div class="form-group colorin" align="center"><!-- Form Grup de X1 -->
        <br>
        <br>
          <!--<div class="row m-t-30" id="xa1" style="display: flex;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Precio de venta sin reacondicionamiento:</label>
                <span class="pull-right"><h5 id="predeventa" style="color: black;">$<?php echo number_format((int)$guia[0]['pdvapsr1'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xc6" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Factor de utilidad minima esperada:</label>
                <span class="pull-right"><h5 id="facdeu" style="color: black;"><?php echo $guia[0]['fdume13']; ?></h5></span>
                <hr>
              </div>
            </div>
          </div>-->

          <div class="row m-t-30" id="xc1" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Precio minimo de venta al publico reacondicionado:</label>
                <span class="pull-right"><h4 id="premindeven" style="color: green;">$<?php echo number_format((int)$guia[0]['pmdvap8'],2,'.',','); ?></h4></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xa2" style="display: flex;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <h4 id="msjr" style="color: green;"></h4>
                <label class="mt2-5">Precio de la toma tope al cliente con iva:</label>
                <span class="pull-right"><h4 id="predetoma" style="color: green;">$<?php echo number_format((int)$guia[0]['pdlttfac2'],2,'.',','); ?></h4></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xb2" style="display: <?php if($guia[0]['tipoguias_id'] >= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Precio de venta al publico con iva y reacondicionamiento:</label>
                <span class="pull-right"><h4 id="predevencon" style="color: green;">$<?php echo number_format((int)$guia[0]['pdvapciyr5'],2,'.',','); ?></h4></span>
                <hr>
              </div>
            </div>
          </div>

          <!--<div class="row m-t-30" id="xb3" style="display: <?php if($guia[0]['tipoguias_id'] >= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Costo real:</label>
                <span class="pull-right"><h5 id="ctoreal" style="color: black;">$<?php echo number_format((int)$guia[0]['crpdltsi3'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xb1" style="display: <?php if($guia[0]['tipoguias_id'] >= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Precio de venta al publico sin iva:</label>
                <span class="pull-right"><h5 id="predevensin" style="color: black;">$<?php echo number_format((int)$guia[0]['pdvapsi4'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xc2" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Costo compra con reacondicionamiento:</label>
                <span class="pull-right"><h5 id="coconrea" style="color: black;">$<?php echo number_format((int)$guia[0]['cccr9'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xc3" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Precio de venta al publico:</label>
                <span class="pull-right"><h5 id="predevenpu" style="color: black;">$<?php echo number_format((int)$guia[0]['pdvap10'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xc4" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Utilidad con IVA de la utilidad:</label>
                <span class="pull-right"><h5 id="uconi" style="color: black;">$<?php echo number_format((int)$guia[0]['ucidlu11'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xc5" style="display: <?php if($guia[0]['tipoguias_id'] <= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">IVA de la utilidad:</label>
                <span class="pull-right"><h5 id="idelau" style="color: black;">$<?php echo number_format((int)$guia[0]['idlu12'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xb2" style="display: <?php if($guia[0]['tipoguias_id'] >= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Precio de venta al publico con iva y reacondicionamiento:</label>
                <span class="pull-right"><h5 id="predevencon" style="color: black;">$<?php echo number_format((int)$guia[0]['pdvapciyr5'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>

          <div class="row m-t-30" id="xa3" style="display: flex;">
            <div class="col-sm-12" >  
              <div class="form-group mb-40 mr-30">
                <label class="mt2-5">Utilidad de la venta:</label>
                <span class="pull-right"><h5 id="udelaven" style="color: black;">$<?php echo number_format((int)$guia[0]['udlv6'],2,'.',','); ?></h5></span>
                <hr>
              </div>
            </div>
          </div>-->

        </div><!-- Form Grup de X1 -->

      </div> <!-- div class de 6 -->

      <!-- <h4>Costos</h4> -->

      <div class="row m-t-30" id="x1" style="display: flex;">
        <div class="col-sm-6" >
          <div class="form-group">
           <!-- <label>Precio de Venta sin Reacondicionamiento</label>-->
           <input type="hidden" class="form-control" name="pdvapsr1" id="pdvapsr1" value="<?php echo html_escape($guia[0]['pdvapsr1']); ?>" readonly <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
           <input type="hidden" id="pdvapsr1a" name="pdvapsr1a" value="">
         </div>
       </div>
       <div class="col-sm-4">
        <div class="form-group">
          <!--<label>Precio de la Toma tope al cliente con IVA</label>-->
          <input type="hidden" class="form-control" name="pdlttfac2" id="pdlttfac2" readonly value="<?php echo html_escape($guia[0]['pdlttfac2']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
          <input type="hidden" id="pdlttfac2a" name="pdlttfac2a" value="">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <!--<label>Utilidad de la Venta</label>-->
          <input type="hidden" class="form-control" name="udlv6" id="udlv6" readonly value="<?php echo html_escape($guia[0]['udlv6']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
          <input type="hidden" id="udlv6a" name="udlv6a" value="">
        </div>
      </div>
    </div>

    <div class="row m-t-30" id="x2" style="display: <?php if($guia[0]['tipoguias_id'] >= 4){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
      <div class="col-sm-3">
        <div class="form-group">
         <!-- <label>Precio de Venta al Publico sin IVA</label>-->
         <input type="hidden" class="form-control" name="pdvapsi4" id="pdvapsi4" readonly value="<?php echo html_escape($guia[0]['pdvapsi4']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
         <input type="hidden" id="pdvapsi4a" name="pdvapsi4a" value="">
       </div>
     </div>
     <div class="col-sm-6">
      <div class="form-group">
       <!-- <label>Precio de Venta al Publico con IVA y Reacondicionamiento</label>-->
       <input type="hidden" class="form-control" name="pdvapciyr5" id="pdvapciyr5" readonly value="<?php echo html_escape($guia[0]['pdvapciyr5']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
       <input type="hidden" id="pdvapciyr5a" name="pdvapciyr5a" value="">
     </div>
   </div>
   <div class="col-sm-3">
    <div class="form-group">
      <!--<label>Costo Real</label>-->
      <input type="hidden" class="form-control" name="crpdltsi3" id="crpdltsi3" readonly value="<?php echo html_escape($guia[0]['crpdltsi3']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      <input type="hidden" id="crpdltsi3a" name="crpdltsi3a" value="">
    </div>
  </div>

</div>

<div class="row m-t-30" id="x3" style="display: <?php if($guia[0]['tipoguias_id'] >= 4){echo html_escape('flex');}else{echo html_escape('none');} ?>;">
  <div class="col-sm-4">
    <div class="form-group">
     <!-- <label>Precio Minimo de Venta al Publico Reacondicionado</label>-->
     <input type="hidden" class="form-control" name="pmdvap8" id="pmdvap8" readonly value="<?php echo html_escape($guia[0]['pmdvap8']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
     <input type="hidden" id="pmdvap8a" name="pmdvap8a" value="">
   </div>
 </div>
 <div class="col-sm-4">
  <div class="form-group">
    <!--<label>Costo Compra con Reacondicionamiento</label>-->
    <input type="hidden" class="form-control" name="cccr9" id="cccr9" readonly value="<?php echo html_escape($guia[0]['cccr9']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
    <input type="hidden" id="cccr9a" name="cccr9a" value="">
  </div>
</div>
<div class="col-sm-4">
  <div class="form-group">
    <!--<label>Precio de Venta al Publico</label>-->
    <input type="hidden" class="form-control" name="pdvap10" id="pdvap10" readonly value="<?php echo html_escape($guia[0]['pdvap10']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
    <input type="hidden" id="pdvap10a" name="pdvap10a" value="">
  </div>
</div>
</div>

<div class="row m-t-30" id="x4" style="display: <?php if($guia[0]['tipoguias_id'] >= 4){echo html_escape('flex');}else{echo html_escape('none');} ?>;">
  <div class="col-sm-4">
    <div class="form-group">
      <!--<label>Utilidad con IVA de la Utilidad</label>-->
      <input type="hidden" class="form-control" name="ucidlu11" id="ucidlu11" readonly value="<?php echo html_escape($guia[0]['ucidlu11']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      <input type="hidden" id="ucidlu11a" name="ucidlu11a" value="">
    </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group">
     <!-- <label>IVA de la Utilidad</label>-->
     <input type="hidden" class="form-control" name="idlu12" id="idlu12" readonly value="<?php echo html_escape($guia[0]['idlu12']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
     <input type="hidden" id="idlu12a" name="idlu12a" value="">
   </div>
 </div>
 <div class="col-sm-3">
  <div class="form-group">
    <!--<label>Factor de Utilidad Minima Esperada</label>-->
    <input type="hidden" class="form-control" name="fdume13" id="fdume13" readonly value="<?php echo html_escape($guia[0]['fdume13']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
    <input type="hidden" id="fdume13a" name="fdume13a" value="">
  </div>
</div>
</div>


<input type="hidden" id="id" name="id" value="<?php echo html_escape($guia['0']['id']); ?>">
<input type="hidden" id="idmarca" name="idmarca" value="<?php echo html_escape($guia['0']['idmarca']); ?>">
<input type="hidden" id="idanio" name="idanio" value="<?php echo html_escape($guia['0']['idanio']); ?>">
<input type="hidden" id="idmodelo" name="idmodelo" value="<?php echo html_escape($guia['0']['idmodel']); ?>">
<!-- csrf token -->
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

<hr>

</div> <!-- div class row 30 -->



<div class="row m-t-30">
  <div class="col-sm-12" name="botones" id="botones" style="display: flex;">
    <?php if (isset($page_title) && $page_title == "Editar"): ?>
      <button type="submit" id="btncambios" class="btn btn-info pull-left" >Guardar Cambios</button>
    <?php else: ?>
      <button type="submit" id="btnguardar" class="btn btn-info pull-left" >Guardar Guía</button>
    <?php endif; ?>
  </div>
</div>

</form>
</div>

<div class="box-footer">  </div>
</div>

<?php if (isset($page_title) && $page_title != "Editar"): ?>

  <div class="box list_area">
    <div class="box-header with-border">
      <?php if (isset($page_title) && $page_title == "Editar"): ?>
        <h3 class="box-title">Editar guia <a href="<?php echo base_url('admin/guias') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i> Regresar</a></h3>
      <?php else: ?>
        <h3 class="box-title">Todas las guias </h3>
      <?php endif; ?>

      <div class="box-tools pull-right">
       <a href="#" class="pull-right btn btn-info btn-sm add_btn"><i class="fa fa-plus"></i> Agregar Nueva guia</a><br>
     </div>
   </div>

   <div class="box-body">

    <div class="col-md-12 col-sm-12 col-xs-12 scroll table-responsive">
      <table class="table table-bordered datatable" id="dg_table">
        <thead>
          <tr>
            <th>#</th>
            <th>Agencia</th>
            <th>Folio</th>
            <th>Marca</th>
            <!--<th>Modelo</th>-->
            <th>KM</th>
            <th>Tipo</th>
            <th>Toma</th>
            <th>Status</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach ($guias as $row): ?>
          <tr id="row_<?php echo ($row->id); ?>">
            <td width="3%"><?php echo $i; ?></td>
            <td width="12%"><?php echo html_escape($row->nameag); ?></td>
             <td width="12%"><?php echo html_escape($row->folio); ?></td>
            <td width="5%"><?php echo html_escape($row->brandname); ?></td>
            <!--<td><?php echo html_escape($row->modelname.' - '.$row->version.' - '.$row->anioname); ?></td>-->
            <td width="5%"><?php echo html_escape($row->km); ?></td>
            <td width="12%"> 
              <?php if ($row->tipoguias_id >= 4): ?>
                <div class="label label-table label-info"><i class="fa fa-times-circle"></i> <?php echo html_escape($row->guiaversion);?> - No Factura</div>
              <?php else: ?>
                <div class="label label-table label-warning"><i class="fa fa-check-circle"></i> <?php echo html_escape($row->guiaversion);?> - Si Factura</div>
              <?php endif ?>
            </td>

            <td width="7%"> 
             <?php if ($row->statustoma == 0): ?>
              <div class="label label-table label-success"><i class="fa fa-times-circle"></i> Disponible</div>
            <?php else: ?>
              <div class="label label-table label-danger"><i class="fa fa-check-circle"></i> Utilizada</div>
            <?php endif ?>
          </td>

          <td width="7%"> 
           <?php if ($row->status == 0): ?>
            <div class="label label-table label-danger"><i class="fa fa-times-circle"></i> Pendiente</div>
          <?php else: ?>
            <div class="label label-table label-success"><i class="fa fa-check-circle"></i> Autorizada</div>
          <?php endif ?>
        </td>

        <td class="actions" width="12%">


          <a href="<?php echo base_url('admin/guias/edit/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a> &nbsp; 

          <?php if ($row->status == 1): ?>
            <a href="<?php echo base_url('admin/guias/deactive/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Desactivar"><i class="fa fa-close"></i></a> &nbsp; 
          <?php else: ?>
           <a href="<?php echo base_url('admin/guias/active/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Autorizar"><i class="fa fa-check"></i></a> &nbsp; 
         <?php endif ?>

         <!-- <a data-val="guia" data-id="<?php echo html_escape($row->id); ?>" href="<?php echo base_url('admin/guias/delete/'.html_escape($row->id));?>" class="on-default remove-row delete_item" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash-o"></i></a> &nbsp; -->

         <a href="<?php echo base_url('admin/guias/export_pdf_guia/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Exportar"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 
       </td>

     </tr>
     <?php $i++; endforeach; ?>
   </tbody>
 </table>
</div>

</div>

<div class="box-footer">

</div>
</div>
<?php endif; ?>

</section>

<script>

  console.log(navigator.userAgent);

  $("#compraautometrica").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#ventaautometrica").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#reacondicionamiento").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#equipamiento").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#premio").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#valor").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#pdvapsr1").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#pdlttfac2").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#crpdltsi3").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#pdvapsi4").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#pdvapciyr5").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#udlv6").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#pmdvap8").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#cccr9").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#pdvap10").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#ucidlu11").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default

  $("#idlu12").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'$',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default



  $("#test3").inputmask("datetime", {
    inputFormat: "dd/mm/yyyy",
    outputFormat: "mm-yyyy-dd",
    inputEventOnly: true
  });


</script>

</div>


