<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="box add_area" style="display: <?php if($page_title == "Editar"){echo "block";}else{echo "none";} ?>">
      <div class="box-header with-border">
        <?php if (isset($page_title) && $page_title == "Editar"): ?>
          <h3 class="box-title">Editar Toma</h3>
        <?php else: ?>
          <h3 class="box-title">Add Nueva Toma </h3>
        <?php endif; ?>
        <div class="box-tools pull-right">
          <?php if (isset($page_title) && $page_title == "Editar"): ?>
            <?php $required = ''; ?>
            <a href="<?php echo base_url('admin/tomas') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i>Regresar</a>
          <?php else: ?>
            <?php $required = 'required'; ?>
            <a href="#" class="text-right btn btn-primary btn-sm cancel_btn"><i class="fa fa-list"></i> Todas las Tomas</a>
          <?php endif; ?>
        </div>
      </div>

      <div class="box-body">
        <form id="cat-form" method="post" enctype="multipart/form-data" class="validate-form" action="<?php echo base_url('admin/tomas/add')?>" role="form" novalidate>

         <br> 
         <div class="row m-t-30">

          <!--<div class="col-sm-2" align="right"> <h4>Agencia</h4></div>
          <div class="col-sm-4">
           <div class="form-group">
            <select class="form-control single_select" id="agencia" name="agencia" style="width: 100%" required >
              <option value="">Seleccionar</option>
              <?php foreach ($agencias as $agencia): ?>
                <?php if (!empty($agencia->name)): ?>
                  <option value="<?php echo html_escape($agencia->id); ?>" 
                    <?php echo ($toma[0]['agencys_id'] == $agencia->id) ? 'selected' : ''; ?>>
                    <?php echo html_escape($agencia->name); ?>
                  </option>
                <?php endif ?>
              <?php endforeach ?>
            </select><p class="help-block text-danger"></p>
          </div>
        </div> -->
        <div class="col-sm-2" align="right"> <h4>Guía Rápida</h4></div>
        <div class="col-sm-4">
         <div class="form-group">
          <select class="form-control single_select" id="guia" name="guia" style="width: 100%" required >
            <?php if (isset($page_title) && $page_title == "Tomar Seminuevos"): ?>
              <option value="">Seleccionar</option>
            <?php endif ?>
            <?php foreach ($guias as $guia): ?>
              <?php if (!empty($guia->id)): ?>
                <option value="<?php echo html_escape($guia->id); ?>" 
                  <?php echo ($toma[0]['guias_id'] == $guia->id) ? 'selected' : ''; ?>
                  <?php if (!empty($guia->statustoma)): ?>
                    <?php echo ($toma[0]['guias_id'] != $guia->id) ? 'disabled' : ''; ?>
                  <?php endif ?>
                  >
                  <?php echo html_escape($guia->folio); ?>
                  <?php $valordelacompra = html_escape($guia->pdlttfac2); ?>
                </option>
              <?php endif ?>
            <?php endforeach ?>
          </select><p class="help-block text-danger"></p>
        </div>
      </div> 
    </div>

    <!--<input type="hidden" name="guiaactual" value="<?php echo html_escape($toma['0']['guias_id']); ?>">-->

    <hr>
    <div class="row m-t-30">
     <div class="col-sm-4">
      <div class="form-group">
        <label>Nombre Completo</label>
        <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo html_escape($toma[0]['nombre']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>

    <div class="col-sm-2">
      <div class="form-group">
        <label>Fecha Contrato</label>
        <input type="text" class="form-control" name="fechacontrato" id="fechacontrato" value="<?php echo html_escape($toma[0]['fechacontrato']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label>Calle</label>
        <input type="text" class="form-control" name="calle" id="calle" value="<?php echo html_escape($toma[0]['calle']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>N.Ext</label>
        <input type="text" class="form-control" name="numext" id="numext" value="<?php echo html_escape($toma[0]['numext']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>N.Int</label>
        <input type="text" class="form-control" name="numint" id="numint" value="<?php echo html_escape($toma[0]['numint']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label>Colonia</label>
        <input type="text" class="form-control" name="colonia" id="colonia" value="<?php echo html_escape($toma[0]['colonia']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>CP</label>
        <input type="text" class="form-control" name="cp" id="cp" value="<?php echo html_escape($toma[0]['cp']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Ciudad</label>
        <input type="text" class="form-control" name="ciudad" id="ciudad" value="<?php echo html_escape($toma[0]['ciudad']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Municipio</label>
        <input type="text" class="form-control" name="municipio" id="municipio" value="<?php echo html_escape($toma[0]['municipio']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
  </div>

  <div class="row m-t-30">
    

    

    <div class="col-sm-2">
      <div class="form-group">
        <label>Estado</label>
        <input type="text" class="form-control" name="estado" id="estado" value="<?php echo html_escape($toma[0]['estado']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>

    <div class="col-sm-2">
      <div class="form-group">
        <label>RFC</label>
        <input type="text" class="form-control" name="rfc" id="rfc" value="<?php echo html_escape($toma[0]['rfc']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="form-group">
        <label>Número Identificación</label>
        <input type="text" class="form-control" id="nidenti" name="nidenti" value="<?php echo html_escape($toma[0]['nidenti']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Tipo Identificación</label>
        <input type="text" class="form-control" id="tipoidenti" name="tipoidenti" value="<?php echo html_escape($toma[0]['tipoidenti']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="form-group">
        <label>Fecha Identificación</label>
        <input type="text" class="form-control" name="fechaidenti" id="fechaidenti" value="<?php echo html_escape($toma[0]['fechaidenti']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
  </div>

  <div class="row m-t-30">

    <div class="col-sm-2">
      <div class="form-group">
        <label>Mes del Recibo</label>
        <input type="text" class="form-control" id="mesrecibo" name="mesrecibo" value="<?php echo html_escape($toma[0]['mesrecibo']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    
  </div>

  <br><h4>Datos del Vehiculo</h4><hr>

  <div class="row m-t-30">
    <div class="col-sm-3">
      <div class="form-group">
        <label>Factura Ampara Vehivulo</label>
        <input type="text" class="form-control" name="facturavehi" id="facturavehi" value="<?php echo html_escape($toma[0]['facturavehi']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Expedida Por</label>
        <input type="text" class="form-control" id="expedida" name="expedida" value="<?php echo html_escape($toma[0]['expedida']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Fecha Factura</label>
        <input type="text" class="form-control" name="fechafac" id="fechafac" value="<?php echo html_escape($toma[0]['fechafac']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Marca Vehiculo</label>
        <input type="text" class="form-control" id="marcavehi" name="marcavehi" value="<?php echo html_escape($toma[0]['marcavehi']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="form-group">
        <label>Modelo Vehiculo</label>
        <input type="text" class="form-control" id="modelovehi" name="modelovehi" value="<?php echo html_escape($toma[0]['modelovehi']); ?>"<?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Año Modelo</label>
        <input type="text" class="form-control" id="aniomodelo" name="aniomodelo" value="<?php echo html_escape($toma[0]['aniomodelo']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Color Ext</label>
        <input type="text" class="form-control" id="colorext" name="colorext" value="<?php echo html_escape($toma[0]['colorext']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Color Intr</label>
        <input type="text" class="form-control" id="colorint" name="colorint" value="<?php echo html_escape($toma[0]['colorint']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
     <div class="col-sm-2">
      <div class="form-group">
        <label>Version</label>
        <input type="text" class="form-control" id="versionvehi" name="versionvehi" value="<?php echo html_escape($toma[0]['versionvehi']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Clave Vehicular</label>
        <input type="text" class="form-control" id="clavevehi" name="clavevehi" value="<?php echo html_escape($toma[0]['clavevehi']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Número de Serie</label>
        <input type="text" class="form-control" id="nserie" name="nserie" value="<?php echo html_escape($toma[0]['nserie']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
  </div>

  <div class="row m-t-30">
    
   
    
    <div class="col-sm-2">
      <div class="form-group">
        <label>Numero de Motor</label>
        <input type="number" class="form-control" id="nmotor" name="nmotor" value="<?php echo html_escape($toma[0]['nmotor']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>

    <!--<div class="col-sm-2">
      <div class="form-group">
        <label>Kilometraje</label>
        <input type="text" class="form-control" id="km" name="km" value="<?php echo html_escape($toma[0]['km']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>-->

    <div class="col-sm-2">
      <div class="form-group">
        <label>Numero de Baja</label>
        <input type="text" class="form-control" id="nbaja" name="nbaja" value="<?php echo html_escape($toma[0]['nbaja']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Edo Emisor de Baja</label>
        <input type="text" class="form-control" name="edoemisor" value="<?php echo html_escape($toma[0]['edoemisor']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Fecha Baja</label>
        <input type="text" class="form-control" name="fechabaja" id="fechabaja" value="<?php echo html_escape($toma[0]['fechabaja']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Placas de Baja</label>
        <input type="text" class="form-control" id="placasbaja" name="placasbaja" value="<?php echo html_escape($toma[0]['placasbaja']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
     <div class="col-sm-2">
      <div class="form-group">
        <label>Num. Verificación</label>
        <input type="text" class="form-control" id="nverificacion" name="nverificacion" value="<?php echo html_escape($toma[0]['nverificacion']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
  </div>

  <div class="row m-t-30">
    
   
    <!--<div class="col-sm-2">
      <div class="form-group">
        <label>Valor de Compra</label>
        <input type="text" class="form-control" id="valorcompra" name="valorcompra" value="<?php echo html_escape($toma[0]['pdlttfac2']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>  
      </div>
    </div>-->
    <!--<div class="col-sm-3">
      <div class="form-group">
        <label>Valor en Letra</label>
        <input type="text" class="form-control" id="valorletra" name="valorletra" value="<?php echo html_escape($toma[0]['valorletra']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
  </div>-->

  <!--<div class="row m-t-30">
    <div class="col-sm-2">
      <div class="form-group">
        <label>Valor Guia Autometrica</label>
        <input type="text" class="form-control" id="valorautometrica" name="valorautometrica" value="<?php echo html_escape($toma[0]['valorautometrica']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>-->
    <div class="col-sm-5">
      <div class="form-group">
        <label>Nombre a Quien se Vende la Unidad</label>
        <input type="text" class="form-control" id="aquiensevende" name="aquiensevende" value="<?php echo html_escape($toma[0]['aquiensevende']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Fecha de Fac. Final</label>
        <input type="text" class="form-control" name="fechafacfinal" id="fechafacfinal" value="<?php echo html_escape($toma[0]['fechafacfinal']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Fecha de Toma</label>
        <input type="text" class="form-control" name="fechatoma" id="fechatoma" value="<?php echo html_escape($toma[0]['fechatoma']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="form-group">
        <label>Costo de Adquisicion</label>
        <input type="text" style="text-align: right;" class="form-control" id="costoadquisicion" name="costoadquisicion" value="<?php echo html_escape($toma[0]['costoadquisicion']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Años de Tenencias</label>
        <input type="text" data-role="tagsinput" class="form-control" id="tenencias" name="tenencias" value="<?php echo html_escape($toma[0]['tenencias']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
  </div>

  <div class="row m-t-30">

    

  </div>

  <br> <h4>Solicitud CFDI</h4><hr>

  <div class="row m-t-30">
    <div class="col-sm-2">
      <div class="form-group">
        <label>Nombres</label>
        <input type="text" class="form-control" id="nombrescfdi" name="nombrescfdi" value="<?php echo html_escape($toma[0]['nombrescfdi']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Apellido Pat</label>
        <input type="text" class="form-control" id="apellido1" name="apellido1" value="<?php echo html_escape($toma[0]['apellido1']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Apellido Mat</label>
        <input type="text" class="form-control" id="apellido2" name="apellido2" value="<?php echo html_escape($toma[0]['apellido2']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>eMail</label>
        <input type="text" class="form-control" id="email" name="email" value="<?php echo html_escape($toma[0]['email']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Teléfono</label>
        <input type="text" class="form-control" id="tel" name="tel" value="<?php echo html_escape($toma[0]['tel']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Celular</label>
        <input type="text" class="form-control" id="cel" name="cel" value="<?php echo html_escape($toma[0]['cel']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Fecha Nac.</label>
        <input type="text" class="form-control" id="fechanaci" name="fechanaci" value="<?php echo html_escape($toma[0]['fechanaci']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>

    <div class="col-sm-2">
      <div class="form-group">
        <label>Entidad Nac.</label>
        <input type="text" class="form-control" id="entidadnaci" name="entidadnaci" value="<?php echo html_escape($toma[0]['entidadnaci']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>

    <div class="col-sm-2">
      <div class="form-group">
        <label>Actividad</label>
        <input type="text" class="form-control" id="actividad" name="actividad" value="<?php echo html_escape($toma[0]['actividad']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Curp</label>
        <input type="text" class="form-control" id="curp" name="curp" value="<?php echo html_escape($toma[0]['curp']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>País</label>
        <input type="text" class="form-control" id="pais" name="pais" value="<?php echo html_escape($toma[0]['pais']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
  </div>

  <br> <h4>Solicitud de Cheque</h4><hr>

  <div class="row m-t-30">
    <div class="col-sm-2">
      <div class="form-group">
        <label>Banco</label>
        <input type="text" class="form-control" name="banco" value="<?php echo html_escape($toma[0]['banco']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Cuenta</label>
        <input type="text" class="form-control" name="cuenta" value="<?php echo html_escape($toma[0]['cuenta']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Clabe</label>
        <input type="text" class="form-control" name="clabe" value="<?php echo html_escape($toma[0]['clabe']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-1">
      <div class="form-group">
        <label>Sucursal</label>
        <input type="text" class="form-control" name="sucursal" value="<?php echo html_escape($toma[0]['sucursal']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-1">
      <div class="form-group">
        <label>Convenio</label>
        <input type="text" class="form-control" name="convenio" value="<?php echo html_escape($toma[0]['convenio']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Referencia</label>
        <input type="text" class="form-control" name="referencia" value="<?php echo html_escape($toma[0]['referencia']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>Folio CFDI</label>
        <input type="text" class="form-control" name="foliocfdi" value="<?php echo html_escape($toma[0]['foliocfdi']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
      </div>
    </div>
  </div>

  <input type="hidden" name="id" value="<?php echo html_escape($toma['0']['id']); ?>">
  <!-- csrf token -->
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

  <hr>

  <div class="row m-t-30">
    <div class="col-sm-12">
      <?php if (isset($page_title) && $page_title == "Editar"): ?>
        <button type="submit" class="btn btn-info pull-left">Guardar Cambios</button>
      <?php else: ?>
        <button type="submit" class="btn btn-info pull-left">Guardar toma</button>
      <?php endif; ?>
    </div>
  </div>

</form>
</div>

<div class="box-footer">  </div>
</div>

<?php if (isset($page_title) && $page_title != "Editar"): ?>

  <div class="box list_area">
    <div class="box-header with-border">
      <?php if (isset($page_title) && $page_title == "Editar"): ?>
        <h3 class="box-title">Editar toma <a href="<?php echo base_url('admin/tomas') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i> Back</a></h3>
      <?php else: ?>
        <h3 class="box-title">Todas las Tomas </h3>
      <?php endif; ?>

      <div class="box-tools pull-right">
       <a href="#" class="pull-right btn btn-info btn-sm add_btn"><i class="fa fa-plus"></i> Agregar Nueva Toma</a><br>
     </div>
   </div>

   <div class="box-body">

    <div class="col-md-12 col-sm-12 col-xs-12 scroll table-responsive">
      <table class="table table-bordered datatable" id="dg_table">
        <thead>
          <tr>
            <th>#</th>
            <th>Agencia</th>
            <th>Fecha</th>
            <th>Folio Guía</th>
            <th>N. de Serie</th>
            <th>Status</th>
            <th>Checklist</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach ($tomas as $row): ?>
          <tr id="row_<?php echo ($row->id); ?>">

            <td width="5%"><?php echo $i; ?></td>
            <td width="11%"><?php echo html_escape($row->nameag); ?></td>
            <td width="7%"><?php echo date("d-m-Y", strtotime($row->created_at)); ?></td>
            <td width="11%"><?php echo html_escape($row->folio); ?></td>
            <td><?php echo html_escape($row->nserie); ?></td>
             
            <td> 
             <?php if ($row->status == 0): ?>
              <div class="label label-table label-danger"><i class="fa fa-times-circle"></i> Inactivo</div>
            <?php else: ?>
              <div class="label label-table label-success"><i class="fa fa-check-circle"></i> Activo</div>
            <?php endif ?>
          </td>

          <td> 
           <?php if ($row->statuscheck == 0): ?>
            <div class="label label-table label-success"><i class="fa fa-times-circle"></i> Libre</div>
          <?php elseif ($row->statuscheck == 1): ?>
            <div class="label label-table label-danger"><i class="fa fa-check-circle"></i> Pendiente</div>
          <?php elseif ($row->statuscheck == 2): ?>
            <div class="label label-table label-warning"><i class="fa fa-check-circle"></i> En Proceso</div>
          <?php elseif ($row->statuscheck == 3): ?>
            <div class="label label-table label-info"><i class="fa fa-check-circle"></i> Terminado</div>
          <?php endif ?>
        </td>

        <td class="actions" width="35%">
          <a href="<?php echo base_url('admin/tomas/edit/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a> &nbsp; 

          <?php if ($row->status == 1): ?>
            <a href="<?php echo base_url('admin/tomas/deactive/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Desactivar"><i class="fa fa-close"></i></a> &nbsp; 
          <?php else: ?>
           <a href="<?php echo base_url('admin/tomas/active/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Activar"><i class="fa fa-check"></i></a> &nbsp; 
         <?php endif ?>

         <!-- <a data-val="toma" data-id="<?php echo html_escape($row->id); ?>" href="<?php echo base_url('admin/tomas/delete/'.html_escape($row->id));?>" class="on-default remove-row delete_item" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash-o"></i></a> &nbsp; -->

         <a href="<?php echo base_url('admin/tomas/export_pdf_toma/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Exportar Toma"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

         <a href="<?php echo base_url('admin/tomas/export_pdf_solicitudcfdi/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Solicitud CFDI"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

         <a href="<?php echo base_url('admin/tomas/export_pdf_autentificaciondoctos/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Autentificacion de Documentos"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

         <a href="<?php echo base_url('admin/tomas/export_pdf_cartanoretencion/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Carta de No Retencion ISR"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

         <a href="<?php echo base_url('admin/tomas/export_pdf_cartaresponsiva/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Carta Responsiva"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

         <a href="<?php echo base_url('admin/tomas/export_pdf_contratocompraventa/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="contratocompraventa"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

         <a href="<?php echo base_url('admin/tomas/export_pdf_avisodeprivacidad/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Aviso de Privacidad"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

         <a href="<?php echo base_url('admin/tomas/export_pdf_dacionenpago/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Dacion en Pago"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

         <a href="<?php echo base_url('admin/tomas/export_pdf_solicituddecheque/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Solicitud de Cheque"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

         <a href="<?php echo base_url('admin/tomas/export_pdf_calculoderetencion/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Calculo de Retencion"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

         <a href="<?php echo base_url('admin/tomas/export_pdf_reglamento/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Reglamento"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 


       </td>

     </tr>
     <?php $i++; endforeach; ?>
   </tbody>
 </table>
</div>

</div>

<div class="box-footer">

</div>
</div>
<?php endif; ?>

</section>
</div>


<script type="text/javascript">
  $(document).ready(function(){

    $('#cliente').change(function(){ 
      var id=$(this).val();
      $.ajax({
        url : "<?php echo site_url('tomas/get_datoscliente');?>",
        method : "POST",
        data : {id: id},
        async : true,
        dataType : 'json',
        success: function(data){

          var html = '';
          var i;
          for(i=0; i<data.length; i++){
            html += '<option value='+data[i].nombre+'>'+data[i].rfc+'</option>';
          }
          $('#datoscliente').html(html);

        }
      });
      return false;
    }); 

  });
</script>