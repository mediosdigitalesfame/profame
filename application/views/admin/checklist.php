<div class="content-wrapper">
	<!-- Main content -->
	<section class="content">
		<div class="box add_area" style="display: <?php if($page_title == "Editar"){echo "block";}else{echo "none";} ?>">
			<div class="box-header with-border">
				<?php if (isset($page_title) && $page_title == "Editar"): ?>
					<h3 class="box-title">Editar Checklist</h3>
					<?php else: ?>
						<h3 class="box-title">Add Nuevo Checklist </h3>
					<?php endif; ?>
					<div class="box-tools pull-right">
						<?php if (isset($page_title) && $page_title == "Editar"): ?>
							<?php $required = ''; ?>
							<a href="<?php echo base_url('admin/checklist') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i>Regresar</a>
							<?php else: ?>
								<?php $required = 'required'; ?>
								<a href="#" class="text-right btn btn-primary btn-sm cancel_btn"><i class="fa fa-list"></i> Todos los checklist</a>
							<?php endif; ?>
						</div>
					</div>

					<div class="box-body">
						<form id="cat-form" method="post" enctype="multipart/form-data" class="validate-form" action="<?php echo base_url('admin/checklist/add')?>" role="form" novalidate>

							<br> 
							<div class="row m-t-30">
								<div class="col-sm-3">
									<h4>Seleccionar la Toma</h4>
								</div>
								<div class="col-sm-1" align="right"> <h4> </h4></div>
								<div class="col-sm-4">
									<div class="form-group">
										<select class="form-control single_select" id="tomasel" name="tomasel" style="width: 100%" required >
											<option value="">Seleccionar</option>

											<?php if (isset($page_title) && $page_title == "Editar"): ?>

												<?php foreach ($tomag2 as $tomar): ?>
													<?php if (!empty($tomar->id)): ?>
														<option value="<?php echo html_escape($tomar->id); ?>" 
															<?php echo ($toma[0]['seminuevostomados_id'] == $tomar->id) ? 'selected' : ''; ?>
															<?php //if (!empty($tomar->id)): ?>
															<?php //echo ($toma[0]['seminuevostomados_id'] != $tomar->id) ? 'disabled' : ''; ?>
															<?php //endif ?>
															>
															<?php echo html_escape($tomar->folio).'-'.html_escape($tomar->vin); ?>
														</option>
													<?php endif ?>
												<?php endforeach ?>

												<?php else: ?>

													<?php foreach ($tomag as $tomar): ?>
														<?php if (!empty($tomar->id)): ?>
															<option value="<?php echo html_escape($tomar->id); ?>" 
																<?php echo ($toma[0]['seminuevostomados_id'] == $tomar->id) ? 'selected' : ''; ?>
																<?php //if (!empty($tomar->id)): ?>
																<?php //echo ($toma[0]['seminuevostomados_id'] != $tomar->id) ? 'disabled' : ''; ?>
																<?php //endif ?>
																>
																<?php echo html_escape($tomar->folio).'-'.html_escape($tomar->vin); ?>
															</option>
														<?php endif ?>
													<?php endforeach ?>

												<?php endif; ?>

											</select>
											<p class="help-block text-danger"></p>
										</div>
									</div> 
								</div>
								<hr>
								<br>

								<input type="hidden" name="id" value="<?php echo html_escape($toma['0']['id']); ?>">
								<input type="hidden" name="id2" value="<?php echo html_escape($toma['0']['idc']); ?>">



								<div class="row m-t-30">
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Solicitud CFDI</label>
											<span class="pull-right"><input type="checkbox" name="solicitudcfdi" id="solicitudcfdi" value="1" <?php if($toma[0]['solicitudcfdi'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span> 
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Cambio de Rol</label>
											<span class="pull-right"><input type="checkbox" name="cambioderol" id="cambioderol" value="1" <?php if($toma[0]['cambioderol'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Cédula de Id. Fiscal</label>
											<span class="pull-right"><input type="checkbox" name="cedulafiscal" id="cedulafiscal" value="1" <?php if($toma[0]['cedulafiscal'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
								</div>

								<div class="row m-t-30">
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">CFDI Sin Act. Emp.</label>
											<span class="pull-right"><input type="checkbox" name="cfdisinactividad" id="cfdisinactividad" value="0" <?php if($toma[0]['cfdisinactividad'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">XML de CFDI</label>
											<span class="pull-right"><input type="checkbox" name="xml" id="xml" value="1" <?php if($toma[0]['xml'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Ratificación de RFC</label>
											<span class="pull-right"><input type="checkbox" name="rfc" id="rfc" value="1" <?php if($toma[0]['rfc'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
								</div>

								<div class="row m-t-30">
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Validación Fiscal de CFDI</label>
											<span class="pull-right"><input type="checkbox" name="validacioncfdi" id="validacioncfdi" value="1" <?php if($toma[0]['validacioncfdi'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Verifiación Fiscal de CFDI</label>
											<span class="pull-right"><input type="checkbox" name="verificacioncfdi" id="verificacioncfdi" value="1" <?php if($toma[0]['verificacioncfdi'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Autenticacion de Doctos.</label>
											<span class="pull-right"><input type="checkbox" name="autenticaciondoctos" id="autenticaciondoctos" value="1" <?php if($toma[0]['autenticaciondoctos'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
								</div>

								<div class="row m-t-30">
								<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Reporte de Transunion</label>
											<span class="pull-right"><input type="checkbox" name="reportetransunion" id="reportetransunion" value="1" <?php if($toma[0]['reportetransunion'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Cer. No Retencion de ISR</label>
											<span class="pull-right"><input type="checkbox" name="noretencion" id="noretencion" value="1" <?php if($toma[0]['noretencion'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Facturas del Auto</label>
											<span class="pull-right"><input type="checkbox" name="secuenciafacturas" id="secuenciafacturas" value="1" <?php if($toma[0]['secuenciafacturas'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
								</div>

								<div class="row m-t-30">
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Identificación</label>
											<span class="pull-right"><input type="checkbox" name="identificacionoficial" id="identificacionoficial" value="1" <?php if($toma[0]['identificacionoficial'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">CURP</label>
											<span class="pull-right"><input type="checkbox" name="curp" id="curp" value="1" <?php if($toma[0]['curp'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Comprobante Domicilio</label>
											<span class="pull-right"><input type="checkbox" name="comprobante" id="comprobante" value="1" <?php if($toma[0]['comprobante'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
								</div>

								<div class="row m-t-30">
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Baja de Placas</label>
											<span class="pull-right"><input type="checkbox" name="bajaplacas" id="bajaplacas" value="1" <?php if($toma[0]['bajaplacas'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Historial de Tenencias</label>
											<span class="pull-right"><input type="checkbox" name="tenencias" id="tenencias" value="1" <?php if($toma[0]['tenencias'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Tarjeta de Circulación</label>
											<span class="pull-right"><input type="checkbox" name="tarjetacirculacion" id="tarjetacirculacion" value="1" <?php if($toma[0]['tarjetacirculacion'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
								</div>

								<div class="row m-t-30">
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Carta Res. Compraventa</label>
											<span class="pull-right"><input type="checkbox" name="responsivacompraventa" id="responsivacompraventa" value="1" <?php if($toma[0]['responsivacompraventa'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Contrato Compraventa</label>
											<span class="pull-right"><input type="checkbox" name="contratocompraventa" id="contratocompraventa" value="1" <?php if($toma[0]['contratocompraventa'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Aviso de Privasidad</label>
											<span class="pull-right"><input type="checkbox" name="avisoprivacidad" id="avisoprivacidad" value="1" <?php if($toma[0]['avisoprivacidad'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
								</div>

								<div class="row m-t-30">
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Check de Rev. y Cert.</label>
											<span class="pull-right"><input type="checkbox" name="checklistrevision" id="checklistrevision" value="1" <?php if($toma[0]['checklistrevision'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Evaluación Mecánica</label>
											<span class="pull-right"><input type="checkbox" name="evaluacionmecanica" id="evaluacionmecanica" value="1" <?php if($toma[0]['evaluacionmecanica'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Guía Autométrica</label>
											<span class="pull-right"><input type="checkbox" name="guiaautometrica" id="guiaautometrica" value="1" <?php if($toma[0]['guiaautometrica'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
								</div>

								<div class="row m-t-30">
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Póliza de Recepción</label>
											<span class="pull-right"><input type="checkbox" name="polizaderecepcion" id="polizaderecepcion" value="1" <?php if($toma[0]['polizaderecepcion'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Cuentas pos Pagar</label>
											<span class="pull-right"><input type="checkbox" name="cuentasporpagar" id="cuentasporpagar" value="1" <?php if($toma[0]['cuentasporpagar'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Formato Dación en Pago</label>
											<span class="pull-right"><input type="checkbox" name="formatodedacion" id="formatodedacion" value="1" <?php if($toma[0]['formatodedacion'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
								</div>

								<div class="row m-t-30">
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Consulta R. de Robo</label>
											<span class="pull-right"><input type="checkbox" name="reportederobo" id="reportederobo" value="1" <?php if($toma[0]['reportederobo'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Cheque de Proveedor</label>
											<span class="pull-right"><input type="checkbox" name="chequedeproveedor" id="chequedeproveedor" value="1" <?php if($toma[0]['chequedeproveedor'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Solicitud de Cheque</label>
											<span class="pull-right"><input type="checkbox" name="solicitudcheque" id="solicitudcheque" value="1" <?php if($toma[0]['solicitudcheque'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									</div>

									<div class="row m-t-30">
										<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Estado de Cuenta</label>
											<span class="pull-right"><input type="checkbox" name="edocuenta" id="edocuenta" value="1" <?php if($toma[0]['edocuenta'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Calculo de Retención</label>
											<span class="pull-right"><input type="checkbox" name="calculoretencion" id="calculoretencion" value="1" <?php if($toma[0]['calculoretencion'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Autorizacion P/Toma</label>
											<span class="pull-right"><input type="checkbox" name="autorizaciontoma" id="autorizaciontoma" value="1" <?php if($toma[0]['autorizaciontoma'] == 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
										</div>
									</div>
								</div>

									
									<div class="row m-t-30">
										<div class="col-sm-4">
										<div class="form-group mb-40 mr-30">
											<label class="mt-5">Status</label>
											<span class="pull-right"> <select class="form-control single_select" name="status" id="status" style="width: 100%" required >
												<option value="">Seleccionar</option>                      
												<option value="0" <?php echo ($toma[0]['status'] == 0) ? 'selected' : ''; ?>>Libre</option>
												<option value="1" <?php echo ($toma[0]['status'] == 1) ? 'selected' : ''; ?>>Pendiente</option>
												<option value="2" <?php echo ($toma[0]['status'] == 2) ? 'selected' : ''; ?>>En Proceso</option>
												<option value="3" <?php echo ($toma[0]['status'] == 3) ? 'selected' : ''; ?>>Terminado</option>
											</select></span>
										</div>
									</div>
								</div>

								

								<div class="row m-t-30">

									<div class="col-sm-6">
										<?php if (isset($page_title) && $page_title == "Editar"): ?>
											<div class="form-group mb-40 mr-30">
												<label class="mt-5">Comentarios</label>
												<textarea class="form-control" name="comentarios" id="comentarios" maxlength="250"  onkeyup="javascript:this.value=this.value.toUpperCase();" required> 
												</textarea>
												<p class="help-block text-danger"></p>
											</div>
										<?php endif; ?>
										<p class="help-block text-danger"></p>
									</div>

									<div class="col-sm-12">
										<?php if (isset($page_title) && $page_title == "Editar"): ?>
											<table class="table table-bordered datatable" id="dg_table">
												<thead>
													<tr>
														<th>Usuario</th>
														<th>Comentarios</th>
														<th>Fecha</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach ($comentarios as $rows): ?>
													<?php if ($toma[0]['id'] == $rows->cheid): ?>
														<tr id="row_<?php echo ($rows->id); ?>">
															<td width="5%" style="display:none;"><?php echo $i; ?></td>

															<td><?php echo html_escape($rows->nameus); ?></td>
															<td width="35%"><?php echo html_escape($rows->comentarios); ?></td>
															<td><?php echo date("d-m-Y H:i:s", strtotime($rows->fechacomentario)); ?></td>

															<td class="actions">
																<?php if ($rows->status == 0): ?>
																	<a style="color:#FF0000;" href="<?php echo base_url('admin/checklist/activemess/'.html_escape($rows->idcoment));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Marcar como leido"><i class="fa fa-eye-slash"></i></a> &nbsp; 
																	<?php else: ?>
																		<a style="color:#2B9B08;" href="<?php echo base_url('admin/checklist/deactivemess/'.html_escape($rows->idcoment));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Marcar como NO leido"><i class="fa fa-eye"></i></a> &nbsp; 
																	<?php endif ?>
																</td>

															</tr>
														<?php endif ?>
														<?php $i++; endforeach; ?>
													</tbody>
												</table>
											<?php endif; ?>
										</div>

									</div>

									<!-- csrf token -->
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

									<hr>

									<div class="row m-t-30">
										<div class="col-sm-12">
											<?php if (isset($page_title) && $page_title == "Editar"): ?>
												<button type="submit" class="btn btn-info pull-left">Guardar Cambios</button>
												<?php else: ?>
													<button type="submit" class="btn btn-info pull-left">Guardar toma</button>
												<?php endif; ?>
											</div>
										</div>

									</form>
								</div>



								<div class="box-footer">  </div>
							</div>

							<?php if (isset($page_title) && $page_title != "Editar"): ?>

								<div class="box list_area">
									<div class="box-header with-border">
										<?php if (isset($page_title) && $page_title == "Editar"): ?>
											<h3 class="box-title">Editar Checklist <a href="<?php echo base_url('admin/checklist') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i> Back</a></h3>
											<?php else: ?>
												<h3 class="box-title">Todos las Checklist </h3>
											<?php endif; ?>

											<div class="box-tools pull-right">
												<a href="#" class="pull-right btn btn-info btn-sm add_btn"><i class="fa fa-plus"></i> Agregar Nuevo Checklist</a><br>
											</div>
										</div>

										<div class="box-body">

											<div class="col-md-12 col-sm-12 col-xs-12 scroll table-responsive">
												<table class="table table-bordered datatable" id="dg_table">
													<thead>
														<tr>
															<th>#</th>
															<th>Agencia</th>
															<th>Folio Guía</th>
															<th>Vin del Auto</th>
															<th>Status</th>
															<th>Acciones</th>
														</tr>
													</thead>
													<tbody>
														<?php $i=1; foreach ($tomas as $row): ?>
														<tr id="row_<?php echo ($row->id); ?>">

															<td width="5%"><?php echo $i; ?></td>
															<td><?php echo html_escape($row->nameag); ?></td>
															<td><?php echo html_escape($row->folio); ?></td>
															<td><?php echo html_escape($row->vin); ?></td>

															<td> 
																<?php if ($row->status == 0): ?>
																	<div class="label label-table label-success"><i class="fa fa-times-circle"></i> Libre</div>
																	<?php elseif ($row->status == 1): ?>
																		<div class="label label-table label-danger"><i class="fa fa-check-circle"></i> Pendiente</div>
																		<?php elseif ($row->status == 2): ?>
																			<div class="label label-table label-warning"><i class="fa fa-check-circle"></i>En Proceso</div>
																			<?php elseif ($row->status == 3): ?>
																				<div class="label label-table label-success"><i class="fa fa-check-circle"></i> Terminado</div>
																			<?php endif ?>
																		</td>

																		<td class="actions" width="35%">
																			<a href="<?php echo base_url('admin/checklist/edit/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a> &nbsp; 

																			<!-- <a data-val="toma" data-id="<?php echo html_escape($row->id); ?>" href="<?php echo base_url('admin/checklist/delete/'.html_escape($row->id));?>" class="on-default remove-row delete_item" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash-o"></i></a> &nbsp; -->

																			<a href="<?php echo base_url('admin/checklist/export_pdf_checklist/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Exportar"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 

																		</td>

																	</tr>
																	<?php $i++; endforeach; ?>
																</tbody>
															</table>
														</div>

													</div>

													<div class="box-footer">

													</div>
												</div>
											<?php endif; ?>

										</section>
									</div>


									<script type="text/javascript">


										$('#km').keyup(function(){
											var id=$(this).val();
											if (id.length > 1){ document.getElementById("compraautometrica").disabled = false;} 
											else { document.getElementById("compraautometrica").disabled = true; }
											if(id > 80000){swal("Detalle!", "El Kilometraje No Puede ser superior a 80,000", "error")
												document.getElementById("km").value = "";
										}
									});




								</script> 