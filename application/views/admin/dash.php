<div class="content-wrapper">

  <section class="content">

    <div class="row">
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center round-success"><i class="icon-docs"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($guia)) {echo $guia->total;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Total de Guías</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center bg-primary"><i class="icon-docs"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($puser)) {echo $guia->active_guias;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Guias Autorizadas</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center bg-danger"><i class="icon-docs"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($puser)) {echo $guia->inactive_guias;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Guias Pendientes</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center bg-warning"><i class="icon-docs"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($puser)) {echo $guia->tomadas_guias;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Guias Tomadas</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>

    <div class="row">
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round align-self-center round-success"><i class="icon-login"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h2 class="m-b-0"><?php if(!empty($toma)) {echo $toma->total;}else{echo 0;} ?></h2>
                                <h5 class="text-muted m-b-0">Total de Tomas</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round align-self-center bg-primary"><i class="icon-login"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h2 class="m-b-0"><?php if(!empty($toma)) {echo $toma->active_tomas;}else{echo 0;} ?></h2>
                                <h5 class="text-muted m-b-0">Tomas Autorizadas</h5></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row"> 
                            <div class="round align-self-center bg-danger"><i class="icon-login"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h2 class="m-b-0"><?php if(!empty($toma->inactive_tomas)) {echo $toma->inactive_tomas;}else{echo 0;} ?></h2>
                                <h5 class="text-muted m-b-0">Tomas Pendientes</h5></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round align-self-center bg-warning"><i class="icon-login"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h2 class="m-b-0"><?php if(!empty($toma->tomadas_tomas)) {echo $toma->tomadas_tomas;}else{echo 0;} ?></h2>
                                <h5 class="text-muted m-b-0">Guias Tomadas</h5></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>

    <div class="row">
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center round-success"><i class="icon-book-open"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($check)) {echo $check->total;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Total de Checklist</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center bg-primary"><i class="icon-book-open"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($check->pendientes_checks)) {echo $check->pendientes_checks;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Checklist Pendientes</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row"> 
                        <div class="round align-self-center bg-danger"><i class="icon-book-open"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($check->enproceso_checks)) {echo $check->enproceso_checks;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Checklist en Proceso</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center bg-warning"><i class="icon-book-open"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($check->terminados_checks)) {echo $check->terminados_checks;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Checklist Terminados</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>










    <div class="row">
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center round-success"><i class="icon-people"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($user)) {echo $user->total;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Total Usuarios</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center bg-primary"><i class="icon-user-following"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($puser)) {echo $puser->active_user;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Usuarios Activos</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center bg-danger"><i class="icon-user-unfollow"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($puser)) {echo $puser->inactive_user;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Usuarios Inactivos</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center bg-warning"><i class="icon-user"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h2 class="m-b-0"><?php if(!empty($puser)) {echo $puser->expire_user;}else{echo 0;} ?></h2>
                            <h5 class="text-muted m-b-0">Usuarios Expirados</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>

<!--  
    <div class="row">
    </div>
  -->
  </section>

</div>