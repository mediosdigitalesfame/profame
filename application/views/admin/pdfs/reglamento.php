<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','IB',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,4,utf8_decode("Carta de no retención de ISR"),0,0,'L',1);
            $this->setFont('Arial','I',9); 
            $this->cell(105,4,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),205,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 2.5,'30','15','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($toma){
        $this->Ln(5);     
        $textypos = 5;

        $this->SetFont('Arial', 'B',14);
        $this->setY(18);$this->Multicell(0,$textypos,utf8_decode("REGLAMENTO COMPRA DE UNIDADES USADAS"),0,"C");

        $this->SetFont('Arial', '',10);
        $this->setY(25);$this->Multicell(0,$textypos,utf8_decode("1.0 OBJETIVO:"),0,"J");
        $this->setY(29);$this->Multicell(0,$textypos,utf8_decode("Asegurar que los expedientes emitidos por las agencias de Grupo Fame para la compra de unidades usadas sean alineados de acuerdo a lo estipulado en este reglamento."),0,"J");

        $this->setY(40);$this->Multicell(0,$textypos,utf8_decode("2.0 ALCANCE:"),0,"J");
        $this->setY(44);$this->Multicell(0,$textypos,utf8_decode("Aplicable a todas las agencias y áreas de Grupo FAME que participen en el Proceso de la Compra de Unidades Usadas."),0,"J");

        $this->setY(51);$this->Multicell(0,$textypos,utf8_decode("3.0 POLÍTICA APLICABLE, NORMATIVIDAD, LEYES, REGLAMENTOS Y CIRCULARES:"),0,"J");
        $this->setY(55);$this->Multicell(0,$textypos,utf8_decode("En toda compra de auto usado se deberá tener un expediente, el cual contendrá sin excepción los siguientes documentos: "),0,"J");
        $this->setY(60);$this->Multicell(0,$textypos,utf8_decode("1)    FACTURA DE COMPRA DE UNIDADES USADAS: \n   1.1.              Para Personas Físicas sin Actividad Empresarial (no facturan directamente)"),0,"J");

        $this->setY(71);$this->Multicell(0,$textypos,utf8_decode("i)      Tener el Formato Solicitud de CFDI (ANEXO A) firmado por el proveedor conteniendo los datos del mismo, en el cual autoriza la expedición del Comprobante Fiscal Digital (CFDI), por la venta del vehículo usado.) \nii)   Enviar Formato Solicitud de CFDI (ANEXO A) acompañada de los documentos que comprueben los datos del proveedor (nombre, RFC, régimen fiscal, dirección, etc. \niii)    Tener la factura original y el endoso por el último propietario regularizado (cambios de propietario regularizados ante la Secretaría de Finanzas).)."),0,"J");

        $this->setY(105);$this->Multicell(0,$textypos,utf8_decode("a)    Solo deben aceptar facturas endosadas que no tengan el IVA desglosado en la factura que ampara la compra del vehículo o que tengan el IVA desglosado con RFC genérico (XAXX010101000). \nb) Con RFC sin homoclave (ROMM9002929_ _ _), o con RFC completo pero cuyo último propietario tribute en el régimen de Arrendamiento, Ingresos por Dividendos, ingresos por intereses o bien sueldos y salarios comprobando con la Guía de obligaciones (FL-GF-CMP-013). (Art. 9 Frac. IV de LIVA y Art. 20 de RLIVA). Esto se demuestra con la guía de obligaciones emitida por el SAT actualizada. \n\nNota: Todos los clientes aplicables al punto 1.1. Para personas Físicas sin Actividad Empresarial o Sin actividad Profesional sin RFC, enviar la Ratificación del RFC. FL-GF-CMP-019 \nLa Ratificación se obtiene de la página www.SAT.gob.mx."),0,"J");

        $this->setY(158);$this->Multicell(0,$textypos,utf8_decode("iv)   Demostrar la propiedad de la unidad usada mediante la factura electrónica (CFD O CFDI) (FL-GF-CMP-001) en original y tener el archivo “XML” (FL-GF-CMP-002) debidamente validado ( FL-GF-CMP-017) (sello digital valido) y verificado (FL-GF-CMP-016 )(folio fiscal vigente) (a validación y verificación del XML se realiza en la página del SAT www.consulta.sat.gob.mx) para poder asegurar la autenticidad de la factura deberán contar con el escrito por el encargado de usados (ANEXO E) de que se confirmo la expedición de dicho documento con la entidad emisora. "),0,"J");

        $this->setY(186);$this->Multicell(0,$textypos,utf8_decode("1.2.        Para Personas Morales (S.A., S.A. De C.V., S. De R.L., SOCIEDADES COOPERATIVAS) y Personas Físicas con Actividad Empresarial (Régimen General de Ley, Régimen de incorporación Fiscal, Régimen de Agricultura, Ganadería, Pesca y Silvicultura) (facturan directamente):"),0,"J");
        
        $this->setY(206);$this->Multicell(0,$textypos,utf8_decode("i)             Tener la factura original de la empresa o persona física que nos está vendiendo (FL-GF-CMP-018) a nombre de la agencia que está comprando, con el IVA desglosado (Total de la unidad entre 1.16 igual a subtotal y este por el 16% igual a IVA en factura).  En caso de personas Físicas con Actividad Profesional (honorarios), pueden facturar sin el IVA desglosado sustentado con la Guía de Obligaciones Vigente emitida por el SAT en donde acredite su Régimen Fiscal)."),0,"J");
        $this->setY(230);$this->Multicell(0,$textypos,utf8_decode("ii)            Tener Archivo “XML” (FL-GF-CMP-002) debidamente validado (sello digital valido) y verificado (folio fiscal vigente). La validación (FL-GF-CMP-017)  y  verificación  (FL-GF-CMP-016) del XML se realiza en la página del SAT www.consulta.sat.gob.mx. No se debe tomar una unidad usada con factura electrónica si no cuenta con el archivo XML  (FL-GF-CMP-002)  (el PDF de la factura debe ser una copia fiel del archivo XML)."),0,"J");
                
        $this->AddPage('','','Letter');
        $this->setFont('Arial','I',9);
        $this->setFillColor(255,255,255);
        $this->Line(10,$this->GetY(),200,$this->GetY());
        $this->Image(base_url().'assets/porto/img/logo.png', 85, 1,'40','20','png','http:grupofame.com');
        $this->Ln(5);


        $this->SetFont('Arial', '',10);
        $this->setY(20);$this->Multicell(0,$textypos,utf8_decode("Nota: Están prohibidas las compras entre agencias de usados (si las unidades usadas fueron compradas a Personas Físicas por la agencia que vende, ya que solo desglosará el IVA sobre la utilidad) así como a  PERSONAS MORALES DE NATURALEZA NO EMPRESARIAL (Asociaciones o Sociedades Civiles, Dependencias de Gobierno, Instituciones de Asistencia Pública y Organismos Descentralizados), estas personas morales al vender la unidad no causan el IVA, pero si nos obliga al momento de venderlo, causar el IVA sobre el total de la venta, lo que representa una pérdida operativa."),0,"J");

        #$this->setY(40);$this->Multicell(0,$textypos,utf8_decode("2.0 ALCANCE:"),0,"J");
        $this->setY(47);$this->Multicell(0,$textypos,utf8_decode("2.    CARTA DE NO RETENCIÓN           "),0,"J");

        $this->setY(52);$this->Multicell(0,$textypos,utf8_decode("2.1.        Para Personas Físicas con Actividad Empresarial, cuando la compra de unidad es mayor a $227,400.00, deberán firmar la CARTA DE NO RETENCIÓN DE ISR PERSONAS FISICAS CON ACTIVIDAD EMPRESARIAL. (ANEXO L)"),0,"J");

        $this->setY(63);$this->Multicell(0,$textypos,utf8_decode("2.2.        Para Personas Físicas sin Actividad Empresarial, cuando la compra de unidad es mayor a $227,400.00, se deberá firmar la CARTA DE NO RETENCIÓN DE ISR PERSONAS FISICAS SIN ACTIVIDAD EMPRESARIAL (ANEXO K)"),0,"J");

        $this->setY(74);$this->Multicell(0,$textypos,utf8_decode("3.    SECUENCIA DE FACTURAS DEL VEHÍCULO (copia) (FL-GF-CMP-004), desde la agencia que vendió la unidad como nueva. "),0,"J");

        $this->setY(85);$this->Multicell(0,$textypos,utf8_decode("4.    IDENTIFICACIÓN OFICIAL del proveedor vigente (copia) (FL-GF-CMP-005) \ni)              En caso de persona moral: Identificación de representante legal y Acta notarial Carta Poder Notarial. "),0,"J");

        $this->setY(96);$this->Multicell(0,$textypos,utf8_decode("5.    CURP (copia) ( FL-GF-CMP-006) \n6.    COMPROBANTE DE DOMICILIO no mayor a tres meses (copia) (CFE, Telmex, recibo de agua, Estado de Cuenta Bancario). (FL-GF-CMP-007)"),0,"J");
        
        $this->setY(111);$this->Multicell(0,$textypos,utf8_decode("7.    BAJA DE PLACAS a nombre del último dueño. (FL-GF-CMP-008)"),0,"J");

        $this->setY(117);$this->Multicell(0,$textypos,utf8_decode("8.    TODAS LAS TENENCIAS DESDE QUE SE VENDIÓ COMO NUEVO (Copia) (FL-GF-CMP-009). Verificar que las tenencias no sean apócrifas."),0,"J");

        $this->setY(127);$this->Multicell(0,$textypos,utf8_decode("9.    TARJETA DE CIRCULACIÓN DEL ÚLTIMO DUEÑO (copia)  (FL-GF-CMP-012)"),0,"J");

        $this->setY(132);$this->Multicell(0,$textypos,utf8_decode("10.  Carta responsiva al comprar (ANEXO B) donde el vendedor se hace responsable de la autenticidad de los documentos, que sirven de base para esta transacción (factura original, endosos, tenencias, baja, etc.), asegurando que el vehículo no tiene problemas legales, fiscales o de procedencia, en esta responsiva se deberá mencionar que el vendedor recibe el pago de la cuenta del comprador y deberá recabar también copia de la identificación oficial con firma, la cual debe coincidir con la de la responsiva."),0,"J");

        $this->setY(157);$this->Multicell(0,$textypos,utf8_decode("11.  Contrato de compra venta (ANEXO C) debidamente firmado por el Gerente General de la Empresa y el proveedor."),0,"J");

        $this->setY(162);$this->Multicell(0,$textypos,utf8_decode("12.  AVISO DE PRIVACIDAD (ANEXO D) firmado por parte del proveedor de la unidad usada (solo aplica para personas físicas)."),0,"J");

        $this->setY(172);$this->Multicell(0,$textypos,utf8_decode("13.  AVALÚO DE AUTOS USADOS (ANEXO F) firmado por el gerente de usados, Gerente de Ventas o Gerente de Sucursal (en su caso) y Gerente General. "),0,"J");

        $this->setY(182);$this->Multicell(0,$textypos,utf8_decode("13.1.      En el avalúo deberá constar el precio de compra de la guía EBC o de la guía Auto métrica (el autorizado por la marca), mencionando de que fecha es dicha guía y nunca deberá exceder el precio de compra de la misma. En caso contrario deberá estar autorizado por el Director Regional. "),0,"J");

        $this->setY(197);$this->Multicell(0,$textypos,utf8_decode("14.   EVALUACIÓN MECÁNICA (ANEXO H) firmado por el gerente de usados, Gerente de Ventas o Gerente de Sucursal (en su caso) y Gerente General."),0,"J");

        $this->setY(207);$this->Multicell(0,$textypos,utf8_decode("15.  GUÍA AUTO MÉTRICA O EBC (copia) (FL-GF-CMP-010)  (anotar el mes al que corresponde la copia de la guía). En caso de que la compra rebase el monto máximo marcado en la Guía EBC, se deberá tener la autorización del Director de Marca para que se pueda realizar la compra."),0,"J");

        $this->setY(222);$this->Multicell(0,$textypos,utf8_decode("16.  PÓLIZA DE RECEPCIÓN DE LA UNIDAD USADA (ANEXO J)"),0,"J");

        $this->setY(227);$this->Multicell(0,$textypos,utf8_decode("17.  CARTERA DE CUENTAS POR PAGAR (ANEXO G)"),0,"J");

        $this->setY(232);$this->Multicell(0,$textypos,utf8_decode("18.  FORMATO DE DACIÓN EN PAGO, CUANDO APLIQUE. (ANEXO M)"),0,"J");

        $this->setY(237);$this->Multicell(0,$textypos,utf8_decode("19.  IMPRESIÓN DE CONSULTA SIN REPORTE DE ROBO  (FL-GF-CMP-011)"),0,"J");

        $this->setY(242);$this->Multicell(0,$textypos,utf8_decode("-       www2.repuve.gob.mx:/8080/ciudadania/ "),0,"J");

        $this->setY(247);$this->Multicell(0,$textypos,utf8_decode("-       rapi.pgjdf.gob.mx"),0,"J");

        $this->setY(252);$this->Multicell(0,$textypos,utf8_decode("-       www.amda.mx"),0,"J");

        $this->AddPage('','','Letter');
        $this->setFont('Arial','I',9);
        $this->setFillColor(255,255,255);
        $this->Line(10,$this->GetY(),200,$this->GetY());
        $this->Image(base_url().'assets/porto/img/logo.png', 85, 1,'40','20','png','http:grupofame.com');
        $this->Ln(5);

        $this->setY(20);$this->Multicell(0,$textypos,utf8_decode("20.  COPIA DEL CHEQUE DE PROVEEDOR (S).  (FL-GF-CMP-014)"),0,"J");

        $this->setY(25);$this->Multicell(0,$textypos,utf8_decode("21.  SOLICITUD DE CHEQUE (ANEXO I)"),0,"J");

        $this->setY(30);$this->Multicell(0,$textypos,utf8_decode("22.  ESTADO DE CUENTA (FL-GF-CMP-015)"),0,"J");

        $this->setY(40);$this->Multicell(0,$textypos,utf8_decode("5.0.  REGISTROS DE CALIDAD "),0,"J");

        $this->setY(45);$this->Multicell(0,$textypos,utf8_decode("5.1. Registros de calidad Internos"),0,"J");

        $this->setY(50);$this->Multicell(0,$textypos,utf8_decode("Anexo A"),0,"J");
        $this->setY(50);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Solicitud CFDI"),0,"J");

        $this->setY(55);$this->Multicell(0,$textypos,utf8_decode("Anexo B"),0,"J");
        $this->setY(55);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Carta responsiva de compra venta"),0,"J");

        $this->setY(60);$this->Multicell(0,$textypos,utf8_decode("Anexo C"),0,"J");
        $this->setY(60);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Contrato de compraventa"),0,"J");

        $this->setY(65);$this->Multicell(0,$textypos,utf8_decode("Anexo D"),0,"J");
        $this->setY(65);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Aviso de privacidad integral"),0,"J");

        $this->setY(70);$this->Multicell(0,$textypos,utf8_decode("Anexo E"),0,"J");
        $this->setY(70);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Autentificación de documentos"),0,"J");

        $this->setY(75);$this->Multicell(0,$textypos,utf8_decode("Anexo F"),0,"J");
        $this->setY(75);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Avalúo rápido"),0,"J");

        $this->setY(80);$this->Multicell(0,$textypos,utf8_decode("Anexo G"),0,"J");
        $this->setY(80);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Cartera de cuentas por pagar"),0,"J");

        $this->setY(85);$this->Multicell(0,$textypos,utf8_decode("Anexo H"),0,"J");
        $this->setY(85);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Evaluación mecánica"),0,"J");

        $this->setY(90);$this->Multicell(0,$textypos,utf8_decode("Anexo I"),0,"J");
        $this->setY(90);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Solicitud de cheque"),0,"J");

        $this->setY(95);$this->Multicell(0,$textypos,utf8_decode("Anexo J"),0,"J");
        $this->setY(95);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Póliza de recepción de la unidad usada"),0,"J");

        $this->setY(100);$this->Multicell(0,$textypos,utf8_decode("Anexo K"),0,"J");
        $this->setY(100);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Carta de no retención de ISR personas físicas sin actividad empresarial"),0,"J");

        $this->setY(105);$this->Multicell(0,$textypos,utf8_decode("Anexo L"),0,"J");
        $this->setY(105);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Carta de no retención de ISR personas físicas con actividad empresarial"),0,"J");

        $this->setY(110);$this->Multicell(0,$textypos,utf8_decode("Anexo M"),0,"J");
        $this->setY(110);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Formato de Dación en pago"),0,"J");

        $this->setY(120);$this->Multicell(0,$textypos,utf8_decode("5.2. Registros de calidad externos"),0,"J");

        $this->setY(125);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-001"),0,"J");
        $this->setY(125);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("CFDI factura persona física sin actividad empresarial"),0,"J");

        $this->setY(130);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-002"),0,"J");
        $this->setY(130);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("XML de CFDI"),0,"J");

        $this->setY(135);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-003"),0,"J");
        $this->setY(135);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Factura de proveedor"),0,"J");

        $this->setY(140);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-004"),0,"J");
        $this->setY(140);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Secuencia de factura del auto"),0,"J");

        $this->setY(145);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-005"),0,"J");
        $this->setY(145);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Identificación oficial"),0,"J");

        $this->setY(150);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-006"),0,"J");
        $this->setY(150);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("CURP"),0,"J");

        $this->setY(155);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-007"),0,"J");
        $this->setY(155);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Comprobante de domicilio"),0,"J");

        $this->setY(160);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-008"),0,"J");
        $this->setY(160);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Baja de placas"),0,"J");

        $this->setY(165);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-009"),0,"J");
        $this->setY(165);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Historial de tenencias"),0,"J");

        $this->setY(170);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-010"),0,"J");
        $this->setY(170);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Guía autométrica"),0,"J");

        $this->setY(175);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-011"),0,"J");
        $this->setY(175);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Consulta sin reporte de robo"),0,"J");

        $this->setY(180);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-012"),0,"J");
        $this->setY(180);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Tarjeta de circulación"),0,"J");

        $this->setY(185);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-013)"),0,"J");
        $this->setY(185);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Cedula de identificación fiscal"),0,"J");

        $this->setY(190);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-014"),0,"J");
        $this->setY(190);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Copia de cheque de proveedor"),0,"J");

        $this->setY(195);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-015"),0,"J");
        $this->setY(195);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Estado de cuenta "),0,"J");

        $this->setY(200);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-016"),0,"J");
        $this->setY(200);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Verificación fiscal de CFDI"),0,"J");

        $this->setY(205);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-017"),0,"J");
        $this->setY(205);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Validación fiscal de CFDI"),0,"J");

        $this->setY(210);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-018"),0,"J");
        $this->setY(210);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("CFDI factura persona moral"),0,"J");
        
        $this->setY(215);$this->Multicell(0,$textypos,utf8_decode("FL-GF-CMP-019"),0,"J");
        $this->setY(215);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("Ratificación de RFC"),0,"J");

        $this->setY(225);$this->Multicell(0,$textypos,utf8_decode("-       Formatos de Compra Integral"),0,"J");

        #$this->SetFont('Arial','',10);
        #$this->setY(242);$this->Cell(105,$textypos,"________________________________",0,1,"C");
        #$this->setY(249);$this->Cell(105,$textypos,utf8_decode("La firma deberá ser igual a la de la Identificación."),0,1,"C");

        #$this->setY(242);$this->Cell(290,$textypos,"________________________________",0,1,"C");	        

        #$this->SetFont('Arial','B',9);    
        
        #$this->setY(246);$this->Cell(105,$textypos,$toma[0]['nombre'],0,1,"C");

        #$this->setY(246);$this->Cell(290,$textypos,$toma[0]['arsocial'],0,1,"C");

        $this->setY(60);$this->setX(135);
        $this->Ln();           
    }

    function Footer(){
        $this->SetY(-17);
        $this->Line(10,$this->GetY(),205,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($toma);
$pdf->Output();
