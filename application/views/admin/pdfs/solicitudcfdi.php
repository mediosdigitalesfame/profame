<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','IB',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,4,"Solicitud CFDI",0,0,'L',1);
            $this->setFont('Arial','I',9); 
            $this->cell(105,4,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),205,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 2.5,'30','15','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($toma){
        //$this->Cell(20,10,html_escape($toma[0]['facturavehiculo']),0,1,'C');

        //$this->Cell(400,10,html_escape($toma[0]['nameagency']),0,1,'C');
        $this->Ln(5);     
        $textypos = 5;

        $this->setY(15);$this->setX(185);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechacontrato'])));

        $this->SetFont('Arial', '',9);
        $this->setY(25);$this->Multicell(0,$textypos,utf8_decode("Autorización para la expedición del Comprobante Fiscal Digital (CFDI) por la enajenación de mi vehículo usado para solicitar en su caso mi inscripción al RFC."),0,"J");

        $this->setY(35);$this->Multicell(0,$textypos,utf8_decode("Por este conducto y bajo protesta de decir verdad les informo que No me encuentro dado de alta bajo el régimen de Actividad Empresarial, profesional o Régimen Incorporación Fiscal autorizo expresamente a:"),0,"J");

        $this->setY(52);$this->Multicell(0,$textypos,utf8_decode("Para que sus funcionarios facultados  lleven a cabo el tramite de expedición del Comprobante Fiscal Digital ( CFDI) de acuerdo a la reforma del Código Fiscal de la Federación que entro en vigor a partir del 1° de Enero de 2014, en lo concerniente a la enajenación de mi vehículo usado con las siguientes características:"),0,"J");

        $this->setY(84);$this->Cell(0,$textypos,utf8_decode("*CURP o copia del Acta de Nacimiento."),0,1,"C");
        $this->setY(89);$this->Cell(0,$textypos,utf8_decode("*Comprobante de domiciliario."),0,1,"C");
        $this->setY(94);$this->Cell(0,$textypos,utf8_decode("*Copia del Registro Federal de Contribuyentes."),0,1,"C");
        $this->setY(99);$this->Cell(0,$textypos,utf8_decode("*Identificación Oficial (IFE o Pasaporte vigente."),0,1,"C");

        $this->setY(67);$this->setX(35);$this->Cell(5,$textypos,utf8_decode($toma[0]['marcavehi']));
        $this->setY(67);$this->setX(85);$this->Cell(5,$textypos,utf8_decode($toma[0]['aniomodelo']));
        //$this->setY(75);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['aniomodelo']modelovehi);

        $this->setY(72);$this->Multicell(0,$textypos,utf8_decode("y en su caso tramitar mi inscripción ante el SAT para la obtención de mi RFC."),0,"J");

        $this->setY(105.5);$this->Multicell(0,$textypos,utf8_decode("Datos del proveedor (Persona Física sin actividad empresarial, profesional o en régimen de Incorporación)"),0,"J");

        $this->setY(118);$this->setX(55);$this->Cell(5,$textypos,utf8_decode($toma[0]['nombrescfdi']));
        $this->setY(123);$this->setX(55);$this->Cell(5,$textypos,utf8_decode($toma[0]['apellido1']));
        $this->setY(128);$this->setX(55);$this->Cell(5,$textypos,utf8_decode($toma[0]['apellido2']));
        $this->setY(133);$this->setX(55);$this->Cell(5,$textypos, date("d-m-Y", strtotime($toma[0]['fechanaci'])));
        $this->setY(138);$this->setX(55);$this->Cell(5,$textypos,$toma[0]['curp']);
        $this->setY(143);$this->setX(55);$this->Cell(5,$textypos,utf8_decode($toma[0]['actividad']));
        $this->setY(148);$this->setX(55);$this->Cell(5,$textypos,$toma[0]['srfc']);
        $this->setY(153);$this->setX(55);$this->Cell(5,$textypos,$toma[0]['tel']);
		$this->setY(158);$this->setX(55);$this->Cell(5,$textypos,$toma[0]['cel']);
		

		$this->setY(172);$this->Multicell(0,$textypos,utf8_decode("Firmo de conformidad y declaro bajo protesta de decir verdad que los datos y documentación proporcionada son verdaderos."),0,"J");

		//$this->setY(19);$this->setX(15);$this->Cell(5,$textypos,"Nombre Completo:");
        $this->setY(212);$this->Cell(0,$textypos,"________________________________",0,1,"C");
        $this->setY(219);$this->Cell(0,$textypos,utf8_decode("La firma deberá ser igual a la de la Identificación."),0,1,"C");

		$this->setY(118);$this->setX(148);$this->Cell(5,$textypos,utf8_decode($toma[0]['scalle']));
		$this->setY(123);$this->setX(148);$this->Cell(5,$textypos,$toma[0]['numext']);
		$this->setY(128);$this->setX(148);$this->Cell(5,$textypos,utf8_decode($toma[0]['scolonia']));
		$this->setY(133);$this->setX(148);$this->Cell(5,$textypos,$toma[0]['scp']);
		$this->setY(138);$this->setX(148);$this->Cell(5,$textypos,utf8_decode($toma[0]['sciudad']));
		$this->setY(143);$this->setX(148);$this->Cell(5,$textypos,utf8_decode($toma[0]['sestado']));
		$this->setY(148);$this->setX(148);$this->Cell(5,$textypos,utf8_decode($toma[0]['pais']));
		$this->setY(153);$this->setX(148);$this->Cell(5,$textypos,utf8_decode($toma[0]['email']));

        $this->SetFont('Arial', '',9);
        $this->setY(67);$this->setX(158);$this->Cell(5,$textypos,$toma[0]['nserie']);

        $this->SetFont('Arial', 'B',8);
        $this->setY(226);$this->Multicell(0,3,utf8_decode("RMF 2020 2.4.3; Para los efectos del articulo 27 del CFF, podrán inscribirse en el RFC a través de los adquirientes de sus productos o de los contribuyentes a los que les otorgue el uso o gose, de conformidad con el procedimiento que se señala en la pagina del internet del SAT, los contribuyentes personas fisicas que: IV. Enajenen vehiculos usados, con excepción de aquellas que contribuyen en los términos de las secciones I y II, del Capitulo II del Titulo IV de la Ley del ISR (en los Regímenes de las Personas Físicas con Actividades Empresariales y Profesionales y el de Incorporación Fiscal)."),0,"J");

        $this->SetFont('Arial', '',8);
        $this->setY(241);$this->Multicell(0,3,utf8_decode("...Los contribuyentes que opten por aplicar lo dispuesto en esta regla, deberán proporcionar a dichos adquirientes o a sus arrendatarios, así como a las agricolas, pecuarios, acuicolas o pesqueros de un sistema Producto según sea el caso, lo siguiente: \nA) Nombre. \nB) Curp o copia del acta de nacimiento. \nC) Actividad preponderante que realiza. \nD) Domicilio Fiscal."),0,"J");

        $this->SetFont('Arial','B',9);
        $this->setY(15);$this->setX(156);$this->Cell(5,$textypos,"Fecha de Contrato:");
        
        $this->setY(20);$this->Multicell(0,3,"Asunto:",0,"J");
        $this->setY(46);$this->setX(75);$this->Cell(5,$textypos,utf8_decode($toma[0]['arsocial']));

        $this->setY(67);$this->setX(10);$this->Cell(5,$textypos,"Marca Vehiculo:");
        $this->setY(67);$this->setX(72);$this->Cell(5,$textypos,"Modelo:");
        //$this->setY(75);$this->setX(100);$this->Cell(5,$textypos,utf8_decode(", Año Modelo:"));
        $this->setY(67);$this->setX(110);$this->Cell(5,$textypos, utf8_decode("No. de identificación vehicular:"));

        $this->setY(113);$this->setX(15);$this->Cell(5,$textypos,"Datos proveedor");
        $this->setY(118);$this->setX(15);$this->Cell(5,$textypos,"Nombre(s):");
        $this->setY(123);$this->setX(15);$this->Cell(5,$textypos,"Primer Apellido:");
        $this->setY(128);$this->setX(15);$this->Cell(5,$textypos,"Segundo Apellido:");
        $this->setY(133);$this->setX(15);$this->Cell(5,$textypos,"Fecha Nacimiento:");
        $this->setY(138);$this->setX(15);$this->Cell(5,$textypos,"Curp:");
        $this->setY(143);$this->setX(15);$this->Cell(5,$textypos,"Actividad Preponderante:");
        $this->setY(148);$this->setX(15);$this->Cell(5,$textypos,"RFC:");
        $this->setY(153);$this->setX(15);$this->Cell(5,$textypos,"Telefono Fijo:");
        $this->setY(158);$this->setX(15);$this->Cell(5,$textypos,"Telefono Movil:");

        $this->setY(113);$this->setX(110);$this->Cell(5,$textypos,"Domicilio");
        $this->setY(118);$this->setX(110);$this->Cell(5,$textypos,"Calle:");
        $this->setY(123);$this->setX(110);$this->Cell(5,$textypos,"Numero Ext:");
        $this->setY(128);$this->setX(110);$this->Cell(5,$textypos,"Colonia:");
        $this->setY(133);$this->setX(110);$this->Cell(5,$textypos,"Codigo Postal:");
        $this->setY(138);$this->setX(110);$this->Cell(5,$textypos,"Localidad o municipio: ");
        $this->setY(143);$this->setX(110);$this->Cell(5,$textypos,"Entidad Federativa:");
        $this->setY(148);$this->setX(110);$this->Cell(5,$textypos,"Pais:");
        $this->setY(153);$this->setX(110);$this->Cell(5,$textypos,"Correo Electronico:");

        $this->setY(165);$this->Multicell(0,$textypos,utf8_decode("Todos los datos son obligatorios."),0,"J");
        
        $this->setY(216);$this->Cell(0,$textypos,$toma[0]['nombre'],0,1,"C");


        
        $this->SetFont('Arial','B',12);
        $this->setY(79);$this->setX(35);$this->Multicell(0, 4, utf8_decode("Por tal motivo proporciono la siguiente información y documentación."));
  

        $this->setY(60);$this->setX(135);
        $this->Ln();           
    }

    function Footer(){
        $this->SetY(-17);
        $this->Line(10,$this->GetY(),205,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($toma);
$pdf->Output();
