<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','IB',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,4,utf8_decode("Carta Responsiva de Compra Venta"),0,0,'L',1);
            $this->setFont('Arial','I',9); 
            $this->cell(105,4,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),205,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 2.5,'30','15','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($toma){
        $this->Ln(5);     
        $textypos = 5;

        $this->SetFont('Arial', 'B',14);
        $this->setY(20);$this->Multicell(0,$textypos,utf8_decode("CARTA RESPONSIVA DE COMPRA VENTA."),0,"C");

        $this->SetFont('Arial', '',10);
        $this->setY(27);$this->Multicell(0,$textypos,utf8_decode("Reunidos por una parte el comprador ".$toma[0]['arsocial']." con domicilio fiscal: BLVD. ADOLFO RUIZ CORTINEZ 4000 BIS, JARDINES DEL PEDREGAL C.P. 01900, CIUDAD DE MÉXICO. En la ciudad de ".$toma[0]['aciudad']."."),0,"J");

        $this->setY(40);$this->Multicell(0,$textypos,utf8_decode("Reunidos por una parte el comprador ".$toma[0]['nombre'].", con domicilio en ".$toma[0]['scalle']." número ".$toma[0]['numext'].", colonia ".$toma[0]['scolonia'].", ".$toma[0]['municipio']." en el estado de ".$toma[0]['sestado'].", código postal ".$toma[0]['scp']." se identificó con identificación oficial: ".$toma[0]['nidenti'].". se acordó elaborar una carta responsiva al tenor de las siguientes cláusulas:"),0,"J");

        $this->setY(60);$this->Multicell(0,$textypos,utf8_decode("1a. El vendedor entrega en este acto al comprador los documentos y el automóvil que a continuación se describe:"),0,"J");

        $this->setY(102);$this->Multicell(0,$textypos,utf8_decode("Tenencias por los años: ".$toma[0]['tenencias']." baja de placas, verificación. Quien manifiesta que este automóvil"),0,"J");

        $this->setY(112);$this->Multicell(0,$textypos,utf8_decode("es de su propiedad y lo vende:                     ".$toma[0]['arsocial']."                                  en la cantidad de:"),0,"J"); 


        $this->setY(119);$this->setX(35);$this->Cell(5,$textypos,"$".number_format($toma[0]['valorcompra'],2,".",","),'0',0,'L');
        $this->setY(120);$this->Cell(5,$textypos,"________________________________");

        $this->setY(119);$this->setX(135);$this->Cell(5,$textypos,"(".utf8_decode($toma[0]['valorletra'])." 00/100 M.N)",0,1,"C");
        $this->setY(120);$this->setX(105);$this->Cell(5,$textypos,"________________________________");

        $this->setY(125);$this->Multicell(0,$textypos,utf8_decode("Entregando la documentación antes descrita."),0,"J");

        $this->setY(135);$this->Multicell(0,$textypos,utf8_decode("2a. Declaro expresamente bajo protesta de decir verdad, que el vehículo que les he vendido lo adquirí legalmente y lo entrego  completamente  libre  de  cualquier  gravamen  o vicio, constituyéndome en único responsable de su saneamiento,  solventaré todo  problema  que  se  presente  respecto  de  su  legítima  propiedad, por su legal y definitiva estancia en el país o por cualquier responsabilidad de carácter civil, penal o  fiscal  que  por lo mismo resultare hasta la fecha, por  lo que, en su caso me obligo expresamente a efectuar de inmediato la devolución del dinero que he recibido independientemente de la liquidación y reparación del monto de los daños y perjuicios que les causaren con tal motivo, así como el de las reparaciones hechas al vehículo."),0,"J");

        $this->setY(175);$this->Multicell(0,$textypos,utf8_decode("3a. Las partes manifiestan su absoluta conformidad con las cláusulas que anteceden, firmando al calce para constancia."),0,"J");

        $this->setY(185);$this->Multicell(0,$textypos,utf8_decode("OBSERVACION:"),0,"J");

        OBSERVACION:
         
        $this->SetFont('Arial', 'B',11);
        $this->setY(65);$this->Multicell(0,$textypos,utf8_decode("Datos del Vehículo:"),0,"J");

        $this->setY(70);$this->setX(25);$this->Cell(5,$textypos,"Marca Vehiculo:");
        $this->setY(75);$this->setX(25);$this->Cell(5,$textypos,utf8_decode("Año Modelo:"));
        $this->setY(80);$this->setX(25);$this->Cell(5,$textypos,"Color Exterior:");
        $this->setY(85);$this->setX(25);$this->Cell(5,$textypos,"No. Motor:");
        $this->setY(90);$this->setX(25);$this->Cell(5,$textypos,"Color Interior:");
        $this->setY(95);$this->setX(25);$this->Cell(5,$textypos,utf8_decode("Fecha adquisición:"));

        $this->setY(70);$this->setX(100);$this->Cell(5,$textypos,"Modelo:");
        $this->setY(75);$this->setX(100);$this->Cell(5,$textypos,"Tipo:");
        $this->setY(80);$this->setX(100);$this->Cell(5,$textypos,utf8_decode("Número de serie:"));
        $this->setY(85);$this->setX(100);$this->Cell(5,$textypos,"Placas:");
        $this->setY(90);$this->setX(100);$this->Cell(5,$textypos,"Factura:");
        $this->setY(95);$this->setX(100);$this->Cell(5,$textypos,"Expedida por:");

        $this->SetFont('Arial','U',10);
        $this->setY(70);$this->setX(65);$this->Cell(5,$textypos,utf8_decode($toma[0]['marcavehi']));
        $this->setY(75);$this->setX(65);$this->Cell(5,$textypos,$toma[0]['aniomodelo']);
        $this->setY(80);$this->setX(65);$this->Cell(5,$textypos,utf8_decode($toma[0]['colorext']));
        $this->setY(85);$this->setX(65);$this->Cell(5,$textypos,$toma[0]['nmotor']);
        $this->setY(90);$this->setX(65);$this->Cell(5,$textypos,utf8_decode($toma[0]['colorint']));
        $this->setY(95);$this->setX(65);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechafac'])));

        $this->setY(70);$this->setX(135);$this->Cell(0,$textypos,utf8_decode($toma[0]['modelovehi']));
        $this->setY(75);$this->setX(135);$this->Cell(0,$textypos,$toma[0]['versionvehi']);
        $this->setY(80);$this->setX(135);$this->Cell(0,$textypos,$toma[0]['nserie']);
        $this->setY(85);$this->setX(135);$this->Cell(5,$textypos,$toma[0]['placasbaja']);
        $this->setY(90);$this->setX(135);$this->Cell(5,$textypos,$toma[0]['facturavehi']);
        $this->setY(95);$this->setX(135);$this->Cell(5,$textypos,utf8_decode($toma[0]['expedida']));


        $this->SetFont('Arial','',10);
        $this->setY(242);$this->Cell(60,$textypos,"___________________________",0,1,"C");

        $this->setY(242);$this->Cell(195,$textypos,"___________________________",0,1,"C");

        $this->setY(242);$this->Cell(330,$textypos,"____________________________",0,1,"C");	        

        $this->SetFont('Arial','B',9);   
        
        $this->setY(246);$this->Cell(60,$textypos,$toma[0]['nombre'],0,1,"C");

        $this->setY(246);$this->Cell(195,$textypos,$toma[0]['ausers_id_gg'],0,1,"C");

        $this->setY(246);$this->Cell(330,$textypos,$toma[0]['ausers_id_gs'],0,1,"C");


        $this->setY(250);$this->cell(60,$textypos,utf8_decode("Vendedor"),0,1,"C");

        $this->setY(250);$this->cell(195,$textypos,utf8_decode("Gerente General"),0,1,"C");

        $this->setY(250);$this->cell(330,$textypos,utf8_decode("Gerente Seminuevos"),0,1,"C");

        $this->setY(60);$this->setX(135);
        $this->Ln();           
    }

    function Footer(){
        $this->SetY(-17);
        $this->Line(10,$this->GetY(),205,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($toma);
$pdf->Output();
