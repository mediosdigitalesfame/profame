<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

  function Header(){
    if ($this->PageNo() == 1){
      $this->setFont('Arial','I',9);
      $this->setFillColor(255,255,255);
      $this->cell(90,6,utf8_decode("Guía Rápida"),0,0,'L',1);
      $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
      $this->Line(10,$this->GetY(),200,$this->GetY());
      $this->Image(base_url().'assets/images/logo.png', 85, 1,'40','20','png','http:grupofame.com');
      $this->Ln(5);
    }
    else{
      $this->setFont('Arial','I',9);
      $this->setFillColor(255,255,255);
      $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
      $this->cell(100,6,'',0,1,'R',1); 
      $this->Ln(2);
    }
  }

  function Content($toma){

    $this->SetFont('Arial','B',16);
    $this->Cell(80);
        //$this->Cell(20,10,html_escape($toma[0]['folio']),0,1,'C');

    $this->SetFont('Arial','B',20);    
    $textypos = 5;

    $this->SetFont('Arial','B',10);    
    $this->setY(35);$this->setX(10);
        //$this->Cell(5,$textypos,"Datos");
    $this->SetFont('Arial','B',13);
    
    $this->setY(20);$this->setX(140);$this->Cell(5,$textypos,"Folio:");
    $this->setY(20);$this->setX(155);$this->Cell(5,$textypos,utf8_decode($toma[0]['folio']));

    $this->SetFont('Arial','B',12);
    $this->setY(20);$this->setX(10);$this->Cell(5,$textypos,"Fecha:");
    $this->setY(20);$this->setX(24);$this->cell(5,$textypos,$toma[0]['created_at']);
    $this->setY(29);$this->setX(46);$this->Cell(5,$textypos,"Tipo Guia:");
    $this->setY(29);$this->setX(68);$this->Cell(5,$textypos,utf8_decode($toma[0]['guiapersona']));
    $this->setY(29);$this->setX(89);$this->Cell(5,$textypos,"/");
    $this->setY(29);$this->setX(91);$this->Cell(5,$textypos,utf8_decode($toma[0]['guiaversion']));

    $this->SetFont('Arial','',12);
        //PRIMER BLOQUE


    $this->setY(48);$this->setX(26);$this->Cell(5,$textypos,"Marca: ");
    $this->setY(48);$this->setX(40);$this->Cell(5,$textypos,$toma[0]['brandname']);

    $this->setY(55);$this->setX(26);$this->Cell(5,$textypos,"Modelo: ");
    $this->setY(55);$this->setX(42);$this->Cell(5,$textypos,$toma[0]['modelname']);

    $this->setY(62);$this->setX(26);$this->Cell(5,$textypos,"Kilometraje: ");
    $this->setY(62);$this->setX(49);$this->Cell(5,$textypos,number_format($toma[0]['km'],0,".",","),'0',0,'L');

        //$this->setY(53);$this->setX(10);$this->Cell(5,$textypos,"% de Utilidad:");
        //$this->setY(53);$this->setX(156);$this->Cell(5,$textypos,"%");
        //$this->setY(53);$this->setX(151);$this->Cell(5,$textypos,$toma[0]['pdudlv7']);

    $this->setY(75);$this->setX(26);$this->Cell(5,$textypos,"Compra Autometrica:");
    $this->setY(75);$this->setX(95);$this->Cell(5,$textypos,"$".number_format($toma[0]['compraautometrica'],2,".",","),'0',0,'R');

    $this->setY(75);$this->setX(117);$this->Cell(5,$textypos,"Venta Autometrica:");      
    $this->setY(75);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['ventaautometrica'],2,".",","),'0',0,'R');

    $this->setY(82);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Equipamiento:"));
    $this->setY(82);$this->setX(95);$this->Cell(5,$textypos,"$".number_format($toma[0]['equipamiento'],2,".",","),'0',0,'R');

    $this->setY(82);$this->setX(117);$this->Cell(5,$textypos,utf8_decode("Premio KM:"));       
    $this->setY(82);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['premio'],2,".",","),'0',0,'R');

    $this->setY(89);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Reacondicionamiento:"));
    $this->setY(89);$this->setX(95);$this->Cell(5,$textypos,"$".number_format($toma[0]['reacondicionamiento'],2,".",","),'0',0,'R');

    $this->setY(89);$this->setX(117);$this->Cell(5,$textypos,utf8_decode("Valor del Coche:"));
    $this->setY(89);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['valor'],2,".",","),'0',0,'R');

        //SEGUNDO BLOQUE
    $this->setY(112);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Precio de Venta sin Reacondicionamiento:"));
    $this->setY(112);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['pdvapsr1'],2,".",","),'0',0,'R');

    $this->setY(139);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Costo Real:"));
    $this->setY(139);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['crpdltsi3'],2,".",","),'0',0,'R');

    $this->setY(146);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Precio de Venta al Publico sin IVA:"));
    $this->setY(146);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['pdvapsi4'],2,".",","),'0',0,'R');

    $this->setY(160);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Utilidad de la Venta:"));
    $this->setY(160);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['udlv6'],2,".",","),'0',0,'R');

    $this->setY(167);$this->setX(26);$this->Cell(5,$textypos,"Utilidad:");
    $this->setY(167);$this->setX(161);$this->Cell(5,$textypos,$toma[0]['pdudlv7']."%");

    $this->setY(153);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Precio de Venta al Publico con IVA y Reacondicionamiento:"));
    $this->setY(153);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['pdvapciyr5'],2,".",","),'0',0,'R');

    $this->SetFont('Arial','B',13);
    $this->setY(40);$this->setX(26);$this->Cell(5,$textypos,"Datos ingresados:");
    $this->setY(105);$this->setX(26);$this->Cell(5,$textypos,"Datos del Sistema:");

        //BLOQUE NEGRITAS
    $this->SetFont('Arial','B',16);

    $this->setY(125);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Precio de la Toma tope al cliente con IVA:"));

    $this->setY(125);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['pdlttfac2'],2,".",","),'0',0,'R');

        //$this->setY(65);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("Equipamiento:"));
        //$this->setY(65);$this->setX(38);$this->Cell(5,$textypos,$toma[0]['equipamiento']);
    $this->SetFont('Arial','',12);           
    if ($toma[0]['pmdvap8']>0){
     $this->setY(185);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Factor de Utilidad Minima Esperada:"));
     $this->setY(185);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['fdume13'],2,".",","),'0',0,'R');

     $this->setY(190);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Precio Minimo de Venta al Publico Reacondicionado:"));
     $this->setY(190);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['pmdvap8'],2,".",","),'0',0,'R');          

     $this->setY(195);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Costo Compra con Reacondicionamiento:"));
     $this->setY(195);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['cccr9'],2,".",","),'0',0,'R');

     $this->setY(200);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Precio de Venta al Publico:"));
     $this->setY(200);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['pdvap10'],2,".",","),'0',0,'R');

     $this->setY(205);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("Utilidad con IVA de la Utilidad: "));
     $this->setY(205);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['ucidlu11'],2,".",","),'0',0,'R');

     $this->setY(210);$this->setX(26);$this->Cell(5,$textypos,utf8_decode("IVA de la Utilidad:"));
     $this->setY(210);$this->setX(182);$this->Cell(5,$textypos,"$".number_format($toma[0]['idlu12'],2,".",","),'0',0,'R');

     

   }
   
   $this->setY(60);$this->setX(135);
   $this->Ln();

   $this->setY(240);$this->setX(38);$this->Cell(5,$textypos,utf8_decode("________________________"));
   $this->setY(245);$this->setX(45);$this->Cell(5,$textypos,utf8_decode("GERENTE GENERAL"));
   $this->setY(240);$this->setX(117);$this->Cell(5,$textypos,utf8_decode("_______________________"));
   $this->setY(245);$this->setX(118);$this->Cell(5,$textypos,utf8_decode("ENCARGADO DE USADOS"));

   $this->AddPage('','','Letter');
   $this->setFont('Arial','I',9);
   $this->setFillColor(255,255,255);
   $this->Line(10,$this->GetY(),200,$this->GetY());
   $this->Image(base_url().'assets/porto/img/logo.png', 85, 1,'40','20','png','http:grupofame.com');
   $this->Ln(5);

   $this->SetFont('Arial','B',14);$this->setY(25);$this->setX(70);
   $this->Cell(5,$textypos,"Imagen de la Guia Autometrica.");
   if(isset($toma[0]['image'])){
     $this->Image(base_url().$toma[0]['image'], 50, 35,'jpg','http:grupofame.com');
   } else {
    $this->SetFont('Arial','B',14);$this->setY(55);$this->setX(80);
    $this->Cell(25,$textypos,"No se se guardo la imagen.");
  }
}

function Footer(){
  $this->SetY(-15);
  $this->Line(10,$this->GetY(),200,$this->GetY());
  $this->SetFont('Arial','I',9);
  $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
  $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
}
}

$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($toma);
$pdf->Output();
