<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','IB',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,4,utf8_decode("Autentificación de documentos"),0,0,'L',1);
            $this->setFont('Arial','I',9); 
            $this->cell(105,4,date(''),0,1,'R',1); 
            $this->Line(10,$this->GetY(),205,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 2.5,'30','15','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($toma){
        //$this->Cell(20,10,html_escape($toma[0]['facturavehiculo']),0,1,'C');

        //$this->Cell(400,10,html_escape($toma[0]['nameagency']),0,1,'C');
        $this->Ln(5);     
        $textypos = 5;

        $this->SetFont('Arial','B',9,'R');
        $this->setY(15);$this->setX(153);$this->Cell(5,$textypos,utf8_decode($toma[0]['aciudad'].", a:".date(' d-m-Y')),0,'R',1);

        $this->SetFont('Arial', '',14);
        $this->setY(55);$this->Multicell(0,$textypos,utf8_decode("A QUIEN CORRESPONDA:"),0,"J");

        $this->setY(95);$this->Multicell(0,$textypos,utf8_decode("Por medio de la presente le hago constar que los documentos recibidos de este vehículo son auténticos y legales ya que se realizó una investigación de dicha unidad. Cualquier duda o aclaración, quedo a sus órdenes."),0,"J");

        $this->SetFont('Arial', '',13);
        $this->setY(143);$this->setX(50);$this->Cell(0,$textypos,utf8_decode($toma[0]['marcavehi']),0,1,"C");
        $this->setY(148);$this->setX(50);$this->Cell(0,$textypos,utf8_decode($toma[0]['modelovehi']),0,1,"C");
        $this->setY(153);$this->setX(50);$this->Cell(0,$textypos,utf8_decode($toma[0]['aniomodelo']),0,1,"C");
        $this->setY(158);$this->setX(50);$this->Cell(0,$textypos,$toma[0]['versionvehi'],0,1,"C");
        $this->setY(163);$this->setX(50);$this->Cell(0,$textypos,$toma[0]['nserie'],0,1,"C");
        $this->setY(168);$this->setX(50);$this->Cell(0,$textypos,$toma[0]['nmotor'],0,1,"C");

		//$this->setY(19);$this->setX(15);$this->Cell(5,$textypos,"Nombre Completo:");
        $this->setY(232);$this->Cell(0,$textypos,"________________________________",0,1,"C");
        $this->setY(242);$this->Cell(0,$textypos,utf8_decode("Gerente Seminuevos."),0,1,"C");


        $this->SetFont('Arial', 'B',13);
        $this->setY(143);$this->Cell(138,$textypos,"Marca Vehiculo",0,1,"C");
        $this->setY(148);$this->Cell(138,$textypos,"Modelo",0,1,"C");
        $this->setY(153);$this->Cell(138,$textypos,utf8_decode("Año Modelo"),0,1,"C");
        $this->setY(158);$this->Cell(138,$textypos,"Tipo",0,1,"C");
        $this->setY(163);$this->Cell(138,$textypos,utf8_decode("Número de serie:"),0,1,"C");
        $this->setY(168);$this->Cell(138,$textypos,"No. Motor:",0,1,"C");
        
        $this->SetFont('Arial', 'B',12);
        $this->setY(237);$this->Cell(0,$textypos,$toma[0]['ausers_id_gs'],0,1,"C");
  

        $this->setY(60);$this->setX(135);
        $this->Ln();           
    }

    function Footer(){
        $this->SetY(-17);
        $this->Line(10,$this->GetY(),205,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($toma);
$pdf->Output();
