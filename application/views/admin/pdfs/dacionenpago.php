<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','IB',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,4,utf8_decode("DACIÓN EN PAGO"),0,0,'L',1);
            $this->setFont('Arial','I',9); 
            $this->cell(105,4,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),205,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 2.5,'30','15','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($toma){
        $this->Ln(5);     
        $textypos = 5;

        $this->SetFont('Arial', 'B',14);
        $this->setY(20);$this->Multicell(0,$textypos,utf8_decode("DACIÓN EN PAGO."),0,"C");

        $this->SetFont('Arial', '',12);
        $this->setY(35);$this->Multicell(0,$textypos,utf8_decode("                   El que suscribe, "),0,"J");
        $this->setY(35);$this->setX(55);$this->Cell(0,$textypos,$toma[0]['nombre'],0,1,"C");
        $this->setY(35.1);$this->setX(132);$this->Cell(5,$textypos,"____________________________________________________________",0,1,"C");


        $this->setY(42);$this->Multicell(0,$textypos,utf8_decode("autorizo a"),0,"J");
        $this->setY(42);$this->setX(35);$this->Cell(0,$textypos,$toma[0]['arsocial'],0,1,"C");
        $this->setY(42.1);$this->setX(30);$this->Cell(5,$textypos,"__________________________________________________________________________",0,1,"J");

        $this->setY(49);$this->Multicell(0,$textypos,utf8_decode(", para que de acuerdo a los artículos 2185 al 2223 del C.C. Aplique en compensación el importe correspondiente a la venta que le estoy haciendo del vehículo que ampara la factura número:"),0,"J");

        $this->setY(59);$this->setX(20);$this->Cell(0,$textypos,$toma[0]['facturavehi'],0,1,"J");
        $this->setY(59.1);$this->setX(10);$this->Cell(5,$textypos,"______________",0,1,"J");

        $this->setY(59);$this->setX(45);$this->Multicell(0,$textypos,utf8_decode("expedida por "),0,"J");
        $this->setY(59);$this->setX(45);$this->Cell(0,$textypos,$toma[0]['arsocial'],0,1,"C");
        $this->setY(59.1);$this->setX(136);$this->Cell(5,$textypos,"________________________________________________________",0,1,"C");

        $this->setY(65);$this->setX(10);$this->Multicell(0,$textypos,utf8_decode("de fecha"),0,"J");
        $this->setY(65);$this->setX(39);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechafac'])));
        $this->setY(65.1);$this->setX(27);$this->Cell(5,$textypos,"____________________, ",0,1,"J");
        $this->setY(65);$this->setX(77);$this->Multicell(0,$textypos,utf8_decode("marca"),0,"J");
        $this->setY(65);$this->setX(114);$this->Cell(5,$textypos,utf8_decode($toma[0]['marcavehi']),0,1,"C");
        $this->setY(65.1);$this->setX(90);$this->Cell(5,$textypos,"____________________, ",0,1,"J");
        $this->setY(65);$this->setX(141);$this->Multicell(0,$textypos,utf8_decode("modelo"),0,"J");
        $this->setY(65);$this->setX(176);$this->Cell(5,$textypos,utf8_decode($toma[0]['aniomodelo']),0,1,"C");
        $this->setY(65.1);$this->setX(156);$this->Cell(5,$textypos,"____________________, ",0,1,"J");

        $this->setY(71);$this->setX(10);$this->Cell(5,$textypos,"Tipo");
        $this->setY(71);$this->setX(50);$this->Cell(5,$textypos,$toma[0]['versionvehi'],0,1,"C");
        $this->setY(71.1);$this->setX(20);$this->Cell(5,$textypos,"____________________________, ",0,1,"J");
        $this->setY(71);$this->setX(91);$this->Cell(5,$textypos,"y No. De serie");
        $this->setY(71);$this->setX(161);$this->Cell(5,$textypos,$toma[0]['nserie'],0,1,"C");
        $this->setY(71.1);$this->setX(119);$this->Cell(5,$textypos,"____________________________________",0,1,"J");
        $this->setY(76);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("mismo que en la actualidad es usado en el estado y condiciones de uso en que se encuentra."),0,1,"J");

        $this->setY(88);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("mismo que en la actualidad es usado en el estado y condiciones de uso en que se encuentra."),0,1,"J");
        $this->setY(93);$this->Cell(0,$textypos,$toma[0]['arsocial'],0,1,"C");
        $this->setY(93.1);$this->setX(10);$this->Cell(5,$textypos,"__________________________________________________________________________________",0,1,"J");
        $this->setY(99);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("en la cantidad de:"),0,1,"J");
        $this->setY(99);$this->setX(115);$this->Cell(5,$textypos,"$".number_format($toma[0]['valorcompra'],2,".",","),'0',0,'C');
        $this->setY(99.1);$this->setX(45);$this->Cell(5,$textypos,"___________________________________________________________________",0,1,"J");
        $this->setY(105);$this->setX(150);$this->Cell(5,$textypos,"(".utf8_decode($toma[0]['valorletra'])." 00/100 M.N)",0,1,"C");
        $this->setY(105.1);$this->Cell(0,$textypos,"_______________________________________________",0,1,"R");
        $this->setY(111);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("importe que debe ser compensado contra el adeudo que con ellos tengo a nombre de:"),0,1,"J");
        $this->setY(117);$this->Cell(0,$textypos,$toma[0]['nombre'],0,1,"C");
        $this->setY(117.1);$this->setX(10);$this->Cell(5,$textypos,"__________________________________________________________________________________",0,1,"J");


        $this->setY(123);$this->setX(180);$this->cell(5,4,date('d-m-Y'),0,1,'C');
        $this->setY(123.1);$this->Cell(0,$textypos,"____________________",0,1,"R");


        $this->setY(152);$this->Cell(105,$textypos,utf8_decode("Autorización"),0,1,"C");
        $this->setY(152);$this->Cell(290,$textypos,utf8_decode("El Apoderado"),0,1,"C"); 
        


        $this->SetFont('Arial','',10);
        $this->setY(190);$this->Cell(105,$textypos,"________________________________",0,1,"C");

        $this->setY(190);$this->Cell(290,$textypos,"________________________________",0,1,"C");	 
        $this->setY(242);$this->Cell(105,$textypos,"________________________________",0,1,"C");
        $this->setY(254);$this->Cell(105,$textypos,utf8_decode("La firma deberá ser igual a la de la Identificación."),0,1,"C");

        $this->setY(242);$this->Cell(290,$textypos,"________________________________",0,1,"C");        

        $this->SetFont('Arial','B',9);    
        
        $this->setY(194);$this->Cell(105,$textypos,$toma[0]['nombre'],0,1,"C");

        $this->setY(198);$this->Cell(105,$textypos,utf8_decode("TESTIGO"),0,1,"C");

        $this->setY(194);$this->Cell(290,$textypos,utf8_decode("(NOMBRE Y FIRMA)"),0,1,"C");
        $this->setY(198);$this->Cell(290,$textypos,utf8_decode("TESTIGO"),0,1,"C");

        $this->setY(246);$this->Cell(105,$textypos,utf8_decode("(NOMBRE Y FIRMA)"),0,1,"C");
        $this->setY(250);$this->Cell(105,$textypos,utf8_decode("TESTIGO"),0,1,"C");

        $this->setY(246);$this->Cell(290,$textypos,utf8_decode("(NOMBRE Y FIRMA)"),0,1,"C");
        $this->setY(250);$this->Cell(290,$textypos,utf8_decode("TESTIGO"),0,1,"C");

        $this->setY(60);$this->setX(135);
        $this->Ln();           
    }

    function Footer(){
        $this->SetY(-17);
        $this->Line(10,$this->GetY(),205,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($toma);
$pdf->Output();
