<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','IB',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,4,utf8_decode("Carta de no retención de ISR"),0,0,'L',1);
            $this->setFont('Arial','I',9); 
            $this->cell(105,4,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),205,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 2.5,'30','15','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($toma){
        $this->Ln(5);     
        $textypos = 5;

        $this->setY(15);$this->setX(185);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechacontrato'])));

        $this->SetFont('Arial', 'B',14);
        $this->setY(20);$this->Multicell(0,$textypos,utf8_decode("CONTRATO DE COMPRAVENTA DE AUTOMOVIL"),0,"C");

        $this->SetFont('Arial', '',10);
        $this->setY(27);$this->Multicell(0,$textypos,utf8_decode("Reunidos por una parte el comprador ".$toma[0]['arsocial']." con domicilio fiscal: Blvd. Adolfo Ruiz Cortinez 4000 bis, jardines del pedregal C.P. 01900, ciudad de México. Y como vendedor: ".$toma[0]['nombre']),0,"J");

        $this->SetFont('Arial', 'B',10);
        $this->setY(40);$this->Multicell(0,$textypos,utf8_decode("EN LOS TERMINOS Y SUJETANDOSE EXPRESAMENTE A LAS SIGUIENTES:"),0,"J");

        $this->setY(50);$this->Multicell(0,$textypos,utf8_decode("DECLARACIONES:"),0,"C");
        $this->setY(130);$this->Multicell(0,$textypos,utf8_decode("CLAUSULAS:"),0,"C");

        $this->SetFont('Arial', '',10);
        $this->setY(60);$this->Multicell(0,$textypos,utf8_decode("PRIMERA.- El Vendedor declara tener plena capacidad jurídica y material para celebrar el presente contrato, así como tener pleno conocimiento de sus alcances."),0,"J");

        $this->setY(73);$this->Multicell(0,$textypos,utf8_decode("SEGUNDA.- El vendedor declara que su domicilio es:"),0,"J");

        $this->setY(85.1);$this->Multicell(0,$textypos,utf8_decode("__________________________________________________________________________________________________"),0,"J");

        $this->setY(95.1);$this->Multicell(0,$textypos,utf8_decode("__________________________________________________________________________________________________"),0,"J");

        $this->setY(100.1);$this->setX(27);$this->Multicell(0,$textypos,utf8_decode("____________________________________"),0,"J");

        $this->setY(100.1);$this->setX(115);$this->Multicell(0,$textypos,utf8_decode("____________________________________"),0,"J");

        $this->setY(105);$this->Multicell(0,$textypos,utf8_decode("De acuerdo al comprobante de CEA o CFE con fecha de: "),0,"J");
        $this->setY(105.1);$this->setX(101);$this->Multicell(0,$textypos,utf8_decode("__________________________"),0,"J");


        $this->setY(120.1);$this->setX(43);$this->Multicell(0,$textypos,utf8_decode("__________________________"),0,"J");
        $this->setY(120.1);$this->setX(123);$this->Multicell(0,$textypos,utf8_decode("_____________________________"),0,"J");

        $this->setY(115);$this->Multicell(0,$textypos,utf8_decode("TERCERA.- El Vendedor declara tener plena capacidad jurídica y económica para Celebrar el presente contrato, identificándose con:"),0,"J");

        $this->setY(120);$this->setX(65);$this->Cell(5,$textypos,$toma[0]['tipoidenti']);

        $this->setY(120);$this->Cell(0,$textypos,"con folio:",0,1,"C");
        $this->setY(120);$this->setX(100);$this->Cell(0,$textypos,$toma[0]['nidenti'],0,1,"C");    

         $this->setY(140);$this->Multicell(0,$textypos,utf8_decode("PRIMERA.- Objeto del contrato, el vendedor hace formal entrega en éste acto al comprador del automóvil que se describe a continuación, en las condiciones y características de uso en que se encuentra a la fecha de la celebración del presente contrato."),0,"J");      

        $this->SetFont('Arial','B',10);
        $this->setY(80);$this->Cell(5,$textypos,"Calle:");
        $this->setY(80);$this->Cell(0,$textypos,"Numero Ext:",0,1,"C");
        $this->setY(80);$this->Cell(190,$textypos,"Numero Int:",0,1,'R',0);

        $this->setY(90);$this->Cell(5,$textypos,"Colonia:");
        $this->setY(90);$this->Cell(0,$textypos,"Ciudad:",0,1,"C");
        $this->setY(90);$this->Cell(190,$textypos,"Municipio:",0,1,'R',0);

        $this->setY(100);$this->Cell(5,$textypos,"Estado:");
        $this->setY(100);$this->Cell(0,$textypos,"CP:",0,1,"C");
       

        $this->setY(160);$this->setX(25);$this->Cell(5,$textypos,"Marca Vehiculo:");
        $this->setY(166);$this->setX(25);$this->Cell(5,$textypos,utf8_decode("Año Modelo:"));
        $this->setY(172);$this->setX(25);$this->Cell(5,$textypos,"Color Exterior:");
        $this->setY(178);$this->setX(25);$this->Cell(5,$textypos,"No. Motor:");
        $this->setY(184);$this->setX(25);$this->Cell(5,$textypos,"Color Interior:");
        $this->setY(189);$this->setX(25);$this->Cell(5,$textypos,utf8_decode("Expedida por:"));
        $this->setY(196);$this->setX(25);$this->Cell(5,$textypos,utf8_decode("No. de verificación:"));
        $this->setY(201);$this->setX(25);$this->Cell(5,$textypos,utf8_decode("Entidad:"));
        $this->setY(206);$this->setX(25);$this->Cell(5,$textypos,utf8_decode("Tenencias por los años:"));
        $this->setY(211);$this->setX(25);$this->Cell(5,$textypos,utf8_decode("Placas y tarjeta de circulación:"));

        $this->setY(160);$this->setX(100);$this->Cell(5,$textypos,"Modelo:");
        $this->setY(166);$this->setX(100);$this->Cell(5,$textypos,"Tipo:");
        $this->setY(172);$this->setX(100);$this->Cell(5,$textypos,utf8_decode("Número de serie:"));
        $this->setY(178);$this->setX(100);$this->Cell(5,$textypos,"Kilometraje:");
        $this->setY(184);$this->setX(100);$this->Cell(5,$textypos,"Factura:");
        $this->setY(196);$this->setX(100);$this->Cell(5,$textypos,"Folio de baja:");
        $this->setY(201);$this->setX(100);$this->Cell(5,$textypos,utf8_decode("Fecha de baja:"));

        $this->SetFont('Arial','',10);
        $this->setY(85);$this->Cell(5,$textypos,utf8_decode($toma[0]['scalle']));
        $this->setY(85);$this->Cell(0,$textypos,$toma[0]['numext'],0,1,"C");
        $this->setY(85);$this->Cell(190,$textypos,$toma[0]['numint'],0,1,'R',0);

        $this->setY(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['scolonia']));
        $this->setY(95);$this->Cell(0,$textypos,utf8_decode($toma[0]['sciudad']),0,1,"C");
        $this->setY(95);$this->Cell(186,$textypos,utf8_decode($toma[0]['municipio']),0,1,'R',0);

        $this->setY(100);$this->setX(50);$this->Cell(5,$textypos,utf8_decode($toma[0]['sestado']));
        $this->setY(100);$this->setX(100);$this->Cell(0,$textypos,$toma[0]['scp'],0,1,"C");
        $this->setY(105);$this->setX(123);$this->Cell(5,$textypos,utf8_decode($toma[0]['mesrecibo']),0,1,"C");

        $this->setY(160);$this->setX(72);$this->Cell(5,$textypos,utf8_decode($toma[0]['marcavehi']),0,1,"C");
        $this->setY(160.1);$this->setX(55);$this->Multicell(0,$textypos,utf8_decode("____________________"),0,"J");
        $this->setY(166);$this->setX(72);$this->Cell(5,$textypos,$toma[0]['aniomodelo'],0,1,"C");
        $this->setY(166.1);$this->setX(55);$this->Multicell(0,$textypos,utf8_decode("____________________"),0,"J");
        $this->setY(172);$this->setX(72);$this->Cell(5,$textypos,utf8_decode($toma[0]['colorext']),0,1,"C");
        $this->setY(172.1);$this->setX(55);$this->Multicell(0,$textypos,utf8_decode("____________________"),0,"J");
        $this->setY(178);$this->setX(72);$this->Cell(5,$textypos,$toma[0]['nmotor'],0,1,"C");
        $this->setY(178.1);$this->setX(55);$this->Multicell(0,$textypos,utf8_decode("____________________"),0,"J");
        $this->setY(184);$this->setX(72);$this->Cell(5,$textypos,utf8_decode($toma[0]['colorint']),0,1,"C");
        $this->setY(184.1);$this->setX(55);$this->Multicell(0,$textypos,utf8_decode("____________________"),0,"J");
        $this->setY(189);$this->setX(82);$this->Cell(5,$textypos,utf8_decode($toma[0]['expedida']),0,1,"C");
        $this->setY(189.1);$this->setX(55);$this->Multicell(0,$textypos,utf8_decode("______________________________________________________________"),0,"J");
        $this->setY(196);$this->setX(75);$this->Cell(5,$textypos,utf8_decode($toma[0]['nverificacion']),0,1,"C");
        $this->setY(196.1);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("__________________"),0,"J");
        $this->setY(201);$this->setX(75);$this->Cell(5,$textypos,utf8_decode($toma[0]['edoemisor']),0,1,"C");
        $this->setY(201.1);$this->setX(60);$this->Multicell(0,$textypos,utf8_decode("__________________"),0,"J");
        $this->setY(206);$this->setX(105);$this->Cell(5,$textypos,utf8_decode($toma[0]['tenencias']),0,1,"C");
        $this->setY(206.1);$this->setX(67);$this->Multicell(0,$textypos,utf8_decode("________________________________________"),0,"J");
        $this->setY(211);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['placasbaja']),0,1,"C");
        $this->setY(211.1);$this->setX(80);$this->Multicell(0,$textypos,utf8_decode("__________________"),0,"J");





        //Segunda parte 
        $this->setY(160);$this->setX(105);$this->Cell(0,$textypos,utf8_decode($toma[0]['modelovehi']),0,1,"C");
        $this->setY(160.1);$this->setX(130);$this->Multicell(0,$textypos,utf8_decode("________________________"),0,"J");
        $this->setY(166);$this->setX(105);$this->Cell(0,$textypos,$toma[0]['versionvehi'],0,1,"C");
        $this->setY(166.1);$this->setX(130);$this->Multicell(0,$textypos,utf8_decode("________________________"),0,"J");
        $this->setY(172);$this->setX(105);$this->Cell(0,$textypos,$toma[0]['nserie'],0,1,"C");
        $this->setY(172.1);$this->setX(130);$this->Multicell(0,$textypos,utf8_decode("________________________"),0,"J");
        $this->setY(178);$this->setX(153);$this->Cell(5,$textypos,$toma[0]['placasbaja'],0,1,"C");
        $this->setY(178.1);$this->setX(130);$this->Multicell(0,$textypos,utf8_decode("________________________"),0,"J");
        $this->setY(184);$this->setX(153);$this->Cell(5,$textypos,$toma[0]['facturavehi'],0,1,"C");
        $this->setY(184.1);$this->setX(130);$this->Multicell(0,$textypos,utf8_decode("________________________"),0,"J");

        $this->setY(196);$this->setX(153);$this->Cell(5,$textypos,$toma[0]['nbaja'],0,1,"C");
        $this->setY(196.1);$this->setX(130);$this->Multicell(0,$textypos,utf8_decode("________________________"),0,"J");
        $this->setY(201);$this->setX(153);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechabaja'])),0,1,"C");
        $this->setY(201.1);$this->setX(130);$this->Multicell(0,$textypos,utf8_decode("________________________"),0,"J");

        $this->setY(221);$this->Multicell(0,$textypos,utf8_decode("SEGUNDA.- Como consecuencia de la compraventa del automóvil antes descrito, el vendedor hace formal y material entrega en éste acto al comprador de la documentación en original que ampara la propiedad."),0,"J"); 
        $this->setY(235);$this->Multicell(0,$textypos,utf8_decode("TERCERA.- PRECIO.- Ambas partes convienen que el precio pactado por la presente compraventa es de:"),0,"J"); 

        $this->setY(240);$this->setX(135);$this->Cell(5,$textypos,"(".utf8_decode($toma[0]['valorletra'])." 00/100 M.N)",0,1,"C");
        $this->setY(240.1);$this->setX(25);$this->Cell(5,$textypos,"_______________________________________________________________________________");

        $this->setY(245);$this->Multicell(0,$textypos,utf8_decode("Satisfecho el pago correspondiente."),0,"J");       

        $this->SetFont('Arial','B',10);
        $this->setY(240);$this->setX(45);$this->Cell(5,$textypos,"$".number_format($toma[0]['valorcompra'],2,".",","),'0',0,'C');

        //Segunda pagina
        $this->AddPage('','','Letter');
        $this->setFont('Arial','I',9);
        $this->setFillColor(255,255,255);
        $this->Line(10,$this->GetY(),200,$this->GetY());
        $this->Image(base_url().'assets/porto/img/logo.png', 85, 1,'40','20','png','http:grupofame.com');
        $this->Ln(5);


        $this->SetFont('Arial','',10);
        $this->setY(25);$this->Multicell(0,$textypos,utf8_decode("CUARTA.- Declaro expresamente bajo protesta de decir verdad, que el vehículo que les he vendido lo adquirí legalmente y lo entrego completamente libre de cualquier gravamen o vicio, constituyéndome en único responsable de su saneamiento, solventaré todo problema que se presente respecto de su legítima propiedad, por su legal y definitiva estancia en el país o por cualquier responsabilidad de carácter civil, penal o fiscal que por lo mismo resultare hasta la fecha, por lo que, en su caso me obligo expresamente a efectuar de inmediato la devolución del dinero que he recibido independientemente de la liquidación y reparación del monto de los daños y perjuicios que les causaren con tal motivo, así como el de las reparaciones hechas al vehículo."),0,"J"); 

        $this->setY(65);$this->Multicell(0,$textypos,utf8_decode("QUINTA.- A partir de la firma del presente contrato, el comprador se hace responsable de cualquier daño en propiedad ajena o a terceros ocasionados con motivo del automóvil materia de la compraventa; así como de toda responsabilidad con motivo de que, a partir de ésta fecha, dicho comprador adquiere la posesión y propiedad del vehículo en comento."),0,"J"); 

        $this->setY(85);$this->Multicell(0,$textypos,utf8_decode("Las partes contratantes manifiestan que han leído en su integridad el contenido del presente contrato, estando conformes con sus alcances legales, reiterando que tiene plena capacidad jurídica para comprometerse y firmarlo con absoluta libertad y sin existir dolo alguno."),0,"J");

        $this->setY(105);$this->Multicell(0,$textypos,utf8_decode("Firmado el contratante en dos originales que corresponden uno para cada parte, en"),0,"J");

        $this->SetFont('Arial','',10);
        $this->setY(242);$this->Cell(105,$textypos,"________________________________",0,1,"C");
        $this->setY(254);$this->Cell(105,$textypos,utf8_decode("Firman las partes todas las hojas que integran el presente contrato."),0,1,"C");

        $this->setY(242);$this->Cell(290,$textypos,"________________________________",0,1,"C");	        


        $this->setY(152);$this->Cell(105,$textypos,"VENDEDOR",0,1,"C");
        $this->setY(152);$this->Cell(290,$textypos,"COMPRADOR",0,1,"C"); 

        $this->SetFont('Arial','B',9);
        #$this->setY(15);$this->setX(156);$this->Cell(5,$textypos,"Fecha de Contrato:");    
        
        $this->setY(246);$this->Cell(105,$textypos,$toma[0]['nombre'],0,1,"C");

        $this->setY(246);$this->Cell(290,$textypos,$toma[0]['arsocial'],0,1,"C");

        #$this->setY(60);$this->setX(135);
        #$this->Ln();           
    }

    function Footer(){
        $this->SetY(-17);
        $this->Line(10,$this->GetY(),205,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($toma);
$pdf->Output();
