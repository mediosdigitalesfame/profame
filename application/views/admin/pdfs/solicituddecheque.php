<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','IB',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,4,utf8_decode("SOLICITUD DE CHEQUE"),0,0,'L',1);
            $this->setFont('Arial','I',9); 
            $this->cell(105,4,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),205,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 2.5,'30','15','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($toma){
        $this->Ln(5);     
        $textypos = 5;

        $this->SetFont('Arial', 'B',14);
        $this->setY(20);$this->Multicell(0,$textypos,utf8_decode("SOLICITUD DE CHEQUE."),0,"C");

        $this->SetFont('Arial', '',12);
        $this->setY(45);$this->Multicell(0,$textypos,utf8_decode("NOMBRE: "."______________________________________________"),0,"J");

        $this->setY(45);$this->setX(82);$this->Cell(5,$textypos,$toma[0]['nombre'],0,1,"C");

        $this->setY(52);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("IMPORTE: "."______________________________________________"),0,"J");
        $this->setY(52);$this->setX(70);$this->Cell(5,$textypos,"$".number_format($toma[0]['valorcompra'],2,".",","),'0',0,'L');

        $this->setY(59);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("IMP. LETRA: "."____________________________________________"),0,"J");
        $this->setY(59);$this->setX(82);$this->Cell(5,$textypos,"(".utf8_decode($toma[0]['valorletra'])." 00/100 M.N)",0,1,"C");


        $this->setY(79);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("CONCEPTO: "."______________________________________________"),0,"J");
        $this->setY(87);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("FOLIOS DOCTO: "."__________________________________________"),0,"J");


        $this->setY(107);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("BANCO: "."_______________________"),0,"J");
        $this->setY(107);$this->setX(45);$this->Cell(0,$textypos,utf8_decode($toma[0]['banco']),0,"C");
        $this->setY(115);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("CUENTA: "."______________________"),0,"J");
        $this->setY(115);$this->setX(39);$this->Cell(0,$textypos,$toma[0]['cuenta'],0,"C");
        $this->setY(122);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("CLABE: "."________________________"),0,"J");
        $this->setY(122);$this->setX(33);$this->Cell(5,$textypos,$toma[0]['clabe'],0,"C");



        $this->setY(107);$this->setX(120);$this->Cell(0,$textypos,utf8_decode("SUCURSAL: "."_______________________"),0,"J");
        $this->setY(107);$this->setX(167);$this->Cell(0,$textypos,utf8_decode($toma[0]['sucursal']),0,"C");
        $this->setY(115);$this->setX(120);$this->Cell(0,$textypos,utf8_decode("CONVENIO: "."_______________________"),0,"J");
        $this->setY(115);$this->setX(165);$this->Cell(0,$textypos,$toma[0]['convenio'],0,'C');
        $this->setY(122);$this->setX(120);$this->Cell(0,$textypos,utf8_decode("REFERENCIA: "."_____________________"),0,"J");
        $this->setY(122);$this->setX(161);$this->Cell(5,$textypos,$toma[0]['referencia']);

        $this->setY(142);$this->setX(10);$this->Cell(5,$textypos,utf8_decode("AREA: "."_____________________________________________________________"),0,"J");


        
$this->SetFont('Arial','',10);
        $this->setY(242);$this->Cell(105,$textypos,"________________________________",0,1,"C");
        #$this->setY(249);$this->Cell(105,$textypos,utf8_decode("La firma deberá ser igual a la de la Identificación."),0,1,"C");

        $this->setY(242);$this->Cell(290,$textypos,"________________________________",0,1,"C");	           
        
        #$this->setY(246);$this->Cell(105,$textypos,$toma[0]['nombre'],0,1,"C");
        $this->setY(251);$this->Cell(105,$textypos,"ENLACE (CONTADOR )",0,1,"C");

        $this->setY(246);$this->Cell(290,$textypos,$toma[0]['ausers_id_gg'],0,1,"C");
        $this->setY(251);$this->Cell(290,$textypos,"GERENTE GENERAL",0,1,"C");
     

        $this->setY(60);$this->setX(135);
        $this->Ln();           
    }

    function Footer(){
        $this->SetY(-17);
        $this->Line(10,$this->GetY(),205,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($toma);
$pdf->Output();
