<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','IB',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,4,utf8_decode("AVISO DE PRIVACIDAD INTEGRAL"),0,0,'L',1);
            $this->setFont('Arial','I',9); 
            $this->cell(105,4,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),205,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 2.5,'30','15','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($toma){
        $this->Ln(5);     
        $textypos = 5;

        //Negritas
        $this->SetFont('Arial', 'B',14);
        $this->setY(18);$this->Multicell(0,$textypos,utf8_decode("AVISO DE PRIVACIDAD INTEGRAL"),0,"C");

        $this->SetFont('Arial', 'B',8);
        $this->setY(25);$this->Multicell(0,$textypos,utf8_decode("I. IDENTIFICACIÓN DEL RESPONSABLE"),0,"J");
        $this->setY(55);$this->Multicell(0,$textypos,utf8_decode("II. MEDIOS DE OBTENCIÓN Y DATOS PERSONALES QUE SE RECABAN"),0,"J");
        $this->setY(72);$this->Multicell(0,$textypos,utf8_decode("a.        Datos personales que recabamos de persona a persona:"),0,"J");
        $this->setY(89);$this->Multicell(0,$textypos,utf8_decode("Datos personales sensibles:"),0,"J");
        $this->setY(100);$this->Multicell(0,$textypos,utf8_decode("b.        Datos personales que recabamos de manera directa a través de la página web, correo electrónico y vía telefónica."),0,"J");
        $this->setY(111);$this->Multicell(0,$textypos,utf8_decode("c.      Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso públicos que están permitidas por la Ley."),0,"J");
        $this->setY(141);$this->Multicell(0,$textypos,utf8_decode("III. FINALIDADES"),0,"J");
        $this->setY(163);$this->Multicell(0,$textypos,utf8_decode("a. Necesaria para la relación Jurídica con el responsable:"),0,"J");
        $this->setY(227);$this->Multicell(0,$textypos,utf8_decode("b. No necesaria para la relación jurídica con el responsable:"),0,"J");

        //Letras normales
        $this->SetFont('Arial', '',8);
        $this->setY(30);$this->Multicell(0,$textypos,utf8_decode("En, ".$toma[0]['arsocial']." en lo sucesivo ".$toma[0]['arsocial'].", con domicilio en  BLVD. ADOLFO RUIZ CORTINEZ 4000 BIS, JARDINES DEL PEDREGAL C.P. 01900, CIUDAD DE MÉXICO, le comunicamos que la información personal de todos nuestros proveedores y prospectos a proveedores es tratada de forma estrictamente confidencial, por lo que podrán sentirse plenamente seguros de que hacemos un esfuerzo permanente para resguardarla y protegerla."),0,"J");

        $this->setY(60);$this->Multicell(0,$textypos,utf8_decode($toma[0]['arsocial'].", recaba los siguientes datos personales que son necesarios y aplicables para dar cumplimiento a las finalidades establecidas en el presente Aviso de Privacidad:"),0,"J");

        $this->setY(77);$this->Multicell(0,$textypos,utf8_decode("Datos personales de identificación, laborales, patrimoniales, financieros, datos de terceros (Al proporciona los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales)."),0,"J");

        $this->setY(94);$this->Multicell(0,$textypos,utf8_decode("Requerimos su consentimiento expreso y por escrito para el tratamiento de sus datos personales sensibles, financieros y/o patrimoniales."),0,"J");

        $this->setY(105);$this->Multicell(0,$textypos,utf8_decode("Datos personales de identificación."),0,"J");

        $this->setY(121);$this->Multicell(0,$textypos,utf8_decode("Datos personales de identificación. \nSe le informa que las imágenes y sonidos captados a través de un sistema de circuito cerrado, serán utilizados para su seguridad y de las personas que nos visitan."),0,"J");

        $this->setY(146);$this->Multicell(0,$textypos,utf8_decode("En ".$toma[0]['arsocial'].", tenemos la filosofía de mantener una relación estrecha y activa con nuestros proveedores y prospectos a proveedores. En términos de lo establecido por la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y su Reglamento, los datos que nos proporcione serán utilizados para las siguientes finalidades:"),0,"J");

        $this->setY(168);$this->Multicell(0,$textypos,utf8_decode("i.        Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y/o servicios de nuestros proveedores y prospectos a proveedores. \nii.         Para la adquisición de automóviles y camiones. \niii.        Para la comercialización de bienes, productos y/o servicios. \niv.        Investigación para confirmar la información y documentación requerida y otorgada por el proveedor y prospecto a proveedor. \nv.        Trámite a nombre y por cuenta del proveedor y cualquiera otra requerida en la relación de compra-venta. \nvi.        Investigación ante las dependencias oficiales correspondientes de las garantías otorgadas (Aval y Obligados Solidarios). \nvii.       Procesar solicitudes de facturación y aclaraciones. \nviii.      Dar cumplimiento a requerimientos legales. \nix.        Mantener actualizados nuestros registros para poder responder a sus consultas e invitarle a eventos que tengan relación el bien adquirido. \nx.         La realización de cualquier actividad necesaria para el cumplimiento de nuestra relación contractual."),0,"J");

        $this->setY(232);$this->Multicell(0,$textypos,utf8_decode("1. Realizar Actividades de mercadotecnia, publicidad y prospección comercial, diversas al servicio que brinda ".$toma[0]['arsocial']),0,"J");

        $this->setY(237);$this->Multicell(0,$textypos,utf8_decode("2. realizar actividades de mercadotecnia, publicidad y prospección comercial relacionada con los productos y/o servicios de  ".$toma[0]['arsocial']."  las finalidades no necesarias son importantes para ofrecerle a través de campañas de mercadotecnia, publicidad y prospección comercial, productos y servicios exclusivos, por lo usted tiene derecho a oponerse, o bien, a revocar su consentimiento para que ".$toma[0]['arsocial'].", deje de tratar sus datos personales para dichas finalidades."),0,"J");

        

        

        //Segunda Pagina
        $this->AddPage('','','Letter');
        $this->setFont('Arial','I',8);
        $this->setFillColor(255,255,255);
        $this->Line(10,$this->GetY(),205,$this->GetY());
        $this->Image(base_url().'assets/porto/img/logo.png', 85, 1,'35','17.5','png','http:grupofame.com');
        $this->Ln(5);

        //Negritas
        $this->SetFont('Arial', 'B',8);
        $this->setY(20);$this->Multicell(0,$textypos,utf8_decode("IV. OPCIONES PARA LIMITAR EL USO O DIVULGACIÓN DE SUS DATOS PERSONALES."),0,"J");
        $this->setY(45);$this->Multicell(0,$textypos,utf8_decode("V. SOLICITUD DE ACCESO, RECTIFICACIÓN, CANCELACIÓN U OPOSICIÓN DE DATOS PERSONALES (DERECHOS ARCO) Y REVOCACIÓN DE CONSENTIMIENTO."),0,"J");
        $this->setY(121);$this->Multicell(0,$textypos,utf8_decode("VI. TRANSFERENCIAS DE DATOS PERSONALES."),0,"J");
        $this->setY(126);$this->Multicell(0,$textypos,utf8_decode("Transferencia sin necesidad de consentimiento:"),0,"J");
        $this->setY(146);$this->Multicell(0,$textypos,utf8_decode("Transferencias con consentimiento:"),0,"J");
        $this->setY(171);$this->Multicell(0,$textypos,utf8_decode("VII. USO DE COOKIES"),0,"J");
        $this->setY(191);$this->Multicell(0,$textypos,utf8_decode("VII. USO DE COOKIES"),0,"J");

        $this->setY(216);$this->Multicell(0,$textypos,utf8_decode("Estoy enterado del tratamiento que recibirán mis datos personales en términos de lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares."),0,"J");

        //Letras Normales
        $this->SetFont('Arial', '',8);
        $this->setY(25);$this->Multicell(0,$textypos,utf8_decode($toma[0]['arsocial']." le comunica que si usted desea dejar de recibir mensajes de mercadotecnia, publicidad o de prospección comercial, relativos a las finalidades referidas al aparto III inciso (b), puede hacerlo valer por medio del correo electrónico avisodeprivacidad@grupofame.com, así mismo, por éste medio atenderemos todas sus dudas y comentarios acerca del tratamiento de sus datos personales."),0,"J");

        $this->setY(56);$this->Multicell(0,$textypos,utf8_decode("Todos sus datos personales son tratados de acuerdo a la legislación aplicable y vigente en el país, por ello le informamos que usted tiene en todo momento el derecho de Acceder, Rectificar, Cancelar, u Oponerse al tratamiento que le damos a sus datos personales, así como Revocar el consentimiento otorgado para el tratamiento de los mismos; derechos que podrá hacer valer a través de los Gerente y/o Jefes Administrativos, personas que ".$toma[0]['arsocial']." ha designado para tal efecto, o bien, enviando un correo electrónico a avisodeprivacidad@grupofame.com, o si lo prefiere, accediendo a nuestro sitio WEB ".$toma[0]['aurl']." sección Aviso de Privacidad, para que le sea proporcionado el Formato de Solicitud de Derechos ARCO, mismo que deberá presentar requisitado de manera personal en el domicilio del responsable, debiendo adjuntar una copia de su identificación oficial para acreditar su titularidad."),0,"J");

        $this->setY(93);$this->Multicell(0,$textypos,utf8_decode("La respuesta a su solicitud de Derechos ARCO se le hará llegar al correo electrónico que haya proporcionado, dentro del término de  20 días hábiles contados a partir de la recepción de dicha solicitud. Así mismo, se le informa que el derecho de acceso se tendrá por cumplido cuando se haga llegar la respuesta correspondiente a través del correo electrónico que usted nos haya indicado para tal efecto."),0,"J");

        $this->setY(111);$this->Multicell(0,$textypos,utf8_decode("En caso de no estar de acuerdo en el tratamiento de sus datos personales, puede acudir ante el IFAI."),0,"J");

        $this->setY(131);$this->Multicell(0,$textypos,utf8_decode("Se le informa que a fin de dar cumplimiento a las finalidades establecidas en el apartado III inciso (a) del presente Aviso de Privacidad, sus datos personales pueden ser transferidos y tratados dentro y fuera de los Estados Unidos Mexicanos por personas ".$toma[0]['arsocial'].". En éste sentido y con fundamente en La Ley y su Reglamento, sus datos personales podrán ser transferidos sin necesidad de su consentimiento."),0,"J");

        $this->setY(151);$this->Multicell(0,$textypos,utf8_decode($toma[0]['arsocial']." podrá transferir sus datos personales de identificación y de contacto a empresas de marketing y publicidad para  las finalidades descritas en el apartado iii del inciso (b) del presente aviso de privacidad. si usted no desea que sus datos personales sean transferidos a dichos terceros, puede manifestar su negativa conforme al procedimiento  establecido en el apartado v del presente aviso de privacidad."),0,"J");

        $this->setY(176);$this->Multicell(0,$textypos,utf8_decode($toma[0]['arsocial']."no utiliza cookies y web beacons, para obtener información personal de usted de manera automática. Los datos  personales que recabamos de manera electrónica, así como las finalidades del tratamiento se encuentran establecidos en el presente  Aviso de Privacidad."),0,"J");

        $this->setY(196);$this->Multicell(0,$textypos,utf8_decode("Este Aviso de Privacidad podrá ser modificado de tiempo en tiempo por ".$toma[0]['arsocial'].", dichas modificación podrán consultares a través de los siguientes medios:"),0,"J");

        $this->setY(205);$this->Multicell(0,$textypos,utf8_decode("1.  Nuestra página de internet ( :".$toma[0]['aurl']." )"),0,"J");
        $this->setY(208);$this->Multicell(0,$textypos,utf8_decode("2.  Avisos visibles en nuestras instalaciones de ".$toma[0]['arsocial']),0,"J");
        $this->setY(211);$this->Multicell(0,$textypos,utf8_decode("3.  Cualquier otro medio de comunicación que ".$toma[0]['arsocial'].", determine para tal efecto."),0,"J");

        $this->SetFont('Arial','',10);
        $this->setY(242);$this->Cell(75,$textypos,"________________________________",0,1,"C");
        $this->setY(242);$this->Cell(332,$textypos,"__________________________",0,1,"C");
        $this->setY(242);$this->Cell(210,$textypos,"__________________________",0,1,"C"); 
        $this->setY(250);$this->Cell(195,$textypos,utf8_decode("Fecha de elaboración del presente aviso ".date('d-m-Y')),0,1,"R");
        $this->setY(254);$this->Cell(195,$textypos,utf8_decode("Fecha de última actualización 08/01/2021"),0,1,"R");
        $this->setY(246);$this->setX(45);$this->Cell(5,$textypos,utf8_decode("Nombre del titular"),0,1,"C");
        $this->setY(246);$this->setX(112);$this->Cell(5,$textypos,utf8_decode("Firma o huella dactilar"),0,1,"C");
        $this->setY(246);$this->setX(174);$this->Cell(5,$textypos,utf8_decode("Fecha (dd/mm/aaaa)"),0,1,"C");




        $this->SetFont('Arial','B',9);  
        
        $this->setY(241.5);$this->setX(45);$this->Cell(5,$textypos,$toma[0]['nombre'],0,1,"C");

        $this->setY(242);$this->cell(175,4,date('d-m-Y'),0,1,'R');

        $this->setY(60);$this->setX(135);
        $this->Ln();           
    }

    function Footer(){
        $this->SetY(-17);
        $this->Line(10,$this->GetY(),205,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($toma);
$pdf->Output();
