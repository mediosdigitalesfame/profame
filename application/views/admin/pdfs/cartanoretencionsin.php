<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','IB',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,4,utf8_decode("Carta de no retención de ISR"),0,0,'L',1);
            $this->setFont('Arial','I',9); 
            $this->cell(105,4,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),205,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 2.5,'30','15','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($toma){
        $this->Ln(5);     
        $textypos = 5;

        $this->setY(15);$this->setX(185);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechacontrato'])));

        $this->SetFont('Arial', 'B',14);
        $this->setY(20);$this->Multicell(0,$textypos,utf8_decode("CARTA DE NO RETENCIÓN DE ISR PERSONAS FÍSICAS CON ACTIVIDAD EMPRESARIAL."),0,"C");

        $this->SetFont('Arial', '',12);
        $this->setY(35);$this->Multicell(0,$textypos,utf8_decode("Yo:"),0,"J");
        $this->setY(43);$this->Multicell(0,$textypos,utf8_decode("Con Domicilio en:"),0,"J");

        $this->setY(94);$this->Multicell(0,$textypos,utf8_decode("De acuerdo a la regla  3.15.7 y 3.15.2 de la  Resolución Miscelánea Fiscal 2015, publicada el 29 de Septiembre de 2015, solicito no me sea retenido el 20% por concepto de pago provisional de ISR, para lo cual manifiesto por medio de la presente y bajo protesta de decir la verdad, que no obtengo ingresos por Actividades Empresariales, Profesionales y/o RIF; ademas que no efectué deducción alguna de la unidad que estoy vendiendo, y que de acuerdo al art. 93 fracc. XIX inciso b),la utilidad en la enajenación del vehículo vendido a ".$this->Cell(0,$textypos,utf8_decode($toma[0]['arsocial']),0,1,"C");.", resultante de la comparación entre el costo de adquisición sin actualizar y el valor de enajenación,  no excede a 3 veces la UMA elevado al año. ($95,081.47)"),0,"J");

        $this->SetFont('Arial', 'B',12);
        $this->setY(118);$this->Multicell(0,$textypos,utf8_decode("Datos del Vehículo:"),0,"J");

        $this->SetFont('Arial', 'U',12);
        $this->setY(35);$this->Cell(0,$textypos,$toma[0]['nombre'],0,1,"C");

        $this->SetFont('Arial','B',10);
        $this->setY(50);$this->Cell(5,$textypos,"Calle:");
        $this->setY(50);$this->Cell(0,$textypos,"Numero Ext:",0,1,"C");
        $this->setY(50);$this->Cell(190,$textypos,"Numero Int:",0,1,'R',0);

        $this->setY(61);$this->Cell(5,$textypos,"Colonia:");
        $this->setY(61);$this->Cell(0,$textypos,"Ciudad:",0,1,"C");
        $this->setY(61);$this->Cell(190,$textypos,"Municipio:",0,1,'R',0);

        $this->setY(72);$this->Cell(5,$textypos,"Estado:");
        $this->setY(72);$this->Cell(0,$textypos,"CP:",0,1,"C");
        $this->setY(72);$this->Cell(185,$textypos,"RFC:",0,1,'R',0);

        $this->setY(124);$this->setX(25);$this->Cell(5,$textypos,"Marca Vehiculo:");
        $this->setY(129);$this->setX(25);$this->Cell(5,$textypos,utf8_decode("Año Modelo:"));
        $this->setY(134);$this->setX(25);$this->Cell(5,$textypos,"Color Exterior:");
        $this->setY(139);$this->setX(25);$this->Cell(5,$textypos,"No. Motor:");
        $this->setY(144);$this->setX(25);$this->Cell(5,$textypos,"Color Interior:");
        $this->setY(149);$this->setX(25);$this->Cell(5,$textypos,utf8_decode("Fecha adquisición:"));

        $this->setY(124);$this->setX(100);$this->Cell(5,$textypos,"Modelo:");
        $this->setY(129);$this->setX(100);$this->Cell(5,$textypos,"Tipo:");
        $this->setY(134);$this->setX(100);$this->Cell(5,$textypos,utf8_decode("Número de serie:"));
        $this->setY(139);$this->setX(100);$this->Cell(5,$textypos,"Placas:");
        $this->setY(144);$this->setX(100);$this->Cell(5,$textypos,"Factura:");
        $this->setY(149);$this->setX(100);$this->Cell(5,$textypos,"Expedida por:");

        $this->setY(164);$this->setX(40);$this->Cell(5,$textypos,utf8_decode("Costo de Adquisición (Último valor en Factura):"));
        $this->setY(169);$this->setX(40);$this->Cell(5,$textypos,utf8_decode("Monto de la transacción (compra):"));

        $this->setY(189);$this->Multicell(0,$textypos,utf8_decode("* Anexo Guía de Obligaciones Vigente"),0,"J");


        $this->SetFont('Arial', 'U',13);
        $this->setY(179);$this->setX(95);$this->Cell(5,$textypos,"(".utf8_decode($toma[0]['valorletra'])." 00/100 M.N)",0,1,"C");


        $this->SetFont('Arial','U',10);
        $this->setY(55);$this->Cell(5,$textypos,utf8_decode($toma[0]['scalle']));
        $this->setY(55);$this->Cell(0,$textypos,$toma[0]['numext'],0,1,"C");
        $this->setY(55);$this->Cell(190,$textypos,$toma[0]['numint'],0,1,'R',0);

        $this->setY(66);$this->Cell(5,$textypos,utf8_decode($toma[0]['scolonia']));
        $this->setY(66);$this->Cell(0,$textypos,utf8_decode($toma[0]['sciudad']),0,1,"C");
        $this->setY(66);$this->Cell(186,$textypos,utf8_decode($toma[0]['municipio']),0,1,'R',0);

        $this->setY(77);$this->Cell(5,$textypos,utf8_decode($toma[0]['sestado']));
        $this->setY(77);$this->Cell(0,$textypos,$toma[0]['scp'],0,1,"C");
        $this->setY(77);$this->Cell(195,$textypos,$toma[0]['srfc'],0,1,'R',0);

        $this->setY(124);$this->setX(65);$this->Cell(5,$textypos,utf8_decode($toma[0]['marcavehi']));
        $this->setY(129);$this->setX(65);$this->Cell(5,$textypos,$toma[0]['aniomodelo']);
        $this->setY(134);$this->setX(65);$this->Cell(5,$textypos,utf8_decode($toma[0]['colorext']));
        $this->setY(139);$this->setX(65);$this->Cell(5,$textypos,$toma[0]['nmotor']);
        $this->setY(144);$this->setX(65);$this->Cell(5,$textypos,utf8_decode($toma[0]['colorint']));
        $this->setY(149);$this->setX(65);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechafac'])));

        $this->setY(124);$this->setX(135);$this->Cell(0,$textypos,utf8_decode($toma[0]['modelovehi']));
        $this->setY(129);$this->setX(135);$this->Cell(0,$textypos,$toma[0]['versionvehi']);
        $this->setY(134);$this->setX(135);$this->Cell(0,$textypos,$toma[0]['nserie']);
        $this->setY(139);$this->setX(135);$this->Cell(5,$textypos,$toma[0]['placasbaja']);
        $this->setY(144);$this->setX(135);$this->Cell(5,$textypos,$toma[0]['facturavehi']);
        $this->setY(149);$this->setX(135);$this->Cell(5,$textypos,utf8_decode($toma[0]['expedida']));

        $this->setY(164);$this->setX(125);$this->Cell(5,$textypos,"$".number_format($toma[0]['costoadquisicion'],2,".",","),'0',0,'L');

        $this->setY(169);$this->setX(125);$this->Cell(5,$textypos,"$".number_format($toma[0]['valorcompra'],2,".",","),'0',0,'L');

        $this->SetFont('Arial','',11);
        $this->setY(86);$this->Multicell(0,$textypos,utf8_decode("De acuerdo al Artículo 126, cuarto párrafo, de la Ley del ISR, solicito a: "),0,"J");


        $this->SetFont('Arial','UB',10);
        $this->setY(86);$this->setX(140);$this->Cell(5,$textypos,utf8_decode($toma[0]['arsocial']));

        $this->setY(108);$this->Cell(0,$textypos,utf8_decode($toma[0]['arsocial']),0,1,"C");

        
$this->SetFont('Arial','',10);
        $this->setY(242);$this->Cell(105,$textypos,"________________________________",0,1,"C");
        $this->setY(249);$this->Cell(105,$textypos,utf8_decode("La firma deberá ser igual a la de la Identificación."),0,1,"C");

        $this->setY(242);$this->Cell(290,$textypos,"________________________________",0,1,"C");	        

        $this->SetFont('Arial','B',9);
        $this->setY(15);$this->setX(156);$this->Cell(5,$textypos,"Fecha de Contrato:");    
        
        $this->setY(246);$this->Cell(105,$textypos,$toma[0]['nombre'],0,1,"C");

        $this->setY(246);$this->Cell(290,$textypos,$toma[0]['arsocial'],0,1,"C");

        $this->setY(60);$this->setX(135);
        $this->Ln();           
    }

    function Footer(){
        $this->SetY(-17);
        $this->Line(10,$this->GetY(),205,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($toma);
$pdf->Output();
