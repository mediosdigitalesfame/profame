<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,4,"Toma de Seminuevo",0,0,'L',1); 
            $this->cell(165,4,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),270,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 130, 2.5,'30','15','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($toma){
        //$this->Cell(20,10,html_escape($toma[0]['facturavehiculo']),0,1,'C');

        //$this->Cell(400,10,html_escape($toma[0]['nameagency']),0,1,'C');
        $this->Ln(5);

        $this->SetFont('Arial','B',10); 

          
        $textypos = 5;

        $this->SetFont('Arial','B',10);    
        $this->setY(15);$this->setX(15);$this->Cell(5,$textypos,"Proveedor");        
        $this->setY(29);$this->setX(15);$this->Cell(5,$textypos,"Domicilio");        
        $this->setY(88);$this->setX(15);$this->Cell(5,$textypos,"Vehiculo"); 

        $this->setY(15);$this->setX(162);$this->Cell(5,$textypos,"Solicitud CFDI:");
        $this->setY(66);$this->setX(162);$this->Cell(5,$textypos,"Datos Agencia:");
        $this->setY(125);$this->setX(162);$this->Cell(5,$textypos,"Carta de No retencion:");
        $this->setY(140);$this->setX(162);$this->Cell(5,$textypos,"Solicitud Cheque:");

        $this->SetFont('Arial','',8); 
        //PROVEEDOR
        $this->setY(19);$this->setX(15);$this->Cell(5,$textypos,"Nombre Completo:");
        $this->setY(19);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['nombre']);

        $this->setY(23);$this->setX(15);$this->Cell(5,$textypos,"Fecha de Contrato:");
        $this->setY(23);$this->setX(95);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechacontrato'])));

        //DOMICILIO
        $this->setY(33);$this->setX(15);$this->Cell(5,$textypos,"Calle:");
        $this->setY(33);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['scalle']));

        $this->setY(37);$this->setX(15);$this->Cell(5,$textypos,"Numero Ext:");
        $this->setY(37);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['numext']);

        $this->setY(41);$this->setX(15);$this->Cell(5,$textypos,"Numero Int:");
        $this->setY(41);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['numint']);

        $this->setY(45);$this->setX(15);$this->Cell(5,$textypos,"Colonia:");
        $this->setY(45);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['scolonia']));

        
        $this->setY(49);$this->setX(15);$this->Cell(5,$textypos,"Ciudad: ");
        $this->setY(49);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['sciudad']));

        $this->setY(53);$this->setX(15);$this->Cell(5,$textypos,"Municipio:");
        $this->setY(53);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['municipio']));

        $this->setY(57);$this->setX(15);$this->Cell(5,$textypos,"Estado:");
        $this->setY(57);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['sestado']));

        $this->setY(61);$this->setX(15);$this->Cell(5,$textypos,"CP:");
        $this->setY(61);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['scp']);

        $this->setY(65);$this->setX(15);$this->Cell(5,$textypos,"RFC:");
        $this->setY(65);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['srfc']);

        $this->setY(69);$this->setX(15);$this->Cell(5,$textypos,utf8_decode("Num. Identificación:"));
        $this->setY(69);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['nidenti']);

        $this->setY(73);$this->setX(15);$this->Cell(5,$textypos,utf8_decode("Tipo Identificación:"));
        $this->setY(73);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['tipoidenti']);

        $this->setY(77);$this->setX(15);$this->Cell(5,$textypos,"Mes del Recibo:");
        $this->setY(77);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['mesrecibo']));

        $this->setY(81);$this->setX(15);$this->Cell(5,$textypos,"Fecha:");
        $this->setY(81);$this->setX(95);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechaidenti'])));

        //VEHICULO
        $this->setY(92);$this->setX(15);$this->Cell(5,$textypos,"Factura que amparael vehiculo No.:");
        $this->setY(92);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['facturavehi']);

        $this->setY(96);$this->setX(15);$this->Cell(5,$textypos,"Expedida por:");
        $this->setY(96);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['expedida']));

        $this->setY(100);$this->setX(15);$this->Cell(5,$textypos,"Fecha de factura");
        $this->setY(100);$this->setX(95);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechafac'])));

        $this->setY(104);$this->setX(15);$this->Cell(5,$textypos,"Marca Vehiculo:");
        $this->setY(104);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['marcavehi']));

        $this->setY(108);$this->setX(15);$this->Cell(5,$textypos,utf8_decode("Año Modelo:"));
        $this->setY(108);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['aniomodelo']);

        $this->setY(112);$this->setX(15);$this->Cell(5,$textypos,"Color Exterior:");
        $this->setY(112);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['colorext']));

        $this->setY(116);$this->setX(15);$this->Cell(5,$textypos,"Color Interior:");
        $this->setY(116);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['colorint']));

        $this->setY(120);$this->setX(15);$this->Cell(5,$textypos,"Tipo Version:");
        $this->setY(120);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['versionvehi']));

        $this->setY(124);$this->setX(15);$this->Cell(5,$textypos,"Clave Vehicular:");
        $this->setY(124);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['clavevehi']);

        $this->setY(128);$this->setX(15);$this->Cell(5,$textypos,"No. de Serie (17 digitos):");
        $this->setY(128);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['nserie']);

        $this->setY(132);$this->setX(15);$this->Cell(5,$textypos,"No. Motor:");
        $this->setY(132);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['nmotor']);

        $this->setY(136);$this->setX(15);$this->Cell(5,$textypos,"Kilometraje:");
        $this->setY(136);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['km']);

        $this->setY(140);$this->setX(15);$this->Cell(5,$textypos,"No. De baja vehicular:");
        $this->setY(140);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['nbaja']);

        $this->setY(144);$this->setX(15);$this->Cell(5,$textypos,"Estado que emite la baja:");
        $this->setY(144);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['edoemisor']));

        $this->setY(148);$this->setX(15);$this->Cell(5,$textypos,"Fecha de la baja:");
        $this->setY(148);$this->setX(95);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechabaja'])));

        $this->setY(152);$this->setX(15);$this->Cell(5,$textypos,utf8_decode("Años de tenencias:"));
        $this->setY(152);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['tenencias']);

        $this->setY(156);$this->setX(15);$this->Cell(5,$textypos,"Placas dadas de baja:");
        $this->setY(156);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['placasbaja']);

        $this->setY(160);$this->setX(15);$this->Cell(5,$textypos,"No. de verificacion:");
        $this->setY(160);$this->setX(95);$this->Cell(5,$textypos,$toma[0]['nverificacion']);

        $this->setY(164);$this->setX(15);$this->Cell(5,$textypos,"Valor de compra (enajenacion):");
        $this->setY(164);$this->setX(95);$this->Cell(5,$textypos,"$".number_format($toma[0]['valorcompra'],2,".",","),'0',0,'L');

        $this->setY(168);$this->setX(15);$this->Cell(5,$textypos,"Valor en letra:");
        $this->setY(168);$this->setX(95);$this->Cell(5,$textypos,"(".utf8_decode($toma[0]['valorletra'])." 00/100 M.N)");

        $this->setY(172);$this->setX(15);$this->Cell(5,$textypos,"Valor Guia Autometrica:");
        $this->setY(172);$this->setX(95);$this->Cell(5,$textypos,"$".number_format($toma[0]['valorautometrica'],2,".",","),'0',0,'L');

        $this->setY(176);$this->setX(15);$this->Cell(5,$textypos,"Nombre a quien se vende la unidad nueva o adecuada:");
        $this->setY(176);$this->setX(95);$this->Cell(5,$textypos,utf8_decode($toma[0]['aquiensevende']));

        $this->setY(180);$this->setX(15);$this->Cell(5,$textypos,"Fecha de factura final (calculo de retencion):");
        $this->setY(180);$this->setX(95);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechafacfinal'])));

        $this->setY(184);$this->setX(15);$this->Cell(5,$textypos,"Fecha de toma por Grupo FAME (fecha de contrato):");
        $this->setY(184);$this->setX(95);$this->Cell(5,$textypos,date("d-m-Y", strtotime($toma[0]['fechatoma'])));

        //SOLICITUD CFDI
        $this->SetFont('Arial','',8);
        $this->setY(19);$this->setX(162);$this->Cell(5,$textypos,"Nombre o Nombres (CFDI):");
        $this->setY(19);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['nombrescfdi']));

        $this->setY(23);$this->setX(162);$this->Cell(5,$textypos,"Primer Apellido (CFDI):");
        $this->setY(23);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['apellido1']));

        $this->setY(27);$this->setX(162);$this->Cell(5,$textypos,"Segundo Apellido (CFDI):");
        $this->setY(27);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['apellido2']));

        $this->setY(31);$this->setX(162);$this->Cell(5,$textypos,"eMail (CFDI):");
        $this->setY(31);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['email']));

        $this->setY(35);$this->setX(162);$this->Cell(5,$textypos,"Telefono Fijo (CFDI):");
        $this->setY(35);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['tel']);

        $this->setY(39);$this->setX(162);$this->Cell(5,$textypos,"Telefono Movil (CFDI):");
        $this->setY(39);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['cel']);

        $this->setY(43);$this->setX(162);$this->Cell(5,$textypos,"Fecha Nacimiento:");
        $this->setY(43);$this->setX(212);$this->Cell(5,$textypos, date("d-m-Y", strtotime($toma[0]['fechanaci'])));

        $this->setY(47);$this->setX(162);$this->Cell(5,$textypos,"Entidad de Nacimiento:");
        $this->setY(47);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['entidadnaci']));

        $this->setY(51);$this->setX(162);$this->Cell(5,$textypos,"Actividad Preponderante:");
        $this->setY(51);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['actividad']));

        $this->setY(55);$this->setX(162);$this->Cell(5,$textypos,"Curp:");
        $this->setY(55);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['curp']);

        $this->setY(59);$this->setX(162);$this->Cell(5,$textypos,"Pais:");
        $this->setY(59);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['pais']));


        //DATOS AGENCIA
        $this->setY(70);$this->setX(162);$this->Cell(5,$textypos,"Razon Social:");
        $this->setY(70);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['arsocial']));

        $this->setY(74);$this->setX(162);$this->Cell(5,$textypos,"Nombre comercial:");
        $this->setY(74);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['aname']));

        $this->setY(78);$this->setX(162);$this->Cell(5,$textypos,utf8_decode("Calle:"));
        $this->setY(78);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['acalle']));

        $this->setY(82);$this->setX(162);$this->Cell(5,$textypos,utf8_decode("Número:"));
        $this->setY(82);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['aext']));

        $this->setY(86);$this->setX(162);$this->Cell(5,$textypos,"Colonia:");
        $this->setY(86);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['acolonia']));

        $this->setY(90);$this->setX(162);$this->Cell(5,$textypos,"Codigo postal:");
        $this->setY(90);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['acp']);

        $this->setY(94);$this->setX(162);$this->Cell(5,$textypos,"Ciudad/Estado (Fiscal):");
        $this->setY(94);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['aciudad']));

        $this->setY(98);$this->setX(162);$this->Cell(5,$textypos,"Gerente General:");
        $this->setY(98);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['ausers_id_gg']));

        $this->setY(102);$this->setX(162);$this->Cell(5,$textypos,"Gerente Seminuevos");
        $this->setY(102);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['ausers_id_gs']));

        $this->setY(106);$this->setX(162);$this->Cell(5,$textypos,"Gerente Administrativo:");
        $this->setY(106);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['ausers_id_ga']));

        $this->setY(110);$this->setX(162);$this->Cell(5,$textypos,"Ciudad / Estado:");
        $this->setY(110);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['aestado']));

        $this->setY(114);$this->setX(162);$this->Cell(5,$textypos,"Sitio WEB Agencia:");
        $this->setY(114);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['aurl']);

        $this->setY(118);$this->setX(162);$this->Cell(5,$textypos,"Link Aviso de Privacidad:");
        $this->setY(118);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['aurlp']);

        //CARTA NO RETENCION
        $this->setY(129);$this->setX(162);$this->Cell(5,$textypos,"Costo de Adquisicion (Ultimo ");
        $this->setY(133);$this->setX(162);$this->Cell(5,$textypos,"valor en Factura):");
        $this->setY(133);$this->setX(212);$this->Cell(5,$textypos,"$".number_format($toma[0]['costoadquisicion'],2,".",","),'0',0,'L');

        //SOLICITUD CHEQUE
        $this->setY(144);$this->setX(162);$this->Cell(5,$textypos,"Banco:");
        $this->setY(144);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['banco']));

        $this->setY(148);$this->setX(162);$this->Cell(5,$textypos,"Cuenta:");
        $this->setY(148);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['cuenta']);

        $this->setY(152);$this->setX(162);$this->Cell(5,$textypos,"CLABE:");
        $this->setY(152);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['clabe']);

        $this->setY(156);$this->setX(162);$this->Cell(5,$textypos,"Sucursal:");
        $this->setY(156);$this->setX(212);$this->Cell(5,$textypos,utf8_decode($toma[0]['sucursal']));

        $this->setY(160);$this->setX(162);$this->Cell(5,$textypos,"Convenio:");
        $this->setY(160);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['convenio']);

        $this->setY(164);$this->setX(162);$this->Cell(5,$textypos,"Referencia:");
        $this->setY(164);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['referencia']);

        $this->setY(168);$this->setX(162);$this->Cell(5,$textypos,"Folio de CFDI:");
        $this->setY(168);$this->setX(212);$this->Cell(5,$textypos,$toma[0]['foliocfdi']);

                

        $this->setY(60);$this->setX(135);
        $this->Ln();

    

        
        


        //$this->setY(180);$this->setX(10);
        //$this->Cell(5,$textypos,"TERMINOS Y CONDICIONES");
        //$this->setY(190);$this->setX(10);
        //$this->Cell(5,$textypos,"El cliente se compromete a pagar la factura.");
      






        //$this->Ln(5);
        //$this->Ln(5);
        //$this->setFont('Arial','',9);
        //$this->setFillColor(255,255,255); 
        //$this->cell(10,6,'1',1,0,'L',1);
        //$this->cell(25,6,'2',1,0,'L',1);
        //$this->cell(50,6,'3',1,0,'L',1);
        //$this->cell(105,6,'4',1,1,'L',1);            
    }

    function Footer(){
        $this->SetY(-10);
        $this->Line(10,$this->GetY(),270,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(-20,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


$pdf = new PDF('L','mm');
$pdf->AliasNbPages();
$pdf->AddPage('L');
$pdf->Content($toma);
$pdf->Output();
