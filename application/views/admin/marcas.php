<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">

    <div class="box add_area" style="display: <?php if($page_title == "Editar"){echo "block";}else{echo "none";} ?>">

      <div class="box-header with-border">
        <?php if (isset($page_title) && $page_title == "Editar"): ?>
          <h3 class="box-title">Editar Marca</h3>
        <?php else: ?>
          <h3 class="box-title">Add Nueva Marca </h3>
        <?php endif; ?>

        <div class="box-tools pull-right">
          <?php if (isset($page_title) && $page_title == "Editar"): ?>
            <?php $required = ''; ?>
            <a href="<?php echo base_url('admin/marcas') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i>Regresar</a>
          <?php else: ?>
            <?php $required = 'required'; ?>
            <a href="#" class="text-right btn btn-primary btn-sm cancel_btn"><i class="fa fa-list"></i> Todas las Marcas</a>
          <?php endif; ?>
        </div>
      </div>

      <div class="box-body">
        <form id="cat-form" method="post" enctype="multipart/form-data" class="validate-form" action="<?php echo base_url('admin/marcas/add')?>" role="form" novalidate>

         <div class="row m-t-30">
          <div class="col-sm-4">
            <div class="form-group">
              <label>Nombre</label>
              <input type="text" class="form-control" name="name" value="<?php echo html_escape($marca[0]['name']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
            </div>
          </div>
        </div>

        <input type="hidden" name="id" value="<?php echo html_escape($marca['0']['id']); ?>">
        <!-- csrf token -->
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

        <hr>

        <div class="row m-t-30">
          <div class="col-sm-12">
            <?php if (isset($page_title) && $page_title == "Editar"): ?>
              <button type="submit" class="btn btn-info pull-left">Guardar Cambios</button>
            <?php else: ?>
              <button type="submit" class="btn btn-info pull-left">Guardar Marca</button>
            <?php endif; ?>
          </div>
        </div>

      </form>
    </div>

    <div class="box-footer">  </div>
  </div>

  <?php if (isset($page_title) && $page_title != "Editar"): ?>

    <div class="box list_area">
      <div class="box-header with-border">
        <?php if (isset($page_title) && $page_title == "Editar"): ?>
          <h3 class="box-title">Editar Marca <a href="<?php echo base_url('admin/marcas') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i> Back</a></h3>
        <?php else: ?>
          <h3 class="box-title">Todas las Marcas </h3>
        <?php endif; ?>

        <div class="box-tools pull-right">
         <a href="#" class="pull-right btn btn-info btn-sm add_btn"><i class="fa fa-plus"></i> Agregar Nueva Marca</a><br>
       </div>
     </div>

     <div class="box-body">

      <div class="col-md-12 col-sm-12 col-xs-12 scroll table-responsive">
        <table class="table table-bordered datatable" id="dg_table">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Status</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=1; foreach ($marcas as $row): ?>
            <tr id="row_<?php echo ($row->id); ?>">

              <td width="5%"><?php echo $i; ?></td>
              <td><?php echo html_escape($row->name); ?></td>

              <td> 
               <?php if ($row->status == 0): ?>
                <div class="label label-table label-danger"><i class="fa fa-times-circle"></i> Inactivo</div>
              <?php else: ?>
                <div class="label label-table label-success"><i class="fa fa-check-circle"></i> Activo</div>
              <?php endif ?>
            </td>

            <td class="actions" width="13%">
              <a href="<?php echo base_url('admin/marcas/edit/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a> &nbsp; 

              <?php if ($row->status == 1): ?>
                <a href="<?php echo base_url('admin/marcas/deactive/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Desactivar"><i class="fa fa-close"></i></a> &nbsp; 
              <?php else: ?>
               <a href="<?php echo base_url('admin/marcas/active/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Activar"><i class="fa fa-check"></i></a> &nbsp; 
             <?php endif ?>

             <a data-val="marca" data-id="<?php echo html_escape($row->id); ?>" href="<?php echo base_url('admin/marcas/delete/'.html_escape($row->id));?>" class="on-default remove-row delete_item" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash-o"></i></a> &nbsp;
             
           </td>

         </tr>
         <?php $i++; endforeach; ?>
       </tbody>
     </table>
   </div>

 </div>

 <div class="box-footer">

 </div>
</div>
<?php endif; ?>

</section>
</div>
