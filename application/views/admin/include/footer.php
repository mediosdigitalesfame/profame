<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
    <div class="version">Version: <?php echo html_escape($settings->version) ?></div>
  </div>
  <a href="<?php echo base_url() ?>"><?php echo html_escape($settings->copyright) ?></a>
</footer>
 
<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $success = $this->session->flashdata('msg'); ?>
<?php $error = $this->session->flashdata('error'); ?>
<input type="hidden" id="success" value="<?php echo html_escape($success); ?>">
<input type="hidden" id="error" value="<?php echo html_escape($error);?>">
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">

<!-- jQuery 3 -->
<!-- <script src="<?php echo base_url() ?>assets/admin/js/jquery 3.5.1.js"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- popper -->
<script src="<?php echo base_url() ?>assets/admin/js/popper.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url() ?>assets/admin/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>
<!-- Custom js -->
<script src="<?php echo base_url() ?>assets/admin/js/admin.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/toast.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/sweet-alert.min.js"></script>
<!-- Datatables-->
<script src="<?php echo base_url() ?>assets/admin/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/validation.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url() ?>assets/admin/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/ckeditor/ckeditor.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>assets/admin/js/fastclick.js"></script>
<!-- MinimalPro Admin App -->
<script src="<?php echo base_url() ?>assets/admin/js/template.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/bootstrap-datepicker.min.js"></script>
<!-- MinimalPro Admin for demo purposes -->
<script src="<?php echo base_url() ?>assets/admin/js/demo.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/select2.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/bootstrap4-toggle.min.js"> </script>

<!-- Color Picker Plugin JavaScript -->
<script src="<?php echo base_url() ?>assets/admin/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

<!-- bt-switch -->
<script src="<?php echo base_url() ?>assets/admin/js/bootstrap-switch.min.js"></script>
<script type="text/javascript">
  $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
  var radioswitch = function() {
    var bt = function() {
      $(".radio-switch").on("switch-change", function() {
        $(".radio-switch").bootstrapSwitch("toggleRadioState")
      }), $(".radio-switch").on("switch-change", function() {
        $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck")
      }), $(".radio-switch").on("switch-change", function() {
        $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck", !1)
      })
    };
    return {
      init: function() {
        bt()
      }
    }
  }();
  $(document).ready(function() {
    radioswitch.init()
  });
</script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="<?php echo base_url() ?>assets/admin/js/jQuery.style.switcher.js"></script>

<script type="text/javascript">
  <?php if (isset($success)): ?>
  $(document).ready(function() {
    var msg = $('#success').val();
    $.toast({
      heading: 'Correcto',
      text: msg,
      position: 'top-right',
      loaderBg:'#fff',
      icon: 'success',
      hideAfter: 3500
    });

  });
  <?php endif; ?>

  <?php if (isset($error)): ?>
  $(document).ready(function() {
    var msg = $('#error').val();
    $.toast({
      heading: 'Error',
      text: msg,
      position: 'top-right',
      loaderBg:'#fff',
      icon: 'error',
      hideAfter: 3500
    });

  });
  <?php endif; ?>
</script>

<script>
  ! function(window, document, $) {
    "use strict";
    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
  }(window, document, jQuery);
</script>

<script type="text/javascript">
  jQuery('#datepicker').datepicker({
    format: 'yyyy-mm-dd'
  });

      //colorpicker start
      $('.colorpicker-default').colorpicker({
        format: 'hex'
      });
      $('.colorpicker-rgba').colorpicker();

      $('.multiple_select').select2();
      $('.single_select').select2();
    </script>

    <!-- Ckeditor -->
    <script>
      if($("#ckEditor").length > 0){
        CKEDITOR.replace('ckEditor', {
          language: 'en'
        });
      }



    </script>

 


    <?php switch ($page_title) {
      case 'Guias Rapidas':
      $this->load->view('include/guias-js');  
      break;

      case 'Tomar Seminuevos':
      $this->load->view('include/tomas-js');
      break;  

      case 'Appointment':
      $this->load->view('include/appoint-js');
      break;  

      case 'Dashboard':
      $this->load->view('include/dash-js');
      break;      
      
      default:
      break;
    }
    ?>

    <?php switch ($page) {
      case 'Editar Guia':
      $this->load->view('include/guias-js');
      $this->load->view('include/guias3-js');  
      break;

      case 'Agregar Guia':
      $this->load->view('include/guias-js');
      $this->load->view('include/guias2-js');  
      break;

         
      
      default:
      break;
    }
    ?>

 

    <?php $this->load->view('include/stripe-js'); ?>

  </body>
  </html>

