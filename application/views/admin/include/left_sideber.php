 <aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar mt-10">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">


      <?php if (is_admin()): ?>
        <li class="<?php if(isset($page_title) && $page_title == "Dashboard"){echo "active";} ?>">
          <a href="<?php echo base_url('admin/dashboard') ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>

        <li class="<?php if(isset($page_title) && $page_title == "Settings"){echo "active";} ?>">
          <a href="<?php echo base_url('admin/settings') ?>">
            <i class="fa fa-cog"></i> <span>Configuraciones</span>
          </a>
        </li>

          <!--<li class="<?php if(isset($page_title) && $page_title == "Payment Settings"){echo "active";} ?>">
            <a href="<?php echo base_url('admin/payment/settings') ?>">
              <i class="fa fa-money"></i> <span>Configuración de pago</span>
            </a>
          </li>-->

          <!--<li class="<?php if(isset($page_title) && $page_title == "Package"){echo "active";} ?>">
            <a href="<?php echo base_url('admin/package') ?>">
              <i class="fa fa-dollar"></i> <span>Precios de Paquetes</span>
            </a>
          </li>-->

          <li class="treeview <?php if(isset($page_title) && $page_title == "Grupo " || isset($page) && $page == "Grupo"){echo "active";} ?>">
            <a href="#"><i class="fa fa-google"></i>
              <span>Grupo</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('admin/empresas') ?>"><i class="fa fa-circle"></i>Empresas</a></li>
              <li><a href="<?php echo base_url('admin/marcas') ?>"><i class="fa fa-circle"></i>Marcas</a></li>
              <li><a href="<?php echo base_url('admin/agencias') ?>"><i class="fa fa-circle"></i>Agencias</a></li>
            </ul>
          </li> 

          <li class="treeview <?php if(isset($page_title) && $page_title == "Seminuevos " || isset($page) && $page == "Seminuevos"){echo "active";} ?>">
            <a href="#"><i class="fa fa-handshake-o"></i>
              <span>Seminuevos</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if (is_superadmin()): ?>

                <li><a href="<?php echo base_url('admin/guias') ?>"><i class="fa fa-circle"></i>Guias Rapidas</a></li>
              <?php else: ?>
                <li><a href="<?php echo base_url('admin/guias') ?>"><i class="fa fa-circle"></i>Guia Rapida</a></li>
              <?php endif ?>

              <li class="treeview <?php if(isset($page_title) && $page_title == "Tomar Seminuevos" || isset($page) && $page == "Tomar Seminuevos"){echo "active";} ?>">
                <a href="#"><i class="fa fa-car"></i>
                  <span>Tomar Seminuevos</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo base_url('admin/tomas') ?>"><i class="fa fa-circle"></i>Llenar Datos</a></li>
                  <li><a href="<?php echo base_url('admin/checklist') ?>"><i class="fa fa-circle"></i>Check List</a></li>
                </ul>
              </li> 


            </ul>
          </li> 

          <li class="treeview <?php if(isset($page_title) && $page_title == "Leads " || isset($page) && $page == "Leads"){echo "active";} ?>">
            <a href="#"><i class="fa fa-id-card-o"></i>
              <span>Leads</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('admin/leads') ?>"><i class="fa fa-circle"></i>Añadir Leads</a></li>
              <li><a href="<?php echo base_url('admin/blog') ?>"><i class="fa fa-circle"></i>Seguimientos</a></li>
              <li><a href="<?php echo base_url('admin/asesores') ?>"><i class="fa fa-circle"></i>Asesores</a></li>
              <li><a href="<?php echo base_url('admin/procedencias') ?>"><i class="fa fa-circle"></i>Procedencias</a></li>
              <li><a href="<?php echo base_url('admin/eventos') ?>"><i class="fa fa-circle"></i>Eventos</a></li>
              <li><a href="<?php echo base_url('admin/solicitudes') ?>"><i class="fa fa-circle"></i>Solicitudes</a></li>
              <li><a href="<?php echo base_url('admin/tipos') ?>"><i class="fa fa-circle"></i>Tipos</a></li>
            </ul>
          </li> 

           <!--<li class="treeview <?php if(isset($page_title) && $page_title == "Clientes " || isset($page) && $page == "Clientes"){echo "active";} ?>">
            <a href="#"><i class="fa fa-file-text"></i>
              <span>Clientes</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('admin/clientes') ?>"><i class="fa fa-circle"></i>Añadir Cliente</a></li>
            </ul>
          </li> -->

          <li class="<?php if(isset($page_title) && $page_title == "Users"){echo "active";} ?>">
            <a href="<?php echo base_url('admin/users') ?>">
              <i class="fa fa-users"></i> <span>Usuarios</span>
            </a>
          </li>

          <!--<li class="treeview <?php if(isset($page_title) && $page_title == "Blog " || isset($page) && $page == "Blog"){echo "active";} ?>">
            <a href="#"><i class="fa fa-file-text"></i>
              <span>Blog</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('admin/blog_category') ?>"><i class="fa fa-circle"></i>Añadir Categoría</a></li>
              <li><a href="<?php echo base_url('admin/blog') ?>"><i class="fa fa-circle"></i>Publicaciones del Blog</a></li>
            </ul>
          </li> -->

          <!--<li class="<?php if(isset($page_title) && $page_title == "Service"){echo "active";} ?>">
            <a href="<?php echo base_url('admin/services') ?>">
              <i class="fa fa-cog"></i> <span>Servicios</span>
            </a>
          </li>-->

          <!--<li class="<?php if(isset($page_title) && $page_title == "Faq"){echo "active";} ?>">
            <a href="<?php echo base_url('admin/faq') ?>">
              <i class="fa fa-question-circle"></i> <span>Faq</span>
            </a>
          </li>-->

          <!--<li class="<?php if(isset($page_title) && $page_title == "Pages"){echo "active";} ?>">
            <a href="<?php echo base_url('admin/pages') ?>">
              <i class="fa fa-file-text-o"></i> <span>Páginas</span>
            </a>
          </li>-->

          <li class="<?php if(isset($page_title) && $page_title == "Contact"){echo "active";} ?>">
            <a href="<?php echo base_url('admin/contact') ?>">
              <i class="fa fa-envelope"></i> <span>Mensajes</span>
            </a>
          </li>

        <?php else: ?>

          <li class="<?php if(isset($page_title) && $page_title == "Profile"){echo "active";} ?>">
            <a href="<?php echo base_url('admin/profile') ?>">
              <i class="fa fa-user-o"></i> <span>Mi Perfil</span>
            </a>
          </li>

          <li class="<?php if(isset($page_title) && $page_title == "Subscription"){echo "active";} ?>">
            <a href="<?php echo base_url('admin/subscription') ?>">
              <i class="fa fa-dollar"></i> <span>Subscripción</span>
            </a>
          </li>

          <?php if (check_my_payment_status() == TRUE || settings()->enable_paypal == 0): ?>
            <?php include'left_menu.php'; ?>
          <?php endif ?>
        <?php endif ?>

        <li class="<?php if(isset($page_title) && $page_title == "Change Password"){echo "active";} ?>">
          <a href="<?php echo base_url('change_password') ?>">
            <i class="fa fa-lock"></i> <span>Cambiar Password</span>
          </a>
        </li>

      </ul>
    </section>
  </aside>