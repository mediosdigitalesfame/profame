<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">

    <div class="box add_area" style="display: <?php if($page_title == "Editar"){echo "block";}else{echo "none";} ?>">

      <div class="box-header with-border">
        <?php if (isset($page_title) && $page_title == "Editar"): ?>
          <h3 class="box-title">Editar Empresa</h3>
        <?php else: ?>
          <h3 class="box-title">Add Nueva Empresa </h3>
        <?php endif; ?>

        <div class="box-tools pull-right">
          <?php if (isset($page_title) && $page_title == "Editar"): ?>
            <?php $required = ''; ?>
            <a href="<?php echo base_url('admin/empresas') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i>Regresar</a>
          <?php else: ?>
            <?php $required = 'required'; ?>
            <a href="#" class="text-right btn btn-primary btn-sm cancel_btn"><i class="fa fa-list"></i> Todas las Empresas</a>
          <?php endif; ?>
        </div>
      </div>

      <div class="box-body">
        <form id="cat-form" method="post" enctype="multipart/form-data" class="validate-form" action="<?php echo base_url('admin/empresas/add')?>" role="form" novalidate>

         <div class="row m-t-30">
          <div class="col-sm-4">
            <div class="form-group">
              <label>Nombre</label>
              <input type="text" class="form-control" name="nombre" value="<?php echo html_escape($empresa[0]['nombre']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label>RFC</label>
              <input type="text" class="form-control" name="rfc" value="<?php echo html_escape($empresa[0]['rfc']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="col-sm-5">
            <div class="form-group">
              <label>Razon Social</label>
              <input type="text" class="form-control" name="rsocial" value="<?php echo html_escape($empresa[0]['rsocial']); ?>" <?php echo html_escape($required); ?>><p class="help-block text-danger"></p>
            </div>
          </div>
        </div>

        <div class="row m-t-30">
          
          <div class="col-sm-4">
            <div class="form-group">
              <label>Calle</label>
              <input type="text" class="form-control" name="calle" value="<?php echo html_escape($empresa[0]['calle']); ?>" >
            </div>
          </div>
           <div class="col-sm-1">
            <div class="form-group">
              <label>No.Int</label>
              <input type="text" class="form-control" name="int" value="<?php echo html_escape($empresa[0]['int']); ?>" >
            </div>
          </div>
           <div class="col-sm-1">
            <div class="form-group">
              <label>No.Ext</label>
              <input type="text" class="form-control" name="ext" value="<?php echo html_escape($empresa[0]['ext']); ?>" >
            </div>
          </div>
           <div class="col-sm-4">
            <div class="form-group">
              <label>Colonia</label>
              <input type="text" class="form-control" name="colonia" value="<?php echo html_escape($empresa[0]['colonia']); ?>" >
            </div>
          </div>
          <div class="col-sm-2">
            <div class="form-group">
              <label>CP</label>
              <input type="text" class="form-control" name="cp" value="<?php echo html_escape($empresa[0]['cp']); ?>" >
            </div>
          </div>
        </div>

        <div class="row m-t-30">
           <div class="col-sm-4">
            <div class="form-group">
              <label>Ciudad</label>
              <input type="text" class="form-control" name="ciudad" value="<?php echo html_escape($empresa[0]['ciudad']); ?>" >
            </div>
          </div>
           <div class="col-sm-4">
            <div class="form-group">
              <label>Estado</label>
              <input type="text" class="form-control" name="estado" value="<?php echo html_escape($empresa[0]['estado']); ?>" >
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label>Telefono</label>
              <input type="number" class="form-control" name="telefono" value="<?php echo html_escape($empresa[0]['tel']); ?>" >
            </div>
          </div>
          
        </div>

        <div class="row m-t-30">
          <div class="col-sm-4">
            <div class="form-group">
              <label>Banco</label>
              <input type="text" class="form-control" name="banco" value="<?php echo html_escape($empresa[0]['bank']); ?>">
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label>Cuenta</label>
              <input type="text" class="form-control" name="cuenta" value="<?php echo html_escape($empresa[0]['account']); ?>">
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label>Pago</label>
              <input type="text" class="form-control" name="pago" value="<?php echo html_escape($empresa[0]['pay']); ?>">
            </div>
          </div>
        </div>

        <input type="hidden" name="id" value="<?php echo html_escape($empresa['0']['id']); ?>">
        <!-- csrf token -->
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

        <hr>

        <div class="row m-t-30">
          <div class="col-sm-12">
            <?php if (isset($page_title) && $page_title == "Editar"): ?>
              <button type="submit" class="btn btn-info pull-left">Guardar Cambios</button>
            <?php else: ?>
              <button type="submit" class="btn btn-info pull-left">Guardar Empresa</button>
            <?php endif; ?>
          </div>
        </div>

      </form>
    </div>

    <div class="box-footer">  </div>
  </div>

  <?php if (isset($page_title) && $page_title != "Editar"): ?>

    <div class="box list_area">
      <div class="box-header with-border">
        <?php if (isset($page_title) && $page_title == "Editar"): ?>
          <h3 class="box-title">Editar Empresa <a href="<?php echo base_url('admin/empresas') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i> Back</a></h3>
        <?php else: ?>
          <h3 class="box-title">Todas las Empresas </h3>
        <?php endif; ?>

        <div class="box-tools pull-right">
         <a href="#" class="pull-right btn btn-info btn-sm add_btn"><i class="fa fa-plus"></i> Agregar Nueva Empresa</a><br>
       </div>
     </div>

     <div class="box-body">

      <div class="col-md-12 col-sm-12 col-xs-12 scroll table-responsive">
        <table class="table table-bordered datatable" id="dg_table">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>RFC</th>
              <th>R. Social</th>
              <!--<th>Tel</th>
              <th>Domicilio</th>
              <th>Ciudad</th>
              <th>Creación</th>-->
              <th>Status</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=1; foreach ($empresas as $row): ?>
            <tr id="row_<?php echo ($row->id); ?>">

              <td width="5%"><?php echo $i; ?></td>
              <td><?php echo html_escape($row->nombre); ?></td>
              <td><?php echo html_escape($row->rfc); ?></td>
              <td><?php echo html_escape($row->rsocial); ?></td>
              <!--<td><?php echo html_escape($row->tel); ?></td>
              <td><?php echo html_escape($row->calle); ?></td>
              <td><?php echo html_escape($row->ciudad); ?></td>
              <td><?php echo html_escape($row->created_at); ?></td>-->

              <td> 
               <?php if ($row->status == 0): ?>
                <div class="label label-table label-danger"><i class="fa fa-times-circle"></i> Inactivo</div>
              <?php else: ?>
                <div class="label label-table label-success"><i class="fa fa-check-circle"></i> Activo</div>
              <?php endif ?>
            </td>

            <td class="actions" width="13%">
              <a href="<?php echo base_url('admin/empresas/edit/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a> &nbsp; 

              <?php if ($row->status == 1): ?>
                <a href="<?php echo base_url('admin/empresas/deactive/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Desactivar"><i class="fa fa-close"></i></a> &nbsp; 
              <?php else: ?>
               <a href="<?php echo base_url('admin/empresas/active/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Activar"><i class="fa fa-check"></i></a> &nbsp; 
             <?php endif ?>

             <a data-val="empresa" data-id="<?php echo html_escape($row->id); ?>" href="<?php echo base_url('admin/empresas/delete/'.html_escape($row->id));?>" class="on-default remove-row delete_item" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash-o"></i></a> &nbsp;

             <!--<a href="<?php echo base_url('admin/empresas/export_pdf/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Exportar"><i class="fa fa-pdf"></i></a> &nbsp; 

              <a href="<?php echo base_url('admin/empresas/export_pdf2/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Exportar"><i class="fa fa-check"></i></a> &nbsp;--> 

             
           </td>

         </tr>
         <?php $i++; endforeach; ?>
       </tbody>
     </table>
   </div>

 </div>

 <div class="box-footer">

 </div>
</div>
<?php endif; ?>

</section>
</div>
