<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="box add_area" style="display: <?php if($page_title == "Editar"){echo "block";}else{echo "none";} ?>">
      <div class="box-header with-border">
        <?php if (isset($page_title) && $page_title == "Editar"): ?>
          <h3 class="box-title">Editar Toma</h3>
        <?php else: ?>
          <h3 class="box-title">Add Nueva Toma </h3>
        <?php endif; ?>
        <div class="box-tools pull-right">
          <?php if (isset($page_title) && $page_title == "Editar"): ?>
            <?php $required = ''; ?>
            <a href="<?php echo base_url('admin/chec') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i>Regresar</a>
          <?php else: ?>
            <?php $required = 'required'; ?>
            <a href="#" class="text-right btn btn-primary btn-sm cancel_btn"><i class="fa fa-list"></i> Todas las chec</a>
          <?php endif; ?>
        </div>
      </div>

      <div class="box-body">
        <form id="cat-form" method="post" enctype="multipart/form-data" class="validate-form" action="<?php echo base_url('admin/chec/add')?>" role="form" novalidate>

         <br> 
         <div class="row m-t-30">


 

          <div class="col-sm-2" align="right"> <h4>Guía Rápida</h4></div>
          <div class="col-sm-4">
           <div class="form-group">
            <select class="form-control single_select" id="tomasel" name="tomasel" style="width: 100%" required >
              <option value="">Seleccionar</option>
              <?php foreach ($tomag as $tomar): ?>
                <?php if (!empty($tomar->id)): ?>
                  <option value="<?php echo html_escape($tomar->id); ?>" 
                    <?php echo ($result[0]['id'] == $tomar->id) ? 'selected' : ''; ?>>
                    <?php echo html_escape($tomar->folio).'-'.html_escape($tomar->vin); ?>
                  </option>
                <?php endif ?>
              <?php endforeach ?>
            </select>
            <p class="help-block text-danger"></p>
          </div>
        </div> 
      </div>

      <input type="hidden" name="id" value="<?php echo html_escape($toma['0']['id']); ?>">



      <div class="row m-t-30">
        <div class="col-sm-3">
          <div class="form-group mb-40 mr-30">
            <label class="mt-5">Solicitud CFDI</label>
            <span class="pull-right"><input type="checkbox" name="solicitudcfdi" id="solicitudcfdi" value="1" <?php if($toma[0]['solicitudcfdi'] = 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span> 
          </div>
        </div>

        <div class="col-sm-3">
          <div class="form-group mb-40 mr-30">
            <label class="mt-5">Cambio de Rol</label>
            <span class="pull-right"><input type="checkbox" name="cambioderol" id="cambioderol" value="1" <?php if($toma[0]['cambioderol'] = 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="form-group mb-40 mr-30">
            <label class="mt-5">Cédula de Identificación Fiscal</label>
            <span class="pull-right"><input type="checkbox" name="cedulafiscal" id="cedulafiscal" value="1" <?php if($toma[0]['cedulafiscal'] = 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="form-group mb-40 mr-30">
            <label class="mt-5">CFDI PF Sin Actividad Emp.</label>
            <span class="pull-right"><input type="checkbox" name="cfdisinactividad" id="cfdisinactividad" value="0" <?php if($toma[0]['cfdisinactividad'] = 1){echo 'checked';} ?> data-toggle="toggle" data-onstyle="info" data-width="100"></span>
          </div>
        </div>
      </div>


      <!-- csrf token -->
      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

      <hr>

      <div class="row m-t-30">
        <div class="col-sm-12">
          <?php if (isset($page_title) && $page_title == "Editar"): ?>
            <button type="submit" class="btn btn-info pull-left">Guardar Cambios</button>
          <?php else: ?>
            <button type="submit" class="btn btn-info pull-left">Guardar toma</button>
          <?php endif; ?>
        </div>
      </div>

    </form>
  </div>

  <div class="box-footer">  </div>
</div>

<?php if (isset($page_title) && $page_title != "Editar"): ?>

  <div class="box list_area">
    <div class="box-header with-border">
      <?php if (isset($page_title) && $page_title == "Editar"): ?>
        <h3 class="box-title">Editar toma <a href="<?php echo base_url('admin/chec') ?>" class="pull-right btn btn-primary btn-sm"><i class="fa fa-angle-left"></i> Back</a></h3>
      <?php else: ?>
        <h3 class="box-title">Todas las chec </h3>
      <?php endif; ?>

      <div class="box-tools pull-right">
       <a href="#" class="pull-right btn btn-info btn-sm add_btn"><i class="fa fa-plus"></i> Agregar Nueva Toma</a><br>
     </div>
   </div>

   <div class="box-body">

    <div class="col-md-12 col-sm-12 col-xs-12 scroll table-responsive">
      <table class="table table-bordered datatable" id="dg_table">
        <thead>
          <tr>
            <th>#</th>
            <th>Agencia</th>
            <th>Folio Guía</th>
            <th>Vin del Auto</th>
            <th>Status</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach ($tomas as $row): ?>
          <tr id="row_<?php echo ($row->id); ?>">

            <td width="5%"><?php echo $i; ?></td>
            <td><?php echo html_escape($row->nameag); ?></td>
            <td><?php echo html_escape($row->folio); ?></td>
            <td><?php echo html_escape($row->vin); ?></td>
            
            <td> 
             <?php if ($row->status == 0): ?>
              <div class="label label-table label-success"><i class="fa fa-times-circle"></i> Libre</div>
            <?php elseif ($row->status == 1): ?>
              <div class="label label-table label-danger"><i class="fa fa-chec-circle"></i> Pendiente</div>
            <?php elseif ($row->status == 2): ?>
              <div class="label label-table label-warning"><i class="fa fa-chec-circle"></i> Procesando</div>
            <?php elseif ($row->status == 3): ?>
              <div class="label label-table label-success"><i class="fa fa-chec-circle"></i> Terminado</div>
            <?php endif ?>
          </td>

          <td class="actions" width="35%">
            <a href="<?php echo base_url('admin/chec/edit/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a> &nbsp; 

            <a data-val="toma" data-id="<?php echo html_escape($row->id); ?>" href="<?php echo base_url('admin/chec/delete/'.html_escape($row->id));?>" class="on-default remove-row delete_item" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash-o"></i></a>&nbsp; 
            
            <a href="<?php echo base_url('admin/chec/export_pdf_checlist/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Exportar"><i class="fa fa-file-pdf-o"></i></a> &nbsp; 


 <?php if ($row->status == 1): ?>
            <a href="<?php echo base_url('admin/chec/deactive/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Desactivar"><i class="fa fa-close"></i></a> &nbsp; 
          <?php else: ?>
           <a href="<?php echo base_url('admin/chec/active/'.html_escape($row->id));?>" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="Activar"><i class="fa fa-check"></i></a> &nbsp; 
         <?php endif ?>



          </td>

        </tr>
        <?php $i++; endforeach; ?>

      </tbody>
    </table>
  </div>

</div>

<div class="box-footer">

</div>
</div>
<?php endif; ?>

</section>
</div>


<script type="text/javascript">
  $(document).ready(function(){

    $('#cliente').change(function(){ 
      var id=$(this).val();
      $.ajax({
        url : "<?php echo site_url('tomas/get_datoscliente');?>",
        method : "POST",
        data : {id: id},
        async : true,
        dataType : 'json',
        success: function(data){

          var html = '';
          var i;
          for(i=0; i<data.length; i++){
            html += '<option value='+data[i].nombre+'>'+data[i].rfc+'</option>';
          }
          $('#datoscliente').html(html);

        }
      });
      return false;
    }); 

  });
</script>