<div role="main" class="main">
 

    <div class="slider-container rev_slider_wrapper" style="height: 530px;">
        <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1920, 'gridheight': 550, 'responsiveLevels': [4096,1200,992,500], 'navigation' : {'arrows': { 'enable': true }, 'bullets': {'enable': false, 'style': 'bullets-style-1 bullets-color-primary', 'h_align': 'center', 'v_align': 'bottom', 'space': 7, 'v_offset': 70, 'h_offset': 0}}}">
            <!-- <?php //$this->load->view('grupo/content/banners'); ?>-->

            <ul>

                <li class="slide-overlay" data-transition="fade">
                    <img src="<?php echo base_url(); ?>assets/porto/img/blank.gif"  
                    alt=""
                    data-bgposition="center center" 
                    data-bgfit="cover" 
                    data-bgrepeat="no-repeat" 
                    class="rev-slidebg">

                    <div class="rs-background-video-layer" 
                    data-forcerewind="on" 
                    data-volume="mute" 
                    data-videowidth="100%" 
                    data-videoheight="100%" 
                    data-videomp4="<?php echo base_url(); ?>assets/porto/video/Cars - 2269.mp4" 
                    data-videopreload="preload" 
                    data-videoloop="loop" 
                    data-forceCover="1" 
                    data-aspectratio="16:9" 
                    data-autoplay="true" 
                    data-autoplayonlyfirsttime="false" 
                    data-nextslideatend="false">
                </div>

                <div class="tp-caption"
                data-x="center" data-hoffset="['-125','-125','-125','-215']"
                data-y="center" data-voffset="['-50','-50','-50','-75']"
                data-start="1000"
                data-transform_in="x:[-300%];opacity:0;s:500;"
                data-transform_idle="opacity:0.2;s:500;" style="z-index: 5;"><img src="<?php echo base_url(); ?>assets/porto/img/slides/slide-title-border.png" alt=""></div>

                <div class="tp-caption text-color-light font-weight-normal"
                data-x="center"
                data-y="center" data-voffset="['-50','-50','-50','-75']"
                data-start="700"
                data-fontsize="['22','22','22','40']"
                data-lineheight="['25','25','25','45']"
                data-transform_in="y:[-50%];opacity:0;s:500;" style="z-index: 5;"> </div>

                <div class="tp-caption d-none d-md-block"
                data-frames='[{"delay":2400,"speed":500,"frame":"0","from":"opacity:0;x:10%;","to":"opacity:1;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                data-x="center" data-hoffset="['72','72','72','127']"
                data-y="center" data-voffset="['-33','-33','-33','-55']" style="z-index: 5;">
                     <img src="<?php echo base_url() ?>assets/porto/img/slides/slide-blue-line.png" alt="illustration">
                </div>

                <div class="tp-caption"
                data-x="center" data-hoffset="['125','125','125','215']"
                data-y="center" data-voffset="['-50','-50','-50','-75']"
                data-start="1000"
                data-transform_in="x:[300%];opacity:0;s:500;"
                data-transform_idle="opacity:0.2;s:500;" style="z-index: 5;"><img src="<?php echo base_url(); ?>assets/porto/img/slides/slide-title-border.png" alt=""></div>

                <div class="tp-caption font-weight-extra-bold text-color-light negative-ls-2"
                data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                data-x="center"
                data-y="center"
                data-fontsize="['50','50','50','90']"
                data-lineheight="['55','55','55','95']" style="z-index: 5;"> </div>

                <div class="tp-caption font-weight-light"
                data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                data-x="center"
                data-y="center" data-voffset="['40','40','40','80']"
                data-fontsize="['18','18','18','50']"
                data-lineheight="['20','20','20','55']"
                style="color: #b5b5b5; z-index: 5;"> </div>

                <a class="tp-caption slider-scroll-button"
                data-hash
                data-hash-offset="80"
                href="#marcas"
                data-x="center"
                data-y="bottom" data-voffset="['30','30','30','30']"
                data-start="1600"                    
                data-transform_in="y:[100%];s:500;"
                data-transform_out="y:[100%];opacity:0;s:500;"
                data-mask_in="x:0px;y:0px;" style="z-index: 5;"></a>

                <div class="tp-dottedoverlay tp-opacity-overlay"></div>
            </li>

            <li class="slide-overlay" data-transition="fade">
                <img src="<?php echo base_url(); ?>assets/porto/img/blank.gif"  
                alt=""
                data-bgposition="center center" 
                data-bgfit="cover" 
                data-bgrepeat="no-repeat" 
                class="rev-slidebg">

                <div class="rs-background-video-layer" 
                data-forcerewind="on" 
                data-volume="mute" 
                data-videowidth="100%" 
                data-videoheight="100%" 
                data-videomp4="<?php echo base_url(); ?>assets/porto/video/City - 8248.mp4" 
                data-videopreload="preload" 
                data-videoloop="loop" 
                data-forceCover="1" 
                data-aspectratio="16:9" 
                data-autoplay="true" 
                data-autoplayonlyfirsttime="false" 
                data-nextslideatend="false">
            </div>

            <div class="tp-caption"
            data-x="center" data-hoffset="['-125','-125','-125','-215']"
            data-y="center" data-voffset="['-50','-50','-50','-75']"
            data-start="1000"
            data-transform_in="x:[-300%];opacity:0;s:500;"
            data-transform_idle="opacity:0.2;s:500;" style="z-index: 5;"><img src="<?php echo base_url(); ?>assets/porto/img/slides/slide-title-border.png" alt=""></div>

            <div class="tp-caption text-color-light font-weight-normal"
            data-x="center"
            data-y="center" data-voffset="['-50','-50','-50','-75']"
            data-start="700"
            data-fontsize="['22','22','22','40']"
            data-lineheight="['25','25','25','45']"
            data-transform_in="y:[-50%];opacity:0;s:500;" style="z-index: 5;"> </div>

            <div class="tp-caption d-none d-md-block"
            data-frames='[{"delay":2400,"speed":500,"frame":"0","from":"opacity:0;x:10%;","to":"opacity:1;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
            data-x="center" data-hoffset="['72','72','72','127']"
            data-y="center" data-voffset="['-33','-33','-33','-55']" style="z-index: 5;"><img src="<?php echo base_url(); ?>assets/porto/img/slides/slide-blue-line.png" alt=""></div>

            <div class="tp-caption"
            data-x="center" data-hoffset="['125','125','125','215']"
            data-y="center" data-voffset="['-50','-50','-50','-75']"
            data-start="1000"
            data-transform_in="x:[300%];opacity:0;s:500;"
            data-transform_idle="opacity:0.2;s:500;" style="z-index: 5;"><img src="<?php echo base_url(); ?>assets/porto/img/slides/slide-title-border.png" alt=""></div>

            <div class="tp-caption font-weight-extra-bold text-color-light negative-ls-2"
            data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
            data-x="center"
            data-y="center"
            data-fontsize="['50','50','50','90']"
            data-lineheight="['55','55','55','95']" style="z-index: 5;"> </div>

            <div class="tp-caption font-weight-light"
            data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
            data-x="center"
            data-y="center" data-voffset="['40','40','40','80']"
            data-fontsize="['18','18','18','50']"
            data-lineheight="['20','20','20','55']"
            style="color: #b5b5b5; z-index: 5;"> </div>

            <a class="tp-caption slider-scroll-button"
            data-hash
            data-hash-offset="80"
            href="#marcas"
            data-x="center"
            data-y="bottom" data-voffset="['30','30','30','30']"
            data-start="1600"                    
            data-transform_in="y:[100%];s:500;"
            data-transform_out="y:[100%];opacity:0;s:500;"
            data-mask_in="x:0px;y:0px;" style="z-index: 5;"></a>

            <div class="tp-dottedoverlay tp-opacity-overlay"></div>
        </li>

        <li class="slide-overlay" data-transition="fade">
            <img src="<?php echo base_url(); ?>assets/porto/img/blank.gif"  
            alt=""
            data-bgposition="center center" 
            data-bgfit="cover" 
            data-bgrepeat="no-repeat" 
            class="rev-slidebg">

            <div class="rs-background-video-layer" 
            data-forcerewind="on" 
            data-volume="mute" 
            data-videowidth="100%" 
            data-videoheight="100%" 
            data-videomp4="<?php echo base_url(); ?>assets/porto/video/TYPE_R.mp4" 
            data-videopreload="preload" 
            data-videoloop="loop" 
            data-forceCover="1" 
            data-aspectratio="16:9" 
            data-autoplay="true" 
            data-autoplayonlyfirsttime="false" 
            data-nextslideatend="false">
        </div>

        <div class="tp-caption"
        data-x="center" data-hoffset="['-125','-125','-125','-215']"
        data-y="center" data-voffset="['-50','-50','-50','-75']"
        data-start="1000"
        data-transform_in="x:[-300%];opacity:0;s:500;"
        data-transform_idle="opacity:0.2;s:500;" style="z-index: 5;"><img src="<?php echo base_url(); ?>assets/img/slides/slide-title-border.png" alt=""></div>

        <div class="tp-caption text-color-light font-weight-normal"
        data-x="center"
        data-y="center" data-voffset="['-50','-50','-50','-75']"
        data-start="700"
        data-fontsize="['22','22','22','40']"
        data-lineheight="['25','25','25','45']"
        data-transform_in="y:[-50%];opacity:0;s:500;" style="z-index: 5;"> </div>

        <div class="tp-caption d-none d-md-block"
        data-frames='[{"delay":2400,"speed":500,"frame":"0","from":"opacity:0;x:10%;","to":"opacity:1;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
        data-x="center" data-hoffset="['72','72','72','127']"
        data-y="center" data-voffset="['-33','-33','-33','-55']" style="z-index: 5;"><img src="<?php echo base_url(); ?>assets/img/slides/slide-blue-line.png" alt=""></div>

        <div class="tp-caption"
        data-x="center" data-hoffset="['125','125','125','215']"
        data-y="center" data-voffset="['-50','-50','-50','-75']"
        data-start="1000"
        data-transform_in="x:[300%];opacity:0;s:500;"
        data-transform_idle="opacity:0.2;s:500;" style="z-index: 5;"><img src="<?php echo base_url(); ?>assets/img/slides/slide-title-border.png" alt=""></div>

        <div class="tp-caption font-weight-extra-bold text-color-light negative-ls-2"
        data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
        data-x="center"
        data-y="center"
        data-fontsize="['50','50','50','90']"
        data-lineheight="['55','55','55','95']" style="z-index: 5;"> </div>

        <div class="tp-caption font-weight-light"
        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
        data-x="center"
        data-y="center" data-voffset="['40','40','40','80']"
        data-fontsize="['18','18','18','50']"
        data-lineheight="['20','20','20','55']"
        style="color: #b5b5b5; z-index: 5;"> </div>

        <a class="tp-caption slider-scroll-button"
        data-hash
        data-hash-offset="80"
        href="#marcas"
        data-x="center"
        data-y="bottom" data-voffset="['30','30','30','30']"
        data-start="1600"                    
        data-transform_in="y:[100%];s:500;"
        data-transform_out="y:[100%];opacity:0;s:500;"
        data-mask_in="x:0px;y:0px;" style="z-index: 5;"></a>

        <div class="tp-dottedoverlay tp-opacity-overlay"></div>
    </li>

    <li data-transition="fade">
        <img src="<?php echo base_url(); ?>assets/porto/img/banners/1.jpg"    
        alt=""
        data-bgposition="center center" 
        data-bgfit="cover" 
        data-bgrepeat="no-repeat" 
        class="rev-slidebg">

        <div class="tp-caption text-color-dark font-weight-semibold"
        data-x="center"
        data-y="center" data-voffset="['210','-100','-100','-125']"
        data-start="700"
        data-fontsize="['22','22','22','40']"
        data-lineheight="['25','25','25','45']"
        data-transform_in="y:[-50%];opacity:0;s:500;">TEXTO EN BANNER</div>

        <a class="tp-caption btn btn-primary font-weight-bold"
        href="#"
        data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"y:50%;opacity:0;","to":"y:0;o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
        data-x="center" data-hoffset="0"
        data-y="center" data-voffset="['170','70','70','135']"
        data-paddingtop="['15','15','15','30']"
        data-paddingbottom="['15','15','15','30']"
        data-paddingleft="['40','40','40','57']"
        data-paddingright="['40','40','40','57']"
        data-fontsize="['13','13','13','25']"
        data-lineheight="['20','20','20','25']"> Boton Banner <i class="fas fa-arrow-right ml-1"></i></a>
    </li>

</ul>








</div>
</div>

<div class="home-intro bg-primary" id="home-intro">
    <div class="container">

        <div class="row align-items-center">
            <div class="col-lg-1"></div>
            <div class="col-lg-6">
                <p>
                    Bienvenido a la pagina oficial de  <span class="highlighted-word"><?php echo html_escape($settings->site_name) ?></span>
                    <span>Piensa en Auto... Piensa en FAME.</span>
                </p>
            </div>
            <div class="col-lg-4">
                <div class="get-started text-left text-lg-right">
                    <a href="#" class="btn btn-dark btn-lg text-3 font-weight-semibold px-4 py-3">Contactanos</a>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>

    </div>
</div>

<div class="container mb-5 pb-4">

    <div class="row text-center pt-4">
        <div role="marcas" class="col">
            <h2 class="word-rotator slide font-weight-bold text-8 mb-2">
                <span>Estas un buscando un</span>
                <span class="word-rotator-words bg-primary">
                    <b class="is-visible">BMW</b>
                    <b>Nissan</b>
                    <b>Cadillac</b>
                    <b>Buick</b>
                    <b>Chevrolet</b>
                    <b>Chrysler</b>
                    <b>Daswelt</b>
                    <b>Dodge</b>
                    <b>Fiat</b>
                    <b>GMC</b>
                    <b>Honda</b>
                    <b>Isuzu</b>
                    <b>Jeep</b>
                    <b>KIA</b>
                    <b>MINI</b>
                    <b>Mitsubishi</b>
                    <b>Motorrad</b>
                    <b>Ram</b>
                    <b>Seminuevos</b>
                    <b>Volkswagen</b>

                </span>
                <span> Nosotros lo tenemos.</span>
            </h2>
            <h4 class="text-primary lead tall text-4">TENEMOS MAS DE 20 MARCAS EN TRES ESTADOS DE LA REPUBLICA.</h4>
        </div>
    </div>

    <div class="row text-center mt-5">
        <div class="owl-carousel owl-theme carousel-center-active-item" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 7}, '1200': {'items': 7}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">

         <?php for ($i=1; $i < 23; $i++) { ?>
         <div>
            <img class="img-fluid" src="<?php echo base_url() ?>assets/admin/marcas/marca (<?php echo $i; ?>).png" alt="">
        </div>
        <?php } ?>

    </div>
</div>

</div>














<div role="main" class="main pt-3 mt-3">
    <div class="container">
        <div class="row pb-1 pt-2">
            <div class="col-md-12">


                <div class="heading heading-border heading-middle-border">
                    <h3 class="text-4"><strong class="font-weight-bold text-1 px-3 text-light py-2 bg-tertiary">Blog</strong></h3>
                </div>
                <!-- start posts -->
                <div class="row pb-1">
                    <div class="col-lg-6">

                        <!-- start item -->
                        <?php foreach ($posts as $post): ?>

                            <article class="thumb-info thumb-info-side-image thumb-info-no-zoom bg-transparent border-radius-0 pb-4 mb-2">
                                <div class="row align-items-center pb-1">
                                    <div class="col-sm-4">
                                        <a href="<?php echo base_url('post/'.$post->slug) ?>">
                                            <img src="<?php echo base_url($post->image) ?>" class="img-fluid border-radius-0" alt="<?php echo html_escape($post->title) ?>">
                                        </a>
                                    </div>
                                    <div class="col-sm-7 pl-sm-0">
                                        <div class="thumb-info-caption-text">
                                            <div class="d-inline-block text-default text-1 float-none">
                                                <a href="<?php echo base_url('category/'.$post->category_slug) ?>" class="text-decoration-none text-color-default"><?php echo html_escape($post->category) ?></a>
                                            </div>
                                            <h4 class="d-block pb-2 line-height-2 text-3 text-dark font-weight-bold mb-0">
                                                <a href="blog-post.html" class="text-decoration-none text-color-dark"><?php echo character_limiter($post->details, 100)?></a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        <?php endforeach ?>
                        <!-- end item -->  
                    </div>
                </div>
                <!-- end posts -->

                <!-- start pagination -->
                <div class="row">
                    <div class="mt-md-12 text-center">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>
                <!-- end pagination -->



            </div>
        </div>











        <section class="section section-custom-map appear-animation" data-appear-animation="fadeInUpShorter">
            <section class="section section-default section-footer">
                <div class="container">
                    <div class="row mt-5 appear-animation" data-appear-animation="fadeInUpShorter">
                        <div class="col-lg-3">

                        </div>
                        <div class="col-lg-6">
                            <h2 class="font-weight-normal text-6 mb-4"><strong class="font-weight-extra-bold">Opiniones </strong> </h2>
                            <div class="row">
                                <div class="owl-carousel owl-theme dots-title dots-title-pos-2 mb-0" data-plugin-options="{'items': 1, 'autoHeight': true}">
                                    <div>
                                        <div class="col">
                                            <div class="testimonial testimonial-primary">
                                                <blockquote>
                                                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
                                                </blockquote>
                                                <div class="testimonial-arrow-down"></div>
                                                <div class="testimonial-author">
                                                    <div class="testimonial-author-thumbnail">
                                                        <img src="img/clients/client-1.jpg" class="rounded-circle" alt="">
                                                    </div>
                                                    <p><strong>John Doe</strong><span>Okler</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="col">
                                            <div class="testimonial testimonial-primary">
                                                <blockquote>
                                                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
                                                </blockquote>
                                                <div class="testimonial-arrow-down"></div>
                                                <div class="testimonial-author">
                                                    <div class="testimonial-author-thumbnail">
                                                        <img src="img/clients/client-1.jpg" class="rounded-circle" alt="">
                                                    </div>
                                                    <p><strong>John Doe</strong><span>Okler</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>


    </div>
