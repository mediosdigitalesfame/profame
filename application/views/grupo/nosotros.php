

<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Nosotros</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="#">Home</a></li>
						<li class="active">Nosotros</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container pb-1">

		<div class="row pt-4">
			<div class="col">
				<div class="overflow-hidden mb-3">
					<h2 class="word-rotator slide font-weight-bold text-8 mb-0 appear-animation" data-appear-animation="maskUp">
						<span>En Motorrad Morelia esta tu proxima Motocicleta</span>
						<!-- <span class="word-rotator-words bg-primary">
							<b class="is-visible">Honda</b>
							<b>Chevrolet</b>
							<b>Toyota</b>
						</span>-->
					</h2>
				</div>
			</div>
		</div>

		<div class="row mb-2">
			<div class="col-lg-10">
				<div class="overflow-hidden">
					<p class="lead mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="250">
						Descripción de la agencia
					</p>
				</div>
			</div>
			<div class="col-lg-2 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="450">
				<a href="#" class="btn btn-modern btn-primary mt-1">Contactanos</a>
			</div>
		</div>
	</div>

</div>

<section class="section section-default border-0 my-5 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="750">
	<div class="container py-4">

		<div class="row align-items-center">
			<div class="col-md-6 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="1000">
				<div class="owl-carousel owl-theme nav-inside mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 6000, 'loop': true}">
					<div>
						<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/img/generic/generic-corporate-3-2-full.jpg">
					</div>
					<div>
						<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/img/generic/generic-corporate-3-3-full.jpg">
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="overflow-hidden mb-2">
					<h2 class="text-color-dark font-weight-normal text-5 mb-0 pt-0 mt-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="1200">Quienes <strong class="font-weight-extra-bold">Somos</strong></h2>
				</div>
				<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1400"> Motorrad Morelia es una agencia comprometida . . . </p>
				<p class="mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1600">Calidad y Servicio.</p>
			</div>
		</div>

	</div>
</section>

<div class="container">

					<div class="row mt-5 py-3">
						<div class="col-md-3">
							 
						</div>

						<div class="col-md-6">
							<div class="toggle toggle-primary toggle-simple m-0" data-plugin-toggle>
								<section class="toggle mt-0">
									<label>Nuestra Vision</label>
									<div class="toggle-content">
										<p>La Vision de Motorrad Morelia.</p>
									</div>
								</section>
								<section class="toggle">
									<label>Nuestra Mission</label>
									<div class="toggle-content">
										<p>La Misin de Motorrad Morelia.</p>
									</div>
								</section>
							</div>
						</div>

						<div class="col-md-3">
							 
						</div>
					</div>
					<div class="row">
						<div class="col py-4">
							<hr class="solid">
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 mx-md-auto text-center">

							<h2 class="text-color-dark font-weight-normal text-5 mb-0 pt-2">Nuestra  <strong class="font-weight-extra-bold">Historia</strong></h2>
							<p>Los origenes de Motorrad Morelia.</p>

							<section class="timeline" id="timeline">
								<div class="timeline-body">
									<div class="timeline-date">
										<h3 class="text-primary font-weight-bold">2018</h3>
									</div>

									<article class="timeline-box left text-left appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="200">
										<div class="timeline-box-arrow"></div>
										<div class="p-2">
											<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/img/history/history-3.jpg" />
											<h3 class="font-weight-bold text-3 mt-3 mb-1">Nueva Sucursal</h3>
											<p class="mb-0 text-2">Fecha de apertura de nueva sucursal.</p>
										</div>
									</article>

									<div class="timeline-date">
										<h3 class="text-primary font-weight-bold">2012</h3>
									</div>

									<article class="timeline-box right text-left appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="400">
										<div class="timeline-box-arrow"></div>
										<div class="p-2">
											<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/img/history/history-2.jpg" />
											<h3 class="font-weight-bold text-3 mt-3 mb-1">Nuevo Modelo</h3>
											<p class="mb-0 text-2">Fecha cuando salio esl nuevo modelo.</p>
										</div>
									</article>

									<div class="timeline-date">
										<h3 class="text-primary font-weight-bold">2006</h3>
									</div>

									<article class="timeline-box left text-left appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="600">
										<div class="timeline-box-arrow"></div>
										<div class="p-2">
											<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/img/history/history-1.jpg" />
											<h3 class="font-weight-bold text-3 mt-3 mb-1">Fundacion</h3>
											<p class="mb-0 text-2">Fecha de fundacion de la agencia..</p>
										</div>
									</article>
								</div>
							</section>

						</div>
					</div>

				</div>

				<section class="section section-default border-0 my-5">
					<div class="container py-4">

						<div class="row">
							<div class="col pb-4 text-center">
								<h2 class="text-color-dark font-weight-normal text-5 mb-0 pt-2">Nuestro <strong class="font-weight-extra-bold">Equipo</strong></h2>
								<p>Equipo laborar al servicio de la agencia.</p>
							</div>
						</div>
						<div class="row pb-4 mb-2">
							<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
								<span class="thumb-info thumb-info-hide-wrapper-bg bg-transparent border-radius-0">
									<span class="thumb-info-wrapper border-radius-0">
										<a href="about-me.html">
											<img src="<?php echo base_url(); ?>assets/img/team/team-2.jpg" class="img-fluid border-radius-0" alt="">
											<span class="thumb-info-title">
												<span class="thumb-info-inner">Juan perez</span>
												<span class="thumb-info-type">Asesor de Ventas</span>
											</span>
										</a>
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text">Descripcion del asesor de ventas.</span>
										<span class="thumb-info-social-icons">
											<a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
											<a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
											<a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
										</span>
									</span>
								</span>
							</div>

							<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
								<span class="thumb-info thumb-info-hide-wrapper-bg bg-transparent border-radius-0">
									<span class="thumb-info-wrapper border-radius-0">
										<a href="about-me.html">
											<img src="<?php echo base_url(); ?>assets/img/team/team-2.jpg" class="img-fluid border-radius-0" alt="">
											<span class="thumb-info-title">
												<span class="thumb-info-inner">Juan perez</span>
												<span class="thumb-info-type">Asesor de Ventas</span>
											</span>
										</a>
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text">Descripcion del asesor de ventas.</span>
										<span class="thumb-info-social-icons">
											<a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
											<a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
											<a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
										</span>
									</span>
								</span>
							</div>

							<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
								<span class="thumb-info thumb-info-hide-wrapper-bg bg-transparent border-radius-0">
									<span class="thumb-info-wrapper border-radius-0">
										<a href="about-me.html">
											<img src="<?php echo base_url(); ?>assets/img/team/team-2.jpg" class="img-fluid border-radius-0" alt="">
											<span class="thumb-info-title">
												<span class="thumb-info-inner">Juan perez</span>
												<span class="thumb-info-type">Asesor de Ventas</span>
											</span>
										</a>
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text">Descripcion del asesor de ventas.</span>
										<span class="thumb-info-social-icons">
											<a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
											<a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
											<a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
										</span>
									</span>
								</span>
							</div>

							<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
								<span class="thumb-info thumb-info-hide-wrapper-bg bg-transparent border-radius-0">
									<span class="thumb-info-wrapper border-radius-0">
										<a href="about-me.html">
											<img src="<?php echo base_url(); ?>assets/img/team/team-2.jpg" class="img-fluid border-radius-0" alt="">
											<span class="thumb-info-title">
												<span class="thumb-info-inner">Juan perez</span>
												<span class="thumb-info-type">Asesor de Ventas</span>
											</span>
										</a>
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text">Descripcion del asesor de ventas.</span>
										<span class="thumb-info-social-icons">
											<a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
											<a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
											<a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
										</span>
									</span>
								</span>
							</div>

							 
							 
						</div>
					</div>
				</section>

				<div class="container">
					<div class="row py-5 my-5">
						<div class="col">
					
							<div class="owl-carousel owl-theme mb-0 " data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 7}, '1200': {'items': 7}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/c400gt.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/c400x.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/c650gt.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/c650sport.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/f750gs.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/f800r.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/f850gs.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/f850gsadventure.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/f900r.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/f900xr.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/g310gs.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/g310r.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/hp4race.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/k1600b.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/k1600grandamerica.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/k1600gt.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/k1600gtl.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/r1000rr.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/r1250gsa.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/r1250r.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/r1250rs.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/r1250rt.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/rninet.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/rninetpure.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/rninetscrambler.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/rnineturbangs.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/s1000r.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/s1000xr.jpg" alt="">
								</div>
								<div>
									<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/modelos/s1000xr-2.jpg" alt="">
								</div>
							</div>
							
						</div>
					</div>
				</div>

			</div>

