<div class="container">
			<div class="row my-5">
				<div class="col-lg-12 text-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
					<h2 class="font-weight-normal text-6 mt-3 mb-5">Fichas  <strong class="font-weight-extra-bold">Técnicas</strong></h2>
				</div>
				<div class="col my-3">
					
					<div class="row text-center my-3">

						<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': true, 'nav': true, 'dots': false, 'stagePadding': 40}">
							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/altima.pdf" title=""><img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/nissan/altima.jpg" target="_blank" alt="">
								<div class="thumb-info-inner">Altima<em></em></div> </a>
							</div>
							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/370z.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/370z.jpg" alt="">
								<div class="thumb-info-inner">370z<em></em></div> </a>
							</div>
							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/370znismo.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/370znismo.jpg" alt="">
								<div class="thumb-info-inner">370z Nismo<em></em></div> </a>
							</div>
							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/armada.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/armada.jpg" alt="">
								<div class="thumb-info-inner">Armada<em></em></div> </a>
							</div>
							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/frontierpro4x.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/frontierpro4x.jpg" alt="">
								<div class="thumb-info-inner">Frontier Pro 4X<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/gtr.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/gtr.jpg" alt="">
								<div class="thumb-info-inner">GTR<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/kicks.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/kicks.jpg" alt="">
								<div class="thumb-info-inner">Kicks<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/leaf.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/leaf.jpg" alt="">
								<div class="thumb-info-inner">Leaf<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/march.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/march.jpg" alt="">
								<div class="thumb-info-inner">March<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/maxima.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/maxima.jpg" alt="">
								<div class="thumb-info-inner">Maxima<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/murano.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/murano.jpg" alt="">
								<div class="thumb-info-inner">Murano<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/np300.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/np300.jpg" alt="">
								<div class="thumb-info-inner">NP 300<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/np300frontier.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/np300frontier.jpg" alt="">
								<div class="thumb-info-inner">NP 300 Frontier<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/nv350.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/nv350.jpg" alt="">
								<div class="thumb-info-inner">NV350<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/pathfinder.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/pathfinder.jpg" alt="">
								<div class="thumb-info-inner">Pathfinder<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/sentra.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/sentra.jpg" alt="">
								<div class="thumb-info-inner">Sentra<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/sentranismo.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/sentranismo.jpg" alt="">
								<div class="thumb-info-inner">Sentra Nismo<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/versa.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/versa.jpg" alt="">
								<div class="thumb-info-inner">Versa<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/versago.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/versago.jpg" alt="">
								<div class="thumb-info-inner">Versa Go<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/versavdrive.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/versavdrive.jpg" alt="">
								<div class="thumb-info-inner">Versa V-Drive<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/xtrail.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/xtrail.jpg" alt="">
								<div class="thumb-info-inner">X-Trail<em></em></div> </a>
							</div>

							<div>
								<a href="<?php echo base_url(); ?>assets/fichas/nissan/xtrailhybrid.pdf" title=""><img class="img-fluid " src="<?php echo base_url(); ?>assets/img/modelos/nissan/xtrailhybrid.png" alt="">
								<div class="thumb-info-inner">X-Trail Hybrid<em></em></div> </a>
							</div>

							 
							 

						</div>
					</div>

				</div>
			</div>
		</div>