<div class="container">
		<div class="row my-5">
			<div class="col-lg-12 text-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
				<h2 class="font-weight-normal text-6 mt-3 mb-5">Nuestras <strong class="font-weight-extra-bold">Marcas</strong></h2>
			</div>
			<div class="col my-3">

				<div class="row text-center my-3">

					<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 5, 'margin': 10, 'loop': true, 'nav': true, 'dots': false, 'stagePadding': 40}">
						<div>
							<a href="https://www.grupofame.com/agencias/nissan/" title="">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/nissan.png" alt="">
							<div class="thumb-info-inner">Nissan<em></em></div> 
							</a>
						</div>
						<div>
							<a href="https://www.grupofame.com/agencias/talisman/" title="">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/bmw.png" alt="">
							<div class="thumb-info-inner">BMW<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/gmc/" title="">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/buick.png" alt="">
							<div class="thumb-info-inner">GMC<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/gmc/" title="">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/cadillac.png" alt="">
							<div class="thumb-info-inner">Cadillac<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/chevrolet/" title="">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/chevrolet.png" alt="">
							<div class="thumb-info-inner">Chevrolet<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/chrysler/" title="">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/chrysler.png" alt="">
							<div class="thumb-info-inner">Chrysler<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/daswelt/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/daswelt.png" alt="">
							<div class="thumb-info-inner">Daswelt<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/chrysler/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/dodge.png" alt="">
							<div class="thumb-info-inner">Dodge<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/fiat/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/fiat.png" alt="">
							<div class="thumb-info-inner">FIAT<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/gmc/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/gmc.png" alt="">
							<div class="thumb-info-inner">GMC<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/honda/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
							<div class="thumb-info-inner">Honda<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/isuzu/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/isuzu.png" alt="">
							<div class="thumb-info-inner">Isuzu<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/chrysler/" title="">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/jeep.png" alt="">
							<div class="thumb-info-inner">Jeep<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/kia/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/kia.png" alt="">
							<div class="thumb-info-inner">KIA<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/talisman/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/mini.png" alt="">
							<div class="thumb-info-inner">MINI<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/mitsubishi/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/mitsubishi.png" alt="">
							<div class="thumb-info-inner">Mitsubishi<em></em></div> 
							</a>
						</div>

					 
							<div>
								<a href="https://www.grupofame.com/agencias/motorrad/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/motorrad.png" alt="">
							<div class="thumb-info-inner">Motorrad<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/nissan/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/nissan.png" alt="">
							<div class="thumb-info-inner">Nissan<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/chrysler/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/ram.png" alt="">
							<div class="thumb-info-inner">RAM<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.fameseminuevos.com/index.php/agencia/agencia/1" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/seminuevos.png" alt="">
							<div class="thumb-info-inner">Seminuevos<em></em></div> 
							</a>
						</div>

						<div>
							<a href="https://www.grupofame.com/agencias/vw/" title="">
							<img class="img-fluid " src="<?php echo base_url(); ?>assets/img/marcas/volkswagen.png" alt="">
							<div class="thumb-info-inner">Volkswagen<em></em></div> 
							</a>
						</div>

						 

						 

					</div>
				</div>

			</div>
		</div>
	</div>