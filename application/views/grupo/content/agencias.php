<div class="container">
		<div class="row my-5">
			<div class="col-lg-12 text-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
				<h2 class="font-weight-normal text-6 mt-3 mb-5">Nuestras <strong class="font-weight-extra-bold">Agencias</strong></h2>
			</div>
			<div class="col my-3">

				<div class="row text-center my-3">

					<div class="owl-carousel owl-theme mb-0 " data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 7}, '1200': {'items': 7}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
						<div>
							<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg" alt="">
							<div class="thumb-info-inner">Grupo<em></em></div> 
						</div>
						<div>
							<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg" alt="">
							<div class="thumb-info-inner">Grupo<em></em></div> 
						</div>
						<div>
							<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg" alt="">
							<div class="thumb-info-inner">Grupo<em></em></div> 
						</div>
						<div>
							<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg" alt="">
							<div class="thumb-info-inner">Grupo<em></em></div> 
						</div>
						<div>
							<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg" alt="">
							<div class="thumb-info-inner">Grupo<em></em></div> 
						</div>
						<div>
							<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg" alt="">
							<div class="thumb-info-inner">Grupo<em></em></div> 
						</div>
						<div>
							<img class="img-fluid opacity-2" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg" alt="">
							<div class="thumb-info-inner">Grupo<em></em></div> 
						</div>

					</div>
				</div>

			</div>
		</div>
	</div>