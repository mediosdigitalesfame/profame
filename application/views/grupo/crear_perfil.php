<div role="main" class="main">

    <section class="page-header page-header-classic page-header-md">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>Crear Perfil</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
                        <li class="active">Crear Perfil</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="contact-form-area section-padding" id="contact-page">
        <div class="container">

            <?php if ($settings->enable_registration == 0): ?>
                <div class="col-md-12 text-center">
                    <h2 class="text-danger" style="padding: 200px">Los registros estan temporalmente desactivados en el sistema.!</h2>
                </div>
            <?php else: ?>

                <div class="row">
                    <div class="page-title">
                        <h2 class="title">Crea tu perfil</h2>
                        <div class="space-40"></div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-offset-2 col-md-8">
                        <?php if (!empty($this->session->flashdata('msg'))): ?>
                            <div class="alert alert-success alert-dismissible">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong><i class="icon-check"></i> <?php echo $this->session->flashdata('msg'); ?> !</strong>
                          </div>
                      <?php endif ?>

                      <?php if (!empty($this->session->flashdata('error'))): ?>
                        <div class="alert alert-danger alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong><i class="icon-close"></i> <?php echo $this->session->flashdata('error'); ?> !</strong>
                      </div>
                  <?php endif ?>
              </div>

              <div class="container py-4">

                <div class="row mb-5">
                    <div class="col">

                        <form class="contact-form-recaptcha-v3" id="register-form" method="post" action="<?php echo base_url('registrar_usuario'); ?>">

                            <div class="form-row">
                                <div class="form-group col-lg-6">
                                    <label class="required font-weight-bold text-dark text-2">Nombre Completo</label>
                                    <input type="text" value="" placeholder="Nombre Completo" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="required font-weight-bold text-dark text-2">Email</label>
                                    <input type="email" value="" placeholder="Email" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-lg-6">
                                    <label class="required font-weight-bold text-dark text-2">Usuario</label>
                                    <input type="text" value="" placeholder="Usuaio" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="user_name" id="user-name" required>

                                    <div class="bubble loader-bubble" style="display: none;">
                                      <div class="bounce1"></div>
                                      <div class="bounce2"></div>
                                      <div class="bounce3"></div>
                                  </div>

                                  <h5 class="text-danger" id="name_exist" style="display: none;"><i class="icon-close"></i> El nombre de usuario ya está en uso, pruebe con otro</h5>
                                  <h5 class="text-success" id="name_available" style="display: none;"><i class="icon-check"></i> Nombre de usuario disponible </h5>

                              </div>
                              <div class="form-group col-lg-6">
                                <label class="required font-weight-bold text-dark text-2">Contraseña</label>
                                <input value="" maxlength="100" class="form-control" type="password" placeholder="Password" name="password" id="password" required>
                            </div>
                        </div>

                        <!-- csrf token -->
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

                        <div class="form-row">
                            <div class="form-group col-lg-6">
                             <div class="checkbox">
                              <label>
                                <input type="checkbox" class="terms_cond" value="" required>
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                He Leído los <a href="<?php echo base_url('terms-and-conditions') ?>">terminos y condiciones</a> los cuales acepto.
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <?php if ($settings->enable_captcha == 1 && $settings->captcha_site_key != ''): ?>
                            <div class="g-recaptcha pull-left" data-sitekey="<?php echo html_escape($settings->captcha_site_key); ?>"></div>
                            <div class="space-30"></div>
                        <?php endif ?>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-lg-12">
                        <div id="success"></div>
                        <button type="submit" id="create-btn" class="btn btn-primary btn-modern" data-loading-text="Loading...">Create</button>
                        
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
</div>

<?php endif ?>

</div>
    </section><!--/Contact Section-->