<?php $this->load->view('grupo/corporativo/encabezadodepartamentoempleados.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="franciscojavieralvarezllanderal"><li><i class="fas fa-user"></i>Francisco Javier Alvarez Llanderal</li></a>
						<a href="hugoandrade"><li><i class="fas fa-user"></i>Hugo Andrade</li></a>
						<a href="victormandujano"><li><i class="fas fa-user"></i>Victor Mandujano</li></a>
						<a href="jaimemorelos"><li><i class="fas fa-user"></i>Jaime Morelos</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="alejandrorafaelhernandezperez"><li><i class="fas fa-user"></i>Alejandro Rafael Hernandez Perez</li></a>
						<a href="antoniochavezmedina"><li><i class="fas fa-user"></i>Antonio Chavez Medina</li></a>
						<a href="hazaelolivarez"><li><i class="fas fa-user"></i>Hazael Olivares</li></a>
						<a href="eduardoroman"><li><i class="fas fa-user"></i>Eduardo Román</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="rosamarialemusayala"><li><i class="fas fa-user"></i>Rosa Maria Lemus Ayala</li></a>
						<a href="ricardollamas"><li><i class="fas fa-user"></i>Ricardo Llamas</li></a>
						<a href="fernandorotter"><li><i class="fas fa-user"></i>Fernando Rotter</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="hectorjavierchaveznegrete"><li><i class="fas fa-user"></i>Héctor Javier Chávez Negrete</li></a>
						<a href="fernandosaucedo"><li><i class="fas fa-user"></i>Fernando Saucedo</li></a>
						<a href="ricardosanchez"><li><i class="fas fa-user"></i>Ricardo Sanchez</li></a>
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>

<?php $this->load->view('grupo/corporativo/departamentos/piedepartamentos.php'); ?>

