<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-6 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border><?php echo $nombre_agencia; ?></h1>
				</div>
				<div class="col-md-6 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>">Inicio</a></li>
						<li class=""><a href="<?php echo base_url(); ?>corporativo">Corporativo</a></li>
						<li class="active"><?php echo $nombre_agencia; ?></li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<section class="call-to-action featured featured-primary mb-5">
			<div class="col-lg-2">
				<img src="<?php echo base_url(); ?>assets/img/marcas/<?php echo $vista_logo; ?>.png" class="img-fluid">
			</div> 
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="call-to-action-content">
					<h3>  <strong class="font-weight-extra-bold"><?php echo $nombre_agencia; ?></strong> </h3>
					<p class="mb-0">Piensa en <strong class="font-weight-extra-bold">Auto</strong> Piensa en<strong class="font-weight-extra-bold">FAME</strong></p>
				</div>
			</div>
			<div class="col-lg-2"></div> 
			<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
				<div class="call-to-action-btn">
					<a href="<?php echo $nombre_pagina; ?>" target="_blank" class="btn btn-modern text-2 btn-primary">Ir al Sitio</a>
				</div>
			</div>
			<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-6"><a href="<?php echo $nombre_facebook; ?>">
					<div class="feature-box feature-box-style-5 reverse">
						<div class="feature-box-icon">
							<i class="fab fa-facebook-square"></i>
						</div>
					</div></a> 
				</div>
				<div class="col-lg-6"><a href="<?php echo $nombre_instagram; ?>">
					<div class="feature-box feature-box-style-5 reverse">
						<div class="feature-box-icon">
							<i class="fab fa-instagram"></i>
						</div>
					</div></a> 
				</div>
			</div>
		</section>