<div role="main" class="main">
	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-6 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border><?php echo $nombre_asesor; ?></h1>
				</div>
				<div class="col-md-6 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>">Inicio</a></li>
						<li class=""><a href="<?php echo base_url(); ?>corporativo">Corporativo</a></li>
						<li class="active"><a href="<?php echo $vista_agencia; ?>"><?php echo $nombre_agencia; ?></a></li>
						<li class="active"><?php echo $nombre_asesor; ?></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<section class="call-to-action featured featured-primary mb-5">
			<div class="col-lg-2">
				<img src="<?php echo base_url(); ?>assets/img/marcas/<?php echo $vista_logo; ?>.png" class="img-fluid">
			</div> 
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="call-to-action-content">
					<h3>  <strong class="font-weight-extra-bold"><?php echo $nombre_agencia; ?></strong> </h3>
					<p class="mb-0">Piensa en <strong class="font-weight-extra-bold">Auto</strong> Piensa en<strong class="font-weight-extra-bold">FAME</strong></p>
				</div>
			</div>
			<div class="col-lg-2"></div>
			<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
				<div class="call-to-action-btn">
					<a href="<?php echo $nombre_pagina; ?>" target="_blank" class="btn btn-modern text-2 btn-primary">Ir al Sitio</a>
				</div>
			</div>
			<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-6"><a href="<?php echo $nombre_facebook; ?>">
					<div class="feature-box feature-box-style-5 reverse">
						<div class="feature-box-icon">
							<i class="fab fa-facebook-square"></i>
						</div>
					</div></a> 
				</div>
				<div class="col-lg-6"><a href="<?php echo $nombre_instagram; ?>">
					<div class="feature-box feature-box-style-5 reverse">
						<div class="feature-box-icon">
							<i class="fab fa-instagram"></i>
						</div>
					</div></a> 
				</div>
			</div>
		</section>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-12">
						<div class="container py-2">
							<div class="row">
								<div class="col-12 mt-4 mt-lg-0">
									<div class="d-flex justify-content-center mb-4">
										<div class="profile-image-outer-container">
											<div class="profile-image-inner-container bg-color-primary">
												<img src="<?php echo base_url(); ?>assets/img/avatars/<?php echo $vista_foto; ?>">
											</div>
										</div>
									</div>
								</div>
								<div class="col-12 mt-4 mt-lg-0">
									<div class="d-flex justify-content-center mb-4">
										<div class="profile-image-outer-container">
											<div align="center" class="overflow-hidden mb-1">
												<h2 class="font-weight-normal text-7 mb-0"><strong class="font-weight-extra-bold"><?php echo $nombre_asesor; ?></strong> </h2>
											</div>
											<div align="center" class="overflow-hidden mb-4 pb-3">
												<p class="mb-0"><?php echo $nombre_puesto; ?>.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-8 mt-4 mt-lg-0">
									<div class="d-flex justify-content-center mb-4">
										<div class="profile-image-outer-container">
											<h5 class="font-weight-bold pt-4">	Datos de <strong>Contacto</strong></h5>
											<ul class="list list-icons list-icons-style-2 mt-2">
												<!--<li><i class="fas fa-map-marker-alt top-6"></i> <strong class="text-dark">Direccion:</strong> 1234 Calle, Morelia Mich.</li> -->
												<li><i class="fas fa-phone top-6"></i> <strong class="text-dark">Cel:</strong><a href="tel:+<?php echo $asesor_cel; ?>" title=""> <?php echo $asesor_cel; ?></a></li>
												<li><i class="fab fa-whatsapp top-6"></i> <strong class="text-dark">WhatsApp:</strong> <a href="https://wa.me/<?php echo $asesor_wats; ?>"><?php echo $asesor_cel; ?></a></li>
												<li><i class="fas fa-envelope top-6"></i> <strong class="text-dark">Email:</strong> <a href="mailto:<?php echo $asesor_mail; ?>"><?php echo $asesor_mail; ?></a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-4 mt-4 mt-lg-0">
									<div class="d-flex justify-content-center mb-4">
										<div class="profile-image-outer-container">
											<a href="https://wa.me/524422870037" title="">
												<div class="col-lg-10">
													<span class="img-thumbnail d-block">
														<img align="center" class="img-fluid" src="<?php echo base_url(); ?>assets/img/avatars/<?php echo $nombre_qr; ?>" alt="<?php echo $nombre_agencia; ?>">
													</span> 
													<span class="thumb-info-caption">
														<span align="center" class="thumb-info-caption-text">Más Información.</span>
													</span>
												</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 mt-4 mt-lg-0">
									<div class="d-flex justify-content-center mb-4">
											  <a href="<?php echo base_url(); ?>corporativovcf/<?php echo $vcf_asesor; ?>"><button type="button" class="btn btn-3d btn-primary mb-2">Añadir Contacto</button></a></li>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--<div class="col-lg-3">
				<aside class="sidebar">
					<h5 class="font-weight-bold">Contactame <strong></strong></h5>
					<?php //$this->load->view('grupo/formularios/'.$asesor_formulario); ?>
				</aside>
			</div>-->
		</div>
	</div>

