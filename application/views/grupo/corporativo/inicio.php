

<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Corporativo</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
						<li class="active">Corporativo</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col">
				 
				<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>corporativo/direccion">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Dirección<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>corporativo/marketing">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Marketing<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
 
				 
				</div>

			</div>
		</div>

		 

		 

		 
	</div>
</div>
</div>

