<div role="main" class="main">

	<div class="slider-container rev_slider_wrapper" style="height: 530px;">
		<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1920, 'gridheight': 550, 'responsiveLevels': [4096,1200,992,500], 'navigation' : {'arrows': { 'enable': true }, 'bullets': {'enable': false, 'style': 'bullets-style-1 bullets-color-primary', 'h_align': 'center', 'v_align': 'bottom', 'space': 7, 'v_offset': 70, 'h_offset': 0}}}">
			<?php $this->load->view('grupo/content/banners'); ?>
		</div>
	</div>

	<div class="home-intro bg-primary" id="home-intro">
		<div class="container">

			<div class="row align-items-center">
				<div class="col-lg-1"></div>
				<div class="col-lg-6">
					<p>
						Bienvenido a la pagina oficial de  <span class="highlighted-word">Grupo FAME</span>
						<span>Piensa en Auto... Piensa en FAME.</span>
					</p>
				</div>
				<div class="col-lg-4">
					<div class="get-started text-left text-lg-right">
						<a href="#" class="btn btn-dark btn-lg text-3 font-weight-semibold px-4 py-3">Contactanos</a>
					</div>
				</div>
				<div class="col-lg-1"></div>
			</div>

		</div>
	</div>

	<div class="container mb-5 pb-4">

		<div class="row text-center pt-4">
			<div role="marcas" class="col">
				<h2 class="word-rotator slide font-weight-bold text-8 mb-2">
					<span>Estas un buscando un</span>
					<span class="word-rotator-words bg-primary">
						<b class="is-visible">BMW</b>
						<b>Nissan</b>
						<b>Cadillac</b>
						<b>Buick</b>
						<b>Chevrolet</b>
						<b>Chrysler</b>
						<b>Daswelt</b>
						<b>Dodge</b>
						<b>Fiat</b>
						<b>GMC</b>
						<b>Honda</b>
						<b>Isuzu</b>
						<b>Jeep</b>
						<b>KIA</b>
						<b>MINI</b>
						<b>Mitsubishi</b>
						<b>Motorrad</b>
						<b>Ram</b>
						<b>Seminuevos</b>
						<b>Volkswagen</b>

					</span>
					<span> Nosotros lo tenemos.</span>
				</h2>
				<h4 class="text-primary lead tall text-4">TENEMOS MAS DE 20 MARCAS EN TRES ESTADOS DE LA REPUBLICA.</h4>
			</div>
		</div>

		<div class="row text-center mt-5">
			<div class="owl-carousel owl-theme carousel-center-active-item" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 7}, '1200': {'items': 7}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/bmw.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/nissan.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/cadillac.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/buick.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/chevrolet.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/chrysler.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/daswelt.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/dodge.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/fiat.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/gmc.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/isuzu.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/jeep.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/kia.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/mini.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/mitsubishi.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/motorrad.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/ram.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/seminuevos.png" alt="">
				</div>
				<div>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/volkswagen.png" alt="">
				</div>

			</div>
		</div>

	</div>


	<section class="section section-custom-map appear-animation" data-appear-animation="fadeInUpShorter">
		<section class="section section-default section-footer">
			<div class="container">
				<div class="row mt-5 appear-animation" data-appear-animation="fadeInUpShorter">
					<div class="col-lg-3">

					</div>
					<div class="col-lg-6">
						<h2 class="font-weight-normal text-6 mb-4"><strong class="font-weight-extra-bold">Opiniones </strong> </h2>
						<div class="row">
							<div class="owl-carousel owl-theme dots-title dots-title-pos-2 mb-0" data-plugin-options="{'items': 1, 'autoHeight': true}">
								<div>
									<div class="col">
										<div class="testimonial testimonial-primary">
											<blockquote>
												<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
											</blockquote>
											<div class="testimonial-arrow-down"></div>
											<div class="testimonial-author">
												<div class="testimonial-author-thumbnail">
													<img src="img/clients/client-1.jpg" class="rounded-circle" alt="">
												</div>
												<p><strong>John Doe</strong><span>Okler</span></p>
											</div>
										</div>
									</div>
								</div>
								<div>
									<div class="col">
										<div class="testimonial testimonial-primary">
											<blockquote>
												<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
											</blockquote>
											<div class="testimonial-arrow-down"></div>
											<div class="testimonial-author">
												<div class="testimonial-author-thumbnail">
													<img src="img/clients/client-1.jpg" class="rounded-circle" alt="">
												</div>
												<p><strong>John Doe</strong><span>Okler</span></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>

	
</div>
