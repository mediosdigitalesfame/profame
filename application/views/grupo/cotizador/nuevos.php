<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Cotizador</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
						<li class="active">Cotizador</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

</div>

<div class="container py-2">
	<div class="row">
		<div class="col-lg-6">
			<div class="featured-box featured-box-primary">
				<div class="box-content p-5">
					<div class="row">
						<div class="col">

							<div class="row">
								<div class="col-12" align="center"> 
									<h2 class="font-weight-normal text-6">Simula tu crédito</h2>
									<p>Selecciona qué auto quieres.</p>
								</div>
							</div>

							<form action="/" id="frmBillingAddress" method="post">
								<div class="form-row">
									<div class="form-group col">
										<select class="form-control">
											
											<option value="">Modelo 1</option>
										</select>
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col">
										<select class="form-control validate[required]" id="mySelectMarca" name="q26_solicitud" data-component="dropdown" required="" aria-labelledby="label_10">
											
											<option value="Honda">Honda</option>
											<option value="Chevrolet">Chevrolet</option>
										</select>
									</div>
								</div>



								<div class="form-row">

									<div class="form-group col-lg-12">
										<label class="font-weight-bold text-dark text-2">
											<span class="amount"><h6 class="text-primary">El Monto a financiar seria por:</h6></span>
											<h3 class="text-primary text-12">
												<span class="amount" id="montoa">$0</span>
											</h3>

										</label>
									</div>

									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Valor del Auto</label>
										<input id="valordelauto" type="text" value="" class="form-control" onkeyup="PasarValor();" onblur="financiamiento()">
									</div>

									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Enganche</label>
										<input id="enganche" type="text" value="" class="form-control" onblur="financiamiento()">
									</div>

									<div class="form-group col-lg-6"> </div>

									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">
											<span class="amount"><h6 class="text-primary">Enganche sugerido:</h6></span>
											<span class="amount" id="enganchesu"> </span>
										</label>
									</div>
								</div>






								<div class="form-row">
									<div class="form-group col-lg-12">

										<div class="row">
											<div class="col-12" align="center"> 
												<h2 class="font-weight-normal text-6">Escoge tus pagos mensuales</h2>
											</div>
										</div>

										<table class="cart-totals">
											<tbody>
												<tr class="cart-subtotal">
													<td>
														<button id="input6" type="button" class="btn btn-light mb-2" onClick="asignarNota(6); operaciones('pagomensual60')" onmouseover="document.getElementById('input6').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();">
															<span id="pagoa60meses">$0</span>
															<br>
															<strong class="text-5">60 MESES</strong>
														</button>
													</td>
													<td >
														<button id="input5" type="button" class="btn btn-light mb-2" onClick="asignarNota(5); operaciones('pagomensual48')" onmouseover="document.getElementById('input5').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();">
															<span id="pagoa48meses">$0</span>
															<br>
															<strong class="text-5">48 MESES</strong>
														</button>

													</td>
													<td >
														<button id="input4" type="button" class="btn btn-light mb-2" onClick="asignarNota(4); operaciones('pagomensual36')" onmouseover="document.getElementById('input4').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();">
															<span id="pagoa36meses">$0</span>
															<br>
															<strong class="text-5">36 MESES</strong>
														</button>


													</td>
												</tr>
												<tr class="cart-subtotal">
													<td>
														<button id="input3" type="button" class="btn btn-light mb-2" onClick="asignarNota(3); operaciones('pagomensual24')" onmouseover="document.getElementById('input3').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();">
															<span id="pagoa24meses">$0</span>
															<br>
															<strong class="text-5">24 MESES</strong>
														</button>

													</td>
													<td>
														<button id="input2" type="button" class="btn btn-light mb-2" onClick="asignarNota(2); operaciones('pagomensual18')" onmouseover="document.getElementById('input2').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();">
															<span id="pagoa18meses">$0</span>
															<br>
															<strong class="text-5">18 MESES</strong>
														</button>



													</td>
													<td>
														<button id="input1" type="button" class="btn btn-light mb-2" onClick="asignarNota(1); operaciones('pagomensual12')" onmouseover="document.getElementById('input1').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();">
															<span id="pagoa12meses">$0</span>
															<br>
															<strong class="text-5">12 MESES</strong>
														</button>

													</td>
												</tr>
											</tbody>
										</table>

									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="featured-box featured-box-primary">
				<div class="box-content p-5">
					<div class="row">
						<div class="col">

							<div class="row">
								<div class="col-12" align="center"> 
									<h2 class="font-weight-normal text-6">¿Te interesa este plan?</h2>
									<p>Pago mensual a <span id="meseselegidos">0</span> meses</p>
								</div>
							</div>

							<div class="row">
								<div class="col-12" align="center"> 
									<h4 class="text-primary text-9"><p id="resultado" style="color: green;">$0</p></h4>
									<div class="divider divider-small divider-small-center">
										<hr>  
									</div>
								</div>
							</div>

							<hr>
							<div class="row" align="center">
								<div class="col-12" align="center"> 
									<input type="submit" value="Quiero mas información" class="btn btn-xl btn-light text-2 font-weight-semibold text-uppercase float mb-2" data-loading-text="Loading...">
								</div>
							</div>
							<hr>

							<div class="row">
								<div class="col pb-3">



									<table align="center" border="0" cellspacing="4" cellpadding="5">

										<tbody>
											<tr>

											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Monto a Financiar
												</td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>

												<td align="right">
													<span id="montoa2" value="0" >$0</span>
												</td>
											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Valor del Auto
												</td>



												<td align="right">
													<span id="valordelauto2" value="0" >$0</span>
												</td>
											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Enganche
												</td>


												<td align="right">
													<span id="enganche2" value="0" >$0</span>
												</td>
											</tr>
											<!-- <tr>
												<td colspan="4" rowspan="" align="left">
													Seguro de daños
												</td>
												<td align="right">
													<span id="segurodedaños" value="0">$0</span>
												</td>
											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Seguro de vida
												</td>												
												<td align="right">
													<span id="segurodevida" value="0">$0</span>
												</td>
											</tr>-->

											<tr>
												<td colspan="13" rowspan="" align="left">
												</td>
											</tr>
											<tr>
												<td colspan="13" rowspan="" align="left">
												</td>
											</tr>


											<tr>
												<td colspan="4" rowspan="" align="left">
													Tasa de Interes Anual
												</td>



												<td align="right">
													<span id="nuevatasa" value="0" >0%</span>
												</td>
											</tr>
											
											<tr>
												<td colspan="4" rowspan="" align="left">
													CAT
												</td>
												<td align="right">
													<span id="cat" value="0" >0%</span>
												</td>
											</tr>
										</tbody>
									</table>

									<hr class="solid my-5">

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script language="javascript">

	const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'})

	var valordelauto = document.getElementById("valordelauto").value;
	var enganche = document.getElementById("enganche").value;
	var marca = document.getElementById("mySelectMarca").value;
	var tasa = document.getElementById("nuevatasa").value;
	var mes = document.getElementById("meseselegidos").value;

	window.onload = function() {
		//asignarNota(6)
	};

	function financiamiento(){

		asignarNota(6)
		valordelauto = document.getElementById("valordelauto").value;
		enganche = document.getElementById("enganche").value;
		tasa = document.getElementById("nuevatasa").value;
		marca = document.getElementById("mySelectMarca").value;
		mes = document.getElementById("meseselegidos").value;

		if(enganche===''){enganche=0}
			switch (marca)
		{
			case 'Honda': tasa = 1.13;
			break;  
			case 'Chevrolet': tasa = 1.12;
			break;  
			case '': tasa = 0.001;
			break;  
		} 

		var totalapagar = valordelauto-enganche
		var tasados = (tasa/100)/12

		var pagoxmes60 = (totalapagar*tasados)/(1-(1+tasados)**-60)
		var pagoxmes48 = (totalapagar*tasados)/(1-(1+tasados)**-48)
		var pagoxmes36 = (totalapagar*tasados)/(1-(1+tasados)**-36)
		var pagoxmes24 = (totalapagar*tasados)/(1-(1+tasados)**-24)
		var pagoxmes18 = (totalapagar*tasados)/(1-(1+tasados)**-18)
		var pagoxmes12 = (totalapagar*tasados)/(1-(1+tasados)**-12)

		var montoafinanciar = (pagoxmes60*60)
		var interestotal = montoafinanciar-totalapagar
		var cat = (interestotal/totalapagar)*100

		document.getElementById('resultado').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(pagoxmes60)+"</span>";
		document.getElementById('pagoa60meses').innerHTML= usCurrencyFormat.format(pagoxmes60);
		document.getElementById('pagoa48meses').innerHTML= usCurrencyFormat.format(pagoxmes48);
		document.getElementById('pagoa36meses').innerHTML= usCurrencyFormat.format(pagoxmes36);
		document.getElementById('pagoa24meses').innerHTML= usCurrencyFormat.format(pagoxmes24);
		document.getElementById('pagoa18meses').innerHTML= usCurrencyFormat.format(pagoxmes18);
		document.getElementById('pagoa12meses').innerHTML= usCurrencyFormat.format(pagoxmes12);
		document.getElementById('enganche2').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(enganche)+"</span>";
		document.getElementById('enganche').value=enganche;
		document.getElementById('montoa').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(montoafinanciar)+"</span>";
		document.getElementById('montoa2').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(montoafinanciar)+"</span>";
		document.getElementById('valordelauto2').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(valordelauto)+"</span>";
		document.getElementById('nuevatasa').innerHTML="<span style='color: green;'>"+tasa+'%'+"</span>";
		document.getElementById('cat').innerHTML="<span style='color: green;'>"+cat.toFixed(2)+'%'+"</span>";
	} 

	function operaciones(op){
		var ops = {
			montoafinanciar: function sacarMonto(n1, n2, n3) {
				return ((parseFloat(n1)-parseFloat(n2))*((parseFloat(n3)/100)/12))/(1-(1+((parseFloat(n3)/100)/12)**-60));
			},

			pagox60: function sacarMeses60(n1, n2, n3) {
				var totalapagar = n1-n2
				var tasados = (n3/100)/12
				return (totalapagar*tasados)/(1-(1+tasados)**-60)
			},

			pagox48: function sacarMeses48(n1, n2, n3) {
				var totalapagar = n1-n2
				var tasados = (n3/100)/12
				return (totalapagar*tasados)/(1-(1+tasados)**-48)
			},

			pagox36: function sacarMeses36(n1, n2, n3) {
				var totalapagar = n1-n2
				var tasados = (n3/100)/12
				return (totalapagar*tasados)/(1-(1+tasados)**-36)
			},

			pagox24: function sacarMeses24(n1, n2, n3) {
				var totalapagar = n1-n2
				var tasados = (n3/100)/12
				return (totalapagar*tasados)/(1-(1+tasados)**-24)
			},

			pagox18: function sacarMeses18(n1, n2, n3) {
				var totalapagar = n1-n2
				var tasados = (n3/100)/12
				return (totalapagar*tasados)/(1-(1+tasados)**-18)
			},

			pagox12: function sacarMeses12(n1, n2, n3) {
				var totalapagar = n1-n2
				var tasados = (n3/100)/12
				return (totalapagar*tasados)/(1-(1+tasados)**-12)
			},

		};

		valordelauto = document.getElementById("valordelauto").value;
		enganche = document.getElementById("enganche").value;
		tasa = document.getElementById("nuevatasa").value;
		marca = document.getElementById("mySelectMarca").value;
		mes = document.getElementById("meseselegidos").value;

		switch (marca)
		{
			case 'Honda': tasa = 1.13;
			break;  
			case 'Chevrolet': tasa = 1.12;
			break;  
			case '': tasa = 0.001;
			break;  
		} 

    //Comprobamos si se ha introducido números en las cajas
    if (isNaN(parseFloat(valordelauto))) {
    	document.getElementById('resultado').innerHTML="<span style='color: red;'>Introduce Valor del Auto</span>";
    	document.getElementById("valordelauto").innerText = 0;
    	document.getElementById("valordelauto").focus();
    	marcada = 0;
    } 
    else if (isNaN(parseFloat(enganche))) {
    	document.getElementById('resultado').innerHTML="<span style='color: red;'>Introduce Enganche</span>";
    	document.getElementById("enganche").innerText = 0;
    	document.getElementById("enganche").focus();
    	marcada = 0;
    }
    else if (tasa<=0.001) {
    	document.getElementById('resultado').innerHTML="<span style='color: red;'>Selecciona Marca</span>";
    	document.getElementById("mySelectMarca").focus();
    	marcada = 0;
    }

    else {
    //Si se han introducido los números en ámbas cajas, operamos:
    switch(op) {
    	case 'pagomensual60':
    	var resultado = ops.pagox60(valordelauto, enganche, tasa);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(resultado)+"</span>";
    	document.getElementById('nuevatasa').innerHTML="<span style='color: green;'>"+tasa+'%'+"</span>";
    	document.getElementById("meseselegidos").innerHTML = 60
    	var montoafinanciar = resultado*60;
    	document.getElementById("montoa").innerHTML = usCurrencyFormat.format(montoafinanciar);
    	document.getElementById("montoa2").innerHTML = usCurrencyFormat.format(montoafinanciar);
    	document.getElementById("valordelauto2").innerHTML = usCurrencyFormat.format(valordelauto);
    	document.getElementById("enganche2").innerHTML = usCurrencyFormat.format(enganche);
    	break;
    	case