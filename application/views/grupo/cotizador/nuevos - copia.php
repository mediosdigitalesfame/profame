<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Cotizador</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
						<li class="active">Cotizador</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

</div>

<div class="container py-2">
	<div class="row">
		<div class="col-lg-6">
			<div class="featured-box featured-box-primary">
				<div class="box-content p-5">
					<div class="row">
						<div class="col">

							<div class="row">
								<div class="col-12" align="center"> 
									<h2 class="font-weight-normal text-6">Simula tu crédito</h2>
									<p>Selecciona qué auto quieres.</p>
								</div>
							</div>

							<form action="/" id="frmBillingAddress" method="post">
								<div class="form-row">
									<div class="form-group col">
										<select class="form-control">
											<option value="">Selecciona Modelo</option>
											<option value="">Modelo 1</option>
										</select>
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col">
										<select class="form-control validate[required]" id="mySelectMarca" name="q26_solicitud" data-component="dropdown" required="" aria-labelledby="label_10">
											<option value="">Selecciona Marca</option>
											<option value="Honda">Honda</option>
											<option value="Chevrolet">Chevrolet</option>
										</select>
									</div>
								</div>



								<div class="form-row">

									<div class="form-group col-lg-12">
										<label class="font-weight-bold text-dark text-2">
											<span class="amount"><h6 class="text-primary">El Monto a financiar seria por:</h6></span>
											<h3 class="text-primary text-12">
												<span class="amount" id="montoa">$0</span>
											</h3>
										</label>
									</div>

									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Valor del Auto</label>
										<input id="valordelauto" type="text" value="" class="form-control" onkeyup="PasarValor();" onblur="Financiamientoprincipal()">
									</div>

									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Enganche</label>
										<input id="enganche" type="text" value="" class="form-control">
									</div>

									<div class="form-group col-lg-6"> </div>

									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">
											<span class="amount"><h6 class="text-primary">Enganche sugerido:</h6></span>
											<span class="amount" id="enganchesu"> </span>
										</label>
									</div>
								</div>






								<div class="form-row">
									<div class="form-group col-lg-12">

										<div class="row">
											<div class="col-12" align="center"> 
												<h2 class="font-weight-normal text-6">Escoge tus pagos mensuales</h2>
											</div>
										</div>

										<table class="cart-totals">
											<tbody>
												<tr class="cart-subtotal">
													<td >
														<input id="input6" type="button" value="60 Meses" onClick="asignarNota(6); operaciones('pagomensual60')" onmouseover="document.getElementById('input6').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();" class="btn btn-xl btn-light pr-4 pl-4 text-4 font-weight-semibold text-uppercase float-right mb-2">
													</td>
													<td >
														<input id="input5" type="button" value="48 Meses" onClick="asignarNota(5); operaciones('pagomensual48')" onmouseover="document.getElementById('input5').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();" class="btn btn-xl btn-light pr-4 pl-4 text-4 font-weight-semibold text-uppercase float-right mb-2">
													</td>
													<td >
														<input id="input4" type="button" value="36 Meses" onClick="asignarNota(4); operaciones('pagomensual36')" onmouseover="document.getElementById('input4').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();" class="btn btn-xl btn-light pr-4 pl-4 text-4 font-weight-semibold text-uppercase float-right mb-2">
													</td>
												</tr>
												<tr class="cart-subtotal">
													<td>
														<input id="input3" type="button" value="24 Meses" onClick="asignarNota(3); operaciones('pagomensual24')" onmouseover="document.getElementById('input3').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();" class="btn btn-xl btn-light pr-4 pl-4 text-4 font-weight-semibold text-uppercase float-right mb-2">
													</td>
													<td>

														<input id="input2" type="button" value="18 Meses" onClick="asignarNota(2); operaciones('pagomensual18')" onmouseover="document.getElementById('input2').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();" class="btn btn-xl btn-light pr-4 pl-4 text-4 font-weight-semibold text-uppercase float-right mb-2">

													</td>
													<td>
														<input id="input1" type="button" value="12 Meses" onClick="asignarNota(1); operaciones('pagomensual12')" onmouseover="document.getElementById('input1').style.backgroundColor='#186dd6',style.color='white';" onmouseout="desmarcarNotas();" class="btn btn-xl btn-light pr-4 pl-4 text-4 font-weight-semibold text-uppercase float-right mb-2">
													</td>
												</tr>
											</tbody>
										</table>

									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="featured-box featured-box-primary">
				<div class="box-content p-5">
					<div class="row">
						<div class="col">

							<div class="row">
								<div class="col-12" align="center"> 
									<h2 class="font-weight-normal text-6">¿Te interesa este plan?</h2>
									<p>Pago mensual a <span id="meseselegidos">0</span> meses</p>
								</div>
							</div>

							<div class="row">
								<div class="col-12" align="center"> 
									<h4 class="text-primary text-9"><p id="resultado" style="color: green;">$0</p></h4>
									<div class="divider divider-small divider-small-center">
										<hr>  
									</div>
								</div>
							</div>

							<hr>
							<div class="row" align="center">
								<div class="col-12" align="center"> 
									<input type="submit" value="Quiero mas información" class="btn btn-xl btn-light text-2 font-weight-semibold text-uppercase float mb-2" data-loading-text="Loading...">
								</div>
							</div>
							<hr>

							<div class="row">
								<div class="col pb-3">



									<table align="center" border="0" cellspacing="4" cellpadding="5">

										<tbody>
											<tr>

											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Monto a Financiar
												</td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>
												<td colspan="" rowspan="9" headers=""></td>

												<td align="right">
													<span id="montoa2" value="0" name="tasadeinteres">$0</span>
												</td>
											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Valor del Auto
												</td>



												<td align="right">
													<span id="valordelauto2" value="0" name="tasadeinteres">$0</span>
												</td>
											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Enganche
												</td>


												<td align="right">
													<span id="enganche2" value="0" name="tasadeinteres">$0</span>
												</td>
											</tr>
											<!-- <tr>
												<td colspan="4" rowspan="" align="left">
													Seguro de daños
												</td>
												<td align="right">
													<span id="segurodedaños" value="0">$0</span>
												</td>
											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Seguro de vida
												</td>												
												<td align="right">
													<span id="segurodevida" value="0">$0</span>
												</td>
											</tr>-->

											<tr>
												<td colspan="13" rowspan="" align="left">
												</td>
											</tr>
											<tr>
												<td colspan="13" rowspan="" align="left">
												</td>
											</tr>


											<tr>
												<td colspan="4" rowspan="" align="left">
													Tasa de Interes Anual
												</td>



												<td align="right">
													<span id="nuevatasa" value="0" name="tasadeinteres">0%</span>
												</td>
											</tr>
											
											<tr>
												<td colspan="4" rowspan="" align="left">
													CAT
												</td>
												<td align="right">
													<span id="costoanual" value="0" name="tasadeinteres">0%</span>
												</td>
											</tr>
										</tbody>
									</table>

									<hr class="solid my-5">

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script language="javascript">

	const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'})

	var valordelauto = document.getElementById("valordelauto").value;
	var enganche = document.getElementById("enganche").value;
	var marca = document.getElementById("mySelectMarca").value;
	var tasa = document.getElementById("nuevatasa").value;
	var mes = document.getElementById("meseselegidos").value;

	window.onload = function() {
		asignarNota(6)
	};

	function financiamientoprincipal(){

	}

	function operaciones(op){
		var ops = {
			montoafinanciar: function sacarMonto(n1, n2, n3) {
				return ((parseInt(n1) - parseInt(n2))*n3);
			},

			pagox60: function sacarMeses60(n1, n2, n3) {
				return (((parseInt(n1) - parseInt(n2))/60)*n3);
			},

			pagox48: function sacarMeses48(n1, n2, n3) {
				return (((parseInt(n1) - parseInt(n2))/48)*n3);
			},

			pagox36: function sacarMeses36(n1, n2, n3) {
				return (((parseInt(n1) - parseInt(n2))/36)*n3);
			},

			pagox24: function sacarMeses24(n1, n2, n3) {
				return (((parseInt(n1) - parseInt(n2))/24)*n3);
			},

			pagox18: function sacarMeses18(n1, n2, n3) {
				return (((parseInt(n1) - parseInt(n2))/18)*n3);
			},

			pagox12: function sacarMeses12(n1, n2, n3) {
				return (((parseInt(n1) - parseInt(n2))/12)*n3);
			},

			sumar: function sumarNumeros(n1, n2) {
				return (parseInt(n1) + parseInt(n2));
			},

			restar: function restarNumeros(n1, n2) {
				return (parseInt(n1) - parseInt(n2));
			},

			multiplicar: function multiplicarNumeros(n1, n2) {
				return (parseInt(n1) * parseInt(n2));
			},

			dividir: function dividirNumeros(n1, n2) {
				return (parseInt(n1) / parseInt(n2));
			}    
		};

		valordelauto = document.getElementById("valordelauto").value;
		enganche = document.getElementById("enganche").value;
		tasa = document.getElementById("nuevatasa").value;
		marca = document.getElementById("mySelectMarca").value;
		mes = document.getElementById("meseselegidos").value;

		switch (marca)
		{
			case 'Honda': tasa = 1.13;
			break;  
			case 'Chevrolet': tasa = 1.12;
			break;  
			case '': tasa = 0.001;
			break;  
		} 

    //Comprobamos si se ha introducido números en las cajas
    if (isNaN(parseFloat(valordelauto))) {
    	document.getElementById('resultado').innerHTML="<span style='color: red;'>Introduce Valor del Auto</span>";
    	document.getElementById("valordelauto").innerText = 0;
    	document.getElementById("valordelauto").focus();
    	marcada = 0;
    } 
    else if (isNaN(parseFloat(enganche))) {
    	document.getElementById('resultado').innerHTML="<span style='color: red;'>Introduce Enganche</span>";
    	document.getElementById("enganche").innerText = "0";
    	document.getElementById("enganche").focus();
    	marcada = 0;
    }
    else if (tasa<=0.001) {
    	document.getElementById('resultado').innerHTML="<span style='color: red;'>Selecciona Marca</span>";
    	document.getElementById("mySelectMarca").focus();
    	marcada = 0;
    }

    else {
    //Si se han introducido los números en ámbas cajas, operamos:
    switch(op) {
    	case 'pagomensual60':
    	var resultado = ops.pagox60(valordelauto, enganche, tasa);
    	const m60Moneda = usCurrencyFormat.format(resultado);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+m60Moneda+"</span>";
    	document.getElementById('nuevatasa').innerHTML="<span style='color: green;'>"+tasa+'%'+"</span>";
    	document.getElementById("meseselegidos").innerHTML = 60
    	var montoafinanciar = ops.montoafinanciar(valordelauto, enganche, tasa);
    	const montofinalafinanciar = usCurrencyFormat.format(montoafinanciar);
    	const valordelauto2 = usCurrencyFormat.format(valordelauto);
    	const enganche2 = usCurrencyFormat.format(enganche);
    	document.getElementById("montoa").innerHTML = montofinalafinanciar
    	document.getElementById("montoa2").innerHTML = montofinalafinanciar
    	document.getElementById("valordelauto2").innerHTML = valordelauto2
    	document.getElementById("enganche2").innerHTML = enganche2
    	break;
    	case 'pagomensual48':
    	var resultado = ops.pagox48(valordelauto, enganche, tasa);
    	const m48Moneda = usCurrencyFormat.format(resultado);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+m48Moneda+"</span>";
    	document.getElementById('nuevatasa').innerHTML="<span style='color: green;'>"+tasa+'%'+"</span>";
    	document.getElementById("meseselegidos").innerHTML = 48
    	break;
    	case 'pagomensual36':
    	var resultado = ops.pagox36(valordelauto, enganche, tasa);
    	const m36Moneda = usCurrencyFormat.format(resultado);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+m36Moneda+"</span>";
    	document.getElementById('nuevatasa').innerHTML="<span style='color: green;'>"+tasa+'%'+"</span>";
    	document.getElementById("meseselegidos").innerHTML = 36
    	break;
    	case 'pagomensual24':
    	var resultado = ops.pagox24(valordelauto, enganche, tasa);
    	const m24Moneda = usCurrencyFormat.format(resultado);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+m24Moneda+"</span>";
    	document.getElementById('nuevatasa').innerHTML="<span style='color: green;'>"+tasa+'%'+"</span>";
    	document.getElementById("meseselegidos").innerHTML = 24
    	break;
    	case 'pagomensual18':
    	var resultado = ops.pagox18(valordelauto, enganche, tasa);
    	const m18Moneda = usCurrencyFormat.format(resultado);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+m18Moneda+"</span>";
    	document.getElementById('nuevatasa').innerHTML="<span style='color: green;'>"+tasa+'%'+"</span>";
    	document.getElementById("meseselegidos").innerHTML = 18
    	break;
    	case 'pagomensual12':
    	var resultado = ops.pagox12(valordelauto, enganche, tasa);
    	const m12Moneda = usCurrencyFormat.format(resultado);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+m12Moneda+"</span>";
    	document.getElementById('nuevatasa').innerHTML="<span style='color: green;'>"+tasa+'%'+"</span>";
    	document.getElementById("meseselegidos").innerHTML = 12
    	break;
    	case 'sumar':
    	var resultado = ops.sumar(num1, num2);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+resultado+"</span>";
    	break;
    	case 'restar':
    	var resultado = ops.restar(num1, num2);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+resultado+"</span>";
    	break;
    	case 'multiplicar':
    	var resultado = ops.multiplicar(num1, num2);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+resultado+"</span>";
    	break;
    	case 'dividir':
    	var resultado = ops.dividir(num1, num2);
    	document.getElementById('resultado').innerHTML="<span style='color: green;'>"+resultado+"</span>";
    	break;

    }
}

}

desmarcarNotas();
var marcada = 0;

function asignarNota(valor)
{
	marcada = valor;
	desmarcarNotas();

	switch (valor)
	{
		case 1: document.getElementById("meseselegidos").innerHTML = 12;
		break;  
		case 2: document.getElementById("meseselegidos").innerHTML = 18;
		break;  
		case 3: document.getElementById("meseselegidos").innerHTML = 24;
		break;
		case 4: document.getElementById("meseselegidos").innerHTML = 36;
		break;
		case 5: document.getElementById("meseselegidos").innerHTML = 48;
		break;
		case 6: document.getElementById("meseselegidos").innerHTML = 60;
		break;  
	} 
}

function desmarcarNotas() {
	document.getElementById('input1').style.backgroundColor='white';
	document.getElementById('input1').style.color='black';

	document.getElementById('input2').style.backgroundColor='white';
	document.getElementById('input2').style.color='black';

	document.getElementById('input3').style.backgroundColor='white';
	document.getElementById('input3').style.color='black';

	document.getElementById('input4').style.backgroundColor='white';
	document.getElementById('input4').style.color='black';

	document.getElementById('input5').style.backgroundColor='white';
	document.getElementById('input5').style.color='black';

	document.getElementById('input6').style.backgroundColor='white';
	document.getElementById('input6').style.color='black';

	marcarNota(marcada);
}

function marcarNota(id) {
	if(id>0)
	{
		document.getElementById("input"+id).style.backgroundColor='#186dd6';
		document.getElementById("input"+id).style.color='white';
	}
}

function PasarValor()
{
	var ensu = document.getElementById("valordelauto").value *.10;
	const enMoneda = usCurrencyFormat.format(ensu);
	document.getElementById("enganchesu").innerHTML = enMoneda
}





</script>



