

<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Modelos</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
						<li class="active">Modelos</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col" align="center">
				<h4>Honda</h4>
				<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>grupo/r1250gs">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/modelos/cuadro/r1250gs.jpg">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">R 1250 GS<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
						<div align="center" class="thumb-info-inner">R 1250 GS<em></em></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col" align="center">
				<h4>Honda</h4>
				<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/modelos/cuadro/s1000r.jpg">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">S 1000 R<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
						<div align="center" class="thumb-info-inner">S 1000 R<em></em></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col" align="center">
				<h4>Honda</h4>
				<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/modelos/cuadro/rninet.jpg">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">R nineT<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
						<div align="center" class="thumb-info-inner">R nineT<em></em></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col" align="center">
				<h4>Honda</h4>
				<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/modelos/cuadro/c650sport.jpg">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">C 650 Sport<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
						<div align="center" class="thumb-info-inner">C 650 Sport<em></em></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col" align="center">
				<h4>Honda</h4>
				<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/modelos/cuadro/hp4race.jpg">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">HP4 Race<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
						<div align="center" class="thumb-info-inner">HP4 Race<em></em></div>
					</div>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col" align="center">
				<h4>Honda</h4>
				<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/modelos/cuadro/k1600b.jpg">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">K 1600 B<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
						<div align="center" class="thumb-info-inner">K 1600 B<em></em></div>
					</div>
				</div>
			</div>
		</div>

		
	</div>
</div>

