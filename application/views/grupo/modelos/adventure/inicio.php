<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Adventure</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="#">Inicio</a></li>
						<li class=""><a href="#">Modelos</a></li>
						<li class="active">Adventure</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="section section-height-3 bg-color-grey-scale-1 m-0 border-0">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-md-6">
					<h2 class="text-color-dark font-weight-normal text-6 mb-2"><strong class="font-weight-extra-bold">Adventure</strong></h2>
					<p class="lead">Una aventura motociclista deja huellas de neumáticos en la arena, polvo en tu moto y recuerdos en tu corazón.</p>
					<p class="pr-5 mr-5">No te pueden detener ni la naturaleza ni las fronteras del mapa, porque te impulsan el espíritu viajero y tu moto con su potencia duradera y su tecnología fiable.</p>
					<p class="pr-5 mr-5">Con estas motos tienes el mundo a tus pies, con toda su belleza desconocida y sus retos. Están preparadas para todo tipo de terreno. ¿Tú también estás preparado?</p>

				</div>
				<div class="col-md-6">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/oJoO-kmJfYA"></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row justify-content-center">
			
			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">R 1250 GS</strong></h2>
							<p class="mb-0">Marca tu propio camino. </p>
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/s1000xr.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">S 1000 XR</strong></h2>
							<p class="mb-0">Explora el mundo. Más rapido que nunca. </p>
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/f850gs.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">F 850 GS</strong></h2>
							<p class="mb-0">Aventure in the Blood. </p>
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/f750gs.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">F 750 GS</strong></h2>
							<p class="mb-0">El comienzo de algo nuevo. </p>
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/g310gs.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">G 310 GS</strong></h2>
							<p class="mb-0">Aventuras cotidianas. </p>
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/f850gsadventure.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">F 850 GS Adventure</strong></h2>
							<p class="mb-0">El mundo se convierte en una aventura. </p>
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gsa.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">R 1250 GSA</strong></h2>
							<p class="mb-0">Tu camino es la meta. </p>
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/s1000xr-2.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">S 1000 XR</strong></h2>
							<p class="mb-0">Un nuevo reto en cada curva y en cada kilómetro. </p>
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/f900xr.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">F 900 XR</strong></h2>
							<p class="mb-0">Más alto, más rápido, más lejos. </p>
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>




		</div>

	</div>
</div>

