<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>R 1250 GS</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
						<li class=""><a href="<?php echo base_url(); ?>modelos">Modelos</a></li>
						<li class=""><a href="<?php echo base_url(); ?>modelos/adventure">Adventure</a></li>
						<li class="active">R 1250 GS</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="slider-container rev_slider_wrapper" style="height: 530px;">
		<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 530, 'responsiveLevels': [4096,1200,992,500], 'navigation' : {'arrows': { 'enable': true }, 'bullets': {'enable': false, 'style': 'bullets-style-1 bullets-color-primary', 'h_align': 'center', 'v_align': 'bottom', 'space': 7, 'v_offset': 70, 'h_offset': 0}}}">
			<ul>
				<li data-transition="fade">
					<img src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/slide.jpg"  
					alt=""
					data-bgposition="center center" 
					data-bgfit="cover" 
					data-bgrepeat="no-repeat" 
					class="rev-slidebg">

					<a class="tp-caption btn btn-primary font-weight-bold"
					href="#"
					data-frames='[{"delay":3000,"speed":2000,"frame":"0","from":"y:50%;opacity:0;","to":"y:0;o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
					data-x="center" data-hoffset="0"
					data-y="center" data-voffset="['170','70','70','135']"
					data-paddingtop="['15','15','15','30']"
					data-paddingbottom="['15','15','15','30']"
					data-paddingleft="['40','40','40','57']"
					data-paddingright="['40','40','40','57']"
					data-fontsize="['13','13','13','25']"
					data-lineheight="['20','20','20','25']">Solicitar Cotización<i class="fas fa-arrow-right ml-1"></i></a>
				</li>
			</ul>
		</div>
	</div>

	<div class="container">

		<div class="row text-center pt-4 mt-5">
			<div class="col">
				<h1 class="word-rotator slide font-weight-bold text-12 mb-2">
					<span>NUEVA BMW R 1250 GS.</span>
				</h1>
				<h1 class="word-rotator slide font-weight-bold text-8 mb-2">
					<span>CON TODAVÍA MÁS POTENCIA PARA QUE VIVAS TU AVENTURA.</span>
				</h1>
			</div>
		</div>

		<hr>

		<div class="row align-items-center justify-content-center">
			<div class="col-md-6">
				<p class="pr-5 mr-5">Nueva BMW R 1250 GS: rendimiento concentrado con una eficiencia que impresiona. Con la nueva BMW R 1250 GS sentirás el nuevo motor boxer todavía más directamente. El aumento de la cilindrada del motor le aporta más potencia: 100 kW (136 CV) y 1.250 c. c. El sistema de control variable del árbol de levas BMW ShiftCam garantiza una entrega de potencia superior, en los regímenes altos y bajos del motor. Ya sea por la ciudad, en autopista o, por supuesto, fuera de la carretera: cada metro te da más libertad y aumenta el placer de viajar. Nuevos acabados y colores, numerosos detalles técnicos y opciones de lo más emocionantes para personalizar el carácter de tu nueva BMW R 1250 GS.</p>
			</div>
			<div class="col-md-6">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/oJoO-kmJfYA"></iframe>
				</div>
			</div>
		</div>

	</div>

	<section class="parallax section section-parallax section-center" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/fondo.jpg">
		<br><br><br><br><br><br><br><br><br><br>
	</section>

	<div class="container">

		<div class="row text-center pt-4 mt-5">
			<div class="col">
				<h1 class="word-rotator slide font-weight-bold text-10 mb-2">
					<span>EL DISEÑO DE LA BMW R 1250 GS</span>
				</h1>

			</div>
		</div>

		<div class="row">
			<div class="col">
				
				<ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
					<li class="nav-item active" data-option-value=".negrotormenta"><a class="nav-link text-1 text-uppercase" href="#">Negro Tormenta</a></li>
					<li class="nav-item" data-option-value=".azulcosmico"><a class="nav-link text-1 text-uppercase" href="#">Azul Cosmico</a></li>
					<li class="nav-item" data-option-value=".acabadohp"><a class="nav-link text-1 text-uppercase" href="#">Acabado HP</a></li>
					<li class="nav-item" data-option-value=".acabadoexclusive"><a class="nav-link text-1 text-uppercase" href="#">Acabado Exclusive</a></li>
				</ul>

				<div class="sort-destination-loader sort-destination-loader-showing mt-12 pt-2">
					<div class="row portfolio-list sort-destination" data-sort-id="portfolio">

						<div class="col-12 col-sm-12 col-lg-12 isotope-item negrotormenta">
							<div class="portfolio-item">
								<span class="thumb-info thumb-info-lighten border-radius-0">
									<span class="thumb-info-wrapper border-radius-0">
										<img src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/negrotormentametalizado.jpg" class="img-fluid border-radius-0" alt="">
									</span>
								</span>
								<div align="center" class="col">
									<h1 class="word-rotator slide font-weight-bold text-8 mb-2">
										<span>Negro Tormenta metalizado.</span>
									</h1>
									<p class="lead slide font-weight-normal text-4 mb-2">
										<span>El diseño de la versión de serie en tonalidad oscura transmite firmeza y potencia.</span>
									</p>
								</div>
							</div>
						</div>

						<div class="col-12 col-sm-12 col-lg-12 isotope-item azulcosmico">
							<div class="portfolio-item">
								<span class="thumb-info thumb-info-lighten border-radius-0">
									<span class="thumb-info-wrapper border-radius-0">
										<img src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/azulcosmicometalizado.jpg" class="img-fluid border-radius-0" alt="">
									</span>
								</span>
								<div align="center" class="col">
									<h1 class="word-rotator slide font-weight-bold text-8 mb-2">
										<span>Azul Cósmico metalizado.</span>
									</h1>
									<p class="lead slide font-weight-normal text-4 mb-2">
										<span>La versión básica en azul más claro representa la necesidad de recorrer kilómetros y caminos hacia el horizonte, que parece estar al alcance de la mano.</span>
									</p>
								</div>
							</div>
						</div>

						<div class="col-12 col-sm-12 col-lg-12 isotope-item acabadohp">
							<div class="portfolio-item">
								<span class="thumb-info thumb-info-lighten border-radius-0">
									<span class="thumb-info-wrapper border-radius-0">
										<img src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/acabadohp.jpg" class="img-fluid border-radius-0" alt="">
									</span>
								</span>
								<div align="center" class="col">
									<h1 class="word-rotator slide font-weight-bold text-8 mb-2">
										<span>Acabado HP.</span>
									</h1>
									<p class="lead slide font-weight-normal text-4 mb-2">
										<span>El nuevo diseño con los colores de BMW HP Motorsport es dinámico y no admite concesiones. Las características adicionales transmiten un carácter off-road a la moto.</span>
									</p>
								</div>
							</div>
						</div>

						<div class="col-12 col-sm-12 col-lg-12 isotope-item acabadoexclusive">
							<div class="portfolio-item">
								<span class="thumb-info thumb-info-lighten border-radius-0">
									<span class="thumb-info-wrapper border-radius-0">
										<img src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/acabadoexclusive.jpg" class="img-fluid border-radius-0" alt="">
									</span>
								</span>
								<div align="center" class="col">
									<h1 class="word-rotator slide font-weight-bold text-8 mb-2">
										<span>Acabado Exclusive.</span>
									</h1>
									<p class="lead slide font-weight-normal text-4 mb-2">
										<span>Negro Tormenta metalizado mate, combinado con Negro Noche mate, con materiales y superficies de alta calidad, así se materializa el concepto de aventura elegante.</span>
									</p>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="section section-default section-with-divider mb-0">
		<div class="divider divider-solid divider-style-4">
			<i class="fas fa-chevron-up"></i>
		</div>
		<div class="container">
			<div class="row">
				<div class="col">
					<h4 class="mb-0">3 años de garantía.</h4>
					<p class="mb-0">Sin límite de kilometraje y asistencia en el camino.</p>
				</div>
			</div>
		</div>
	</section>
	<br>

	<div class="container">
		<div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'autoplay': true, 'autoplayTimeout': 3000}">
			<div>
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/galeria1-7.jpg" alt="">
			</div>
			<div>
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/galeria1-6.jpg" alt="">
			</div>
			<div>
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/galeria1-5.jpg" alt="">
			</div>
			<div>
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/galeria1-4.jpg" alt="">
			</div>
			<div>
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/galeria1-3.jpg" alt="">
			</div>
			<div>
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/galeria1-2.jpg" alt="">
			</div>
			<div>
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/galeria1-1.jpg" alt="">
			</div>
		</div>
		<br>

		<div align="center" class="row">
			<div class="col">
				<h4 class="mb-0">Datos y Equipamiento.</h4>
				<p class="mb-0">Ya sea en recorridos largos o fuera de la carretera, la nueva BMW R 1250 GS, con su nuevo motor boxer, es la mejor enduro de viaje con la combinación óptima de dinamismo y comodidad, y lograr que la perfección en tecnología se transforma en placer para viajar. Encontrarás más información sobre los datos técnicos y el equipamiento de serie aquí.</p>
				<hr>
			</div>
		</div>

		<div class="row counters text-dark">
			<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
				<div class="counter">
					<strong data-to="100">0</strong>
					<label>kW (136 CV) a 7.750 rpm</label>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
				<div class="counter">
					<strong data-to="143">0</strong>
					<label>43 Nm de par máx. a 6.250 rpm.</label>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 mb-4 mb-sm-0">
				<div class="counter">
					<strong data-to="1254">0</strong>
					<label>c. c. de cilindrada
					</label>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3">
				<div class="counter">
					<strong data-to="850">0</strong>
					<label>/870 mm, altura del asiento sin carga.</label>
				</div>
			</div>
		</div>
		<hr>

		<embed src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/r1250gs.pdf" type="application/pdf" width="100%" height="600px" />

			<hr>

		</div>

		<div class="container">
			<h2 align="center" class="text-color-dark font-weight-normal text-8 mb-2"><strong class="font-weight-extra-bold">MOTOR BICILÍNDRICO BOXER</strong></h2>
			<div class="row align-items-center justify-content-center">
				<div class="col-md-6">
					<h2 class="text-color-dark font-weight-normal text-6 mb-2"><strong class="font-weight-extra-bold">Rendimiento, eficiencia, control.</strong></h2>
					<p class="lead">El motor bicilíndrico boxer de la nueva BMW R 1250 GS fascina por su diversidad desde el primer momento. Su carácter es inequívoco, en cualquier régimen del motor. Ofrece más rendimiento y, a su vez, una marcha suave, a una velocidad baja. El control variable del árbol de levas BMW ShiftCam garantiza una curva de par más potente en toda la gama de regímenes del motor, y proporciona, así, un mayor control en cualquier situación de conducción. Así tendrás más potencia cuando la necesites. Más cilindrada y más potencia con mayor eficiencia. Con un extraordinario rendimiento, y menor cantidad de cambios de marcha, la moto es imponente. En resumen: siente la energía del motor boxer aún más directamente.</p>
				</div>
				<div class="col-md-6">
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/rendimiento.jpg" alt="">
				</div>
			</div>

			<div class="row">

				<div class="col-lg-4">
					<div class="featured-box featured-box-primary featured-box-effect-1">
						<div class="box-content p-5">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/novedades-3.jpg" alt="">
							<h4 class="font-weight-normal text-5 text-dark"><strong class="font-weight-extra-bold">Pantalla</strong></h4>
							<p class="mb-0">La pantalla TFT en color de 6,5 pulgadas muestra informacion como la velocidad y las rpm, datos de navegación, de telefonía, de medios e información adicional sobre la moto. Cuando oscurece o al pasar por un túnel, la iluminación y la intensidad del color de la pantalla se ajustan automáticamente. Incluso con luz solar fuerte, la información se lee muy fácilmente.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4">
					<div class="featured-box featured-box-primary featured-box-effect-1">
						<div class="box-content p-5">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/novedades-3.jpg" alt="">
							<h4 class="font-weight-normal text-5 text-dark"><strong class="font-weight-extra-bold">Opcional 719</strong></h4>
							<p class="mb-0">Opcional 719 ofrece paquetes creados especialmente para definir el diseño de la nueva BMW R 1250 GS. Los paquetes de complementos HP subrayan el carácter deportivo de la moto con componentes de aluminio. El paquete de piezas Classic del Opcional 719 añade una nota de diseño con sus exclusivos componentes en color plateado. Y el pquete Storm del Opcional 719 confiere un aspecto robusto y único a la moto con su tonalidad gris plateado.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4">
					<div class="featured-box featured-box-primary featured-box-effect-1">
						<div class="box-content p-5">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/novedades-3.jpg" alt="">
							<h4 class="font-weight-normal text-5 text-dark"><strong class="font-weight-extra-bold">Faros de Led</strong></h4>
							<p class="mb-0">Tanto con luces de carretera como con luces de cruce, la iluminación es más ancha y homogénea que los faros convencionales. Además, la luz es más blanca, lo que hace que los contornos sean más claros y que aumente significativamente la seguridad pasiva.</p>
						</div>
					</div>
				</div>

			</div>
			<hr>

			<div class="row">

				<div class="row align-items-center justify-content-center">
					<div class="col-md-6">
						<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/modelos/r1250gs/frenos.jpg" alt="">
					</div>
					<div class="col-md-6">
						<h2 class="text-color-dark font-weight-normal text-6 mb-2"><strong class="font-weight-extra-bold">Control Dinámico de los Frenos (DBC).</strong></h2>
						<p class="lead">El DBC apoya en frenados de emergencia contrarrestando el accionamiento en ocasiones no deseado del gas en una situación tan extrema. La intervención en el control del motor reduce el par del motor. A su vez, la potencia de frenado aumenta en la rueda trasera. ¿El resultado? Más seguridad mediante el mejor uso de la capacidad de frenado para estabilizar la moto y reducir la distancia de frenado. </p>
					</div>
				</div>

			</div>
		</div>
	</div>

