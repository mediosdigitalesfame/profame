<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border><?php echo $nombre_marca; ?> - <?php echo $nombre_modelo; ?> <?php echo $ano_modelo; ?></h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>">Inicio</a></li>
						<li class=""><a href="<?php echo base_url(); ?>grupo/modelos">Modelos</a></li>
						<li class=""><a href="<?php echo base_url(); ?>grupo/modeloshonda">Honda</a></li>
						<li class="active"><?php echo $nombre_modelo; ?> <?php echo $ano_modelo; ?></li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="z-index-1 appear-animation" data-appear-animation="fadeInDownShorter" data-appear-animation-delay="500">
		<div class="owl-carousel owl-theme full-width owl-loaded owl-drag owl-carousel-init m-0 mb-4" data-plugin-options="{'items': 1, 'loop': true, 'nav': true, 'dots': false, 'animateOut': 'fadeOut'}">
			<div>
				<img src="<?php echo base_url(); ?>assets/porto/img/modelos/<?php echo $nombre_modelos; ?>/banner/<?php echo $nombre_imagen_modelo; ?>" class="img-fluid border-radius-0" alt="">
			</div>
		</div>
	</div>

	<div class="container py-4">

		<div class="row pt-4 mt-2 mb-5">
			<div class="col-md-8 mb-4 mb-md-0">
				<h2 class="text-color-dark font-weight-normal text-5 mb-2"><strong class="font-weight-extra-bold">Descripción</strong></h2>
				<p><?php echo $descripcion_modelo; ?></p>
			</div>
			<div class="col-md-4">
				<h2 class="text-color-dark font-weight-normal text-5 mb-2"><strong class="font-weight-extra-bold">Detalles</strong></h2>
				<ul class="list list-icons list-primary list-borders text-2">
					<li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Ficha Técnica:</strong> 
						<a href="<?php echo base_url(); ?>assets/porto/fichas/<?php echo $nombre_modelos; ?>/<?php echo $nombre_pdf_modelo; ?>" download="<?php echo $nombre_modelo; ?><?php echo $ano_modelo; ?>" target="_blank" class="text-dark">Descargar</a>
					</li>
				</ul>
			</div>
		</div>

		<hr class="solid my-5">

		<div class="row pt-4 mt-2 mb-5">
		<embed src="<?php echo base_url(); ?>assets/porto/fichas/<?php echo $nombre_modelos; ?>/<?php echo $nombre_pdf_modelo; ?>" type="application/pdf" width="100%" height="600px" />
		</div>

	</div>
</div>



