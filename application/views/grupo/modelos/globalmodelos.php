<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Modelos <?php echo $nombre_marca; ?></h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>">Inicio</a></li>
						<li class=""><a href="<?php echo base_url(); ?>grupo/modelos">Modelos</a></li>
						<li class="active"><?php echo $nombre_marca; ?></li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container">
		<div class="row justify-content-center">

			<?php $this->load->view('grupo/modelos/'.$nombre_modelos); ?>

		</div>
	</div>
</div>

