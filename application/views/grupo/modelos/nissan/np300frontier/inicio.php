<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Nissan NV 350 Frontier</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>">Inicio</a></li>
						<li class=""><a href="<?php echo base_url(); ?>modelos">Modelos</a></li>
						<li class=""><a href="nissan">Nissan</a></li>
						<li class="active">NV 350 Frontier</li>
					</ul>
				</div>
			</div>
		</div>
	</section>


	<div class="container">

		<embed src="<?php echo base_url(); ?>assets/fichas/nissan/np350frontier.pdf" type="application/pdf" width="100%" height="600px" />

			<div class="row py-4">
				<div class="col-lg-12">

					<div class="overflow-hidden mb-1">
						<h2 class="font-weight-normal text-7 mt-2 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">
							<strong class="font-weight-extra-bold">Te</strong> Contactamos</h2>
						</div>
						<div class="overflow-hidden mb-4 pb-3">
							<p class="mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="400">Dejanos tus datos y en un instante uno de nuestros asesores te atendera!</p>
						</div>

						<form id="contactForm" class="contact-form" action="php/contact-form.php" method="POST">
							<div class="contact-form-success alert alert-success d-none mt-4" id="contactSuccess">
								<strong>Success!</strong> EL mensaje fue enviado.
							</div>

							<div class="contact-form-error alert alert-danger d-none mt-4" id="contactError">
								<strong>Error!</strong> Error al enviar el mensaje.
								<span class="mail-error-message text-1 d-block" id="mailErrorMessage"></span>
							</div>

							<div class="form-row">
								<div class="form-group col-lg-3">
									<label class="required font-weight-bold text-dark text-2">Nombre</label>
									<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
								</div>
								<div class="form-group col-lg-3">
									<label class="required font-weight-bold text-dark text-2">Email</label>
									<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
								</div>
								<div class="form-group col-lg-6">
									<label class="font-weight-bold text-dark text-2">Asunto</label>
									<input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
								</div>
							</div>

							<div class="form-row">
								<div class="form-group col">
									<label class="required font-weight-bold text-dark text-2">Mensaje</label>
									<textarea maxlength="5000" data-msg-required="Please enter your message." rows="4" class="form-control" name="message" id="message" required></textarea>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col">
									<input type="submit" value="Enviar" class="btn btn-primary btn-modern" data-loading-text="Loading...">
								</div>
							</div>
						</form>

					</div>


				</div>



			</div>

		</div>

