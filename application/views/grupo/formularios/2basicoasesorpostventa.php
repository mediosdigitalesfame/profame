<?php $host= $_SERVER["HTTP_HOST"]; $url= $_SERVER["REQUEST_URI"];?>
<input type="hidden" id="input_27" class="form-control" value="<?php echo "http://" . $host . $url; ?>" name="q27_origen" />

<script type="text/javascript" src="https://cdn.jotfor.ms/js/formTranslation.v2.js?3.3.15898"></script>

<div class="form-row"> 
  <div class="form-group col">
    <label class="required font-weight-bold text-dark text-2">Nombre</label>
    <input type="text" id="first_14" name="q14_nombreCompleto[first]" class="form-control" size="10" value="" data-component="first" aria-labelledby="label_14 sublabel_14_first" required="" placeholder="Escribe aqui tu nombre" data-msg-required="Por favor ingresa tu nombre." required/>
  </div>
</div>

<div class="form-row"> 
  <div class="form-group col">
    <label class="required font-weight-bold text-dark text-2">Email</label>
    <input type="email" id="input_5" name="q5_correoElectronico" class="form-control" size="30" value="" data-component="email" aria-labelledby="label_5" required="" placeholder="Escribe aqui tu correo electrónico" data-msg-required="Por favor ingresa tu correo electrónico." required/>
  </div>
</div>

<div class="form-row"> 
  <div class="form-group col">
    <label class="required font-weight-bold text-dark text-2">WhatsApp</label>
    <input type="text" id="input_13" name="q13_whatsapp" data-type="input-textbox" class="form-control" size="20" data-masked="true" value="" data-component="textbox" aria-labelledby="label_13" required="" placeholder="Escribe aqui Tu WhatsApp" data-msg-required="Por favor ingresa tu numero de whatsapp." minlength="10" maxlength="15" required/>
  </div>
</div>

<div class="form-row"> 
  <div class="form-group col">
   <label class="required font-weight-bold text-dark text-2">Solicitud</label>
   <select class="form-control validate[required]" id="input_26" name="q26_solicitud" data-component="dropdown" required="" aria-labelledby="label_10">
    <option value=""> Selecciona Tu Solicitud </option>
    <option value="Cita de Servicio"> Cita de Servicio </option>
    <option value="Cotizar Refacciones"> Cotizar Refacciones </option>
  </select>
</div>
</div>

<div class="form-row"> 
  <div class="form-group col">
      <span class="input-group-addon2"><i class="fa fa-car"> </i></span>
      <input type="text" id="input_23" name="q23_kilometraje" class="form-control validate[required]" size="15" value="" data-component="last" aria-labelledby="label_14 sublabel_14_last" required="" placeholder="Escribe aqui el Kilometraje" required/>
    </div>
  </div>


<div class="form-row"> 
  <div class="form-group col">
      <span class="input-group-addon2"><i class="fa fa-car"> </i></span>
      <input type="text" id="input_24" name="q24_ano" class="form-control validate[required]" size="15" value="" data-component="last" aria-labelledby="label_14 sublabel_14_last" required="" placeholder="Escribe aqui el año del modelo" required/>
    </div>
  </div>
 

<div class="form-row"> 
  <div class="form-group col">
    <label class="font-weight-bold text-dark text-2">Mensaje</label>
    <textarea id="input_8" class="form-control" name="q8_comentarios" cols="40" rows="2" data-component="textarea" aria-labelledby="label_8" placeholder="Escribe aqui tus comentarios"></textarea>
  </div>
</div>