

<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Marcas</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
						<li class="active">Marcas</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col">
				 
				<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>grupo/honda">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/honda.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Honda<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/chevrolet.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Chevrolet<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/toyota.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Toyota<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/kia.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">KIA <em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/audi.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Audi<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/bmw.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">BMW<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/mini.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">MINI<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/motorrad.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Motorrad<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/gmc.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">GMC<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/buick.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Buick<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/cadillac.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Cadillac<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/chrysler.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Chrysler<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/dodge.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Dodge<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/jeep.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Jeep<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/ram.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Ram<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/isuzu.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Isuzu<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/mitsubishi.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Mitsubishi<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/fiat.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Fiat<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/nissan.jpg">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Nissan<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/volkswagen.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Volkswagen<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/daswelt.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Daswelt<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="portfolio-single-wide-slider.html">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/marcas/cuadro/seminuevos.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Seminuevos<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					 
				</div>

			</div>
		</div>

		 
	</div>
</div>
</div>

