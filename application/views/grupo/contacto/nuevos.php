<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Cotizador</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
						<li class="active">Cotizador</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

</div>

<div class="container py-2">
	<div class="row">
		<div class="col-lg-6">
			<div class="featured-box featured-box-primary">
				<div class="box-content p-5">
					<div class="row">
						<div class="col">

							<div class="row">
								<div class="col-12" align="center"> 
									<h2 class="font-weight-normal text-6">Simula tu crédito</h2>
									<p>Selecciona qué auto quieres.</p>
								</div>
							</div>

							<form action="/" id="frmBillingAddress" method="post">
								<div class="form-row">
									<div class="form-group col">
										<select class="form-control">
											<option value="">Selecciona Modelo</option>
											<option value="">Modelo 1</option>
										</select>
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col">
										<select class="form-control">
											<option value="">Selecciona Marca</option>
											<option value="Honda">Honda</option>
											<option value="Chevrolet">Chevrolet</option>
										</select>
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Valor del Auto</label>
										<input type="text" value="" class="form-control">
									</div>
									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Enganche</label>
										<input type="text" value="" class="form-control">
									</div>
									<div class="form-group col-lg-6">

									</div>
									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">
											<span class="amount"><h6 class="text-primary">Enganche sugerido:</h6></span>
											<span class="amount">$431</span>
										</label>

									</div>
								</div>

								<div class="form-row">

									<div class="form-group col-lg-12">

										<div class="row">
											<div class="col-12" align="center"> 
												<h2 class="font-weight-normal text-6">Escoge tus pagos mensuales</h2>

											</div>
										</div>

										<table class="cart-totals">
											<tbody>
												<tr class="cart-subtotal">
													<td>
														<input type="submit" value="$<?php echo 431 - 20 ; ?> - 60 Meses" class="btn btn-xl btn-light pr-4 pl-4 text-2 font-weight-semibold text-uppercase float-right mb-2" data-loading-text="Loading...">
													</td>
													<td>
														<input type="submit" value="$<?php echo 431 - 20 ; ?> - 48 Meses" class="btn btn-xl btn-light pr-4 pl-4 text-2 font-weight-semibold text-uppercase float-right mb-2" data-loading-text="Loading...">
													</td>
													<td>
														<input type="submit" value="$<?php echo 431 - 20 ; ?> - 36 Meses" class="btn btn-xl btn-light pr-4 pl-4 text-2 font-weight-semibold text-uppercase float-right mb-2" data-loading-text="Loading...">
													</td>
												</tr>
												<tr class="cart-subtotal">
													<td>
														<input type="submit" value="$<?php echo 431 - 20 ; ?> - 24 Meses" class="btn btn-xl btn-light pr-4 pl-4 text-2 font-weight-semibold text-uppercase float-right mb-2" data-loading-text="Loading...">
													</td>
													<td>
														<input type="submit" value="$<?php echo 431 - 20 ; ?> - 18 Meses" class="btn btn-xl btn-light pr-4 pl-4 text-2 font-weight-semibold text-uppercase float-right mb-2" data-loading-text="Loading...">
													</td>
													<td>
														<input type="submit" value="$<?php echo 431 - 20 ; ?> - 12 Meses" class="btn btn-xl btn-light pr-4 pl-4 text-2 font-weight-semibold text-uppercase float-right mb-2" data-loading-text="Loading...">
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</form>




						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="featured-box featured-box-primary">
				<div class="box-content p-5">
					<div class="row">
						<div class="col">

							<div class="row">
								<div class="col-12" align="center"> 
									<h2 class="font-weight-normal text-6">¿Te interesa este plan?</h2>
									<p>Pago mensual a XX meses</p>
								</div>
							</div>

							<div class="row">
								<div class="col-12" align="center"> 
									<h4 class="text-primary text-9">$ 50,000.00</h4>
									<div class="divider divider-small divider-small-center">
									<hr>
								</div>
								</div>
							</div>

							<hr>
								<div class="row" align="center">
									<div class="col-12" align="center"> 
										<input type="submit" value="Quiero mas información" class="btn btn-xl btn-light text-2 font-weight-semibold text-uppercase float mb-2" data-loading-text="Loading...">
									</div>
								</div>
							<hr>

							<div class="row">
											<div class="col pb-3">

												<table class="table">

													<tbody>
														<tr>
															<td>
																Monto a Financiar
															</td>

															<td>
																$45,254.65
															</td>
														</tr>
														<tr>
															<td>
																Valor del Auto
															</td>

															<td>
																$45,254.65
															</td>
														</tr>
														<tr>
															<td>
																Enganche
															</td>

															<td>
																$45,254.65
															</td>
														</tr>
														<tr>
															<td>
																Seguro de daños
															</td>

															<td>
																$45,254.65
															</td>
														</tr>
														<tr>
															<td>
																Seguro de vida
															</td>

															<td>
																$45,254.65
															</td>
														</tr>
													</tbody>
												</table>

												<hr class="solid my-5">

											</div>
										</div>




						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>