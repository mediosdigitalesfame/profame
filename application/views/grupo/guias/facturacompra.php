<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Guias Rapidas</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
						<li class="active">Guias</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

</div>

<div class="container py-2">
	<h3 align="center">PARA LA TOMA DE UN USADO DE PERSONA FISICA O MORAL QUE FACTURE 12%UB.</h3>
	<div class="row">
		<div class="col-lg-6">
			<div class="featured-box featured-box-primary">
				<div class="box-content p-5">
					<div class="row">
						<div class="col">

							<form action="/" id="frmBillingAddress" method="post">
								
								<div class="form-row">
									<div class="form-group col">
										<select class="form-control validate[required]" id="marca" name="marca" data-component="dropdown" required="" aria-labelledby="label_10">
											
											<option value="Honda">Honda</option>
											<option value="Chevrolet">Chevrolet</option>
										</select>
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col">
										<select class="form-control validate[required]" id="version" name="version" data-component="dropdown" required="" aria-labelledby="label_10">
											<option value="">version 1</option>
										</select>
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col">
										<select class="form-control validate[required]" id="modelo" name="modelo" data-component="dropdown" required="" aria-labelledby="label_10">
											<option value="">Año Modelo 1</option>
										</select>
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Kilometraje</label>
									</div>
									<div class="form-group col-lg-6">
										<input id="valordelauto" type="text" value="" class="form-control" >
									</div>
								</div>

								<div class="row">
									<div class="col-12" align="center"> 
										<h2 class="font-weight-normal text-6">Datos Guia Autometrica</h2>
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Compra  </label>
										<input id="compra" type="text" value="" class="form-control" >
									</div>

									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Venta </label>
										<input id="venta" type="text" value="" class="form-control" >
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Añadir x Equipamiento </label>
										<input id="anadir" type="text" value="" class="form-control" >
									</div>

									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Premio o Castigo x Km </label>
										<input id="premio" type="text" value="" class="form-control" >
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Reacondicionamiento</label>
										<input id="reaco" type="text" value="" class="form-control" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="top" data-content="NOTA: DE ACUERDO A AVALUO MECÁNICO (PROVISIÓN A GASTO, CAPTURAR VALORES NEGATIVOS)">
									</div>

									<div class="form-group col-lg-6">
										<label class="font-weight-bold text-dark text-2">Centrese en Valor de Coche </label>
										<input id="valor" type="text" value="" class="form-control" onblur="preciosinreacondicionamiento()" >
									</div>
								</div>

								<br>

								<div class="form-row">
									<div class="form-group col-lg-12">

										<div class="row">
											<div class="col pb-3">
												<table align="center" border="0" cellspacing="4" cellpadding="5">
													<tbody>
														<tr>
															<td colspan="4" rowspan="" align="left">
																IVA
															</td>

															<td align="right">
																<span id="iva" value="16" >16%</span>
															</td>
														</tr>
														<tr>
															<td colspan="4" rowspan="" align="left">
																Factor de Utilidad a la Venta
															</td>
															<td align="right">
																<span id="factorutilidad" value="12" >12%</span>
															</td>
														</tr>
														<tr>
															<td colspan="4" rowspan="" align="left">
																Factor de Ganancia Sobre el Costo
															</td>
															<td align="right">
																<span id="factorganancia" value="13.636" >13.64%</span>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="featured-box featured-box-primary">
				<div class="box-content p-5">
					<div class="row">
						<div class="col">

							<label class="font-weight-bold text-dark text-2">
								<span class="amount"><h6 class="text-primary">Precio Minimo de la Venta:</h6></span>
								<h3 class="text-primary text-12">
									<span class="preciominimodeventa" id="preciominimodeventa">$0</span>
								</h3>
							</label>

							<hr>

							<div class="row">
								<div class="col pb-3">

									<label class="font-weight-bold text-dark text-2">
										<span class="amount"><h6 class="text-primary">Precio de la toma tope final al cliente con iva </h6></span>
										<h3 class="text-primary text-12">
											<span id="preciodetoma">$0</span>
										</h3>
										<span class="amount"><h6 class="text-primary">Costo de Adquisición del Vehículo con IVA </h6></span>
									</label>
									<hr class="solid my-5">

									<table align="center" border="0" cellspacing="4" cellpadding="5">
										<tbody>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Costo Real(Precio de la toma sin IVA)
												</td>

												<td align="right">
													<span id="costoreal" value="" >$0</span>
												</td>
											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Precio de Venta sin IVA
												</td>
												<td align="right">
													<span id="precioventasiniva" value="" >$0</span>
												</td>
											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Utilidad
												</td>
												<td align="right">
													<span id="utilidad" value="" >$0</span>
												</td>
											</tr>
											<tr>
												<td colspan="4" rowspan="" align="left">
													Porcentaje de la Utilidad
												</td>
												<td align="right">
													<span id="porcentajeutilidad" value="" >0%</span>
												</td>
											</tr>
										</tbody>
									</table>

								</div>
							</div>

							<hr>
							<div class="row">
								<div class="col-12" align="center"> 
									<span class="amount"><h6 class="text-primary">Precio de Venta al Publico con IVA y Reacondicionamiento</h6></span>
									<h4 class="text-primary text-9">
										<p id="precioalpublico" style="color: green;">$0</p>
									</h4>
									<div class="divider divider-small divider-small-center">
										<hr>  
									</div>
								</div>
							</div>
							<hr>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script language="javascript">

	const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'})

	var valordelauto = document.getElementById("valordelauto").value;
	var reacondicionamiento = document.getElementById("reacondicionamiento").value;
	var enganche = document.getElementById("enganche").value;
	var marca = document.getElementById("mySelectMarca").value;
	var tasa = document.getElementById("nuevatasa").value;
	var mes = document.getElementById("meseselegidos").value;

	function preciosinreacondicionamiento(){
		valordelauto = document.getElementById("valor").value;
		reacondicionamiento = document.getElementById("reaco").value;

		var preciosinreacondicionamiento = valordelauto-reacondicionamiento
		var costodeadquisicion = preciosinreacondicionamiento*(1-(12/100))
		var costoreal1 = costodeadquisicion/(1+(16/100))
		var preciodeventasiniva = preciosinreacondicionamiento/(1+(16/100))
		var lautilidad = preciodeventasiniva-costoreal1
		var porcentajedeutilidad = (lautilidad/preciodeventasiniva)*100 
		var precioalpublicocontodo =  preciodeventasiniva*(1+(16/100))-reacondicionamiento

		document.getElementById('preciominimodeventa').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(preciosinreacondicionamiento)+"</span>";	
		document.getElementById('preciodetoma').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(costodeadquisicion)+"</span>";
		document.getElementById('costoreal').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(costoreal1)+"</span>";
		document.getElementById('precioventasiniva').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(preciodeventasiniva)+"</span>";
		document.getElementById('utilidad').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(lautilidad)+"</span>";
		document.getElementById('porcentajeutilidad').innerHTML="<span style='color: green;'>"+porcentajedeutilidad.toFixed(2)+'%'+"</span>";
		document.getElementById('precioalpublico').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(precioalpublicocontodo)+"</span>";
	} 

</script>



