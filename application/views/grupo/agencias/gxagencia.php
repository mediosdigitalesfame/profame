<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border><?php echo $nombre_marca; ?> <?php echo $nombre_agencia; ?></h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>">Inicio</a></li>
						<li class=""><a href="<?php echo base_url(); ?>grupo/<?php echo $nombre_grupo; ?>"><?php echo $nombre_grupo; ?></a></li>
						<li class=""><a href="<?php echo base_url(); ?>grupo/<?php echo $nombre_agencias; ?>"><?php echo $nombre_marca; ?></a></li>
						<li class="active"><?php echo $nombre_agencia; ?></li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<section class="call-to-action featured featured-primary mb-5">
			<div class="col-lg-2">
				<img src="<?php echo base_url(); ?>assets/porto/img/marcas/<?php echo $vista_logo; ?>.png" class="img-fluid">
			</div> 
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="call-to-action-content">
					<h3>  <strong class="font-weight-extra-bold"><?php echo $nombre_marca; ?> <?php echo $nombre_agencia; ?></strong> </h3>
					<p class="mb-0">Piensa en <strong class="font-weight-extra-bold">Auto</strong> Piensa en<strong class="font-weight-extra-bold">FAME</strong></p>
				</div>
			</div>
			<div class="col-lg-4"></div>
			<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
				<div class="call-to-action-btn">
					<a href="<?php echo $nombre_pagina; ?>" target="_blank" class="btn btn-modern text-2 btn-primary">Ir al Sitio</a>
				</div>
			</div>
		</section>

		<div class="row py-4">
			<div class="col-lg-6">

				<div class="overflow-hidden mb-1">
					<h2 class="font-weight-normal text-7 mt-2 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">
						<strong class="font-weight-extra-bold">Te</strong> Contactamos</h2>
					</div>
					<div class="overflow-hidden mb-4 pb-3">
						<p class="mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="400">En un instante uno de nuestros asesores te atendera!</p>
					</div>

					<form id="contactForm" class="contact-form" action="php/contact-form.php" method="POST">
						<div class="contact-form-success alert alert-success d-none mt-4" id="contactSuccess">
							<strong>Success!</strong> EL mensaje fue enviado.
						</div>

						<div class="contact-form-error alert alert-danger d-none mt-4" id="contactError">
							<strong>Error!</strong> Error al enviar el mensaje.
							<span class="mail-error-message text-1 d-block" id="mailErrorMessage"></span>
						</div>

						<div class="form-row">
							<div class="form-group col-lg-6">
								<label class="required font-weight-bold text-dark text-2">Nombre</label>
								<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
							</div>
							<div class="form-group col-lg-6">
								<label class="required font-weight-bold text-dark text-2">Email</label>
								<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<label class="font-weight-bold text-dark text-2">Asunto</label>
								<input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<label class="required font-weight-bold text-dark text-2">Mensaje</label>
								<textarea maxlength="5000" data-msg-required="Please enter your message." rows="8" class="form-control" name="message" id="message" required></textarea>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<input type="submit" value="Enviar" class="btn btn-primary btn-modern" data-loading-text="Loading...">
							</div>
						</div>
					</form>

				</div>
				<div class="col-lg-6">
					<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
						<h4 class="mt-2 mb-1">Nuestra <strong>Ubicacion</strong></h4>
						<ul class="list list-icons list-icons-style-2 mt-2">
							<li><i class="fas fa-map-marker-alt top-6"></i> <strong class="text-dark">Calle:</strong> 1234 Street Name, City Name, United States</li>
							<li><i class="fas fa-phone top-6"></i> <strong class="text-dark">Telefono:</strong> (123) 456-789</li>
							<li><i class="fab fa-whatsapp top-6"></i> <strong class="text-dark">WhatsApp:</strong> (123) 456-789</li>
							<li><i class="fas fa-envelope top-6"></i> <strong class="text-dark">Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></li>
						</ul>
					</div>

					<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="950">
						<h4 class="pt-5">Nuestros <strong>Horarios</strong></h4>
						<ul class="list list-icons list-dark mt-2">
							<li><i class="far fa-clock top-6"></i> Lunes - Viernes - 9am a 5pm</li>
							<li><i class="far fa-clock top-6"></i> Sabado - 9am a 2pm</li>
							<li><i class="far fa-clock top-6"></i> Domingo - Closed</li>
						</ul>
					</div>

					<h4 class="pt-5">Mantente en <strong>Contacto</strong></h4>
					<p class="lead mb-0 text-4">Proporciona tus datos para recibir la informacion que necesitas..</p>

				</div>
			</div>

			<div class="row">
				<div class="col">
					<iframe src="<?php echo $ubicacion_agencia; ?>" style="height: 480px;"  frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" class="google-map mt-0 mb-0"></iframe>
				</div>
			</div>

		</div>

	</div>

