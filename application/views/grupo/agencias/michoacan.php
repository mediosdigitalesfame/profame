<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Michoacan</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>">Inicio</a></li>
						<li class=""><a href="<?php echo base_url(); ?>grupo/agencias">Agencias</a></li>
						<li class="active">Michoacán</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row justify-content-center">
			
			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Monarca Morelia</strong></h2>
							 
							<a href="<?php echo base_url(); ?>grupo/hondamonarcamorelia" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Altozano Morelia</strong></h2>
							 
							<a href="<?php echo base_url(); ?>grupo/hondaaltozano" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Manantiales Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Uruapan</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/toyota.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Valladolid Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/toyota.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Altozano Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/toyota.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Uruapan</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/chevrolet.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/chevrolet.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Apatzingan</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/chevrolet.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Uruapan</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/chevrolet.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Zamora</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/chevrolet.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Lázaro Cárdenas</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/isuzu.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/motorrad.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/bmw.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/mini.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/volkswagen.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/kia.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Mil Cumbres Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/nissan.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Acuedcuto Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/nissan.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Altozano Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/gmc.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Camelinas Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/buick.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Camelinas Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/porto/img/marcas/cadillac.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Camelinas Morelia</strong></h2>
							 
							<a href="/" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			 
			 
			 

			 


		</div>

	</div>
</div>

