

<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Agencias</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
						<li class="active">Agencias</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col" align="center">
				 
				<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>home/michoacan">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/agencias/michoacan.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Michocan<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>home/agencias/queretaro">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/agencias/queretaro.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Querétaro<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>home/agencias/cdmx">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/porto/img/agencias/cdmx.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">CDMX<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					 
  

					 
				</div>

			</div>
		</div>

		 
	</div>
</div>
</div>

