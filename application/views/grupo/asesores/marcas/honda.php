<?php $this->load->view('grupo/asesores/encabezadoagencias.php'); ?>

	<div class="container">
		<div class="row justify-content-center">
			
			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Monarca</strong></h2>
							<a href="asesoreshondamonarcamorelia" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Corregidora</strong></h2>
							<a href="asesoreshondacorregidora" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Altozano</strong></h2>
							<a href="asesoreshondaaltozano" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Atizapan</strong></h2>
							<a href="asesoreshondaatizapan" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Manantiales</strong></h2>
							<a href="asesoreshondamanantiales" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Marquesa</strong></h2>
							<a href="asesoreshondamarquesa" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Monarca DF</strong></h2>
							<a href="asesoreshondamonarcadf" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">San Juan del Río</strong></h2>
							<a href="asesoreshondasanjuan" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/honda.png" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Uruapan</strong></h2>
							<a href="asesoreshondauruapan" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>
			 			
		</div>
	</div>
</div>

