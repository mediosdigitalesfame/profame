<?php $this->load->view('grupo/asesores/encabezadoagencias.php'); ?>

	<div class="container">
		<div class="row justify-content-center">
			
			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Querétaro</strong></h2>
							<a href="asesoresmitsubishiqueretaro" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
				<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
					<div class="featured-box">
						<div class="box-content px-lg-4 px-xl-5 py-lg-5">
							<div>
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/marcas/grupo.jpg" alt="">
							</div>
							<h2 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Uruapan</strong></h2>
							<a href="asesoresmitsubishiuruapan" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 mt-3">VER MAS</a>
						</div>
					</div>
				</div>
			</div>

			 
			 
			
		</div>
	</div>
</div>

