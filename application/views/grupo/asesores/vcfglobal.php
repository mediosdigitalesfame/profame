	<?php

	include("vcardexp.inc.php");
	
	$test = new vcardexp;
	
	$test->setValue("language", "");
	$test->setValue("title", $nombre_puesto);
	$test->setValue("firstName",  $nombre_asesor);
	$test->setValue("lastName",  "");
	$test->setValue("organisation", $nombre_agencia);
	$test->setValue("tel_work", $asesor_cel);
	$test->setValue("tel_home", $asesor_cel);
	$test->setValue("tel_pref", $asesor_wats);
	$test->setValue("url", $nombre_pagina);
	$test->setValue("email_internet", $asesor_mail);
	$test->setValue("email_pref", $asesor_mail);
	$test->setValue("street_home", "");
	$test->setValue("postal_home", "");
	$test->setValue("city_home", "");
	$test->setValue("country_home", "");
	$test->copyPicture("avatar.jpg");
	
	$test->getCard();

?>

