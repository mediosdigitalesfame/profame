<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="jorgealbertorodriguezandrade"><li><i class="fas fa-user"></i>Jorge Alberto Rodriguez Andrade</li></a>
						<a href="lizethayala"><li><i class="fas fa-user"></i>Lizeth Ayala</li></a>
						<a href="karlapaulinarodriguezmendez"><li><i class="fas fa-user"></i>Karla Paulina Rodriguez Mendez</li></a>
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="mariatrinidadpereztorres"><li><i class="fas fa-user"></i>Maria Trinidad  Perez Torres</li></a>
						<a href="elisemishellepimentelalfaro"><li><i class="fas fa-user"></i>Elise Mishelle Pimentel Alfaro</li></a>
						<a href="cristianricardotapiaduarte"><li><i class="fas fa-user"></i>Cristian Ricardo Tapia Duarte</li></a>
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="josedejesusmartinez"><li><i class="fas fa-user"></i>Jose De Jesus Martinez</li></a>
						<a href="carlosalbertosalazarduran"><li><i class="fas fa-user"></i>Carlos Alberto Salazar Duran</li></a>
						<a href="aaronalejandroacevedotamayo"><li><i class="fas fa-user"></i>Aaron Alejandro Acevedo Tamayo</li></a>
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="andreamayaneligomezherrera"><li><i class="fas fa-user"></i>Andrea Mayaneli Gomez Herrera</li></a>
						<a href="armandoramirez"><li><i class="fas fa-user"></i>Armando Ramirez</li></a>
						<a href="brauliosanchezolivarez"><li><i class="fas fa-user"></i>Braulio Sánchez Olivarez</li></a>
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>