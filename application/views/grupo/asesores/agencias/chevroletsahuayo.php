<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="josealbertocarrilloespinoza"><li><i class="fas fa-user"></i>Jose Alberto Carrillo Espinoza</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="patriciadiazvargas"><li><i class="fas fa-user"></i>Patricia Diaz Vargas</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="gustavocastanedabecerra"><li><i class="fas fa-user"></i>Gustavo Castañeda Becerra</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="franciscoponceramirez"><li><i class="fas fa-user"></i>Francisco Ponce Ramirezz</li></a>
						 
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>