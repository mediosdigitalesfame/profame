<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="mauriciojaramillomendez"><li><i class="fas fa-user"></i>Mauricio Jaramillo Méndez</li></a>
						<a href="ruthesperanzafloresportillo"><li><i class="fas fa-user"></i>Ruth Esperanza Flores Portillo</li></a>
						<a href="juanmanuelpizanodelatorre"><li><i class="fas fa-user"></i>Juan Manuel Pizano De La Torre</li></a>
						<a href="israelsarabia"><li><i class="fas fa-user"></i>Israel Sarabia</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="guadaluperivas"><li><i class="fas fa-user"></i>Guadalupe Rivas</li></a>
						<a href="beatrizgarcia"><li><i class="fas fa-user"></i>Beatriz Garcia</li></a>
						<a href="karinanavarro"><li><i class="fas fa-user"></i>Karina Navarro</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="joseluissoriano"><li><i class="fas fa-user"></i>Jose Luis Soriano</li></a>
						<a href="lilianavalenciavillegas"><li><i class="fas fa-user"></i>Liliana Valencia Villegas</li></a>
						<a href="maeugeniaorozco"><li><i class="fas fa-user"></i>Ma Eugenia Orozco</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="joseluisnavarrogutierrez"><li><i class="fas fa-user"></i>Jose Luis Navarro Gutierrez</li></a>
						<a href="mariaguadalupecarbajalhernandez"><li><i class="fas fa-user"></i>Maria Guadalupe Carbajal Hernandez</li></a>
						<a href="olivergomez"><li><i class="fas fa-user"></i>Oliver Gomez</li></a>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>
