<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="antoniotapia"><li><i class="fas fa-user"></i>Antonio Tapia</li></a>
						<a href="gustavoenriquehernandez"><li><i class="fas fa-user"></i>Gustavo Enrique Hernandez</li></a>
						<a href="victormanuelsalas"><li><i class="fas fa-user"></i>Victor Manuel Salas</li></a>
						<a href="salvadorolvera"><li><i class="fas fa-user"></i>Salvador Olvera</li></a>
						<a href="ricardoestrada"><li><i class="fas fa-user"></i>Ricardo Estrada</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="jesusromanmejiarubio"><li><i class="fas fa-user"></i>Jesus Roman Mejia Rubio</li></a>
						<a href="juliocesarcontrerasmaldonado"><li><i class="fas fa-user"></i>Julio Cesar Contreras Maldonado</li></a>
						<a href="carmenconcepcionsotoroldan"><li><i class="fas fa-user"></i>Carmen Concepcion Soto Roldan</li></a>
						<a href="omarnunezuribe"><li><i class="fas fa-user"></i>Omar Nuñez Uribe</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="liliagutierrez"><li><i class="fas fa-user"></i>Lilia Gutierrez</li></a>
						<a href="franciscoavera"><li><i class="fas fa-user"></i>Francisco A. Vera</li></a>
						<a href="jesuscerda"><li><i class="fas fa-user"></i>Jesus Cerda</li></a>
						<a href="javiermartinezhernandez"><li><i class="fas fa-user"></i>Javier Martinez Hernandez</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="victorhugoestrada"><li><i class="fas fa-user"></i>Victor Hugo Estrada</li></a>
						<a href="guillermogonzalez"><li><i class="fas fa-user"></i>Guillermo Gonzalez</li></a>
						<a href="javiersandoval"><li><i class="fas fa-user"></i>Javier Sandoval</li></a>
						<a href="arturojimenez"><li><i class="fas fa-user"></i>Arturo Jimenez</li></a>
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>