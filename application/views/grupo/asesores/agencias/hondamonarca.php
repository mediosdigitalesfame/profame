<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="cesarcardenas"><li><i class="fas fa-user"></i>Cesar Cardenas</li></a>
						<a href="franciscovazquez"><li><i class="fas fa-user"></i>Francisco Vazquez</li></a>
						<a href="danielaponte"><li><i class="fas fa-user"></i>Daniel Aponte</li></a>
						<a href="jorgeeduardogarcia"><li><i class="fas fa-user"></i>Jorge Eduardo Garcia</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="albertovelasco"><li><i class="fas fa-user"></i>Alberto Velasco</li></a>
						<a href="arturofranco"><li><i class="fas fa-user"></i>Arturo Franco</li></a>
						<a href="luisgonzalocruz"><li><i class="fas fa-user"></i>Luis Gonzalo Cruz</li></a>
						<a href="marcoantoniogutierrez"><li><i class="fas fa-user"></i>Marco Antonio Gutierrez</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="janetmclane"><li><i class="fas fa-user"></i>Janet Mc Lane</li></a>
						<a href="silviaavalos"><li><i class="fas fa-user"></i>Silvia Avalos</li></a>
						<a href="anagabrielaherrejon"><li><i class="fas fa-user"></i>Ana Gabriela Herrejon</li></a>
						<a href="humbertocortes"><li><i class="fas fa-user"></i>Humberto Cortes</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="victorhugocendejas"><li><i class="fas fa-user"></i>Victor Hugo Cendejas</li></a>
						<a href="alejandraperez"><li><i class="fas fa-user"></i>Alejandra Perez</li></a>
						<a href="reynaldohuitron"><li><i class="fas fa-user"></i>Reynaldo Huitron</li></a>
						<a href="erikamagana"><li><i class="fas fa-user"></i>Erika Magaña</li></a>
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>