<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="veronicagalindo"><li><i class="fas fa-user"></i>Veronica Galindo</li></a>
						<a href="gabrielageraldinzamoratorres"><li><i class="fas fa-user"></i>Gabriela Geraldin Zamora Torres</li></a>
						<a href="fatimairaisrodriguezcastillomitsu"><li><i class="fas fa-user"></i>Fatima Irais Rodriguez Castillo</li></a>
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="josejesusramirezchavez"><li><i class="fas fa-user"></i>Jose Jesus Ramirez Chavez</li></a>
						<a href="anaaliciaalcalaromero"><li><i class="fas fa-user"></i>Ana Alicia Alcala Romero</li></a>
						<a href="cristiandavidrico"><li><i class="fas fa-user"></i>Cristian David Rico</li></a>
					 						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="braulioramiroromeroramirez"><li><i class="fas fa-user"></i>Braulio Ramiro Romero Ramirez</li></a>
						<a href="luisgerardotapiasagrero"><li><i class="fas fa-user"></i>Luis Gerardo Tapia Sagrero</li></a>
						 						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="lizbethguadalupeyoseffponce"><li><i class="fas fa-user"></i>Lizbeth Guadalupe Yoseff Ponce</li></a>
						<a href="wendyavila"><li><i class="fas fa-user"></i>Wendy Avila</li></a>
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>