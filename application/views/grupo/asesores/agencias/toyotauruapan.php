<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="anapaolafernandezgarcia"><li><i class="fas fa-user"></i>Ana Paola Fernandez Garcia</li></a>
						<a href="juancarloscortessaldana"><li><i class="fas fa-user"></i>Juan Carlos Cortes Saldaña</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="josejesusmadrigaldominguez"><li><i class="fas fa-user"></i>Jose Jesus Madrigal Dominguez</li></a>
						<a href="juanmartinreyesortega"><li><i class="fas fa-user"></i>Juan Martin Reyes Ortega</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="juanramonhernandezarellano"><li><i class="fas fa-user"></i>Juan Ramon Hernandez Arellano</li></a>
						<a href="julioagustinalvarezzavala"><li><i class="fas fa-user"></i>Julio Agustin Alvarez Zavala</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="fernandadiazgrajeda"><li><i class="fas fa-user"></i>Fernanda Diaz Grajeda</li></a>
						<a href="francisconoeramirezramirez"><li><i class="fas fa-user"></i>Francisco Noe Ramirez Ramirez</li></a> 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>