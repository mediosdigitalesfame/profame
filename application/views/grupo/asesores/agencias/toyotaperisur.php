<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="pamelaortega"><li><i class="fas fa-user"></i>Pamela Ortega</li></a>
						<a href="leydavelazquez"><li><i class="fas fa-user"></i>Leyda Velazquez</li></a>
						<a href="vicentehernandez"><li><i class="fas fa-user"></i>Vicente Hernandez</li></a>
						<a href="benjaminrodriguez"><li><i class="fas fa-user"></i>Benjamin Rodriguez</li></a>
						<a href="arturoloza"><li><i class="fas fa-user"></i>Arturo Loza</li></a>
						<a href="blancazermeno"><li><i class="fas fa-user"></i>Blanca Zermeño</li></a>
						<a href="hectorortiz"><li><i class="fas fa-user"></i>Hector Ortiz</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="antoniodominguez"><li><i class="fas fa-user"></i>Antonio Dominguez</li></a>
						<a href="adanortega"><li><i class="fas fa-user"></i>Adan Ortega</li></a>
						<a href="robertosanchez"><li><i class="fas fa-user"></i>Roberto Sanchez</li></a>
						<a href="aurorasalcedo"><li><i class="fas fa-user"></i>Aurora Salcedo</li></a>
						<a href="edgarolmedo"><li><i class="fas fa-user"></i>Edgar Olmedo</li></a>
						<a href="yesicasegovia"><li><i class="fas fa-user"></i>Yesica Segovia</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="gerardocordero"><li><i class="fas fa-user"></i>Gerardo Cordero</li></a>
						<a href="eduardovictoria"><li><i class="fas fa-user"></i>Eduardo Victoria</li></a>
						<a href="ileanagutierrez"><li><i class="fas fa-user"></i>Ileana Gutierrez</li></a>
						<a href="isaacmayoral"><li><i class="fas fa-user"></i>Isaac Mayoral</li></a>
						<a href="froylanruiz"><li><i class="fas fa-user"></i>Froylan Ruiz</li></a>
						<a href="marcoduran"><li><i class="fas fa-user"></i>Marco Duran</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="genaroneri"><li><i class="fas fa-user"></i>Genaro Neri</li></a>
						<a href="erikavazquezrosas"><li><i class="fas fa-user"></i>Erika Vazquez Rosas</li></a>
						<a href="armandogonzalez"><li><i class="fas fa-user"></i>Armando Gonzalez</li></a>
						<a href="rodolforesendiz"><li><i class="fas fa-user"></i>Rodolfo Resendiz</li></a>
						<a href="christianalcantar"><li><i class="fas fa-user"></i>Christian Alcantar</li></a>
						<a href="albertohernandez"><li><i class="fas fa-user"></i>Alberto Hernandez</li></a>
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>