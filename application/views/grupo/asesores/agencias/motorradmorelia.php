<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="eduardoramirezcanals"><li><i class="fas fa-user"></i>Eduardo Ramirez Canals</li></a>					
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="obedalvarez"><li><i class="fas fa-user"></i>Obed Alvarez</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="carlosalvarez"><li><i class="fas fa-user"></i>Carlos Alvarez</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="eldalarissamenasorino"><li><i class="fas fa-user"></i>Elda Larissa Mena Sorino</li></a>
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>