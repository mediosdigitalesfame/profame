<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="joseluislozanodiaz"><li><i class="fas fa-user"></i>José Luis Lozano Díaz</li></a>
						<a href="silviahernandezfuentes"><li><i class="fas fa-user"></i>Silvia Hernández Fuentes</li></a>
						<a href="gilbertoarteagaolivares"><li><i class="fas fa-user"></i>Gilberto Arteaga Olivares</li></a>
						<a href="juliocesargomezcalderon"><li><i class="fas fa-user"></i>Julio Cesar Gomez Calderón</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="luisrigobertodiazduenas"><li><i class="fas fa-user"></i>Luis Rigoberto Díaz Dueñas</li></a>
						<a href="noelantoniozavalatorres"><li><i class="fas fa-user"></i>Noel Antonio Zavala Torres</li></a>
						<a href="griseldarodriguezpineda"><li><i class="fas fa-user"></i>Griselda Rodriguez Pineda</li></a>
						<a href="teresitaesquivelmorales"><li><i class="fas fa-user"></i>Teresita Esquivel Morales</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="miguelangelmendozacervantes"><li><i class="fas fa-user"></i>Miguel Angel Mendoza Cervantes</li></a>
						<a href="ivancalderonarreola"><li><i class="fas fa-user"></i>Ivan Calderón Arreola</li></a>
						<a href="fatimapayanbustamante"><li><i class="fas fa-user"></i>Fatima Payan Bustamante</li></a>
						<a href="fernandotalaverahernandez"><li><i class="fas fa-user"></i>Fernando Talavera Hernández</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="adolfocristiansuarezsanchez"><li><i class="fas fa-user"></i>Adolfo Cristian Suárez Sánchez</li></a>
						<a href="israelhernandezgranados"><li><i class="fas fa-user"></i>Israel Hernández Granados</li></a>
						<a href="oscarjeovanydelgadogarcia"><li><i class="fas fa-user"></i>Oscar Jeovany Delgado Garcia</li></a>
						<a href="leticiahernandezlara"><li><i class="fas fa-user"></i>Leticia Hernández Lara</li></a>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>