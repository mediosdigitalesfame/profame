<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="mauriciomacuzetromero"><li><i class="fas fa-user"></i>Mauricio Macuzet Romero</li></a>
						<a href="sandracastillo"><li><i class="fas fa-user"></i>Sandra Castillo</li></a>
						<a href="luisromero"><li><i class="fas fa-user"></i>Luis Romero</li></a>
						<a href="eduardomiguelmonroymier"><li><i class="fas fa-user"></i>Eduardo Miguel Monroy Mier</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="lauraleticiacalderoncalderon"><li><i class="fas fa-user"></i>Laura Leticia Calderon Calderon</li></a>
						<a href="oscarjuliolopezvargas"><li><i class="fas fa-user"></i>Oscar Julio lopez Vargas</li></a>
						<a href="noerodriguezhuante"><li><i class="fas fa-user"></i>Noe Rodriguez Huante</li></a>
						<a href="rosaliaperezramos"><li><i class="fas fa-user"></i>Rosalia Perez Ramos</li></a> 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="cesaroctaviohernandezvilla"><li><i class="fas fa-user"></i>Cesar Octavio Hernández Villa</li></a>
						<a href="joseeduardoblancosanchez"><li><i class="fas fa-user"></i>José Eduardo Blanco Sánchez</li></a>
						<a href="patriciaperezmaldonado"><li><i class="fas fa-user"></i>Patricia Pérez Maldonado</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="cesaradrianayalavillasenor"><li><i class="fas fa-user"></i>César Adrián Ayala Villaseñor</li></a>
						<!--<a href="franciscolelodelarreaosegera"><li><i class="fas fa-user"></i>Francisco Lelo De Larrea Osegera</li></a>-->
						<a href="claudiaramirez"><li><i class="fas fa-user"></i>Claudia Ramirez</li></a>
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>