<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="faustosegura"><li><i class="fas fa-user"></i>Fausto Segura</li></a>
						<a href="carolinaalcantara"><li><i class="fas fa-user"></i>Carolina Alcantara</li></a>
						<a href="connieorozco"><li><i class="fas fa-user"></i>Connie Orozco</li></a>
						<a href="jessicahernandez"><li><i class="fas fa-user"></i>Jessica Hernandez</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="victormartinez"><li><i class="fas fa-user"></i>Victor Martinez</li></a>
						<a href="marcorodriguez"><li><i class="fas fa-user"></i>Marco Rodriguez</li></a>
						<a href="felipefonseca"><li><i class="fas fa-user"></i>Felipe Fonseca</li></a>
						<a href="josejuanbalderas"><li><i class="fas fa-user"></i>Jose Juan Balderas</li></a> 
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="silviagarciaalonso"><li><i class="fas fa-user"></i>Silvia Garcia Alonso</li></a>
						<a href="yesicaobeso"><li><i class="fas fa-user"></i>Yesica Obeso</li></a>
						<a href="manuelmartinez"><li><i class="fas fa-user"></i>Manuel Martinez</li></a>
						
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="jorgealbertozavala"><li><i class="fas fa-user"></i>Jorge Alberto Zavala</li></a>
						<a href="eduardoalvizar"><li><i class="fas fa-user"></i>Eduardo Alvizar</li></a>
						<a href="taniamendoza"><li><i class="fas fa-user"></i>Tania Mendoza</li></a>
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>