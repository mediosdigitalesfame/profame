<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="vanessahernandez"><li><i class="fas fa-user"></i>Vanessa Hernandez</li></a>
						<a href="guadaluperios"><li><i class="fas fa-user"></i>Guadalupe Ríos</li></a>
						<a href="samantaresendiz"><li><i class="fas fa-user"></i>Samanta Resendiz</li></a>
						<a href="davidzarate"><li><i class="fas fa-user"></i>David Zarate</li></a>
						
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="victormanuelromerogastelum"><li><i class="fas fa-user"></i>Victor Manuel Romero Gastelum</li></a>
						<a href="judithpereapacheco"><li><i class="fas fa-user"></i>Judith Perea Pacheco</li></a>
						<a href="joaquinalvarezpertierra"><li><i class="fas fa-user"></i>Joaquín Álvarez Pertierra</li></a>
						<a href="karlaguerrero"><li><i class="fas fa-user"></i>Karla Guerrero</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="almasoledadosorniosanchez"><li><i class="fas fa-user"></i>Alma Soledad Osornio Sánchez</li></a>
						<a href="luisbarron"><li><i class="fas fa-user"></i>Luis Barron</li></a>
						<a href="lidiahernandez"><li><i class="fas fa-user"></i>Lidia Hernandez</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="diegoadalbertozamudiotrejo"><li><i class="fas fa-user"></i>Diego Adalberto Zamudio Trejo</li></a>
						<a href="eliascarrillo"><li><i class="fas fa-user"></i>Elias Carrillo</li></a>
						<a href="christianguerrero"><li><i class="fas fa-user"></i>Christian Guerrero</li></a>
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>