<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="irmaguadalupevillanuevatorres"><li><i class="fas fa-user"></i>Irma Guadalupe Villanueva Torres</li></a>
						<a href="juliocesarcancinohernandez"><li><i class="fas fa-user"></i>Julio Cesar Cancino Hernández</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="luciaguadalupesotozavala"><li><i class="fas fa-user"></i>Lucia Guadalupe Soto Zavala</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="victorsoriano"><li><i class="fas fa-user"></i>Víctor Soriano</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="pedroaburtosuastegui"><li><i class="fas fa-user"></i>Pedro Aburto Suastegui</li></a>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>

