<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="albertomasso"><li><i class="fas fa-user"></i>Alberto Masso</li></a>
						<a href="anasilviaarce"><li><i class="fas fa-user"></i>Ana Silvia Arce</li></a>
						<a href="joseantoniorincondorantes"><li><i class="fas fa-user"></i>José Antonio Rincón  Dorantes</li></a>
						<a href="valentegomez"><li><i class="fas fa-user"></i>Valente Gomez</li></a>
						<a href="sahirakarinajimenezpalacios"><li><i class="fas fa-user"></i>Sahira Karina Jimenez Palacios</li></a>
						<a href="valentegomez"><li><i class="fas fa-user"></i>Valente Gomez</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="elsavalenzuela"><li><i class="fas fa-user"></i>Elsa Valenzuela</li></a>
						<a href="rodrigotoledolozada"><li><i class="fas fa-user"></i>Rodrigo Toledo Lozada</li></a>
						<a href="araceliugaldecervantes"><li><i class="fas fa-user"></i>Araceli Ugalde Cervantes</li></a>
						<a href="ricardorubencolchadomedellin"><li><i class="fas fa-user"></i>Ricardo Ruben Colchado Medellin</li></a>
						<a href="mariadelcarmenhernandezmonroy"><li><i class="fas fa-user"></i>Maria Del Carmen Hernández Monroy</li></a>
                        <a href="ricardorubencolchadomedellin"><li><i class="fas fa-user"></i>Ricardo Ruben Colchado Medellin</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="joserauljimenezalcantara"><li><i class="fas fa-user"></i>Jose Raúl Jiménez Alcántara</li></a>
						<a href="franciscofabianvazquezhernandez"><li><i class="fas fa-user"></i>Francisco Fabian Vazquez Hernández</li></a>
						<a href="luisjairdominguezpacheco"><li><i class="fas fa-user"></i>Luis Jair Domínguez Pacheco</li></a>
						<a href="luismontufar"><li><i class="fas fa-user"></i>Luis Montufar</li></a>
						<a href="araceliugaldecervantes"><li><i class="fas fa-user"></i>Araceli Ugalde Cervantes</li></a>
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="guadalupebejarbravo"><li><i class="fas fa-user"></i>Guadalupe Bejar Bravo</li></a>
						<a href="luceropamelaescalanteguerrero"><li><i class="fas fa-user"></i>Lucero Pamela Escalante Guerrero</li></a>
						<a href="albertoalvarezhurtado"><li><i class="fas fa-user"></i>Alberto Álvarez Hurtado</li></a>
						<a href="sahirakarinajimenezpalacios"><li><i class="fas fa-user"></i>Sahira Karina Jimenez Palacios</li></a>
						<a href="mariadelcarmenhernandezmonroy"><li><i class="fas fa-user"></i>Maria Del Carmen Hernández Monroy</li></a>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>

