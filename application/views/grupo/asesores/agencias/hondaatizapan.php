<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="ernestocuandon"><li><i class="fas fa-user"></i>Ernesto Cuandon</li></a>
						<a href="almagaona"><li><i class="fas fa-user"></i>Alma Gaona</li></a>
						<a href="teresitadelacruz"><li><i class="fas fa-user"></i>Teresita De La Cruz</li></a>
						<a href="fannyperezvega"><li><i class="fas fa-user"></i>Fanny Perez Vega</li></a>
						<a href="salvadortovargalvan"><li><i class="fas fa-user"></i>Salvador Tovar Galvan</li></a>
					
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="edithsanchez"><li><i class="fas fa-user"></i>Edith Sanchez</li></a>
						<a href="robertocarrasco"><li><i class="fas fa-user"></i>Roberto Carrasco</li></a>
						<a href="elizbethjimenez"><li><i class="fas fa-user"></i>Elizbeth Jimenez</li></a>
						<a href="javiermendoza"><li><i class="fas fa-user"></i>Javier Mendoza</li></a>
						<a href="armandomartinezmorales"><li><i class="fas fa-user"></i>Armando Martínez Morales</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="xaremdonis"><li><i class="fas fa-user"></i>Xarem Donis</li></a>
						<a href="martinisaismarquez"><li><i class="fas fa-user"></i>Martin Isais Marquez</li></a>
						<a href="giselaaburto"><li><i class="fas fa-user"></i>Gisela Aburto</li></a>
                        <a href="paulinaelizabethsanchezpartida"><li><i class="fas fa-user"></i>Paulina Elizabeth Sanchez Partida</li></a>
                        <a href="claudioalbertovelazquezsuarez"><li><i class="fas fa-user"></i>Claudio Alberto Velazquez Suárez</li></a>

					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="joseantoniogomez"><li><i class="fas fa-user"></i>Jose Antonio Gomez</li></a>
						<a href="marissolmadrid"><li><i class="fas fa-user"></i>Marissol Madrid</li></a>
						<a href="carolinabritoolmos"><li><i class="fas fa-user"></i>Carolina Brito Olmos</li></a> 
                        <a href="adriancarromata"><li><i class="fas fa-user"></i>Adrian Carro Mata</li></a>
                        <a href="anaceciliabravonava"><li><i class="fas fa-user"></i>Ana Cecilia Bravo Nava</li></a>

					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>

