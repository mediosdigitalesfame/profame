<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="sergiohernandez"><li><i class="fas fa-user"></i>Sergio Hernandez</li></a>
						<a href="luisalvarezg"><li><i class="fas fa-user"></i>Luis Alvarez</li></a>
 						<a href="wendycaratachea"><li><i class="fas fa-user"></i>Wendy Caratachea</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="sandrohugobravoambriz"><li><i class="fas fa-user"></i>Sandro Hugo Bravo Ambriz</li></a>
						<a href="yuritzimarisolhernandezelizaldi"><li><i class="fas fa-user"></i>Yuritzi Marisol Hernández Elizaldi</li></a>
 						<a href="robertoivanreyesalvarado"><li><i class="fas fa-user"></i>Roberto Ivan Reyes Alvarado</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="alondramariaruizsanchez"><li><i class="fas fa-user"></i>Alondra Maria Ruiz Sanchez</li></a>
						 <a href="itzelmonserratfloresmendoza"><li><i class="fas fa-user"></i>Itzel Monserrat Flores Mendoza</li></a>
						 <a href="raulalejandromelgozahernandez"><li><i class="fas fa-user"></i>Raul Alejandro Melgoza Hernandez</li></a>
 					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="luisalbertoalvarez"><li><i class="fas fa-user"></i>Luis Alberto Alvarez</li></a>
						<a href="moiseshernandez"><li><i class="fas fa-user"></i>Moises Hernandez</li></a>
						<a href="jorgejavierbarraganpointelin"><li><i class="fas fa-user"></i>Jorge Javier Barragán Pointelin</li></a> 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>