<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						 <a href="xaviergonzalezresendiz"><li><i class="fas fa-user"></i>Xavier Gonzalez Resendiz</li></a>
						<a href="rodrigosilva"><li><i class="fas fa-user"></i>Rodrigo Silva</li></a>
						<a href="carlosiledesma"><li><i class="fas fa-user"></i>Carlos I. Ledesma</li></a>
						<a href="juanjosemartinez"><li><i class="fas fa-user"></i>Juan Jose Martinez</li></a>
						<a href="germanbustos"><li><i class="fas fa-user"></i>German Bustos</li></a>
						<a href="hectormanuelarroyavegutierrez"><li><i class="fas fa-user"></i>Hector Manuel Arroyave Gutierrez</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						
						<a href="juanjosemartinezmejia"><li><i class="fas fa-user"></i>Juan Jose Martinez Mejia</li></a>
						<a href="alfonsoandradetrejo"><li><i class="fas fa-user"></i>Alfonso Andrade Trejo</li></a>
						<a href="erikavazquezdelmercadocasas"><li><i class="fas fa-user"></i>Erika Vazquez Del Mercado Casas</li></a>
						<a href="saulperez"><li><i class="fas fa-user"></i>Saul Perez</li></a>
						<a href="hugoalvarado"><li><i class="fas fa-user"></i>Hugo Alvarado</li></a>
						<a href="franciscoalonsofonsecacalderon"><li><i class="fas fa-user"></i>Francisco Alonso Fonseca Calderon</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="rafaeltrejo"><li><i class="fas fa-user"></i>Rafael Trejo</li></a>
						<a href="luisluna"><li><i class="fas fa-user"></i>Luis Luna</li></a>
						<a href="hebertoolveraavila"><li><i class="fas fa-user"></i>Heberto Olvera Avila</li></a>
						<a href="manuelbustamante"><li><i class="fas fa-user"></i>Manuel Bustamante</li></a>
						<a href="davidhernandez"><li><i class="fas fa-user"></i>David Hernandez</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="mariadelosangelesmartinez"><li><i class="fas fa-user"></i>Maria De Los Angeles Martinez</li></a>
						<a href="hectorayala"><li><i class="fas fa-user"></i>Héctor Ayala</li></a>
						<a href="fannygonzalez"><li><i class="fas fa-user"></i>Fanny Gonzalez</li></a>
						<a href="carloscamachorodea"><li><i class="fas fa-user"></i>Carlos Camacho Rodea</li></a>
						<a href="guillermocastillo"><li><i class="fas fa-user"></i>Guillermo Castillo</li></a>
						
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>

