<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="anahirodriguez"><li><i class="fas fa-user"></i>Anahí Rodriguez</li></a>
						<a href="gracielazamora"><li><i class="fas fa-user"></i>Graciela Zamora</li></a>
						<a href="fatimairaisrodriguezcastillo"><li><i class="fas fa-user"></i>Fatima Irais Rodríguez Castillo</li></a>
						<a href="gerardoacuna"><li><i class="fas fa-user"></i>Gerardo Acuña</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="luistapia"><li><i class="fas fa-user"></i>Luis Tapia</li></a>
						<a href="alejandromartinez"><li><i class="fas fa-user"></i>Alejandro Martínez</li></a>
						<a href="gerardogarcia"><li><i class="fas fa-user"></i>Gerardo García</li></a>
						<a href="alejandrocastanedagutierrez"><li><i class="fas fa-user"></i>Alejandro Castañeda Gutierrez</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="josemariacazaresrosales"><li><i class="fas fa-user"></i>José María Cázares Rosales</li></a>
						<a href="fabiolamora"><li><i class="fas fa-user"></i>Fabiola Mora</li></a>
						<a href="enriqueguizar"><li><i class="fas fa-user"></i>Enrique Guizar</li></a>
						<a href="gerardoacuna"><li><i class="fas fa-user"></i>Gerardo Acuña</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="ferminaayalabucio"><li><i class="fas fa-user"></i>Fermina Ayala Bucio</li></a>
						<a href="monserratacosta"><li><i class="fas fa-user"></i>Monserrat Acosta</li></a>
						<a href="jesuschavez"><li><i class="fas fa-user"></i>Jesus Chavez</li></a>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>

