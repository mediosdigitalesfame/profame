<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="ricardoserrato"><li><i class="fas fa-user"></i>Ricardo Serrato</li></a>
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="franciscovega"><li><i class="fas fa-user"></i>Francisco Vega</li></a>
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="luisenriquecontreras"><li><i class="fas fa-user"></i>Luis Enrique Contreras</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>