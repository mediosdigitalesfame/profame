<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="alejandrocoronavillafranco"><li><i class="fas fa-user"></i>Alejandro Corona Villafranco</li></a>
						<a href="itzelitzayanacalderongomez"><li><i class="fas fa-user"></i>Itzel Itzayana Calderon Gomez</li></a> 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="olivertnevaiotavilanaranjo"><li><i class="fas fa-user"></i>Olivert Nevaiot Avila Naranjo</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="blancaasucenapenalozaalvarez"><li><i class="fas fa-user"></i>Blanca Asucena Peñaloza Alvarez</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="miguelgutierrezgonzalez"><li><i class="fas fa-user"></i>Miguel Gutierrez Gonzalez</li></a>
						 
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>