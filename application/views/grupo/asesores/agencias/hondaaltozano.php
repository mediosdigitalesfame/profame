<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="edgarpinacarrillo"><li><i class="fas fa-user"></i>Edgar Piña Carrillo</li></a>
						<a href="jesusmartinparedesalonzo"><li><i class="fas fa-user"></i>Jesús Martín Paredes Alonzo</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="olivertorrestellez"><li><i class="fas fa-user"></i>Oliver Torres Tellez</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="angelicasanchez"><li><i class="fas fa-user"></i>Angélica Sánchez</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="carlosadiaz"><li><i class="fas fa-user"></i>Carlos A Díaz</li></a>
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>