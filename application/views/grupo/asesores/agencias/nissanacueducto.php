<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="giovannyacosta"><li><i class="fas fa-user"></i>Giovanny Acosta</li></a>
						<a href="fernandatakami"><li><i class="fas fa-user"></i>Fernanda Takami</li></a>
						<a href="luismora"><li><i class="fas fa-user"></i>Luis Mora</li></a>
						<a href="maribeltejeda"><li><i class="fas fa-user"></i>Maribel Tejeda</li></a>
						<a href="tzitlalichavez"><li><i class="fas fa-user"></i>Tzitlali Chavez</li></a>
						<a href="eveliajuradobarrera"><li><i class="fas fa-user"></i>Evelia Jurado Barrera</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="ramiroserratorodriguez"><li><i class="fas fa-user"></i>Ramiro Serrato Rodriguez</li></a>
						<a href="catalinahuerta"><li><i class="fas fa-user"></i>Catalina Huerta</li></a>
						<a href="juanbanderasvillanueva"><li><i class="fas fa-user"></i>Juan Banderas Villanueva</li></a>
						<a href="paolaramirez"><li><i class="fas fa-user"></i>Paola Ramirez</li></a>
						<a href="aracelinoemiortizangeles"><li><i class="fas fa-user"></i>Araceli Noemi Ortiz Angeles</li></a>
						<a href="josueramonamarillasperez"><li><i class="fas fa-user"></i>Josue Ramon Amarillas Perez</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="paulinasantoyo"><li><i class="fas fa-user"></i>Paulina Santoyo</li></a>
						<a href="javierperezmercado"><li><i class="fas fa-user"></i>Javier Perez Mercado</li></a>
						<a href="raulespejo"><li><i class="fas fa-user"></i>Raul Espejo</li></a>
						<a href="renemargaillan"><li><i class="fas fa-user"></i>Rene Margaillan</li></a>
						<a href="yesicamariadiazgarcia"><li><i class="fas fa-user"></i>Yesica Maria Diaz Garcia</li></a>
						<a href="lilianahuertasalinas"><li><i class="fas fa-user"></i>Liliana Huerta Salinas</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="salomonyhamelavila"><li><i class="fas fa-user"></i>Salomon Yhamel Avila</li></a>
						<a href="jorgeeduardoesparzamelchor"><li><i class="fas fa-user"></i>Jorge Eduardo Esparza Melchor</li></a>
						<a href="angelicamariasanchezsoria"><li><i class="fas fa-user"></i>Angelica Maria Sanchez Soria</li></a>
						<a href="juancarlosdimastinoco"><li><i class="fas fa-user"></i>Juan Carlos Dimas Tinoco</li></a>
						<a href="xochitlkarinamoroncamarena"><li><i class="fas fa-user"></i>Xochitl Karina Moron Camarena</li></a>
						<a href="marcoantoniocovarrubiasvargas"><li><i class="fas fa-user"></i>Marco Antonio Covarrubias Vargas</li></a>
						
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>