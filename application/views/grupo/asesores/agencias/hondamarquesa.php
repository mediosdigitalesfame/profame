<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="jesusenriquerodriguezherrera"><li><i class="fas fa-user"></i>Jesus Enrique Rodriguez Herrera</li></a>
						<a href="josefelixgallardo"><li><i class="fas fa-user"></i>Jose Felix Gallardo</li></a>
						<a href="josueenriqueventozasanchez"><li><i class="fas fa-user"></i>Josué Enrique Ventoza Sánchez</li></a>
						<a href="marcogonzalez"><li><i class="fas fa-user"></i>Marco González</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="hiramrogeliohernandezflora"><li><i class="fas fa-user"></i>Hiram Rogelio Hernández Flora</li></a>
						<a href="juliocesartellotinajero"><li><i class="fas fa-user"></i>Julio Cesar Tello Tinajero</li></a>
						<a href="mariotorres"><li><i class="fas fa-user"></i>Mario Torres</li></a>
						<a href="albertodelvallerivadeneyra"><li><i class="fas fa-user"></i>Alberto Del Valle Rivadeneyra</li></a>
						<a href="javierantoniozepedarodriguez"><li><i class="fas fa-user"></i>Javier Antonio Zepeda Rodriguez</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="mariacristinaaizpuruydelaportilla"><li><i class="fas fa-user"></i>María Cristina Aizpuru Y De La Portilla</li></a>
						<a href="veronicariverarivas"><li><i class="fas fa-user"></i>Veronica Rivera Rivas</li></a>
						<a href="ariadnamarinsalvatti"><li><i class="fas fa-user"></i>Ariadna Marín Salvatti</li></a>
						<a href="lauraangelicabravomurillo"><li><i class="fas fa-user"></i>Laura Angélica Bravo Murillo</li></a>
						<a href="varinkacruz"><li><i class="fas fa-user"></i>Varinka Cruz</li></a>
						 
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="franciscotorreshernandez"><li><i class="fas fa-user"></i>Francisco Torres Hernandez</li></a>
						<a href="gustavodiaz"><li><i class="fas fa-user"></i>Gustavo Diaz</li></a>
						<a href="emmanuelrodriguezmora"><li><i class="fas fa-user"></i>Emmanuel Rodriguez Mora</li></a>
						<a href="juliocesartellotinabero"><li><i class="fas fa-user"></i>Julio Cesar Tello Tinabero</li></a>
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>