<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="angelsolisrobles"><li><i class="fas fa-user"></i>Angel Solis Robles</li></a>
						<a href="isabelaltamirano"><li><i class="fas fa-user"></i>Isabel Altamirano</li></a>
						<a href="gilmendoza"><li><i class="fas fa-user"></i>Gil Mendoza</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="eduardobenitezflores"><li><i class="fas fa-user"></i>Eduardo Benítez Flores</li></a>
						<a href="gerardoandradeaviles"><li><i class="fas fa-user"></i>Gerardo Andrade Avilés</li></a>
						<a href="karlaruiz"><li><i class="fas fa-user"></i>Karla Ruiz</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="alejandragalvan"><li><i class="fas fa-user"></i>Alejandra Galván</li></a>
						<a href="melissahuertaluna"><li><i class="fas fa-user"></i>Melissa Huerta Luna</li></a>
						<a href="gabrielaaguilarruiz"><li><i class="fas fa-user"></i>Gabriela Aguilar Ruiz</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="norasanchez"><li><i class="fas fa-user"></i>Nora Sanchez</li></a>
						<a href="omarmelquisedecfernandezjimenez"><li><i class="fas fa-user"></i>Omar Melquisedec Fernández Jiménez</li></a>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>