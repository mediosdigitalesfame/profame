<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="oscarnavarro"><li><i class="fas fa-user"></i>Oscar Navarro</li></a>
						<a href="eduardoaguilar"><li><i class="fas fa-user"></i>Eduardo Aguilar</li></a>
						<a href="urielrecendizvaldovinos"><li><i class="fas fa-user"></i>Uriel Recendiz Valdovinos</li></a> 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="jovanyjaimes"><li><i class="fas fa-user"></i>Jovany Jaimes</li></a>
						<a href="melissaduarterojas"><li><i class="fas fa-user"></i>Melissa Duarte Rojas</li></a>
						<a href="pablomacedo"><li><i class="fas fa-user"></i>Pablo Macedo</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="jovanniloyo"><li><i class="fas fa-user"></i>Jovanni Loyo</li></a>
						<a href="davidricardoduransantillan"><li><i class="fas fa-user"></i>David Ricardo Duran Santillan</li></a>						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="rafaelguerrerosandoval"><li><i class="fas fa-user"></i>Rafael Guerrero Sandoval</li></a>
						<a href="jesusjimenez"><li><i class="fas fa-user"></i>Jesus Jimenez</li></a>
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>