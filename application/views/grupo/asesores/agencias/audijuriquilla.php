<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="fernandoolveramartinez"><li><i class="fas fa-user"></i>Fernando Olvera Martinez</li></a>
						<a href="ismaelalexisduranmagana"><li><i class="fas fa-user"></i>Ismael Alexis Duran Magaña</li></a>
						<a href="jonathanadrianhuertacamacho"><li><i class="fas fa-user"></i>Jonathan Adrian Huerta Camacho</li></a>
						<a href="hectorocaranzacerecero"><li><i class="fas fa-user"></i>Hector Ocaranza Cerecero</li></a>
						<a href="angelnaimlopezsegundo"><li><i class="fas fa-user"></i>Ángel Naim López Segundo</li></a>
						<a href="miguelangelmorenovazquez"><li><i class="fas fa-user"></i>Miguel Angel Moreno Vazquez</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="mireyapantojafavela"><li><i class="fas fa-user"></i>Mireya Pantoja Favela</li></a>
						<a href="adolfomatamorossalgado"><li><i class="fas fa-user"></i>Adolfo Matamoros Salgado</li></a>
						<a href="rafaelbarragancruz"><li><i class="fas fa-user"></i>Rafael Barragan Cruz</li></a>
						<a href="rosalindachonggomez"><li><i class="fas fa-user"></i>Rosalinda Chong Gomez</li></a>
						<a href="irwingarturolozanoorozco"><li><i class="fas fa-user"></i>Irwing Arturo Lozano Orozco</li></a>
						<a href="xiomaraeunicerosalesgomez"><li><i class="fas fa-user"></i>Xiomara Eunice Rosales Gomez</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="javierivanmoralestejedo"><li><i class="fas fa-user"></i>Javier Ivan Morales Tejedo</li></a>
						<a href="marcosvargasperez"><li><i class="fas fa-user"></i>Marcos Vargas Pérez</li></a>
						<a href="hugoantobelyverazaramirez"><li><i class="fas fa-user"></i>Hugo Antobely Veraza Ramírez</li></a>
						<a href="lauragonzalezgonzalez"><li><i class="fas fa-user"></i>Laura Gonzalez Gonzalez</li></a>
						<a href="erickamendozagonzalez"><li><i class="fas fa-user"></i>Ericka Mendoza Gonzalez</li></a>
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="lizbethvaldezmartinez"><li><i class="fas fa-user"></i>Lizbeth Valdez Martinez</li></a>
						<a href="eduardomarincovarrubias"><li><i class="fas fa-user"></i>Eduardo Marin Covarrubias</li></a>
						<a href="heinarivanlopezespinosa"><li><i class="fas fa-user"></i>Heinar Ivan Lopez Espinosa</li></a>
						<a href="javierfloresrojas"><li><i class="fas fa-user"></i>Javier Flores Rojas</li></a>
						<a href="michelleestefaniagonzalezsanchez"><li><i class="fas fa-user"></i>Michelle Estefania Gonzalez Sanchez</li></a>
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>

<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>

