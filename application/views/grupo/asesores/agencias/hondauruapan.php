<?php $this->load->view('grupo/asesores/encabezadoagenciaasesores.php'); ?>

<div class="container py-2">
	<div class="row">
		<div class="col">

			<div class="row">
				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="alvarocupa"><li><i class="fas fa-user"></i>Alvaro Cupa</li></a>
						<a href="estebantapia"><li><i class="fas fa-user"></i>Esteban Tapia</li></a>
						<a href="almafuerte"><li><i class="fas fa-user"></i>Alma Fuerte</li></a>
						<a href="miguelangelmontes"><li><i class="fas fa-user"></i>Miguel Angel Montes</li></a>
						<a href="ciprianopena"><li><i class="fas fa-user"></i>Cipriano Peña</li></a>
						<a href="joseluistorres"><li><i class="fas fa-user"></i>José Luis Torres</li></a>
						<a href="alejandrocazares"><li><i class="fas fa-user"></i>Alejandro Cazares</li></a>
						
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="joseluistorres"><li><i class="fas fa-user"></i>José Luis Torres</li></a>
						<a href="ciprianopena"><li><i class="fas fa-user"></i>Cipriano Peña</li></a>
						<a href="miguelangelmontes"><li><i class="fas fa-user"></i>Miguel Angel Montes</li></a>
						<a href="almasoledadfuerte"><li><i class="fas fa-user"></i>Alma Soledad Fuerte</li></a>
						<a href="estebantapia"><li><i class="fas fa-user"></i>Esteban Tapia</li></a>
						<a href="alvarocupa"><li><i class="fas fa-user"></i>Alvaro Cupa</li></a>
						<a href="edgarjosuequirozcastro"><li><i class="fas fa-user"></i>Edgar Josue Quiroz Castro</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="miriammendoza"><li><i class="fas fa-user"></i>Miriam Mendoza</li></a>
						<a href="bulmaroivanmercadovazquez"><li><i class="fas fa-user"></i>Bulmaro Iván Mercado Vázquez</li></a>
						<a href="jesushenry"><li><i class="fas fa-user"></i>Jesus Henry</li></a>
						<a href="edgarjosuequirozcastro"><li><i class="fas fa-user"></i>Edgar Josue Quiroz Castro</li></a>
						<a href="dorianolimpiazamoracamacho"><li><i class="fas fa-user"></i>Dorian Olimpia Zamora Camacho</li></a>
						<a href="alejandrocazares"><li><i class="fas fa-user"></i>Alejandro Cazares</li></a>
						<a href="dorianolimpiazamoracamacho"><li><i class="fas fa-user"></i>Dorian Olimpia Zamora Camacho</li></a>
						 
					</ul>
				</div>

				<div class="col-lg-3">
					<ul class="list list-icons list-icons-lg">
						<a href="jesushenry"><li><i class="fas fa-user"></i>Jesus Henry</li></a>
						<a href="bulmaroivanmercadovazquez"><li><i class="fas fa-user"></i>Bulmaro Iván Mercado Vázquez</li></a>
						<a href="miriammendoza"><li><i class="fas fa-user"></i>Miriam Mendoza</li></a>
						<a href="lorenzolarrinua"><li><i class="fas fa-user"></i>Lorenzo Larrinua</li></a>
						<a href="jonathanrubiosalazar"><li><i class="fas fa-user"></i>Jonathan Rubio Salazar</li></a>
						<a href="lorenzolarrinua"><li><i class="fas fa-user"></i>Lorenzo Larrinua</li></a>
						 
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>


<?php $this->load->view('grupo/asesores/agencias/pieagencias.php'); ?>