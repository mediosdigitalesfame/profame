<div role="main" class="main">

	<section class="page-header page-header-classic page-header-md">
		<div class="container">
			<div class="row">
				<div class="col-md-8 order-2 order-md-1 align-self-center p-static">
					<h1 data-title-border>Asesores</h1>
				</div>
				<div class="col-md-4 order-1 order-md-2 align-self-center">
					<ul class="breadcrumb d-block text-md-right">
						<li><a href="<?php echo base_url(); ?>inicio">Inicio</a></li>
						<li class="active">Asesores</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col">

				<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/zonaautomotriz">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/grupo.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Zona Automotriz<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/nissan">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/nissan.jpg">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Nissan<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/audi">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/audi.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Audi<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/honda">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/honda.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Honda<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/bmw">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/bmw.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">BMW<em></em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/chevrolet">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/chevrolet.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Chevrolet</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/chrysler">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/chrysler.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Chrysler</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/fiat">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/fiat.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">FIAT</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/gmc">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/gmc.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">GMC</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/isuzu">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/isuzu.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Isuzu</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/kia">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/kia.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">KIA</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/mini">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/mini.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">MINI</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/mitsubishi">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/mitsubishi.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Mitsubishi</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/motorrad">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/motorrad.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Motorrad</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/seminuevos">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/seminuevos.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Seminuevos</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/toyota">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/toyota.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Toyota</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div>
						<div class="hover-effect-3d">
							<a href="<?php echo base_url(); ?>asesores/volkswagen">
								<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info">
									<div class="thumb-info-wrapper">
										<img alt="" class="img-fluid rounded" src="<?php echo base_url(); ?>assets/img/marcas/cuadro/volkswagen.png">
										<div class="thumb-info-title">
											<div class="thumb-info-inner">Volkswagen</em></div> 
											<div class="thumb-info-type">ver más</div>  
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>


				</div>

			</div>
		</div>

	</div>
</div>
</div>

