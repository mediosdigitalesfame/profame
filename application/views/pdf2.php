<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{

    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),200,$this->GetY());
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 1,'40','20','png','http:grupofame.com');
            $this->Ln(5);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Datos de Empresa",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Ln(2);
        }
    }

    function Content($empresa){

        $this->SetFont('Arial','B',16);
        $this->Cell(80);
        $this->Cell(20,10,html_escape($empresa[0]['nombre']),0,1,'C');
        $this->Ln(5);

        $this->SetFont('Arial','B',20);    
        $textypos = 5;

        $this->SetFont('Arial','B',10);    
        $this->setY(35);$this->setX(10);
        $this->Cell(5,$textypos,"DE:");
        $this->SetFont('Arial','',10);    
        $this->setY(40);$this->setX(10);
        $this->Cell(5,$textypos,"Nombre de la empresa");
        $this->setY(45);$this->setX(10);
        $this->Cell(5,$textypos,"Direccion de la empresa");
        $this->setY(50);$this->setX(10);
        $this->Cell(5,$textypos,"Telefono de la empresa");
        $this->setY(55);$this->setX(10);
        $this->Cell(5,$textypos,"Email de la empresa");

        $this->SetFont('Arial','B',10);    
        $this->setY(35);$this->setX(75);
        $this->Cell(5,$textypos,"PARA:");
        $this->SetFont('Arial','',10);    
        $this->setY(40);$this->setX(75);
        $this->Cell(5,$textypos,"Nombre del cliente");
        $this->setY(45);$this->setX(75);
        $this->Cell(5,$textypos,"Direccion del cliente");
        $this->setY(50);$this->setX(75);
        $this->Cell(5,$textypos,"Telefono del cliente");
        $this->setY(55);$this->setX(75);
        $this->Cell(5,$textypos,"Email del cliente");

        $this->SetFont('Arial','B',10);    
        $this->setY(35);$this->setX(140);
        $this->Cell(5,$textypos,"FACTURA #12345");
        $this->SetFont('Arial','',10);    
        $this->setY(40);$this->setX(135);
        $this->Cell(5,$textypos,"Fecha: 11/DIC/2019");
        $this->setY(45);$this->setX(135);
        $this->Cell(5,$textypos,"Vencimiento: 11/ENE/2020");
        $this->setY(50);$this->setX(135);
        $this->Cell(5,$textypos,"");
        $this->setY(55);$this->setX(135);
        $this->Cell(5,$textypos,"");

        $this->setY(60);$this->setX(135);
        $this->Ln();

        $header = array("Cod.", "Descripcion","Cant.","Precio","Total");

        $products = array(
            array("0010", "Producto 1",2,120,0),
            array("0024", "Producto 2",5,80,0),
            array("0001", "Producto 3",1,40,0),
            array("0001", "Producto 3",5,80,0),
            array("0001", "Producto 3",4,30,0),
            array("0001", "Producto 3",7,80,0),
        );

        $w = array(20, 95, 20, 25, 25);

        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],7,$header[$i],1,0,'C');
        $this->Ln();

        $total = 0;
        foreach($products as $row)
        {
            $this->Cell($w[0],6,$row[0],1);
            $this->Cell($w[1],6,$row[1],1);
            $this->Cell($w[2],6,number_format($row[2]),'1',0,'R');
            $this->Cell($w[3],6,"$ ".number_format($row[3],2,".",","),'1',0,'R');
            $this->Cell($w[4],6,"$ ".number_format($row[3]*$row[2],2,".",","),'1',0,'R');
            $this->Ln();
            $total+=$row[3]*$row[2];
        }

        $yposdinamic = 60 + (count($products)*10);

        $this->setY($yposdinamic);
        $this->setX(235);
        $this->Ln();

        $header = array("", "");
        $data2 = array(
            array("Subtotal",$total),
            array("Descuento", 0),
            array("Impuesto", 0),
            array("Total", $total),
        );
 
        $w2 = array(40, 40);
 

        $this->Ln();
 
        foreach($data2 as $row)
        {
            $this->setX(115);
            $this->Cell($w2[0],6,$row[0],1);
            $this->Cell($w2[1],6,"$ ".number_format($row[1], 2, ".",","),'1',0,'R');
            $this->Ln();
        }


        $yposdinamic += (count($data2)*10);
        $this->SetFont('Arial','B',10);    

        $this->setY($yposdinamic);
        $this->setX(10);
        $this->Cell(5,$textypos,"TERMINOS Y CONDICIONES");
        $this->SetFont('Arial','',10);    

        $this->setY($yposdinamic+10);
        $this->setX(10);
        $this->Cell(5,$textypos,"El cliente se compromete a pagar la factura.");
        $this->setY($yposdinamic+20);






        $this->Ln(5);
        $this->Ln(5);
        $this->setFont('Arial','',9);
        $this->setFillColor(255,255,255); 
        $this->cell(10,6,'1',1,0,'L',1);
        $this->cell(25,6,'2',1,0,'L',1);
        $this->cell(50,6,'3',1,0,'L',1);
        $this->cell(105,6,'4',1,1,'L',1);            
    }

    function Footer(){
        $this->SetY(-15);
        $this->Line(10,$this->GetY(),200,$this->GetY());
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'AMD '.date('Y').' Grupo FAME',0,0,'L');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}

$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($empresa);
$pdf->Output();
