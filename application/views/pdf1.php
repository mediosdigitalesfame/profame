<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF{
    // Page header
    function Header(){
        if ($this->PageNo() == 1){
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Listado de Empresas",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
            $this->Line(10,$this->GetY(),200,$this->GetY());
            // Logo
            $this->Image(base_url().'assets/porto/img/logo.png', 85, 1,'40','20','png','http:grupofame.com');
 
             
            // Line break
            $this->Ln(5);
            $this->setFont('Arial','B',9);
            $this->setFillColor(230,230,200);
            $this->cell(10,10,'No.',1,0,'L',1);
            $this->cell(25,10,'Nombre',1,0,'L',1);
            $this->cell(50,10,'RFC',1,0,'L',1);
            $this->cell(105,10,'R. Social',1,1,'C',1);
        }else{
            $this->setFont('Arial','I',9);
            $this->setFillColor(255,255,255);
            $this->cell(90,6,"Listado de Empresas",0,0,'L',1); 
            $this->cell(100,6,date('d-m-Y'),0,1,'R',1); 
//            $this->Line(10,$this->GetY(),200,$this->GetY());
            $this->Ln(2);
            $this->setFont('Arial','B',9);
            $this->setFillColor(230,230,200);
            $this->cell(10,10,'No.',1,0,'L',1);
            $this->cell(25,10,'Nombre',1,0,'L',1);
            $this->cell(50,10,'RFC',1,0,'L',1);
            $this->cell(105,10,'R. Social',1,1,'C',1);
        }
    }

    function Content($sSQL){
        
        $no = 1;
        foreach ($sSQL as $key) {
            $this->setFont('Arial','',9);
            $this->setFillColor(255,255,255); 
            $this->cell(10,6,$no,1,0,'L',1);
            $this->cell(25,6,$key->nombre,1,0,'L',1);
            $this->cell(50,6,$key->rfc,1,0,'L',1);
            $this->cell(105,6,$key->calle,1,1,'L',1);
            
            $no++;
        }            
    }

    // Page footer
    function Footer(){
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        //buat garis horizontal
        $this->Line(10,$this->GetY(),200,$this->GetY());
        //Arial italic 9
        $this->SetFont('Arial','I',9);
        $this->Cell(0,10,'Copyright '.date('Y').' Grupo FAME',0,0,'L');
        //nomor halaman
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


 
// Instanciation of inherited class
$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
//$pdf->SetFont('Times','',12);
//for($i=1;$i<=40;$i++)
//    $pdf->Cell(0,10,'Printing line number '.$i,0,1);
$pdf->Content($sSQL);
$pdf->Output();
